# CRATER-DATA

Ce repo contient l'application python crater-data, qui permet de générer l'ensemble des données utilisées par CRATer
sous forme de fichiers csv.

## Comment contribuer ?

Toutes les contributions sont les bienvenues sur le projet !

Etapes à suivre pour contribuer :

0. [pré requis] Disposer d'un compte framagit, si besoin [le créer ici](https://framagit.org/users/sign_up) (attention un délai de 1 à 5 jours peut être nécessaire)
1. installer un environnement de dév sur votre poste
2. se positionner sur une issue
3. développer en local
4. s'assurer que le code est valide
5. faire relire son code et le fusionner





## 1) Installer l'environnement de développement

Les instructions ci-dessous sont valables pour linux, mac.

### Cloner le repository git de crater-data

```
git clone git@framagit.org:lga/crater-data.git
cd crater-data
```

### Installer conda via miniforge, puis conda-lock

Le projet utilise :
* conda via miniforge pour simplifier l'installation sur différents systèmes (conda forge de manière plus systématique les packages compilés, yc pour les nouveaux Mac M1)
* conda-lock pour gérer les dépendances
* des outils relatifs à la cartographie : [pmtiles-cli](https://github.com/protomaps/go-pmtiles) et [tippecanoe](https://github.com/mapbox/tippecanoe)


1/ Installer miniforge

La version recommandée est miniforge3-24.3.0-0
Il est possible d'installer directement miniforge (voir doc sur le site miniforge), ou bien de passer par pyenv (pyenv permet de gérer facilement plusieurs environnements
python)


Pour l'installation via pyenv :
```
# Installer pyenv voir https://github.com/pyenv), voir https://github.com/pyenv/pyenv-installer
curl https://pyenv.run | bash

# Configurer pyenv dans le fichier .bashrc
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc

# Mettre à jour pyenv pour disposer des dernières versions disponibles
pyenv update
# Voir les versions de miniforge
pyenv install -l | grep miniforge
# installer la version souhaitée de miniforge dans pyenv
pyenv install miniforge3-24.3.0-0

# Définir miniforge3-24.3.0-0 comme l'environnement python par défaut pour le projet crater-data
# Cela a pour effet de générer le fichier .python-version, Rq : ce fichier est déjà présent dans le repo git de crater-data
pyenv local miniforge3-24.3.0-0
```

2/ Créer un environnement pour crater-data

Depuis le dossier crater-data :

```
# Installer conda-lock
conda install -c conda-forge conda-lock

# [OPTIONNEL] : générer le fichier conda-lock.yml à partir de environment.yml. NE PAS FAIRE SI LE FICHIER EXISTE DEJA
conda-lock -f environment.yml

# Créer un environnement avec toutes les dépendances du projet dans le dossier ./conda-env
conda-lock install -p conda-env

# Activer l'environnement
conda init bash (ou zsh selon le shell)
# Potentiellement, besoin de relancer le shell
conda activate ./conda-env
```

### Mettre à jour ou installer les dépendances Python

A la racine du projet :

- `conda-lock -f environment.yml` pour mettre à jour le fichier de lock conda-lock.yml
- `conda-lock install -p conda-env` pour installer les dépendances ou les mettre à jour selon leur description dans le fichier lock


### Cas particulier de Windows

Il est également possible d'installer un environnement de développement sur Windows :

* soit en natif, en installant la version windows de miniforge. Dans ce cas, l'ensemble des commandes décrites dans cette documentation ne sont pas utilisables (car nécessitent un shell linux par ex bash)
* soit en installant au préalable WSL (voir [ici](https://learn.microsoft.com/fr-fr/windows/wsl/)), ce qui permet de disposer d'un environnement Linux et d'un shell. Il faut alors installer miniforge en version linux dans cet environnement. Il est également possible d'utiliser VS Code avec cet environnement, voir [ici](https://learn.microsoft.com/fr-fr/windows/wsl/tutorials/wsl-vscode)


### Pour configurer le nouvel environnement dans PyCharm
- Ajouter un interpreteur
- Sélectionner Conda Environment
- Choisir le conda executable (eg /Users/YYY/.pyenv/shims/versions/miniforgeXXXX/bin/conda)
- Charger les environnements
- Utiliser un environnement existant : miniforgeXXXX


### Installer les outils de cartographie

Ces outils sont nécessaires pour exécuter les étapes de construction des cartes vectorielles :

* Installer pmtiles-cli, qui permet de télécharger le fond de carte pour la France.
  * Télécharger l'exécutable pour l'os de votre machine https://github.com/protomaps/go-pmtiles?tab=readme-ov-file#installation
  * Placer l'exécutable dans un dossier de votre PATH, ou configurer le PATH pour accéder à l'exécutable
  * Vérifier que l'exécutable est bien accessible en tapant `pmtiles version` dans un terminal
* Installer tippecanoe, qui permet de construire des cartes vectorielles au format pmtiles à partir de données géographiques (geojson par exemple)
  * Suivre les instructions ici https://github.com/mapbox/tippecanoe?tab=readme-ov-file#installation
  * [Si besoin] Placer l'exécutable dans un dossier de votre PATH, ou configurer le PATH pour accéder à l'exécutable
  * Vérifier que l'exécutable est bien accessible en tapant `tippecanoe --version` dans un terminal


## 2) Se positionner sur une issue

Voir le paragraphe correspondant dans [ce guide](https://framagit.org/lga/crater/-/blob/main/guides/processus-de-developpement.md)

La liste des issues crater-data ouvertes à contribution est visible [dans ce board](https://framagit.org/lga/crater-data/-/boards?label_name[]=Contribution)


## 3) Développer en local

Voir une description de l'architecture générale de crater-data [dans les guides correspondants](https://framagit.org/lga/crater/-/tree/main/guides).

Il faut respecter les standards de dev du projet, voir pour cela les [règles de codage](https://framagit.org/lga/crater/-/blob/main/guides/regles-de-codage.md)


## 4) Vérifier son code

Voir le paragraphe correspondant dans [ce guide](https://framagit.org/lga/crater/-/blob/main/guides/processus-de-developpement.md)

Et en complément, se mettre à la racine du projet, et lancer le script de validation :
```plantuml
./validate.sh
```

Ce script est a exécuter avant chaque commit. Il permet de formatter  le code (avec black) et de lancer les tests unitaires. Il faut vérifier dans le log du script que tous les tests sont OK avant de faire le commit.

## 5) Faire relire son code et le fusionner

Voir le paragraphe correspondant dans [ce guide](https://framagit.org/lga/crater/-/blob/main/guides/processus-de-developpement.md)

## Commplément : Exécuter les chaînes de traitement crater-data

### Pré-requis

a) Dérouler les étapes d'installation (voir 1. ci-dessus)

b) Récupếrer les dossiers contenant les données sources et résultats :

* les données sources sont disponibles sur l'espace partagé des Greniers d'Abondance (demande d'accès à faire par mail
  crater@resiliencealimentaire.org). Le dossier crater-data-sources contenant ces données doit être positionné au même
  niveau de le dossier crater-data (dans le même repertoire parent)
* la dernière version des données résultats est disponible dans le
  repo [crater-data-resultats](https://framagit.org/lga/crater-data-resultats) (demande d'accès à faire via gitlab).

### Lancer un traitement

crater-data utilise les fichiers présents dans crater-data-sources pour générer les résultats sous forme de fichiers csv
dans crater-data-resultats.

crater-data permet également de récupérer certains fichiers sources (en automatisant leur téléchargement par exemple)

Les traitements de téléchargements des sources peuvent être lancés avec le script `crater_data.py`

Pour consulter la liste des téléchargements disponible :

```
python -m crater.crater_data --help
```

Les traitements de calcul des fichiers résultats à partir des sources sont gérés par Dagster. 
Dagster est un orchestrateur de taches, qui permet de piloter l'exécution soit en ligne de commande, soit via une interface graphique, ainsi que de visualiser le graphe des taches.

Cas de l'interface web dagster, qui permet de visualiser le graphe des taches et de lancer les traitements :
* lancer une instance dagster en local : `./web-dagster.sh`. Ce script démarre l'ensemble des services dagster (daemon et web app), et utilise le dossier `crater-data/.dagster` pour stocker les données de dagster.
* se connecter sur http://127.0.0.1:3000/
* aller dans Overview/Jobs, selectionner Job_incomplet_rapide, puis Materialize all

Cas de l'exécution en ligne de commande :
* utiliser la commande `./run-dagster.sh` (à préférer à une utilisation directe de la commande `dagster`); Voir l'aide en ligne de cette commande pour plus de détails
* si besoin, lancer au préalable l'interface web dagster (`./web-dagster.sh`) et la laisser tourner en arrière plan. cela permet de suivre l'avancement des traitements, yc ceux lancés en ligne de commande



