from pathlib import Path

import matplotlib.pyplot as plt
from pandas import DataFrame

from crater.commun import environnement
from crater.commun.calculs.statistiques import filtrer_outliers
from crater.commun.exports.export_fichier import creer_dossier_si_besoin
from crater.commun.logger import log
from crater.config.config_globale import CATEGORIES_TERRITOIRES


def exporter_histogrammes(
    df_indicateur: DataFrame, nom_colonnes_indicateurs: list[str], chemin_dossier_output: Path, bins=50, exclure_outliers=None
) -> None:
    if environnement.ENVIRONNEMENT_EXECUTION != "PROD":
        log.info("INFORMATION : export des diagrammes desactivé")
        return

    creer_dossier_si_besoin(chemin_dossier_output)

    for nom_colonne_indicateur in nom_colonnes_indicateurs:
        for categorie_territoire in CATEGORIES_TERRITOIRES:
            if isinstance(exclure_outliers, bool):
                _exporter_histogramme_indicateur_categorie_territoire(
                    df_indicateur, nom_colonne_indicateur, categorie_territoire, chemin_dossier_output, bins=bins, exclure_outliers=exclure_outliers
                )
            else:
                _exporter_histogramme_indicateur_categorie_territoire(
                    df_indicateur, nom_colonne_indicateur, categorie_territoire, chemin_dossier_output, bins=bins, exclure_outliers=False
                )
                _exporter_histogramme_indicateur_categorie_territoire(
                    df_indicateur, nom_colonne_indicateur, categorie_territoire, chemin_dossier_output, bins=bins, exclure_outliers=True
                )


def _exporter_histogramme_indicateur_categorie_territoire(
    df_indicateur: DataFrame, nom_colonne_indicateur: str, categorie_territoire: str, chemin_dossier_output: Path, bins: int, exclure_outliers: bool
) -> None:
    plt.close("all")

    df_pour_categorie_territoire = df_indicateur.loc[df_indicateur["categorie_territoire"] == categorie_territoire, :]
    if exclure_outliers:
        serie = filtrer_outliers(df_pour_categorie_territoire[nom_colonne_indicateur])
        nom_fichier = f"{nom_colonne_indicateur}_{categorie_territoire}_sans_outliers.png"
    else:
        serie = df_pour_categorie_territoire[nom_colonne_indicateur]
        nom_fichier = f"{nom_colonne_indicateur}_{categorie_territoire}.png"

    histogramme = serie.hist(bins=bins)

    fig = histogramme.get_figure()  # type: ignore # erreur depuis l'upgrade de mypy vers 1.11.2
    fig.savefig(f"{chemin_dossier_output}/{nom_fichier}")
    # Supprimer la figure courante, sinon les histogrammes s'empilent
    fig.clear()


def exporter_piechart_territoires(
    df_indicateur: DataFrame, ids_territoires: list[str], noms_colonnes_indicateurs: list[str], chemin_dossier_output: Path, base_nom_fichier: str
) -> None:
    if environnement.ENVIRONNEMENT_EXECUTION != "PROD":
        log.info("INFORMATION : export des diagrammes desactivé")
        return

    creer_dossier_si_besoin(chemin_dossier_output)

    for id_territoire in ids_territoires:
        _exporter_piechart_territoire(df_indicateur, id_territoire, noms_colonnes_indicateurs, chemin_dossier_output, base_nom_fichier)


def _exporter_piechart_territoire(
    df_indicateur: DataFrame, id_territoire: str, noms_colonnes_indicateurs: list[str], chemin_dossier_output: Path, base_nom_fichier: str
) -> None:
    plt.close("all")

    df_donnees = df_indicateur.reset_index()
    df_donnees = df_donnees.loc[df_donnees["id_territoire"] == id_territoire, noms_colonnes_indicateurs].transpose()
    df_donnees = df_donnees.rename(columns={df_donnees.columns[0]: "valeur"})

    diagramme = df_donnees.plot.pie(y="valeur", figsize=(10, 10), labels=None)

    fig = diagramme.get_figure()

    nom_fichier = f"{base_nom_fichier}_{id_territoire}_piechart.png"

    fig.savefig(f"{chemin_dossier_output}/{nom_fichier}")  # type: ignore # erreur depuis l'upgrade de mypy vers 1.11.2
    # Supprimer la figure courante, sinon les histogrammes s'empilent
    fig.clear()  # type: ignore # erreur depuis l'upgrade de mypy vers 1.11.2
