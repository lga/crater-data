from pathlib import Path

import numpy as np
from geopandas import GeoDataFrame
from matplotlib import pyplot
from pandas import DataFrame

from crater.commun import environnement
from crater.commun.exports.export_fichier import creer_dossier_si_besoin
from crater.commun.chargeurs.chargeur_cartes import (
    charger_contours_regions,
    charger_contours_departements,
    charger_contours_epcis,
    charger_contours_communes,
)
from crater.commun.logger import log
from crater.config.config_sources import (
    CHEMIN_SOURCE_GEOMETRIES_REGIONS,
    CHEMIN_SOURCE_GEOMETRIES_DEPARTEMENTS,
    CHEMIN_SOURCE_GEOMETRIES_EPCIS,
    CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
)

gdf_contours_territoires = {
    "REGION": {"gdf": None, "chargeur": charger_contours_regions, "chemin_fichier": CHEMIN_SOURCE_GEOMETRIES_REGIONS},
    "DEPARTEMENT": {"gdf": None, "chargeur": charger_contours_departements, "chemin_fichier": CHEMIN_SOURCE_GEOMETRIES_DEPARTEMENTS},
    "EPCI": {"gdf": None, "chargeur": charger_contours_epcis, "chemin_fichier": CHEMIN_SOURCE_GEOMETRIES_EPCIS},
    "COMMUNE": {"gdf": None, "chargeur": charger_contours_communes, "chemin_fichier": CHEMIN_SOURCE_GEOMETRIES_COMMUNES},
}


def exporter_cartes(
    df: DataFrame, nom_colonnes_indicateurs: list[str], categories_territoires: list[str], dossier_output: Path, bins=None, cmap="summer_r"
):
    if environnement.ENVIRONNEMENT_EXECUTION != "PROD":
        log.info("INFORMATION : export des cartes desactivé")
        return

    creer_dossier_si_besoin(dossier_output)

    for nom_colonne_indicateur in nom_colonnes_indicateurs:
        for categorie_territoire in categories_territoires:
            _exporter_carte_indicateur_categorie_territoire(df, nom_colonne_indicateur, categorie_territoire, dossier_output, bins, cmap)


def _exporter_carte_indicateur_categorie_territoire(df, nom_colonne_indicateur, categorie_territoire, dossier_output, bins, cmap):
    if gdf_contours_territoires[categorie_territoire]["gdf"] is None:
        gdf_contours_territoires[categorie_territoire]["gdf"] = gdf_contours_territoires[categorie_territoire]["chargeur"](
            gdf_contours_territoires[categorie_territoire]["chemin_fichier"]
        )
    gdf: GeoDataFrame = gdf_contours_territoires[categorie_territoire]["gdf"]
    df = df.reset_index().loc[:, ["id_territoire", nom_colonne_indicateur]]

    if df[nom_colonne_indicateur].dtype in ["float64", "int64", "Float64", "Int64"]:
        df[nom_colonne_indicateur] = df[nom_colonne_indicateur].astype("float64").replace([np.inf, -np.inf], np.nan, inplace=False).dropna()
    gdf_indicateur = gdf.merge(df, on="id_territoire", how="left")

    pyplot.close("all")

    if bins is None:
        gdf_indicateur.plot(column=nom_colonne_indicateur, legend=True, figsize=(15, 15), cmap=cmap)
    else:
        gdf_indicateur.plot(
            column=nom_colonne_indicateur,
            legend=True,
            figsize=(15, 15),
            scheme="User_Defined",
            classification_kwds=dict(bins=bins),
            cmap=cmap,
        )
    log.info(f"   => export du fichier {str(dossier_output)}/carte_{nom_colonne_indicateur}_{categorie_territoire}.png")
    pyplot.savefig(dossier_output / f"carte_{nom_colonne_indicateur}_{categorie_territoire}.png")
