import zipfile
from pathlib import Path
from typing import TypedDict
import pandas as pd
from crater.commun.logger import log

from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
    traduire_code_insee_vers_id_departement_crater,
    traduire_code_insee_vers_id_region_crater,
    traduire_code_insee_vers_id_pays_crater,
)


class FichierAgreste(TypedDict):
    code: str
    annee: str
    dossier: Path


def charger_agreste(chemin_fichier: FichierAgreste):
    log.info(
        f"   => chargement fichier AGRESTE du dossier : {chemin_fichier['dossier']} - "
        f"code : {chemin_fichier['code']} - année : {chemin_fichier['annee']}"
    )

    return pd.read_csv(
        zipfile.ZipFile(chemin_fichier["dossier"] / ("FDS_" + chemin_fichier["code"] + ".zip"), "r").open(
            ("FDS_" + chemin_fichier["code"] + "_" + chemin_fichier["annee"] + ".txt")
        ),
        sep=";",
        dtype={
            "NOM": "str",
            "ANNREF": "str",
            "FRANCE": "str",
            "FRDOM": "str",
            "REGION": "str",
            "DEP": "str",
            "COM": "str",
            chemin_fichier["code"] + "_DIM1": "str",
            chemin_fichier["code"] + "_MOD_DIM1": "str",
            chemin_fichier["code"] + "_LIB_DIM1": "str",
            chemin_fichier["code"] + "_DIM2": "str",
            chemin_fichier["code"] + "_MOD_DIM2": "str",
            chemin_fichier["code"] + "_LIB_DIM2": "str",
            chemin_fichier["code"] + "_DIM3": "str",
            chemin_fichier["code"] + "_MOD_DIM3": "str",
            chemin_fichier["code"] + "_LIB_DIM3": "str",
            "VALEUR": "float",
            "QUALITE": "str",
        },
    )


def filtrer_et_ajouter_colonne_id_territoire(df, communes_absentes=False):
    if communes_absentes:
        mask_departements = (df["DEP"] != "............") & (df["FRDOM"] == "METRO")
        df.loc[mask_departements, "id_territoire"] = traduire_code_insee_vers_id_departement_crater(df.loc[mask_departements, "DEP"])
    else:
        mask_communes = (df["COM"] != "............") & (df["FRDOM"] == "METRO")
        df.loc[mask_communes, "id_territoire"] = traduire_code_insee_vers_id_commune_crater(df.loc[mask_communes, "COM"])

        mask_departements = (df["COM"] == "............") & (df["DEP"] != "............") & (df["FRDOM"] == "METRO")
        df.loc[mask_departements, "id_territoire"] = traduire_code_insee_vers_id_departement_crater(df.loc[mask_departements, "DEP"])

    mask_regions = (df["DEP"] == "............") & (df["REGION"] != "............") & (df["FRDOM"] == "METRO")
    df.loc[mask_regions, "id_territoire"] = traduire_code_insee_vers_id_region_crater(
        df.loc[mask_regions, "REGION"].str.replace("NR", "").str.replace("R", "")
    )

    mask_pays = (df["REGION"] == "............") & (df["FRDOM"] == "METRO")
    df.loc[mask_pays, "id_territoire"] = traduire_code_insee_vers_id_pays_crater(df.loc[mask_pays, "FRANCE"])

    df = df[df["id_territoire"].notna()]
    return df
