from pathlib import Path

import geopandas

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_epci_crater
from crater.commun.logger import log
from crater.config.config_globale import CRS_PROJET


def charger_geometries_epcis(dossier: Path, crs=CRS_PROJET) -> geopandas.GeoDataFrame:
    log.info(f"   => chargement contours epcis depuis {dossier}")

    gdf = geopandas.read_file(dossier, crs=crs)
    gdf["id_epci"] = traduire_code_insee_vers_id_epci_crater(gdf.CODE_SIREN)

    return gdf
