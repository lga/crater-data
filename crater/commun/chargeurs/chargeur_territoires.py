from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log


def charger_referentiel_territoires(fichier_referentiel_territoires: Path) -> DataFrame:

    log.info(f"   => charger_referentiel_territoires : chargement du référentiel à partir de {fichier_referentiel_territoires}")
    return pd.read_csv(
        fichier_referentiel_territoires,
        sep=";",
        dtype={
            "codes_postaux": "str",
            "annee_dernier_mouvement_commune": "Int64",
            "id_territoire_parcel": "str",
            "categorie_regroupement_communes": "str",
            "categorie_epci": "str",
            "niveau_labellisation_pat": "str",
            "id_url_france_pat": "str",
        },
        na_values=[""],
    ).set_index("id_territoire")
