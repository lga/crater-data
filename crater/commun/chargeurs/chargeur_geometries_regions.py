from pathlib import Path

import geopandas

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_region_crater
from crater.commun.logger import log
from crater.config.config_globale import CRS_PROJET


def charger_geometries_regions(dossier: Path, crs=CRS_PROJET) -> geopandas.GeoDataFrame:
    log.info(f"   => chargement contours régions depuis {dossier}")

    gdf = geopandas.read_file(dossier, crs=crs)
    gdf["id_region"] = traduire_code_insee_vers_id_region_crater(gdf.INSEE_REG)

    return gdf
