from geopandas import GeoDataFrame

from crater.commun.chargeurs.chargeur_geometries_communes import charger_geometries_communes
from crater.commun.chargeurs.chargeur_geometries_departements import charger_geometries_departements
from crater.commun.chargeurs.chargeur_geometries_epcis import charger_geometries_epcis
from crater.commun.chargeurs.chargeur_geometries_regions import charger_geometries_regions


def charger_contours_regions(geometries_regions) -> GeoDataFrame:
    regions_gdf = charger_geometries_regions(geometries_regions)
    regions_gdf.rename(columns={"id_region": "id_territoire"}, inplace=True)
    regions_gdf = regions_gdf.loc[:, ["id_territoire", "geometry"]]
    return regions_gdf


def charger_contours_departements(geometries_departements) -> GeoDataFrame:
    departements_gdf = charger_geometries_departements(geometries_departements)
    departements_gdf.rename(columns={"id_departement": "id_territoire"}, inplace=True)
    departements_gdf = departements_gdf.loc[:, ["id_territoire", "geometry"]]
    return departements_gdf


def charger_contours_epcis(geometries_epcis):
    epcis_gdf = charger_geometries_epcis(geometries_epcis)
    epcis_gdf.rename(columns={"id_epci": "id_territoire"}, inplace=True)
    epcis_gdf = epcis_gdf.loc[:, ["id_territoire", "geometry"]]
    return epcis_gdf


def charger_contours_communes(geometries_communes):
    communes_gdf = charger_geometries_communes(geometries_communes)
    communes_gdf.rename(columns={"id_commune": "id_territoire"}, inplace=True)
    communes_gdf = communes_gdf.loc[:, ["id_territoire", "geometry"]]
    return communes_gdf
