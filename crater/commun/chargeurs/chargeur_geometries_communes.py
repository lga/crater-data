from pathlib import Path

import geopandas as gpd

from crater.commun.calculs.outils_geodataframes import calculer_colonne_superficie_en_hectares
from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
)
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.logger import log
from crater.config.config_globale import CRS_PROJET, NUMEROS_DEPARTEMENTS_DROM
from crater.config.config_sources import ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_GEOMETRIES


def charger_geometries_communes(chemin_dossier: Path, crs=CRS_PROJET) -> gpd.GeoDataFrame:
    log.info("   => chargement des géométries des communes depuis %s", chemin_dossier)

    geometries_communes = gpd.read_file(chemin_dossier).to_crs(crs)
    geometries_communes = _supprimer_communes_drom(geometries_communes)
    geometries_communes = _traduire_code_insee_vers_id_crater(geometries_communes)
    verifier_absence_doublons(geometries_communes, "id_commune")
    geometries_communes.rename(columns={"NOM": "nom_commune"}, inplace=True)
    geometries_communes["superficie_ha"] = calculer_colonne_superficie_en_hectares(geometries_communes)

    if ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_GEOMETRIES == ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_GEOMETRIES:
        pass
    elif chemin_dossier.name == "geometries_communes_test":
        pass
    else:
        raise ValueError(
            "ERREUR lors du chargement des géométries des communes. L'année du référentiel a changé."
            "Il faut la mettre à jour en renseignant si besoin les traitements de correction à effectuer."
        )

    return geometries_communes


def _supprimer_communes_drom(geometries_communes):
    geometries_communes["numero_departement"] = geometries_communes["INSEE_DEP"]
    geometries_communes = geometries_communes.loc[
        ~geometries_communes["numero_departement"].isin(NUMEROS_DEPARTEMENTS_DROM),
        geometries_communes.columns != "numero_departement",
    ]
    return geometries_communes


def _traduire_code_insee_vers_id_crater(
    geometries_communes: gpd.GeoDataFrame,
) -> gpd.GeoDataFrame:
    geometries_communes.rename(columns={"INSEE_COM": "id_commune"}, inplace=True)
    geometries_communes.id_commune = traduire_code_insee_vers_id_commune_crater(geometries_communes.id_commune)

    return geometries_communes
