from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log


def charger_df_indicateurs(fichier_indicateur: Path, dtype=None, parse_dates=None) -> DataFrame:
    return pd.read_csv(fichier_indicateur, sep=";", dtype=dtype, parse_dates=parse_dates).set_index("id_territoire")


def charger_et_joindre_df_indicateurs(territoires: DataFrame, fichiers_donnees_ou_indicateurs: Path) -> DataFrame:
    log.info(f"   => chargement des données ou indicateurs pour les joindre au df_territoire depuis {fichiers_donnees_ou_indicateurs}")

    df = charger_df_indicateurs(fichiers_donnees_ou_indicateurs).drop(columns=["nom_territoire", "categorie_territoire"])
    return territoires.join(df)
