from pathlib import Path

import geopandas

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_departement_crater
from crater.commun.logger import log
from crater.config.config_globale import CRS_PROJET


def charger_geometries_departements(dossier: Path, crs=CRS_PROJET) -> geopandas.GeoDataFrame:
    log.info(f"   => chargement contours départements depuis {dossier}")

    gdf = geopandas.read_file(dossier, crs=crs)
    gdf["id_departement"] = traduire_code_insee_vers_id_departement_crater(gdf.INSEE_DEP)

    return gdf
