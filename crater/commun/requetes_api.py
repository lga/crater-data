import requests

from crater.commun.logger import log


def get_retry(url):
    log.info(f"Requete GET retry {url}")
    r = requests.get(url)

    # Re tenter jusqu'à 3 fois
    if r.status_code not in range(200, 299):
        r = requests.get(url)

    if r.status_code not in range(200, 299):
        r = requests.get(url)

    if r.status_code not in range(200, 299):
        log.error(f"ERREUR : la requete GET {url} a échoué après 3 tentatives, abandon.\nRéponse du serveur : {r.status_code} {r.text}")
        return "ERREUR", "Code retour <> 2xx", 0, r.status_code
    else:
        return r
