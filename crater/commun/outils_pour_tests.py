from pathlib import Path
from typing import Optional

import pandas
import json

from crater.commun.logger import log


def assert_csv_files_are_equals(expected_file_path, result_file_path, colonnes_a_exclure=[], skiprows=None):
    log.info("COMPARAISON de %s, avec %s", expected_file_path, result_file_path)
    expected_dataframe = pandas.read_csv(Path(expected_file_path), sep=";", dtype=str, skiprows=skiprows).drop(columns=colonnes_a_exclure)

    result_dataframe = pandas.read_csv(Path(result_file_path), sep=";", dtype=str, skiprows=skiprows).drop(columns=colonnes_a_exclure)

    pandas.testing.assert_frame_equal(expected_dataframe, result_dataframe)


def assert_list_of_csv_files_are_equals(expected_folder_path, result_folder_path, file_names_list):
    for f in file_names_list:
        assert_csv_files_are_equals(expected_folder_path / f, result_folder_path / f)


def assert_json_files_are_equals(self, expected_file_path: Path, result_file_path: Path):
    log.info("COMPARAISON de %s, avec %s", expected_file_path, result_file_path)

    with open(expected_file_path, "r") as file:
        expected_json = json.load(file)
    with open(result_file_path, "r") as file:
        result_json = json.load(file)

    self.assertEqual(expected_json, result_json)


def assert_list_of_json_files_are_equals(self, expected_folder_path, result_folder_path, file_names_list):
    for f in file_names_list:
        assert_json_files_are_equals(self, expected_folder_path / f, result_folder_path / f)


def assert_error_message_raised(self, message, fonction, **kwargs):
    with self.assertRaises(Exception) as c:
        fonction(**kwargs)
    self.assertEqual(str(c.exception), message)


def augmenter_nombre_colonnes_affichees_pandas(nb_colonnes: Optional[int] = None):
    pandas.set_option("display.max_columns", nb_colonnes)
