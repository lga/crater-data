import datetime
import logging
import sys
import time
from functools import wraps

FORMATTER = logging.Formatter("%(asctime)s — %(levelname)s — [%(processName)s] : %(message)s [%(filename)s]")
NOM_FICHIER_LOG_DEFAULT = "crater_data.log"


def creer_console_handler():
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    return console_handler


def creer_file_handler(nom_fichier_log):
    file_handler = logging.FileHandler(nom_fichier_log, mode="w")
    file_handler.setFormatter(FORMATTER)
    return file_handler


def creer_logger(nom_logger=None, nom_fichier_log=NOM_FICHIER_LOG_DEFAULT):
    logger = logging.getLogger(nom_logger)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(creer_console_handler())
    logger.addHandler(creer_file_handler(nom_fichier_log))
    return logger


log = creer_logger("crater_data")


def chronometre(func):
    @wraps(func)
    def decorer(*args, **kwargs):
        debut = time.time()
        log.info(f"  - Début traitement {func.__name__}")

        resultat = func(*args, **kwargs)

        temps = datetime.timedelta(seconds=time.time() - debut)
        temps = temps - datetime.timedelta(microseconds=temps.microseconds)
        log.info(f"  - Fin traitement   {func.__name__}, durée={temps} ")

        return resultat

    return decorer
