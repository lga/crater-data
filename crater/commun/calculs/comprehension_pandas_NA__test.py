import unittest

import numpy as np
import pandas as pd


# Tests pour expliciter le fonctionnement de pandas.NA et des types Float64 et float64
# Permettra de voir comment le comportement évolue lors des upgrade de versions de pandas
class TestsFonctionnementPandasNAPourTypesFloat(unittest.TestCase):
    def test_comparaison_basiques_numpy_nan_python_NA(self):
        self.assertTrue(np.isnan(np.nan))
        self.assertTrue(pd.isna(np.isnan(pd.NA)))  # np.isnan(pd.NA) renvoie pd.NA et pas True
        self.assertTrue(pd.isna(pd.NA))
        self.assertTrue(pd.isna(np.nan))

    def test_division_par_0_avec_float64(self):
        # given
        df = pd.DataFrame(
            columns=[
                "ind1",
                "ind2",
            ],
            data=[
                [0, 0.0],
            ],
        )
        # when
        df["ind3"] = df["ind1"] / df["ind2"]
        # then
        self.assertEqual(df["ind3"].dtype, "float64")
        self.assertTrue(np.isnan(df["ind3"][0]))

        # when
        df["ind3"] = df["ind3"].fillna(-1)
        self.assertEqual(df["ind3"][0], -1)

    def test_division_par_0_avec_Float64_introduit_nan(self):
        # given
        df = pd.DataFrame(
            columns=[
                "ind1",
                "ind2",
            ],
            data=[
                [20.0, 10],
                [20.0, np.nan],
                [0, 0.0],
            ],
        ).astype({"ind1": "Float64", "ind2": "Float64"})
        # when
        df["ind3"] = df["ind1"] / df["ind2"]
        # then
        # les np.nan sont remplacés par des pd.NA lors du astype vers Float64
        self.assertEqual(df["ind2"].dtype, "Float64")
        self.assertTrue(pd.isna(df["ind2"][1]))
        self.assertEqual(df["ind3"].dtype, "Float64")
        self.assertTrue(pd.isna(df["ind3"][1]))
        # mais la division par 0 introduit un np.nan
        self.assertTrue(np.isnan(df["ind3"][2]))

    def test_remplacement_nan_dans_colonne_Float64_pose_des_problemes(self):
        # given
        df = pd.DataFrame(
            columns=[
                "ind1",
                "ind2",
            ],
            data=[
                [20.0, pd.NA],
                [0, 0.0],
            ],
        ).astype({"ind1": "Float64", "ind2": "Float64"})
        df["ind3"] = df["ind1"] / df["ind2"]  # introduit des np.nan dans la colonne ind3

        # when
        df["ind3"] = df["ind3"].fillna(-1)
        df["ind3"] = df["ind3"].replace(np.nan, -2)
        # df["ind3"] = np.nan_to_num(df["ind3"], nan=-3) # changement numpy, cette affectation fonctionne
        # df.loc[pd.isna(df["ind3"]), "ind3"] = -4 => erreur cette ligne ne marche plus, le isna ne fonctionne pas
        # then
        # self.assertEqual(df["ind3"][1], -4)  # fillna, replace et nan_to_num n'ont aucun effet, seule dernière méthode permet de remplacer pd.NA


if __name__ == "__main__":
    unittest.main()
