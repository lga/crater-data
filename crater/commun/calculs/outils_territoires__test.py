import unittest

import numpy as np
import pandas as pd

from crater.commun.calculs.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales,
    extraire_df_communes_appartenant_a_une_categorie_de_territoire,
    calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales,
    _calculer_part_valeurs_na,
    remplir_de_zeros_sous_colonnes_si_totaux_ok,
    initialiser_df_indicateur_par_annees,
    trier_territoires,
    vider_colonne_selon_mouvements_communes,
    vider_colonne_si_est_estimee,
)
from crater.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas

augmenter_nombre_colonnes_affichees_pandas()


class TestsUnitairesOutilsTerritoires(unittest.TestCase):
    def test_vider_colonne_selon_mouvement_communes(self):
        # given
        df = pd.DataFrame(
            data=[
                ["D-1", 2021, 1],
                ["D-2", 2020, 2],
                ["D-3", 2019, 3],
                ["D-4", 2018, 4],
                ["C-1", 2021, 1],
                ["C-2", 2020, 2],
                ["C-3", 2019, 3],
                ["C-4", 2018, 4],
            ],
            columns={
                "id_territoire": "str",
                "annee_dernier_mouvement_commune": "int",
                "colonne": "int",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["D-1", 2021, np.nan],
                ["D-2", 2020, np.nan],
                ["D-3", 2019, 3],
                ["D-4", 2018, 4],
                ["C-1", 2021, np.nan],
                ["C-2", 2020, np.nan],
                ["C-3", 2019, 3],
                ["C-4", 2018, 4],
            ],
            columns={
                "id_territoire": "str",
                "annee_dernier_mouvement_commune": "int",
                "colonne": "int",
            },
        ).set_index(["id_territoire"])
        # when - then
        pd.testing.assert_frame_equal(vider_colonne_selon_mouvements_communes(df, 2019, "colonne"), df_resultat_attendu)

    def test_vider_colonne_si_est_estimee(self):
        # given
        df = pd.DataFrame(
            data=[
                ["T-1", 1, np.nan],
                ["T-2", 2, True],
                ["T-3", 3, False],
            ],
            columns={
                "id_territoire": "str",
                "indicateur_A": "int",
                "indicateur_A_est_estime": "int",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["T-1", 1, np.nan],
                ["T-2", np.nan, True],
                ["T-3", 3, False],
            ],
            columns={
                "id_territoire": "str",
                "indicateur_A": "int",
                "indicateur_A_est_estime": "int",
            },
        ).set_index(["id_territoire"])
        # when - then
        pd.testing.assert_frame_equal(vider_colonne_si_est_estimee(df, "indicateur_A"), df_resultat_attendu)

    def test_calculer_pourcentage_na_dans_colonne(
        self,
    ):
        # given
        df = pd.DataFrame(
            data=[
                ["D-1", 2019, np.nan],
                ["D-1", 2019, np.nan],
                ["D-1", 2019, np.nan],
                ["D-1", 2020, 10],
                ["D-1", 2020, np.nan],
                ["D-2", 2019, 10],
                ["D-2", 2019, 10],
                ["D-2", 2019, 10],
            ],
            columns={
                "id_territoire": "str",
                "annee": "int",
                "colonne": "int",
            },
        ).set_index(["id_territoire", "annee"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["D-1", 2019, 100.0],
                ["D-1", 2020, 50.0],
                ["D-2", 2019, 0.0],
            ],
            columns={
                "id_territoire": "str",
                "annee": "float64",
                "colonne_part_na_pourcent": "float64",
            },
        ).set_index(["id_territoire", "annee"])
        # when - then
        pd.testing.assert_frame_equal(_calculer_part_valeurs_na(df, "colonne"), df_resultat_attendu)

    def test_ajouter_donnees_supraterritoriales_par_somme_donnees_communes_vers_tous_niveaux(
        self,
    ):
        # given
        territoires = pd.DataFrame(
            data=[
                ["P-FR", "PAYS", np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["R-1", "REGION", np.nan, np.nan, np.nan, np.nan, "P-FR", np.nan],
                ["D-1", "DEPARTEMENT", np.nan, np.nan, np.nan, "R-1", "P-FR", np.nan],
                [
                    "Z-1",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    np.nan,
                ],
                [
                    "Z-2",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    np.nan,
                ],
                ["E-1", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", np.nan],
                ["C-1", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", 1],
                ["C-2", "COMMUNE", "E-1", "Z-1|Z-2", "D-1", "R-1", "P-FR", 10],
                ["C-3", "COMMUNE", "E-1", "Z-1|Z-2", "D-1", "R-1", "P-FR", np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["P-FR", "PAYS", np.nan, np.nan, np.nan, np.nan, np.nan, 11],
                ["R-1", "REGION", np.nan, np.nan, np.nan, np.nan, "P-FR", 11],
                ["D-1", "DEPARTEMENT", np.nan, np.nan, np.nan, "R-1", "P-FR", 11],
                [
                    "Z-1",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    11,
                ],
                [
                    "Z-2",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    10,
                ],
                ["E-1", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", 11],
                ["C-1", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", 1],
                ["C-2", "COMMUNE", "E-1", "Z-1|Z-2", "D-1", "R-1", "P-FR", 10],
                ["C-3", "COMMUNE", "E-1", "Z-1|Z-2", "D-1", "R-1", "P-FR", np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire"])
        # when
        df_resultat = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "colonne")
        # then
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)

    def test_ajouter_donnees_supraterritoriales_par_somme_donnees_departements_vers_regions(
        self,
    ):
        # given
        territoires = pd.DataFrame(
            data=[
                ["P-FR", "PAYS", np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["R-1", "REGION", np.nan, np.nan, np.nan, np.nan, "P-FR", np.nan],
                ["D-1", "DEPARTEMENT", np.nan, np.nan, np.nan, "R-1", "P-FR", 100],
                ["D-2", "DEPARTEMENT", np.nan, np.nan, np.nan, "R-1", "P-FR", 10],
                [
                    "Z-1",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    np.nan,
                ],
                [
                    "Z-2",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    np.nan,
                ],
                ["E-1", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", 11],
                ["C-1", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", 1],
                ["C-2", "COMMUNE", "E-1", "Z-1|Z-2", "D-1", "R-1", "P-FR", 10],
                ["C-3", "COMMUNE", "E-1", "Z-1|Z-2", "D-1", "R-1", "P-FR", np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["P-FR", "PAYS", np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["R-1", "REGION", np.nan, np.nan, np.nan, np.nan, "P-FR", 110],
                ["D-1", "DEPARTEMENT", np.nan, np.nan, np.nan, "R-1", "P-FR", 100],
                ["D-2", "DEPARTEMENT", np.nan, np.nan, np.nan, "R-1", "P-FR", 10],
                [
                    "Z-1",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    np.nan,
                ],
                [
                    "Z-2",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    np.nan,
                ],
                ["E-1", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", 11],
                ["C-1", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", 1],
                ["C-2", "COMMUNE", "E-1", "Z-1|Z-2", "D-1", "R-1", "P-FR", 10],
                ["C-3", "COMMUNE", "E-1", "Z-1|Z-2", "D-1", "R-1", "P-FR", np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire"])
        # when
        df_resultat = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            territoires,
            "colonne",
            categorie_territoire_a_sommer="DEPARTEMENT",
            categories_territoires_cibles=["REGION"],
        )
        # then
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)

    def test_ajouter_donnees_supraterritoriales_par_somme_donnees_propagation_nan(self):
        # given
        territoires = pd.DataFrame(
            data=[
                ["P-FR", "PAYS", np.nan, np.nan, np.nan, np.nan, np.nan, 9999],
                ["R-1", "REGION", np.nan, np.nan, np.nan, np.nan, "P-FR", 9999],
                ["D-1", "DEPARTEMENT", np.nan, np.nan, np.nan, "R-1", "P-FR", 9999],
                [
                    "Z-1",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    9999,
                ],
                ["E-1", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", 9999],
                ["C-1", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", np.nan],
                ["C-2", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["P-FR", "PAYS", np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["R-1", "REGION", np.nan, np.nan, np.nan, np.nan, "P-FR", np.nan],
                ["D-1", "DEPARTEMENT", np.nan, np.nan, np.nan, "R-1", "P-FR", np.nan],
                [
                    "Z-1",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    np.nan,
                ],
                ["E-1", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", np.nan],
                ["C-1", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", np.nan],
                ["C-2", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire"])
        # when
        df_resultat = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "colonne")
        # then
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)

    def test_ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales_multi_index(
        self,
    ):
        # given
        territoires = pd.DataFrame(
            data=[
                [
                    "P-FR",
                    "GROUPE1_AVEC_NANs",
                    "PAYS",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    9999,
                ],
                [
                    "Z-1",
                    "GROUPE1_AVEC_NANs",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    9999,
                ],
                [
                    "E-1",
                    "GROUPE1_AVEC_NANs",
                    "EPCI",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    9999,
                ],
                [
                    "C-1",
                    "GROUPE1_AVEC_NANs",
                    "COMMUNE",
                    "E-1",
                    "Z-1",
                    np.nan,
                    np.nan,
                    "P-FR",
                    1,
                ],
                [
                    "C-2",
                    "GROUPE1_AVEC_NANs",
                    "COMMUNE",
                    "E-1",
                    "Z-1",
                    np.nan,
                    np.nan,
                    "P-FR",
                    10,
                ],
                [
                    "C-3",
                    "GROUPE1_AVEC_NANs",
                    "COMMUNE",
                    "E-1",
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    np.nan,
                ],
                [
                    "P-FR",
                    "GROUPE2_SANS_NAN",
                    "PAYS",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    9999,
                ],
                [
                    "Z-1",
                    "GROUPE2_SANS_NAN",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    9999,
                ],
                [
                    "E-1",
                    "GROUPE2_SANS_NAN",
                    "EPCI",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    9999,
                ],
                [
                    "C-1",
                    "GROUPE2_SANS_NAN",
                    "COMMUNE",
                    "E-1",
                    "Z-1",
                    np.nan,
                    np.nan,
                    "P-FR",
                    100,
                ],
                [
                    "C-3",
                    "GROUPE2_SANS_NAN",
                    "COMMUNE",
                    "E-1",
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    50,
                ],
                [
                    "P-FR",
                    "GROUPE3_TOUS_NAN",
                    "PAYS",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    9999,
                ],
                [
                    "Z-1",
                    "GROUPE3_TOUS_NAN",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    9999,
                ],
                [
                    "E-1",
                    "GROUPE3_TOUS_NAN",
                    "EPCI",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    9999,
                ],
                [
                    "C-1",
                    "GROUPE3_TOUS_NAN",
                    "COMMUNE",
                    "E-1",
                    "Z-1",
                    np.nan,
                    np.nan,
                    "P-FR",
                    np.nan,
                ],
            ],
            columns={
                "id_territoire": "str",
                "regroupement": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire", "regroupement"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                [
                    "P-FR",
                    "GROUPE1_AVEC_NANs",
                    "PAYS",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    11,
                ],
                [
                    "Z-1",
                    "GROUPE1_AVEC_NANs",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    11,
                ],
                [
                    "E-1",
                    "GROUPE1_AVEC_NANs",
                    "EPCI",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    11,
                ],
                [
                    "C-1",
                    "GROUPE1_AVEC_NANs",
                    "COMMUNE",
                    "E-1",
                    "Z-1",
                    np.nan,
                    np.nan,
                    "P-FR",
                    1,
                ],
                [
                    "C-2",
                    "GROUPE1_AVEC_NANs",
                    "COMMUNE",
                    "E-1",
                    "Z-1",
                    np.nan,
                    np.nan,
                    "P-FR",
                    10,
                ],
                [
                    "C-3",
                    "GROUPE1_AVEC_NANs",
                    "COMMUNE",
                    "E-1",
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    np.nan,
                ],
                [
                    "P-FR",
                    "GROUPE2_SANS_NAN",
                    "PAYS",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    150,
                ],
                [
                    "Z-1",
                    "GROUPE2_SANS_NAN",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    100,
                ],
                [
                    "E-1",
                    "GROUPE2_SANS_NAN",
                    "EPCI",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    150,
                ],
                [
                    "C-1",
                    "GROUPE2_SANS_NAN",
                    "COMMUNE",
                    "E-1",
                    "Z-1",
                    np.nan,
                    np.nan,
                    "P-FR",
                    100,
                ],
                [
                    "C-3",
                    "GROUPE2_SANS_NAN",
                    "COMMUNE",
                    "E-1",
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    50,
                ],
                [
                    "P-FR",
                    "GROUPE3_TOUS_NAN",
                    "PAYS",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                ],
                [
                    "Z-1",
                    "GROUPE3_TOUS_NAN",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    np.nan,
                ],
                [
                    "E-1",
                    "GROUPE3_TOUS_NAN",
                    "EPCI",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    np.nan,
                ],
                [
                    "C-1",
                    "GROUPE3_TOUS_NAN",
                    "COMMUNE",
                    "E-1",
                    "Z-1",
                    np.nan,
                    np.nan,
                    "P-FR",
                    np.nan,
                ],
            ],
            columns={
                "id_territoire": "str",
                "regroupement": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire", "regroupement"])
        # when
        df_resultat = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "colonne")
        # then
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)

    def test_ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales_avec_calcul_qualite(
        self,
    ):
        # given
        territoires = pd.DataFrame(
            data=[
                # 100% des communes avec donnée
                ["E-1", "EPCI", np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["C-11", "COMMUNE", "E-1", np.nan, np.nan, np.nan, np.nan, 10],
                ["C-12", "COMMUNE", "E-1", np.nan, np.nan, np.nan, np.nan, 10],
                ["C-13", "COMMUNE", "E-1", np.nan, np.nan, np.nan, np.nan, 10],
                # 50% des communes avec donnée
                ["E-2", "EPCI", np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["C-21", "COMMUNE", "E-2", np.nan, np.nan, np.nan, np.nan, np.nan],
                ["C-22", "COMMUNE", "E-2", np.nan, np.nan, np.nan, np.nan, 10],
                # 0% des communes avec donnée
                ["E-3", "EPCI", np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["C-31", "COMMUNE", "E-3", np.nan, np.nan, np.nan, np.nan, np.nan],
                ["C-32", "COMMUNE", "E-3", np.nan, np.nan, np.nan, np.nan, np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["E-1", 30, 0],
                ["C-11", 10, np.nan],
                ["C-12", 10, np.nan],
                ["C-13", 10, np.nan],
                ["E-2", 10, 50],
                ["C-21", np.nan, np.nan],
                ["C-22", 10, np.nan],
                ["E-3", np.nan, 100],
                ["C-31", np.nan, np.nan],
                ["C-32", np.nan, np.nan],
            ],
            columns={
                "id_territoire": "str",
                "colonne": "float64",
                "colonne_part_na_pourcent": "float64",
            },
        ).set_index(["id_territoire"])
        # when
        df_resultat = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            territoires,
            "colonne",
            categorie_territoire_a_sommer="COMMUNE",
            categories_territoires_cibles=["EPCI"],
            compter_part_valeurs_na=True,
        )
        # then
        pd.testing.assert_frame_equal(
            df_resultat.loc[:, ["colonne", "colonne_part_na_pourcent"]],
            df_resultat_attendu.loc[:, ["colonne", "colonne_part_na_pourcent"]],
        )

    def test_ajouter_donnees_supraterritoriales_par_moyenne_pondere(self):
        # given
        territoires = pd.DataFrame(
            data=[
                [
                    "P-FR",
                    "PAYS",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                ],
                [
                    "R-1",
                    "REGION",
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    "P-FR",
                    np.nan,
                    np.nan,
                ],
                [
                    "D-1",
                    "DEPARTEMENT",
                    np.nan,
                    np.nan,
                    np.nan,
                    "R-1",
                    "P-FR",
                    np.nan,
                    np.nan,
                ],
                [
                    "Z-1",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    np.nan,
                    99,
                ],
                ["E-1", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", np.nan, 99],
                ["E-2", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", np.nan, 99],
                ["C-1", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", 2, 3],
                ["C-2", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", 10, 1],
                ["C-3", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", np.nan, 2.0],
                ["C-4", "COMMUNE", "E-2", np.nan, "D-1", "R-1", "P-FR", 9, 1],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "indicateur": "float64",
                "ponderation": "float64",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["P-FR", "PAYS", np.nan, np.nan, np.nan, np.nan, np.nan, 5, np.nan],
                ["R-1", "REGION", np.nan, np.nan, np.nan, np.nan, "P-FR", 5, np.nan],
                [
                    "D-1",
                    "DEPARTEMENT",
                    np.nan,
                    np.nan,
                    np.nan,
                    "R-1",
                    "P-FR",
                    5,
                    np.nan,
                ],
                [
                    "Z-1",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    np.nan,
                    "D-1",
                    "R-1",
                    "P-FR",
                    4,
                    99,
                ],
                ["E-1", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", 4, 99],
                ["E-2", "EPCI", np.nan, np.nan, "D-1", "R-1", "P-FR", 9, 99],
                ["C-1", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", 2, 3],
                ["C-2", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", 10, 1],
                ["C-3", "COMMUNE", "E-1", "Z-1", "D-1", "R-1", "P-FR", np.nan, 2],
                ["C-4", "COMMUNE", "E-2", np.nan, "D-1", "R-1", "P-FR", 9, 1],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "indicateur": "float64",
                "ponderation": "float64",
            },
        ).set_index(["id_territoire"])
        # when
        df_resultat = ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales(territoires, "indicateur", "ponderation")

        # then
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)

    def test_extraire_df_communes_appartenant_a_une_categorie_de_territoire_EPCI(self):
        # given
        territoires = creer_df_territoires_test_filtrage()
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["C-1", "COMMUNE", "E-1", "Z-1|Z-2", np.nan, np.nan, "P-FR", 1],
                ["C-2", "COMMUNE", "E-1", np.nan, np.nan, np.nan, "P-FR", 1],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "ids_regroupements_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire"])
        # when - then
        pd.testing.assert_frame_equal(
            extraire_df_communes_appartenant_a_une_categorie_de_territoire(territoires, "EPCI"),
            df_resultat_attendu,
        )

    def test_extraire_df_communes_appartenant_a_une_categorie_de_territoire_regroupement_communes(
        self,
    ):
        # given
        territoires = creer_df_territoires_test_filtrage()
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["C-1", "COMMUNE", "E-1", "Z-1", np.nan, np.nan, "P-FR", 1],
                ["C-1", "COMMUNE", "E-1", "Z-2", np.nan, np.nan, "P-FR", 1],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_epci": "str",
                "id_regroupement_communes": "str",
                "id_departement": "str",
                "id_region": "str",
                "id_pays": "str",
                "colonne": "float64",
            },
        ).set_index(["id_territoire"])
        # when - then
        pd.testing.assert_frame_equal(
            extraire_df_communes_appartenant_a_une_categorie_de_territoire(territoires, "REGROUPEMENT_COMMUNES"),
            df_resultat_attendu,
        )

    def test_calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
        self,
    ):
        # given
        territoires = pd.DataFrame(
            data=[
                # région avec 1 unique département sans valeur
                ["R-1", "REGION", np.nan, 100, np.nan],
                ["D-11", "DEPARTEMENT", "R-1", np.nan, 10],
                # région avec 1 département sans valeur sur 2
                ["R-2", "REGION", np.nan, 110, np.nan],
                ["D-21", "DEPARTEMENT", "R-2", 10, np.nan],
                ["D-22", "DEPARTEMENT", "R-2", np.nan, 100],
                # région avec 2 départements sans valeur sur 2
                ["R-3", "REGION", np.nan, 200, np.nan],
                ["D-31", "DEPARTEMENT", "R-3", np.nan, 25],
                ["D-32", "DEPARTEMENT", "R-3", np.nan, 75],
                # région avec 2 départements sans valeur et 1 sans poids ni valeur sur 4
                ["R-4", "REGION", np.nan, 210, np.nan],
                ["D-41", "DEPARTEMENT", "R-4", 10, np.nan],
                ["D-42", "DEPARTEMENT", "R-4", np.nan, 25],
                ["D-43", "DEPARTEMENT", "R-4", np.nan, 75],
                ["D-44", "DEPARTEMENT", "R-4", np.nan, np.nan],
                # région avec départements sans valeur ni poids
                ["R-5", "REGION", np.nan, 100, np.nan],
                ["D-51", "DEPARTEMENT", "R-5", np.nan, np.nan],
                ["D-52", "DEPARTEMENT", "R-5", np.nan, np.nan],
                # région avec des communes non concernees par le calcul
                ["R-6", "REGION", np.nan, 100, np.nan],
                ["C-1", "COMMUNE", "R-5", 10, np.nan],
                ["C-2", "COMMUNE", "R-5", np.nan, 10],
                ["C-3", "COMMUNE", "R-5", np.nan, np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "id_region": "str",
                "valeur": "float64",
                "superficie": "float64",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["R-1", 100, False],
                ["D-11", 100, True],
                ["R-2", 110, False],
                ["D-21", 10, False],
                ["D-22", 100, True],
                ["R-3", 200, False],
                ["D-31", 50, True],
                ["D-32", 150, True],
                ["R-4", 210, False],
                ["D-41", 10, False],
                ["D-42", 50, True],
                ["D-43", 150, True],
                ["D-44", 0, True],
                ["R-5", 100, False],
                ["D-51", 50, True],
                ["D-52", 50, True],
                ["R-6", 100, False],
                ["C-1", 10, False],
                ["C-2", np.nan, False],
                ["C-3", np.nan, False],
            ],
            columns={
                "id_territoire": "str",
                "valeur": "float64",
                "valeur_est_estime": "bool",
            },
        ).set_index(["id_territoire"])
        # when - then
        pd.testing.assert_frame_equal(
            calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
                territoires, "valeur", "superficie", "DEPARTEMENT", "REGION"
            )[["valeur", "valeur_est_estime"]],
            df_resultat_attendu[["valeur", "valeur_est_estime"]],
            check_names=False,
            check_dtype=False,
        )

    def test_calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales_multi_index(
        self,
    ):
        # given
        territoires = pd.DataFrame(
            data=[
                ["R-1", 2020, "REGION", np.nan, 100, np.nan],
                ["D-1", 2020, "DEPARTEMENT", "R-1", np.nan, 10],
                ["D-2", 2020, "DEPARTEMENT", "R-1", np.nan, 10],
                ["R-1", 2019, "REGION", np.nan, 200, np.nan],
                ["D-1", 2019, "DEPARTEMENT", "R-1", 20, 10],
                ["D-2", 2019, "DEPARTEMENT", "R-1", np.nan, 10],
            ],
            columns={
                "id_territoire": "str",
                "annee": "int",
                "categorie_territoire": "str",
                "id_region": "str",
                "valeur": "float64",
                "superficie": "float64",
            },
        ).set_index(["id_territoire", "annee"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["R-1", 2020, 100, False],
                ["D-1", 2020, 50, True],
                ["D-2", 2020, 50, True],
                ["R-1", 2019, 200, False],
                ["D-1", 2019, 20, False],
                ["D-2", 2019, 180, True],
            ],
            columns={
                "id_territoire": "str",
                "annee": "int",
                "valeur": "float64",
                "valeur_est_estime": "bool",
            },
        ).set_index(["id_territoire", "annee"])
        # when - then
        pd.testing.assert_frame_equal(
            calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
                territoires, "valeur", "superficie", "DEPARTEMENT", "REGION"
            )[["valeur", "valeur_est_estime"]],
            df_resultat_attendu[["valeur", "valeur_est_estime"]],
            check_names=False,
            check_dtype=False,
        )

    def test_remplir_de_zeros_sous_colonnes_si_totaux_ok(
        self,
    ):
        # given
        donnees_territoires = pd.DataFrame(
            data=[
                # pas de trou
                ["C-1", "COMMUNE", 6, 1, 2, 3, np.nan],
                # 1 trou à remplir
                ["C-2", "COMMUNE", 3, 1, 2, np.nan, np.nan],
                # 3 trous à remplir (dernière colonne hors sélection)
                ["C-3", "COMMUNE", 0, np.nan, np.nan, np.nan, np.nan],
                # ignoré - somme invalide avec ensemble
                ["C-4", "COMMUNE", 1, 1000, 1000, np.nan, np.nan],
                # ignoré - autre catégorie de territoire
                ["E-1", "EPCI", 300, np.nan, np.nan, 200, np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "une_variable.ensemble": "int",
                "une_variable.cat_a": "int",
                "une_variable.cat_b": "int",
                "une_variable.cat_c": "int",
                "une_variable.cat_d": "int",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["C-1", "COMMUNE", 6, 1, 2, 3, np.nan],
                ["C-2", "COMMUNE", 3, 1, 2, 0, np.nan],
                ["C-3", "COMMUNE", 0, 0, 0, 0, np.nan],
                ["C-4", "COMMUNE", 1, 1000, 1000, np.nan, np.nan],
                ["E-1", "EPCI", 300, np.nan, np.nan, 200, np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "une_variable.ensemble": "int",
                "une_variable.cat_a": "int",
                "une_variable.cat_b": "int",
                "une_variable.cat_c": "int",
                "une_variable.cat_d": "int",
            },
        ).set_index(["id_territoire"])
        # when
        remplir_de_zeros_sous_colonnes_si_totaux_ok(donnees_territoires, "une_variable", "cat_a", "cat_c")
        # then
        pd.testing.assert_frame_equal(donnees_territoires, df_resultat_attendu)

    def test_remplir_de_zeros_sous_colonnes_si_totaux_ok_filtre_territoires(
        self,
    ):
        # given
        donnees_territoires = pd.DataFrame(
            data=[
                ["E-1", "EPCI", 20, np.nan, np.nan, 20],
                ["R-6", "DEPARTEMENT", 0, np.nan, np.nan, np.nan],
                # ignoré
                ["C-3", "COMMUNE", 0, np.nan, np.nan, np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "une_variable.ensemble": "int",
                "une_variable.cat_a": "int",
                "une_variable.cat_b": "int",
                "une_variable.cat_c": "int",
            },
        ).set_index(["id_territoire"])
        df_resultat_attendu = pd.DataFrame(
            data=[
                ["E-1", "EPCI", 20, 0, 0, 20],
                ["R-6", "DEPARTEMENT", 0, 0, 0, 0],
                ["C-3", "COMMUNE", 0, np.nan, np.nan, np.nan],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "une_variable.ensemble": "int",
                "une_variable.cat_a": "int",
                "une_variable.cat_b": "int",
                "une_variable.cat_c": "int",
            },
        ).set_index(["id_territoire"])
        # when
        remplir_de_zeros_sous_colonnes_si_totaux_ok(
            donnees_territoires,
            "une_variable",
            "cat_a",
            "cat_c",
            ["EPCI", "DEPARTEMENT"],
        )
        # then
        pd.testing.assert_frame_equal(donnees_territoires, df_resultat_attendu)

    def test_initialiser_df_indicateur_par_annees(
        self,
    ):
        # # given
        territoires = pd.DataFrame(
            data=[
                ["E-1", "Epci", "EPCI"],
                ["C-1", "Commune1", "COMMUNE"],
            ],
            columns={
                "id_territoire": "str",
                "nom_territoire": "str",
                "categorie_territoire": "str",
            },
        ).set_index(["id_territoire"])
        annees = pd.Series([2019, 2019, 2018])
        df_indicateur_attendu = pd.DataFrame(
            data=[
                ["E-1", "Epci", "EPCI", 2018],
                ["E-1", "Epci", "EPCI", 2019],
                ["C-1", "Commune1", "COMMUNE", 2018],
                ["C-1", "Commune1", "COMMUNE", 2019],
            ],
            columns={
                "id_territoire": "str",
                "nom_territoire": "str",
                "categorie_territoire": "str",
                "annee": "int",
            },
        ).set_index(["id_territoire", "annee"])
        # when
        df_indicateur = initialiser_df_indicateur_par_annees(territoires, annees)
        # then
        pd.testing.assert_frame_equal(df_indicateur, df_indicateur_attendu)

    def test_trier_df_par_territoire(
        self,
    ):
        # given
        df_non_trie = pd.DataFrame(
            data=[
                ["C-1", "COMMUNE", 1],
                ["E-1", "EPCI", 10],
                ["PAT-1", "REGROUPEMENT_COMMUNES", 99],
                ["D-2", "DEPARTEMENT", 102],
                ["D-1", "DEPARTEMENT", 101],
                ["R-1", "REGION", 1000],
                ["P-FR", "PAYS", 10000],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "valeur": "float64",
            },
        ).set_index(["id_territoire"])

        df_attendu = pd.DataFrame(
            data=[
                ["P-FR", "PAYS", 10000],
                ["R-1", "REGION", 1000],
                ["D-1", "DEPARTEMENT", 101],
                ["D-2", "DEPARTEMENT", 102],
                ["PAT-1", "REGROUPEMENT_COMMUNES", 99],
                ["E-1", "EPCI", 10],
                ["C-1", "COMMUNE", 1],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "valeur": "float64",
            },
        ).set_index(["id_territoire"])
        # when
        df_resultat = trier_territoires(df_non_trie)
        # then
        pd.testing.assert_frame_equal(df_resultat, df_attendu)

    def test_trier_df_par_territoire_et_annee(
        self,
    ):
        # given
        df_non_trie = pd.DataFrame(
            data=[
                ["D-2", "DEPARTEMENT", 2000, 102],
                ["D-1", "DEPARTEMENT", 2000, 101],
                ["D-1", "DEPARTEMENT", 1999, 101],
                ["R-1", "REGION", 2000, 1000],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "annee": "int",
                "valeur": "float64",
            },
        ).set_index(["id_territoire", "annee"])

        df_attendu = pd.DataFrame(
            data=[
                ["R-1", "REGION", 2000, 1000],
                ["D-1", "DEPARTEMENT", 1999, 101],
                ["D-1", "DEPARTEMENT", 2000, 101],
                ["D-2", "DEPARTEMENT", 2000, 102],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "annee": "int",
                "valeur": "float64",
            },
        ).set_index(["id_territoire", "annee"])
        # when
        df_resultat = trier_territoires(df_non_trie)
        # then
        pd.testing.assert_frame_equal(df_resultat, df_attendu)

    def test_trier_df_par_territoire_et_colonne(
        self,
    ):
        # given
        df_non_trie = pd.DataFrame(
            data=[
                ["D-2", "DEPARTEMENT", 2000, 110],
                ["D-1", "DEPARTEMENT", 2000, 102],
                ["D-1", "DEPARTEMENT", 1999, 101],
                ["R-1", "REGION", 2000, 1000],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "colonne_critere_tri_secondaire": "int",
                "valeur": "float64",
            },
        )

        df_attendu = pd.DataFrame(
            data=[
                ["R-1", "REGION", 2000, 1000],
                ["D-1", "DEPARTEMENT", 1999, 101],
                ["D-1", "DEPARTEMENT", 2000, 102],
                ["D-2", "DEPARTEMENT", 2000, 110],
            ],
            columns={
                "id_territoire": "str",
                "categorie_territoire": "str",
                "colonne_critere_tri_secondaire": "int",
                "valeur": "float64",
            },
        )
        # when
        df_resultat = trier_territoires(df_non_trie, ["colonne_critere_tri_secondaire"])
        # then
        pd.testing.assert_frame_equal(df_resultat.reset_index(drop=True), df_attendu.reset_index(drop=True))


def creer_df_territoires_test_filtrage():
    territoires = pd.DataFrame(
        data=[
            ["P-FR", "PAYS", np.nan, np.nan, np.nan, np.nan, np.nan, 1],
            ["Z-1", "REGROUPEMENT_COMMUNES", np.nan, np.nan, np.nan, np.nan, "P-FR", 1],
            ["Z-2", "REGROUPEMENT_COMMUNES", np.nan, np.nan, np.nan, np.nan, "P-FR", 1],
            ["E-1", "EPCI", np.nan, np.nan, np.nan, np.nan, "P-FR", 1],
            ["C-1", "COMMUNE", "E-1", "Z-1|Z-2", np.nan, np.nan, "P-FR", 1],
            ["C-2", "COMMUNE", "E-1", np.nan, np.nan, np.nan, "P-FR", 1],
            ["C-3", "COMMUNE", np.nan, np.nan, np.nan, np.nan, "P-FR", 1],
        ],
        columns={
            "id_territoire": "str",
            "categorie_territoire": "str",
            "id_epci": "str",
            "ids_regroupements_communes": "str",
            "id_departement": "str",
            "id_region": "str",
            "id_pays": "str",
            "colonne": "float64",
        },
    ).set_index(["id_territoire"])
    return territoires


if __name__ == "__main__":
    unittest.main()
