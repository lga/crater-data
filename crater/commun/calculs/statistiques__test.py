import unittest

import numpy as np
import pandas as pd

from crater.commun.calculs.statistiques import calculer_statistiques, filtrer_outliers, discretiser_indicateur


class TestsUnitairesStatistiques(unittest.TestCase):
    def test_calculer_statistiques(self):
        # given
        df = pd.DataFrame(
            columns=[
                "id_territoire",
                "nom_territoire",
                "categorie_territoire",
                "ind1",
                "ind2",
                "ind3",
            ],
            data=[
                ["P-FR", "France", "PAYS", 1, np.nan, "A"],
                ["C-1", "Commune1", "COMMUNE", 100, 13, "B"],
                ["C-2", "Commune2", "COMMUNE", np.nan, 16, "C"],
                ["C-3", "Commune3", "COMMUNE", 150, 16, np.nan],
            ],
        )
        # when
        df_stats = calculer_statistiques(df)
        # then
        df_attendu = pd.DataFrame(
            columns=[
                "categorie_territoire",
                "indicateur",
                "nb_lignes",
                "nb_uniques",
                "nb_nan",
                "part_nan",
                "somme",
                "moy",
                "min",
                "q1",
                "q2",
                "q3",
                "max",
            ],
            data=[
                ["PAYS", "ind1", 1, 1, 0, 0, 1, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                ["PAYS", "ind2", 1, 0, 1, 100, 0.0, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["PAYS", "ind3", 1, 1, 0, 0, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                ["COMMUNE", "ind1", 3, 2, 1, 33.0, 250.0, 125, 100, 112.5, 125, 137.5, 150],
                ["COMMUNE", "ind2", 3, 2, 0, 0, 45.0, 15, 13, 14.5, 16, 16, 16],
                ["COMMUNE", "ind3", 3, 2, 1, 33.0, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
            ],
        )
        pd.testing.assert_frame_equal(df_stats, df_attendu, check_dtype=False)

    def test_calculer_statistiques_indicateurs_par_annee(self):
        # given
        df = pd.DataFrame(
            columns=[
                "id_territoire",
                "nom_territoire",
                "categorie_territoire",
                "annee",
                "ind1",
                "ind2",
            ],
            data=[
                ["C-1", "Commune1", "COMMUNE", 2021, 100, 10],
                ["C-2", "Commune2", "COMMUNE", 2021, 55, 5],
                ["C-2", "Commune2", "COMMUNE", 2020, 500, 1],
            ],
        )
        # when
        df_stats = calculer_statistiques(df, "annee")
        df_attendu = pd.DataFrame(
            columns=[
                "categorie_territoire",
                "indicateur",
                "annee",
                "nb_lignes",
                "nb_uniques",
                "nb_nan",
                "part_nan",
                "somme",
                "moy",
                "min",
                "q1",
                "q2",
                "q3",
                "max",
            ],
            data=[
                ["COMMUNE", "ind1", 2021, 2, 2, 0, 0.0, 155, 77.5, 55, 66.25, 77.5, 88.75, 100],
                ["COMMUNE", "ind1", 2020, 1, 1, 0, 0.0, 500, 500, 500, 500, 500, 500, 500],
                ["COMMUNE", "ind2", 2021, 2, 2, 0, 0.0, 15, 7.5, 5, 6.25, 7.5, 8.75, 10],
                ["COMMUNE", "ind2", 2020, 1, 1, 0, 0.0, 1, 1, 1, 1, 1, 1, 1],
            ],
        )
        # then
        pd.testing.assert_frame_equal(df_stats.reset_index(drop=True), df_attendu.reset_index(drop=True), check_dtype=False)

    def test_filtrer_outliers(self):
        # given
        s = pd.Series([4, 100, 120, 140, 160, 200, 286])
        s_attendue = pd.Series([100, 120, 140, 160, 200])
        # when
        s_resultat = filtrer_outliers(s)
        # then
        pd.testing.assert_series_equal(s_resultat, s_attendue, check_index=False)

    def test_discretiser_indicateur_vers_classes_string(self):
        # given
        df = pd.DataFrame(columns=["indicateur"], data=[0, 2, 4, 1, 3, np.nan])

        # when
        df["message"] = discretiser_indicateur(
            df["indicateur"],
            [1, 3],
            ["< 1", ">= 1 et <3", ">= 3"],
            pd.StringDtype(),
            "Données non disponible",
        )

        # then
        resultat_attendu = pd.Series(
            data=[
                "< 1",
                ">= 1 et <3",
                ">= 3",
                ">= 1 et <3",
                ">= 3",
                "Données non disponible",
            ],
            dtype=pd.StringDtype(),
        )

        pd.testing.assert_series_equal(resultat_attendu, df["message"], check_names=False, check_dtype=True)

    def test_discretiser_indicateur_vers_classes_int64(self):
        # given
        df = pd.DataFrame(columns=["indicateur"], data=[0, 2, 4, 1, 3, np.nan])

        # when
        df["valeur_message"] = discretiser_indicateur(df["indicateur"], [1, 3], [0, 6, 10], pd.Int64Dtype(), pd.NA)

        # then
        resultat_attendu = pd.Series(data=[0, 6, 10, 6, 10, pd.NA], dtype=pd.Int64Dtype())

        pd.testing.assert_series_equal(resultat_attendu, df["valeur_message"], check_names=False, check_dtype=True)


if __name__ == "__main__":
    unittest.main()
