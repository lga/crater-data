import pandas as pd


def calculer_moyenne_glissante(df_indicateurs, nom_colonne_annee, taille_fenetre):
    resultat = (
        df_indicateurs.sort_index()
        .groupby("id_territoire", as_index=False)
        .rolling(taille_fenetre, min_periods=1)
        .mean()
        .drop(columns=["id_territoire"])
    )
    annees_a_conserver = (
        df_indicateurs.reset_index()[nom_colonne_annee]
        .drop_duplicates()
        .sort_values()
        .rolling(taille_fenetre)
        .max()
        .dropna()
        .astype("int64")
        .sort_values()
        .tolist()
    )
    ids_territoires = df_indicateurs.reset_index()["id_territoire"].drop_duplicates().tolist()
    index = pd.MultiIndex.from_product(
        [ids_territoires, annees_a_conserver],
        names=["id_territoire", nom_colonne_annee],
    )
    return pd.DataFrame(index=index).join(resultat)
