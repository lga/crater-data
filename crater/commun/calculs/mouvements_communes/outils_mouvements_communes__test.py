import unittest
from pathlib import Path

import pandas as pd

from crater.commun.calculs.mouvements_communes.outils_mouvements_communes import (
    calculer_donnee_communes_annee_cible,
    calculer_donnees_territoires_annee_cible,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas, assert_csv_files_are_equals

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestsUnitairesOutilsMouvementsCommunes(unittest.TestCase):

    def test_calculer_colonne_donnees_communes_annee_cible(
        self,
    ):
        # Given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        df_donnees_source_par_communes_annee_2020 = pd.read_csv(CHEMIN_INPUT_DATA / "donnees_communes_2020.csv", sep=";")
        df_territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")

        # when
        df_resultat = calculer_donnee_communes_annee_cible(
            df_territoires,
            2023,
            df_donnees_source_par_communes_annee_2020,
            "colonne_donnee_A",
            2020,
            CHEMIN_INPUT_DATA / "table_passage_annuelle_test_2024",
        )
        df_resultat.to_csv(CHEMIN_OUTPUT_DATA / "donnee_A_communes_2023.csv", sep=";", index=False)
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "donnee_A_communes_2023.csv",
            CHEMIN_OUTPUT_DATA / "donnee_A_communes_2023.csv",
        )

    def test_calculer_donnees_territoires_annee_cible(
        self,
    ):
        # Given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        df_donnees_source_par_communes_annee_2020 = pd.read_csv(CHEMIN_INPUT_DATA / "donnees_communes_2020.csv", sep=";")
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")

        # when
        df_resultat = calculer_donnees_territoires_annee_cible(
            territoires,
            2023,
            df_donnees_source_par_communes_annee_2020,
            ["colonne_donnee_A", "colonne_donnee_B"],
            2020,
            CHEMIN_INPUT_DATA / "table_passage_annuelle_test_2024",
        )
        df_resultat.to_csv(CHEMIN_OUTPUT_DATA / "donnees_2023.csv", sep=";", index=True)
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "donnees_2023.csv",
            CHEMIN_OUTPUT_DATA / "donnees_2023.csv",
        )


if __name__ == "__main__":
    unittest.main()
