import unittest
from pathlib import Path

import pandas as pd

from crater.commun.calculs.mouvements_communes.chargeur_mouvements_communes import charger_mouvements_communes
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestsUnitairesOutilsMouvementsCommunes(unittest.TestCase):
    def test_charger_mouvements_communes_csv_xls_identiques(
        self,
    ):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        df_matrice_mouvements_communes_csv = pd.read_csv(
            CHEMIN_INPUT_DATA / "table_passage_annuelle_test_2024" / "onglet_com.csv", sep=";", dtype=str, skiprows=5
        )

        # when
        df_matrice_mouvements_communes = charger_mouvements_communes(
            CHEMIN_INPUT_DATA / "table_passage_annuelle_test_2024" / "table_passage_annuelle_test_2024.xlsx"
        )

        # then
        pd.testing.assert_frame_equal(
            df_matrice_mouvements_communes.reset_index(drop=True), df_matrice_mouvements_communes_csv.reset_index(drop=True)
        )

    def test_charger_mouvements_communes_cache_ok(
        self,
    ):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        df_matrice_mouvements_communes = charger_mouvements_communes(
            CHEMIN_INPUT_DATA / "table_passage_annuelle_test_2024" / "table_passage_annuelle_test_2024.xlsx"
        )

        # when
        df_matrice_mouvements_communes_en_cache = charger_mouvements_communes(
            CHEMIN_INPUT_DATA / "table_passage_annuelle_test_2024" / "table_passage_annuelle_test_2024.xlsx"
        )

        # then
        pd.testing.assert_frame_equal(
            df_matrice_mouvements_communes.reset_index(drop=True), df_matrice_mouvements_communes_en_cache.reset_index(drop=True)
        )


if __name__ == "__main__":
    unittest.main()
