from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.logger import log

TYPES = {
    "ANNEE_MODIF": "Int64",
    "COM_FIN": "str",
    "COM_INI": "str",
    "LIB_COM_FIN": "str",
    "LIB_COM_INI": "str",
}

NOUVEAUX_NOMS_COLONNES = {
    "ANNEE_MODIF": "annee_modification",
    "COM_FIN": "id_commune_final",
    "COM_INI": "id_commune_initial",
    "LIB_COM_FIN": "nom_commune_final",
    "LIB_COM_INI": "nom_commune_initial",
}

NOMS_COLONNES = [
    "annee_modification",
    "id_commune_final",
    "id_commune_initial",
    "nom_commune_final",
    "nom_commune_initial",
]


def charger_fusions_scissions_communes(chemin_fichier: Path) -> DataFrame:
    return pd.concat(
        [
            charger_fusions_communes(chemin_fichier).assign(type="fusion"),
            charger_scissions_communes(chemin_fichier).assign(type="scission"),
        ]
    ).sort_values(by=["annee_modification"])


def charger_fusions_communes(chemin_fichier: Path) -> DataFrame:
    log.info("   => chargement fusions communes depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="Liste des fusions",
            usecols="A:E",
            skiprows=5,
            dtype=TYPES,
            na_values=[""],
        )
        .rename(columns=NOUVEAUX_NOMS_COLONNES, errors="raise")
        .reindex(columns=NOMS_COLONNES)
    )

    df = _traduire_code_insee_vers_id_crater(df)

    return df


def charger_scissions_communes(chemin_fichier: Path) -> DataFrame:
    log.info("   => chargement scissions communes depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="Liste des scissions",
            usecols="A:E",
            skiprows=5,
            dtype=TYPES,
            na_values=[""],
        )
        .rename(columns=NOUVEAUX_NOMS_COLONNES, errors="raise")
        .reindex(columns=NOMS_COLONNES)
    )

    df = _traduire_code_insee_vers_id_crater(df)

    return df


DF_MATRICE_MOUVEMENTS_COMMUNES_CACHE: DataFrame | None = None
CHEMIN_FICHIER_OU_DOSSIER_MOUVEMENTS_COMMUNES_CACHE: Path | None = None


def charger_mouvements_communes(chemin_fichier_ou_dossier: Path) -> DataFrame:
    log.info("   => chargement des mouvements communes depuis %s", chemin_fichier_ou_dossier)

    global DF_MATRICE_MOUVEMENTS_COMMUNES_CACHE
    global CHEMIN_FICHIER_OU_DOSSIER_MOUVEMENTS_COMMUNES_CACHE

    if (DF_MATRICE_MOUVEMENTS_COMMUNES_CACHE is None) or (
        DF_MATRICE_MOUVEMENTS_COMMUNES_CACHE is not None and CHEMIN_FICHIER_OU_DOSSIER_MOUVEMENTS_COMMUNES_CACHE != chemin_fichier_ou_dossier
    ):
        CHEMIN_FICHIER_OU_DOSSIER_MOUVEMENTS_COMMUNES_CACHE = chemin_fichier_ou_dossier
        if chemin_fichier_ou_dossier.suffix == ".xlsx":
            DF_MATRICE_MOUVEMENTS_COMMUNES_CACHE = pd.read_excel(
                chemin_fichier_ou_dossier,
                sheet_name="COM",
                skiprows=5,
                dtype="str",
                na_values=[""],
            )
        # Pour les tests on utilise un fichier csv pour chaque onglet, temps de chargement bcp plus rapide
        else:
            DF_MATRICE_MOUVEMENTS_COMMUNES_CACHE = pd.read_csv(
                chemin_fichier_ou_dossier / "onglet_com.csv",
                sep=";",
                skiprows=5,
                dtype="str",
            )
    else:
        log.info("     - les mouvements communes ont déjà été chargés, réutilisation des données en cache")

    return DF_MATRICE_MOUVEMENTS_COMMUNES_CACHE


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune_final = traduire_code_insee_vers_id_commune_crater(df.id_commune_final)
    df.id_commune_initial = traduire_code_insee_vers_id_commune_crater(df.id_commune_initial)
    return df
