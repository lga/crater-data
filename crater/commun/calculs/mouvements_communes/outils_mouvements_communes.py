import math
from pathlib import Path

import numpy as np
import pandas
from pandas import DataFrame

from crater.commun.calculs.mouvements_communes.chargeur_mouvements_communes import charger_mouvements_communes
from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
)


def calculer_donnee_communes_annee_cible(
    referentiel_territoires: DataFrame,
    annee_cible: int,
    donnees_source_communes: DataFrame,
    nom_colonne_donnee_source: str,
    annee_source: int,
    fichier_source_mouvements_communes: Path,
) -> DataFrame:
    df_mouvements = _creer_df_synthese_mouvements(annee_source, annee_cible, fichier_source_mouvements_communes)

    df_donnee_source_et_mouvements = _fusionner_df_donnees_et_mouvements(donnees_source_communes, df_mouvements)

    df_donnee_annee_cible = _calculer_donnee_commune_annee_cible(df_donnee_source_et_mouvements, nom_colonne_donnee_source)

    df_donnee_annee_cible = _ajouter_colonne_est_estime(df_donnee_annee_cible, nom_colonne_donnee_source)

    df_donnee_annee_cible = _aligner_donnee_sur_referentiel_communes(referentiel_territoires, df_donnee_annee_cible)

    _verifier_conservation_total(df_donnee_annee_cible, donnees_source_communes, nom_colonne_donnee_source)

    return df_donnee_annee_cible


def _creer_df_synthese_mouvements(annee_origine, annee_cible, fichier_source_mouvements_communes):
    df_mouvements_communes = charger_mouvements_communes(fichier_source_mouvements_communes)
    if annee_cible == annee_origine:
        df_mouvements_communes = df_mouvements_communes.loc[df_mouvements_communes["NIVGEO"] == "COM", ["NIVGEO", f"CODGEO_{annee_cible}"]].drop(
            columns=["NIVGEO"]
        )
    else:
        df_mouvements_communes = df_mouvements_communes.loc[
            df_mouvements_communes["NIVGEO"] == "COM", ["NIVGEO", f"CODGEO_{annee_cible}", f"CODGEO_{annee_origine}"]
        ].drop(columns=["NIVGEO"])
    df_mouvements_communes["id_commune"] = traduire_code_insee_vers_id_commune_crater(df_mouvements_communes[f"CODGEO_{annee_cible}"])
    df_mouvements_communes["id_commune_annee_origine"] = traduire_code_insee_vers_id_commune_crater(df_mouvements_communes[f"CODGEO_{annee_origine}"])
    df_mouvements_communes = df_mouvements_communes.drop_duplicates()  # Les fusions avant l'année origine provoquent des doublons
    df_mouvements_communes = pandas.merge(
        df_mouvements_communes.loc[:, ("id_commune", "id_commune_annee_origine")]
        .groupby("id_commune_annee_origine")
        .count()
        .rename(columns={"id_commune": "nb_communes_apres_scission"}),
        df_mouvements_communes,
        on="id_commune_annee_origine",
    )
    df_mouvements_communes.loc[df_mouvements_communes.nb_communes_apres_scission == 1, "nb_communes_apres_scission"] = np.nan
    return df_mouvements_communes.set_index("id_commune")


def _fusionner_df_donnees_et_mouvements(df_indicateurs_par_communes_annee_origine, df_mouvements):
    df_indicateurs_par_communes_annee_origine = df_indicateurs_par_communes_annee_origine.rename(columns={"id_commune": "id_commune_annee_origine"})
    df_indicateurs_annee_cible = (
        pandas.merge(
            df_mouvements.reset_index(),
            df_indicateurs_par_communes_annee_origine,
            left_on="id_commune_annee_origine",
            right_on="id_commune_annee_origine",
            how="left",
        )
    ).set_index("id_commune")
    return df_indicateurs_annee_cible


def _calculer_donnee_commune_annee_cible(df_indicateur, nom_colonne_indicateur):
    df_indicateur[nom_colonne_indicateur] = df_indicateur[nom_colonne_indicateur] / df_indicateur["nb_communes_apres_scission"].fillna(1)
    df_indicateur["nb_indicateurs_nan_avant_fusion"] = df_indicateur[nom_colonne_indicateur].isna().astype(int)
    df_indicateur["nb_communes_avant_fusion"] = 1
    df_indicateur = df_indicateur.loc[
        :,
        [
            nom_colonne_indicateur,
            "nb_communes_apres_scission",
            "nb_communes_avant_fusion",
            "nb_indicateurs_nan_avant_fusion",
        ],
    ]

    def somme_avec_nan(series):
        if series.isna().all():
            return np.nan
        else:
            return series.sum()

    df_indicateur = df_indicateur.groupby(level=0).agg(somme_avec_nan).reset_index()
    df_indicateur.loc[df_indicateur.nb_communes_avant_fusion == 1, "nb_communes_avant_fusion"] = np.nan
    return df_indicateur.drop_duplicates()


def _ajouter_colonne_est_estime(df_indicateur, nom_colonne_indicateur):
    df_indicateur["est_estime_suite_fusion"] = (
        (df_indicateur["nb_communes_avant_fusion"] > 1)
        & (df_indicateur["nb_indicateurs_nan_avant_fusion"] > 0)
        & (df_indicateur["nb_communes_avant_fusion"] != df_indicateur["nb_indicateurs_nan_avant_fusion"])
    )
    df_indicateur["est_estime_suite_scission"] = (df_indicateur["nb_communes_apres_scission"] > 1) & (~df_indicateur[nom_colonne_indicateur].isna())
    df_indicateur[f"{nom_colonne_indicateur}_est_estime"] = df_indicateur["est_estime_suite_fusion"] | df_indicateur["est_estime_suite_scission"]
    return df_indicateur.loc[:, ["id_commune", nom_colonne_indicateur, f"{nom_colonne_indicateur}_est_estime"]]


def _aligner_donnee_sur_referentiel_communes(referentiel_territoires, df_donnee_annee_cible):
    return pandas.merge(
        referentiel_territoires.loc[referentiel_territoires["categorie_territoire"] == "COMMUNE", []].copy().reset_index(),
        df_donnee_annee_cible,
        how="left",
        left_on="id_territoire",
        right_on="id_commune",
    ).drop(columns=["id_territoire"])


def _verifier_conservation_total(df_indicateur, donnees_source_communes, nom_colonne_donnees_source):
    somme_communes_annee_origine = donnees_source_communes[nom_colonne_donnees_source].sum()
    somme_communes_annee_cible = df_indicateur[nom_colonne_donnees_source].sum()
    difference = somme_communes_annee_origine - somme_communes_annee_cible
    if not math.isclose(somme_communes_annee_origine, somme_communes_annee_cible, abs_tol=1e-3):
        raise ValueError(
            f"Erreur lors de l'application des mouvements communes pour la colonne {nom_colonne_donnees_source} : "
            f"la somme sur toutes les communes annee origine ({somme_communes_annee_origine}) est différente de la somme année "
            f"cible ({somme_communes_annee_cible}), différence = {difference} ("
            f"{(100 * (somme_communes_annee_origine/somme_communes_annee_cible - 1)).round(2)}%)"
        )


def calculer_donnees_territoires_annee_cible(
    referentiel_territoires: DataFrame,
    annee_cible: int,
    df_donnees_communes,
    noms_colonnes,
    annee_origine: int,
    chemin_fichier_source_mouvements_communes,
):

    df_donnees_territoires_annee_cible = referentiel_territoires.copy().reset_index()
    for colonne in noms_colonnes:
        df_donnee_communes_annee_cible = calculer_donnee_communes_annee_cible(
            referentiel_territoires,
            annee_cible,
            df_donnees_communes,
            colonne,
            annee_origine,
            chemin_fichier_source_mouvements_communes,
        )
        df_donnees_territoires_annee_cible = pandas.merge(
            df_donnees_territoires_annee_cible,
            df_donnee_communes_annee_cible,
            how="left",
            left_on="id_territoire",
            right_on="id_commune",
        ).drop(columns=["id_commune"])
        df_donnees_territoires_annee_cible = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            df_donnees_territoires_annee_cible.set_index("id_territoire"), colonne, "COMMUNE"
        ).reset_index()

    df_donnees_territoires_annee_cible = df_donnees_territoires_annee_cible.set_index("id_territoire")
    df_donnees_territoires_annee_cible = df_donnees_territoires_annee_cible.loc[
        :,
        ["nom_territoire", "categorie_territoire"]
        + [col for col in df_donnees_territoires_annee_cible.columns if any(col.startswith(prefix) for prefix in noms_colonnes)],
    ]
    return df_donnees_territoires_annee_cible
