from pandas import Series


def calculer_note_par_interpolation_2_seuils(
    indicateur: Series,
    seuil1: float,
    note_seuil1: float,
    seuil2: float,
    note_seuil2: float,
) -> Series:
    coefficient_directeur = (note_seuil2 - note_seuil1) / (seuil2 - seuil1)

    note: Series = (indicateur - seuil1) * coefficient_directeur + note_seuil1
    note = note.clip(lower=0, upper=10).round(2)
    return note


def calculer_note_par_interpolation_n_seuils(indicateur: Series, seuils: list[float], notes_seuils: list[float]) -> Series:
    if len(seuils) != len(notes_seuils):
        raise ValueError(f"Les listes ne font pas la même taille : {seuils} vs {notes_seuils} !")
    if sorted(seuils) != seuils:
        raise ValueError(f"La liste des seuils n'est pas ordonnée de façon croissante : {seuils} !")
    if sorted(notes_seuils) != notes_seuils and sorted(notes_seuils, reverse=True) != notes_seuils:
        raise ValueError(f"La liste des notes des seuils n'est pas ordonnée : {notes_seuils} !")

    notes: Series = calculer_note_par_interpolation_2_seuils(indicateur, seuils[0], notes_seuils[0], seuils[1], notes_seuils[1])
    for i in range(1, len(seuils) - 1):
        notes = notes.mask(
            indicateur > seuils[i],
            calculer_note_par_interpolation_2_seuils(indicateur, seuils[i], notes_seuils[i], seuils[i + 1], notes_seuils[i + 1]),
        )
    notes = notes.clip(lower=0, upper=10).round(2).astype("float64")

    return notes
