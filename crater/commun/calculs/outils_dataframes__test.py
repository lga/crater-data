import unittest

import numpy as np
import pandas as pd

from crater.commun.calculs.outils_dataframes import (
    normaliser_chaine_caractere,
    produit_cartesien,
    update_colonne_depuis_series_incluant_NAs,
    merge_strict,
    intervertir_colonne_index,
)
from crater.commun.outils_pour_tests import assert_error_message_raised


class TestsUnitairesOutilsDataFrame(unittest.TestCase):
    def test_normaliser_chaine_caractere(self):
        # given
        df = pd.Series(["l'nom d'COMMUNE, avec des é à Ö", "Le pat du nord de la france", "  commune/abc.def,gh/ij-kl  "])
        # when
        df_resultat = normaliser_chaine_caractere(df)
        # then
        df_resultat_attendu = pd.Series(["l-nom-d-commune-avec-des-e-a-o", "le-pat-du-nord-de-la-france", "commune-abc-def-gh-ij-kl"])
        pd.testing.assert_series_equal(df_resultat, df_resultat_attendu)

    def test_produit_cartesien(self):
        # given
        df1 = pd.DataFrame({"col1": [1, np.nan]})
        df2 = pd.DataFrame({"col2": ["a", np.nan]})
        # when
        df_resultat = produit_cartesien(df1, df2)
        # then
        df_resultat_attendu = pd.DataFrame({"col1": [1, 1, np.nan, np.nan], "col2": ["a", np.nan, "a", np.nan]})
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)

    def test_update_colonne_depuis_series_incluant_NAs(self):
        # given
        pd.DataFrame()
        serie_source = pd.Series(name="col1", data=[np.nan, 9])
        df_cible = pd.DataFrame({"col1": [1, np.nan], "col2": [999, 999]})
        # when
        df_resultat = update_colonne_depuis_series_incluant_NAs(df_cible, "col1", serie_source)
        # then
        df_resultat_attendu = pd.DataFrame({"col1": [np.nan, 9], "col2": [999, 999]})
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)


class TestsUnitairesOutilsDataFrameMergeStrict(unittest.TestCase):
    def test_merge_strict_inner_ok(self):
        # given
        df1 = pd.DataFrame({"id1": [1, 2], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id2": [1, 2], "data2": ["x", "y"]})
        # then
        assert merge_strict(df1, df2, left_on="id1", right_on="id2").equals(pd.merge(df1, df2, left_on="id1", right_on="id2"))

        # given
        df1 = pd.DataFrame({"id1": [1, 2], "id2": [-1, -1], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id3": [1, 2], "id4": [-1, -1], "data2": ["x", "y"]})
        # then
        assert merge_strict(df1, df2, left_on=["id1", "id2"], right_on=["id3", "id4"]).equals(
            pd.merge(df1, df2, left_on=["id1", "id2"], right_on=["id3", "id4"])
        )

    def test_merge_strict_inner_ko_parametres_on_absents(self):
        # given
        df1 = pd.DataFrame({"id1": [1, 2], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id2": [1, 2], "data2": ["x", "y"]})
        # then
        assert_error_message_raised(
            self,
            "ERREUR merge_strict : paramètres 'left_on' et 'right_on' absents alors que le paramètre 'on' est absent",
            merge_strict,
            df1=df1,
            df2=df2,
        )

    def test_merge_strict_inner_ko_colonnes_hors_jointure_overlap(self):
        # given
        df1 = pd.DataFrame({"id1": [1, 2], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id2": [1, 2], "data1": ["c", "d"]})
        # then
        assert_error_message_raised(
            self,
            "columns overlap but no suffix specified: Index(['data1'], dtype='object')",
            merge_strict,
            df1=df1,
            df2=df2,
            left_on="id1",
            right_on="id2",
        )

    def test_merge_strict_inner_ko_ids_df1_non_uniques(self):
        # df1 a des ids en double => erreur
        # given
        df1 = pd.DataFrame({"id1": [2, 2], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id2": [1, 2], "data2": ["x", "y"]})
        # then
        assert_error_message_raised(
            self, "ERREUR merge_strict : colonnes 'on' du df1 avec doublons", merge_strict, df1=df1, df2=df2, left_on="id1", right_on="id2"
        )

    def test_merge_strict_inner_ko_ids_df2_non_uniques(self):
        # given
        df1 = pd.DataFrame({"id1": [1, 2], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id2": [2, 2], "data2": ["x", "y"]})
        # then
        assert_error_message_raised(
            self, "ERREUR merge_strict : colonnes 'on' du df2 avec doublons", merge_strict, df1=df1, df2=df2, left_on="id1", right_on="id2"
        )

    def test_merge_strict_inner_ids_df1_df2_differents(self):
        # given
        df1 = pd.DataFrame({"id1": [1, 2], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id2": [1, 3], "data2": ["x", "y"]})
        # then
        assert merge_strict(df1, df2, left_on="id1", right_on="id2", correspondance="lache").equals(pd.merge(df1, df2, left_on="id1", right_on="id2"))
        assert_error_message_raised(
            self,
            "ERREUR merge_strict : colonnes 'on' du df1 != colonnes 'on' du df2",
            merge_strict,
            df1=df1,
            df2=df2,
            left_on="id1",
            right_on="id2",
        )

        # given
        df1 = pd.DataFrame({"id1": [1, 2], "id2": [-1, -2], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id3": [1, 2], "id4": [-1, -3], "data2": ["x", "y"]})
        # then
        assert merge_strict(df1, df2, left_on=["id1", "id2"], right_on=["id3", "id4"], correspondance="lache").equals(
            pd.merge(df1, df2, left_on=["id1", "id2"], right_on=["id3", "id4"])
        )
        assert_error_message_raised(
            self,
            "ERREUR merge_strict : colonnes 'on' du df1 != colonnes 'on' du df2",
            merge_strict,
            df1=df1,
            df2=df2,
            left_on=["id1", "id2"],
            right_on=["id3", "id4"],
        )

    def test_merge_strict_left_ok(self):
        # given
        df1 = pd.DataFrame({"id1": [1, 2, 2], "data1": ["a", "b", "c"]})
        df2 = pd.DataFrame({"id2": [1, 2], "data2": ["x", "y"]})
        # then
        assert merge_strict(df1, df2, left_on="id1", right_on="id2", how="left").equals(pd.merge(df1, df2, how="left", left_on="id1", right_on="id2"))

    def test_merge_strict_left_ko_id2_non_uniques(self):
        # given
        df1 = pd.DataFrame({"id1": [1], "data1": ["a"]})
        df2 = pd.DataFrame({"id2": [1, 1], "data2": ["x", "y"]})
        # then
        assert_error_message_raised(
            self,
            "ERREUR merge_strict : colonnes 'on' du df2 avec doublons",
            merge_strict,
            df1=df1,
            df2=df2,
            left_on="id1",
            right_on="id2",
            how="left",
        )

    def test_merge_strict_left_ids_df2_manquants(self):
        # given
        df1 = pd.DataFrame({"id1": [1, 2], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id2": [1], "data2": ["x"]})
        # then
        assert merge_strict(df1, df2, left_on="id1", right_on="id2", how="left", correspondance="lache").equals(
            pd.merge(df1, df2, left_on="id1", right_on="id2", how="left")
        )
        assert_error_message_raised(
            self,
            "ERREUR merge_strict : des valeurs des colonnes 'on' du df1 sont absentes de df2",
            merge_strict,
            df1=df1,
            df2=df2,
            left_on="id1",
            right_on="id2",
            how="left",
        )
        assert_error_message_raised(
            self,
            "ERREUR merge_strict : les valeurs des colonnes 'on' ne sont pas identiques",
            merge_strict,
            df1=df1,
            df2=df2,
            left_on="id1",
            right_on="id2",
            how="left",
            correspondance="exacte",
        )

    def test_merge_strict_left_ids_df2_en_trop(self):
        # given
        df1 = pd.DataFrame({"id1": [1, 2], "data1": ["a", "b"]})
        df2 = pd.DataFrame({"id2": [1, 2, 3], "data2": ["x", "y", "z"]})
        # then
        assert merge_strict(df1, df2, left_on="id1", right_on="id2", how="left", correspondance="lache").equals(
            pd.merge(df1, df2, how="left", left_on="id1", right_on="id2")
        )
        assert merge_strict(df1, df2, left_on="id1", right_on="id2", how="left").equals(pd.merge(df1, df2, how="left", left_on="id1", right_on="id2"))
        assert_error_message_raised(
            self,
            "ERREUR merge_strict : les valeurs des colonnes 'on' ne sont pas identiques",
            merge_strict,
            df1=df1,
            df2=df2,
            left_on="id1",
            right_on="id2",
            how="left",
            correspondance="exacte",
        )

    def test_inverser_categories_territoire_dans_index(self):
        # given
        df = pd.DataFrame(
            data=[
                ["C-1", "E-1", 2020, "janvier", 10],
                ["C-2", "E-1", 2020, "janvier", 10],
                ["C-3", "E-2", 2020, "janvier", 10],
            ],
            columns={"id_commune": "str", "id_epci": "str", "annee": "str", "mois": "str", "valeur": int},
        ).set_index(["id_commune", "annee", "mois"])
        df_attendu = pd.DataFrame(
            data=[
                ["E-1", "C-1", 2020, "janvier", 10],
                ["E-1", "C-2", 2020, "janvier", 10],
                ["E-2", "C-3", 2020, "janvier", 10],
            ],
            columns={"id_epci": "str", "id_commune": "str", "annee": "str", "mois": "str", "valeur": int},
        ).set_index(["id_epci", "annee", "mois"])
        # when
        df_final = intervertir_colonne_index(df, "id_commune", "id_epci")
        # then
        pd.testing.assert_frame_equal(df_final, df_attendu)


if __name__ == "__main__":
    unittest.main()
