from typing import TypeAlias
from unidecode import unidecode
import pandas
from pandas import DataFrame, Series
from crater.commun.logger import log

DictDataFrames: TypeAlias = dict[str, pandas.DataFrame]


def normaliser_chaine_caractere(serie: Series) -> Series:
    return (
        (" " + serie + " ")
        .str.lower()
        .apply(unidecode)
        .replace(" \(", "_(", regex=True)
        .replace("^\s+|\s+$", "", regex=True)  # supprimer espaces en début et fin de chaine
        .replace("(\.|-|,|/|'|\s+)+", "-", regex=True)  # remplacer tous les caractères non supportés par -
    )


def produit_cartesien(df_1: DataFrame, df_2: DataFrame) -> DataFrame:
    return df_1.assign(key=1).merge(df_2.assign(key=1), on="key").drop("key", axis=1)


# Mettre a jour une colonne depuis les valeurs d'une séries, en copiant également les NAs (ce que ne fait pas Dataframe.update)
def update_colonne_depuis_series_incluant_NAs(df_a_modifier: DataFrame, nom_colonne: str, serie_source: Series):
    df_a_modifier = df_a_modifier.join(serie_source.rename(nom_colonne), rsuffix="_new")
    df_a_modifier.loc[serie_source.index, nom_colonne] = df_a_modifier.loc[serie_source.index, nom_colonne + "_new"]
    df_a_modifier = df_a_modifier.drop(columns=nom_colonne + "_new")
    return df_a_modifier


def merge_strict(df1, df2, how="inner", on=None, left_on=None, right_on=None, correspondance="suffisante"):
    if how not in ["inner", "left"]:
        raise ValueError("ERREUR merge_strict : paramètre 'how' incorrect")

    if correspondance not in ["exacte", "suffisante", "lache"]:
        raise ValueError("ERREUR merge_strict : paramètre 'correspondance' incorrect")

    if on and (left_on or right_on):
        raise ValueError("ERREUR merge_strict : paramètres 'left_on' et/ou 'right_on' donné(s) alors que le paramètre 'on' est donné")
    elif not on and not (left_on and right_on):
        raise ValueError("ERREUR merge_strict : paramètres 'left_on' et 'right_on' absents alors que le paramètre 'on' est absent")

    if on:
        on1 = on
        on2 = on
    else:
        on1 = left_on
        on2 = right_on

    id1 = pandas.DataFrame(df1.loc[:, on1]).sort_values(by=on1).reset_index(drop=True)
    id2 = pandas.DataFrame(df2.loc[:, on2]).sort_values(by=on2).reset_index(drop=True)
    if isinstance(on1, str):
        id2 = id2.set_axis([on1], axis=1)
    else:
        id2 = id2.set_axis(on1, axis=1)

    if how == "inner":
        if not id1.equals(id1.drop_duplicates()):
            log.error(f"ERREUR merge_strict : colonnes 'on' du df1 avec doublons : {id1[id1.duplicated()]}")
            raise ValueError("ERREUR merge_strict : colonnes 'on' du df1 avec doublons")
        if not id2.equals(id2.drop_duplicates()):
            log.error(f"ERREUR merge_strict : colonnes 'on' du df2 avec doublons : {str(id2[id2.duplicated()])}")
            raise ValueError("ERREUR merge_strict : colonnes 'on' du df2 avec doublons")
        if not id1.equals(id2) and correspondance in ["suffisante", "exacte"]:
            log.error(
                f"ERREUR merge_strict : colonnes 'on' du df1 != colonnes 'on' du df2 : "
                f"{sorted(set(df1.loc[:, on1]) - set(df2.loc[:, on2]))} != "
                f"{str(sorted(set(df2.loc[:, on2]) - set(df1.loc[:, on1])))}"
            )
            raise ValueError("ERREUR merge_strict : colonnes 'on' du df1 != colonnes 'on' du df2")

    if how == "left":
        if not id2.equals(id2.drop_duplicates()):
            log.error(f"ERREUR merge_strict : colonnes 'on' du df2 avec doublons : {id2[id2.duplicated()]}")
            raise ValueError("ERREUR merge_strict : colonnes 'on' du df2 avec doublons")
        if not id1.merge(id2).equals(id1) and correspondance == "suffisante":
            log.error(
                f"ERREUR merge_strict : des valeurs des colonnes 'on' du df1 sont absentes de df2 : "
                f"{str(sorted(set(df1.loc[:, on1]) - set(df2.loc[:, on2])))}"
            )
            raise ValueError("ERREUR merge_strict : des valeurs des colonnes 'on' du df1 sont absentes de df2")
        if not id1.equals(id2) and correspondance == "exacte":
            log.error(
                f"ERREUR merge_strict : les valeurs des colonnes 'on' ne sont pas identiques : "
                f"{str(sorted(set(df1.loc[:, on1]) - set(df2.loc[:, on2])))} != "
                f"{str(sorted(set(df2.loc[:, on2]) - set(df1.loc[:, on1])))}"
            )
            raise ValueError("ERREUR merge_strict : les valeurs des colonnes 'on' ne sont pas identiques")

    return pandas.merge(df1, df2, how=how, left_on=on1, right_on=on2, suffixes=(False, False))


def intervertir_colonne_index(df: DataFrame, nom_colonne_pour_reset_index: str, nom_colonne_pour_set_index: str) -> DataFrame:
    df = df.copy().set_index(nom_colonne_pour_set_index, append=True).reset_index(nom_colonne_pour_reset_index)
    if df.index.nlevels > 1:
        df = df.reorder_levels([df.index.nlevels - 1] + list(range(df.index.nlevels - 1)))
    return df
