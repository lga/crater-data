import numpy as np
import pandas
import pandas as pd
from pandas import DataFrame, Series

from crater.config.config_globale import CATEGORIES_TERRITOIRES


def calculer_statistiques(df: DataFrame, nom_colonne_regroupement=None):
    df_stats = _creer_df_statistiques(df, nom_colonne_regroupement)
    df_stats = _calculer_nouvelles_mesures_statistiques(df_stats)
    df_stats = _traiter_colonne_regroupement(df_stats, nom_colonne_regroupement)
    df_stats = _transposer_et_trier(df_stats, nom_colonne_regroupement)
    return df_stats


def _creer_df_statistiques(df_indicateurs, nom_colonne_regroupement):
    nom_colonnes_groupby = (
        (["categorie_territoire"] + [nom_colonne_regroupement]) if (nom_colonne_regroupement is not None) else ["categorie_territoire"]
    )
    df_indicateurs["colonne_nombre"] = 1
    df_indicateurs["colonne_string"] = "string"
    noms_colonnes_indicateurs_numeriques = df_indicateurs.drop(columns=nom_colonnes_groupby).select_dtypes("number").columns.values.tolist()
    noms_colonnes_indicateurs_non_numeriques = df_indicateurs.drop(
        columns=nom_colonnes_groupby + noms_colonnes_indicateurs_numeriques + ["id_territoire", "nom_territoire", "categorie_territoire"]
    ).columns.values.tolist()
    df_indicateurs_numeriques = df_indicateurs.reset_index().loc[:, nom_colonnes_groupby + noms_colonnes_indicateurs_numeriques]
    df_indicateurs_non_numeriques = df_indicateurs.reset_index().loc[:, nom_colonnes_groupby + noms_colonnes_indicateurs_non_numeriques]
    df_stats_indicateurs_numeriques = (
        df_indicateurs_numeriques.groupby(nom_colonnes_groupby, dropna=False)
        .agg(["size", "count", "nunique", "sum", "mean", "min", q1, "median", q3, "max"])
        .copy()
    )
    df_stats_indicateurs_non_numeriques = (
        df_indicateurs_non_numeriques.groupby(nom_colonnes_groupby, dropna=False).agg(["size", "count", "nunique"]).copy()
    )
    df_stats = pd.merge(df_stats_indicateurs_numeriques, df_stats_indicateurs_non_numeriques, left_index=True, right_index=True, how="outer")
    df_stats.rename(columns={"sum": "somme", "nunique": "nb_uniques", "mean": "moy", "median": "q2"}, inplace=True)
    df_stats.columns.set_names(["indicateur", "mesure_statistique"], inplace=True)
    return df_stats


def _calculer_nouvelles_mesures_statistiques(df_stats):
    for nom_colonne_indicateur in df_stats.columns.levels[0]:
        df_stats[nom_colonne_indicateur, "count"] = df_stats[nom_colonne_indicateur, "size"] - df_stats[nom_colonne_indicateur, "count"]
        df_stats[nom_colonne_indicateur, "part_nan"] = round(
            (df_stats[nom_colonne_indicateur, "count"] / df_stats[nom_colonne_indicateur, "size"]) * 100
        )
    df_stats = df_stats.rename(columns={"size": "nb_lignes", "count": "nb_nan"})
    return df_stats


def _traiter_colonne_regroupement(df_stats, nom_colonne_regroupement):
    if nom_colonne_regroupement is not None:
        df_stats = df_stats.reset_index().pivot(index="categorie_territoire", columns=nom_colonne_regroupement)
    return df_stats


def _transposer_et_trier(df_stats, nom_colonne_regroupement):
    nom_colonnes_stack = ["indicateur"] if (nom_colonne_regroupement is None) else ["indicateur", nom_colonne_regroupement]
    df_stats = (
        df_stats.stack(nom_colonnes_stack, future_stack=True)  # future_stack=True sinon le stack plante depuis l'upgrade pandas en 2.2.3
        .reset_index()
        .loc[
            :,
            ["categorie_territoire"]
            + nom_colonnes_stack
            + ["nb_lignes", "nb_uniques", "nb_nan", "part_nan", "somme", "moy", "min", "q1", "q2", "q3", "max"],
        ]
    )
    df_stats["nb_uniques"] = pd.to_numeric(df_stats["nb_uniques"])
    df_stats["part_nan"] = pd.to_numeric(df_stats["part_nan"])
    df_stats["somme"] = pd.to_numeric(df_stats["somme"])
    df_stats["moy"] = pd.to_numeric(df_stats["moy"])
    df_stats["min"] = pd.to_numeric(df_stats["min"])
    df_stats["q1"] = pd.to_numeric(df_stats["q1"])
    df_stats["q2"] = pd.to_numeric(df_stats["q2"])
    df_stats["q3"] = pd.to_numeric(df_stats["q3"])
    df_stats["max"] = pd.to_numeric(df_stats["max"])

    df_stats.columns.name = None

    df_stats["categorie_territoire_pour_tri"] = pandas.Categorical(
        df_stats["categorie_territoire"],
        categories=CATEGORIES_TERRITOIRES[::-1],
        ordered=True,
    )
    colonnes_tri = ["categorie_territoire_pour_tri", "indicateur"]
    sens_tri = [True, True]
    if nom_colonne_regroupement is not None:
        colonnes_tri = colonnes_tri + [nom_colonne_regroupement]
        sens_tri = sens_tri + [False]

    df_stats = (
        df_stats.loc[~df_stats.indicateur.isin(["colonne_nombre", "colonne_string"])]
        .sort_values(by=colonnes_tri, ascending=sens_tri)
        .drop(columns="categorie_territoire_pour_tri")
        .reset_index(drop=True)
    )
    return df_stats


def q1(x):
    return x.quantile(0.25)


def q3(x):
    return x.quantile(0.75)


def filtrer_outliers(s: Series) -> Series:
    q1, q3 = s.quantile([0.25, 0.75]).tolist()
    inter_quartile_range = q3 - q1
    borne_inf = q1 - 1.5 * inter_quartile_range
    borne_sup = q3 + 1.5 * inter_quartile_range
    return s[(s > borne_inf) & (s < borne_sup)]


def discretiser_indicateur(
    indicateur: Series,
    liste_seuils,
    liste_valeurs_classes,
    type_classes,
    valeur_si_na,
):
    valeurs_discretisees = pd.cut(
        indicateur,
        bins=[-np.inf, *liste_seuils, np.inf],
        labels=liste_valeurs_classes,
        right=False,
        include_lowest=True,
        ordered=False,
    )
    return valeurs_discretisees.astype(type_classes).fillna(valeur_si_na)
