import unittest

import numpy as np
import pandas as pd

from crater.commun.calculs.notes_et_messages import (
    calculer_note_par_interpolation_2_seuils,
    calculer_note_par_interpolation_n_seuils,
)
from crater.commun.outils_pour_tests import assert_error_message_raised


class TestsUnitairesNotesEtMessages(unittest.TestCase):
    def test_calculer_note_par_interpolation_2_seuils_cas_coef_directeur_positif(self):
        df = pd.DataFrame(
            columns=["indicateur"],
            data=[
                [-1],
                [6],
                [8],
                [12],
                [17],
                [np.nan],
            ],
        )
        # given
        df_attendu = pd.DataFrame(
            columns=["note"],
            data=[
                [0],
                [3.75],
                [5],
                [7.5],
                [10],
                [np.nan],
            ],
        )
        # when
        df.loc[:, "note"] = calculer_note_par_interpolation_2_seuils(df["indicateur"], 0, 0, 8, 5)
        # then
        pd.testing.assert_series_equal(df["note"], df_attendu["note"], check_names=False)

    def test_calculer_note_par_interpolation_2_seuils_cas_coef_directeur_negatif(self):
        df = pd.DataFrame(
            columns=["indicateur"],
            data=[
                [9],
                [8],
                [4],
                [0],
                [-1],
                [np.nan],
            ],
        )
        # given
        df_attendu = pd.DataFrame(
            columns=["note"],
            data=[
                [0],
                [0],
                [5],
                [10],
                [10],
                [np.nan],
            ],
        )
        # when
        df.loc[:, "note"] = calculer_note_par_interpolation_2_seuils(df["indicateur"], 0, 10, 8, 0)
        df.loc[:, "note_bis"] = calculer_note_par_interpolation_2_seuils(df["indicateur"], 8, 0, 0, 10)

        # then
        pd.testing.assert_series_equal(df["note"], df_attendu["note"], check_names=False)
        pd.testing.assert_series_equal(df["note_bis"], df_attendu["note"], check_names=False)

    def test_calculer_note_par_interpolation_n_seuils_KO(self):
        assert_error_message_raised(
            self,
            "Les listes ne font pas la même taille : [0, 10] vs [0] !",
            calculer_note_par_interpolation_n_seuils,
            indicateur=pd.Series(),
            seuils=[0, 10],
            notes_seuils=[0],
        )
        assert_error_message_raised(
            self,
            "La liste des seuils n'est pas ordonnée de façon croissante : [0, 20, 10, 30, 40] !",
            calculer_note_par_interpolation_n_seuils,
            indicateur=pd.Series(),
            seuils=[0, 20, 10, 30, 40],
            notes_seuils=[0, 1, 2, 3, 4],
        )
        assert_error_message_raised(
            self,
            "La liste des notes des seuils n'est pas ordonnée : [0, 2, 1, 3, 4] !",
            calculer_note_par_interpolation_n_seuils,
            indicateur=pd.Series(),
            seuils=[0, 10, 20, 30, 40],
            notes_seuils=[0, 2, 1, 3, 4],
        )

    def test_calculer_note_par_interpolation_n_seuils(self):
        df = pd.DataFrame(
            columns=["indicateur"],
            data=[
                [-1],
                [0],
                [3],
                [8],
                [13],
                [np.nan],
            ],
        )
        # given
        df_attendu = pd.DataFrame(
            columns=["note_3seuils", "note_3seuils_inverse", "note_4seuils"],
            data=[
                [0, 10, 0],
                [0, 10, 0],
                [5, 5, 5],
                [7.5, 2.5, 7.5],
                [10, 0, 10],
                [np.nan, np.nan, np.nan],
            ],
        )
        # when
        df.loc[:, "note_3seuils"] = calculer_note_par_interpolation_n_seuils(df["indicateur"], [0, 3, 13], [0, 5, 10])
        df.loc[:, "note_3seuils_inverse"] = calculer_note_par_interpolation_n_seuils(df["indicateur"], [0, 3, 13], [10, 5, 0])
        df.loc[:, "note_4seuils"] = calculer_note_par_interpolation_n_seuils(df["indicateur"], [0, 3, 8, 13], [0, 5, 7.5, 10])
        # then
        pd.testing.assert_series_equal(df["note_3seuils"], df_attendu["note_3seuils"], check_names=False)
        pd.testing.assert_series_equal(df["note_3seuils_inverse"], df_attendu["note_3seuils_inverse"], check_names=False)
        pd.testing.assert_series_equal(df["note_4seuils"], df_attendu["note_4seuils"], check_names=False)


if __name__ == "__main__":
    unittest.main()
