import geopandas
from pandas import Series

from crater.config.config_globale import CRS_POUR_CALCUL_SURFACES


def calculer_colonne_superficie_en_hectares(gdf: geopandas.GeoDataFrame) -> Series:
    return (gdf["geometry"].to_crs(CRS_POUR_CALCUL_SURFACES).area / 10**4).round(2)
