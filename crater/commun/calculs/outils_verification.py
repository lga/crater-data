from typing import cast

import geopandas as gpd
import numpy as np
import pandas
from pandas import DataFrame, Series

from crater.commun.logger import log


def verifier_absence_doublons(df: DataFrame, colonnes: str | list[str]) -> None:
    if isinstance(colonnes, str):
        colonnes = [colonnes]
    doublons = cast(list[str], pandas.DataFrame(df[df[colonnes].duplicated()][colonnes]).drop_duplicates().agg("-".join, axis=1).values.tolist())

    if len(doublons) > 0:
        if len(doublons) > 100:
            doublons = doublons[:100] + ["..."]
        raise ValueError("ERREUR: plusieurs exemplaires pour '" + " x ".join(colonnes) + "' : " + ", ".join(doublons))


def verifier_colonne2_unique_par_colonne1(df: DataFrame, colonne1: str, colonne2: str, warning_only=False) -> None:
    nb_uniques_df1 = df.groupby(colonne1).nunique().reset_index()
    l1 = nb_uniques_df1[nb_uniques_df1[colonne2] > 1][colonne1].to_list()
    if len(l1) > 0:
        if len(l1) > 100:
            l1 = l1[:100] + ["..."]
        if warning_only:
            log.warning("ATTENTION, les '" + colonne1 + "' suivants ont plusieurs '" + colonne2 + "' : " + ", ".join(l1))
        else:
            raise ValueError("ERREUR: les '" + colonne1 + "' suivants ont plusieurs '" + colonne2 + "' : " + ", ".join(l1))


def verifier_elements_colonne1_presents_dans_colonne2(s1: Series, s2: Series) -> None:
    elements_absents = [str(x) for x in s1[~s1.isin(s2)].to_list()]

    if len(elements_absents) > 0:
        if len(elements_absents) > 100:
            elements_absents = elements_absents[:100] + ["..."]
        raise ValueError(
            "ERREUR: le(s) "
            + str(len(elements_absents))
            + " élément(s) suivant(s) de la serie 1 sont absents de la série 2 : "
            + ", ".join(elements_absents)
        )


def verifier_absence_na(df: DataFrame, colonne_id: str, colonne: str) -> None:
    liste_territoires_colonne_na = df.loc[df[colonne].isnull() | (df[colonne] == "")].reset_index()[colonne_id].tolist()
    if len(liste_territoires_colonne_na) > 0:
        if len(liste_territoires_colonne_na) > 100:
            liste_territoires_colonne_na = liste_territoires_colonne_na[:100] + ["..."]
        raise ValueError("ERREUR: NA ou '' dans la colonne '" + colonne + "' pour les territoires : " + ", ".join(liste_territoires_colonne_na))


def verifier_valeurs_serie(serie: Series, liste_valeurs_attendues: list[str], ignorer_valeurs_absentes=False) -> None:
    liste_valeurs_uniques = serie.drop_duplicates().to_list()
    if np.nan in liste_valeurs_uniques:
        raise ValueError("ERREUR: valeurs NaN dans la série")
    liste_valeurs_indesirees = sorted(set(liste_valeurs_uniques) - set(liste_valeurs_attendues))
    liste_valeurs_absentes = sorted(set(liste_valeurs_attendues) - set(liste_valeurs_uniques))
    if len(liste_valeurs_indesirees) > 0:
        raise ValueError("ERREUR: valeurs indésirées dans la série : " + ", ".join(liste_valeurs_indesirees))
    if len(liste_valeurs_absentes) > 0 and not ignorer_valeurs_absentes:
        raise ValueError("ERREUR: valeurs absentes dans la série : " + ", ".join(liste_valeurs_absentes))


def verifier_nommage_ids_territoires(df: DataFrame, colonne_id: str) -> None:
    liste_territoires_problematiques = df[df[colonne_id].str.contains(" ")][colonne_id].tolist()
    if len(liste_territoires_problematiques) > 0:
        raise ValueError("ERREUR: des " + colonne_id + " sont problématiques : " + ", ".join(liste_territoires_problematiques))


def verifier_coherence_referentiel_avec_geometries_communes(referentiel_territoires: DataFrame, geometries_communes: gpd.GeoDataFrame) -> None:
    log.info("   => vérification des géométries des communes")

    communes_referentiel = (
        referentiel_territoires.reset_index()
        .query("categorie_territoire == 'COMMUNE'")
        .rename(columns={"id_territoire": "id_commune"})["id_commune"]
        .sort_values()
        .to_list()
    )
    geometries_communes = geometries_communes["id_commune"].sort_values().to_list()

    if geometries_communes != communes_referentiel:
        log.info(
            "Communes présentes dans les géométries mais absentes du réferentiel: "
            + "|".join(sorted(list(set(geometries_communes) - set(communes_referentiel))))
        )
        log.info(
            "Communes présentes dans le réferentiel mais absentes des géométries: "
            + "|".join(sorted(list(set(communes_referentiel) - set(geometries_communes))))
        )
        raise ValueError("ERREUR : référentiel des communes incompatible avec les géométries!")
    else:
        log.info("      ...référentiel des communes compatible avec les géométries")
