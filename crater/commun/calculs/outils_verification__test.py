import unittest

import geopandas as gpd
import numpy as np
import pandas as pd

from crater.commun.calculs.outils_verification import (
    verifier_coherence_referentiel_avec_geometries_communes,
    verifier_absence_doublons,
    verifier_colonne2_unique_par_colonne1,
    verifier_absence_na,
    verifier_nommage_ids_territoires,
    verifier_elements_colonne1_presents_dans_colonne2,
    verifier_valeurs_serie,
)
from crater.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas, assert_error_message_raised

augmenter_nombre_colonnes_affichees_pandas()


class TestsUnitairesOutilsVerification(unittest.TestCase):
    def test_verifier_coherence_referentiel_avec_geometries_communes_ok(self):
        # given
        referentiel_territoires = pd.DataFrame(
            columns=["id_territoire", "categorie_territoire"],
            data=[["C-1", "COMMUNE"], ["C-2", "COMMUNE"], ["D-1", "DEPARTEMENT"]],
        )
        geometries_communes = gpd.GeoDataFrame(columns=["id_commune"], data=[["C-1"], ["C-2"]])
        # then
        verifier_coherence_referentiel_avec_geometries_communes(referentiel_territoires, geometries_communes)

    def test_verifier_coherence_referentiel_avec_geometries_communes_ko(self):
        # given
        referentiel_territoires = pd.DataFrame(
            columns=["id_territoire", "categorie_territoire"],
            data=[["C-1", "COMMUNE"], ["C-2", "COMMUNE"], ["D-1", "DEPARTEMENT"]],
        )
        geometries_communes = gpd.GeoDataFrame(columns=["id_commune"], data=[["C-1"], ["C-3"]])
        # then
        assert_error_message_raised(
            self,
            "ERREUR : référentiel des communes incompatible avec les géométries!",
            verifier_coherence_referentiel_avec_geometries_communes,
            referentiel_territoires=referentiel_territoires,
            geometries_communes=geometries_communes,
        )

    def test_verifier_absence_id_doublons_ok(self):
        # given
        df = pd.DataFrame(columns=["id"], data=[["1"], ["2"], ["3"], ["4"]])
        # then
        verifier_absence_doublons(df, "id")
        # given
        df = pd.DataFrame(columns=["id1", "id2"], data=[["1", "a"], ["1", "b"]])
        # then
        verifier_absence_doublons(df, ["id1", "id2"])

    def test_verifier_absence_id_doublons_ko(self):
        # given
        df = pd.DataFrame(columns=["id"], data=[["1"], ["2"], ["2"]])
        # then
        with self.assertRaises(Exception):
            verifier_absence_doublons(df, "id")
        # given
        df = pd.DataFrame(columns=["id1", "id2"], data=[["1", "a"], ["1", "a"]])
        # then
        assert_error_message_raised(
            self, "ERREUR: plusieurs exemplaires pour 'id1 x id2' : 1-a", verifier_absence_doublons, df=df, colonnes=["id1", "id2"]
        )

    def test_verifier_valeurs_serie_ok(self):
        # given
        serie = pd.Series(data=["1", "2"])
        # then
        verifier_valeurs_serie(serie, ["1", "2"])
        # given
        serie = pd.Series(data=["1"])
        # then
        verifier_valeurs_serie(serie, ["1", "2"], ignorer_valeurs_absentes=True)

    def test_verifier_valeurs_serie_ko(self):
        # given
        serie = pd.Series(data=["1", np.nan])
        # then
        assert_error_message_raised(
            self, "ERREUR: valeurs NaN dans la série", verifier_valeurs_serie, serie=serie, liste_valeurs_attendues=["1", "2", "3"]
        )
        # given
        serie = pd.Series(data=["1"])
        # then
        assert_error_message_raised(
            self, "ERREUR: valeurs absentes dans la série : 2, 3", verifier_valeurs_serie, serie=serie, liste_valeurs_attendues=["1", "2", "3"]
        )
        # given
        serie = pd.Series(data=["1", "2", "3", "4", "5"])
        # then
        assert_error_message_raised(
            self, "ERREUR: valeurs indésirées dans la série : 4, 5", verifier_valeurs_serie, serie=serie, liste_valeurs_attendues=["1", "2", "3"]
        )

    def test_verifier_colonne2_unique_par_colonne1_ok(self):
        # given
        df1 = pd.DataFrame(columns=["id", "nom"], data=[["1", "A"], ["1", "A"]])
        df2 = pd.DataFrame(columns=["id", "nom"], data=[["1", "A"], ["2", "A"]])
        df3 = pd.DataFrame(columns=["id", "nom"], data=[["1", "A"], ["1", "B"]])
        # then
        verifier_colonne2_unique_par_colonne1(df1, "id", "nom")
        verifier_colonne2_unique_par_colonne1(df2, "id", "nom")
        verifier_colonne2_unique_par_colonne1(df3, "id", "nom", warning_only=True)

    def test_verifier_colonne2_unique_par_colonne1_ko(self):
        # given
        df = pd.DataFrame(columns=["id", "nom"], data=[["1", "A"], ["1", "B"]])
        # then
        assert_error_message_raised(
            self, "ERREUR: les 'id' suivants ont plusieurs 'nom' : 1", verifier_colonne2_unique_par_colonne1, df=df, colonne1="id", colonne2="nom"
        )

    def test_verifier_elements_colonne1_presents_dans_colonne2_ok(self):
        # given
        s1 = pd.Series([1, 2])
        s2 = pd.Series([1, 2, 3])
        # then
        verifier_elements_colonne1_presents_dans_colonne2(s1, s2)

    def test_verifier_elements_colonne1_presents_dans_colonne2_ko(self):
        # given
        s1 = pd.Series([1, 4])
        s2 = pd.Series([1, 2, 3])
        # then
        assert_error_message_raised(
            self,
            "ERREUR: le(s) 1 élément(s) suivant(s) de la serie 1 sont absents de la série 2 : 4",
            verifier_elements_colonne1_presents_dans_colonne2,
            s1=s1,
            s2=s2,
        )

    def test_verifier_absence_na_ok(self):
        # given
        df = pd.DataFrame(columns=["id_territoire", "colonne"], data=[["C-1", "1"], ["C-2", "1"]])
        # then
        verifier_absence_na(df, "id_territoire", "colonne")

    def test_verifier_absence_na_ko(self):
        # given
        df = pd.DataFrame(columns=["id_territoire", "colonne"], data=[["C-1", "1"], ["C-2", np.nan]])
        # then
        assert_error_message_raised(
            self,
            "ERREUR: NA ou '' dans la colonne 'colonne' pour les territoires : C-2",
            verifier_absence_na,
            df=df,
            colonne_id="id_territoire",
            colonne="colonne",
        )
        # given
        df = pd.DataFrame(columns=["id_territoire", "colonne"], data=[["C-1", "1"], ["C-2", ""]])
        # then
        assert_error_message_raised(
            self,
            "ERREUR: NA ou '' dans la colonne 'colonne' pour les territoires : C-2",
            verifier_absence_na,
            df=df,
            colonne_id="id_territoire",
            colonne="colonne",
        )

    def test_verifier_nommage_ids_territoires_ok(self):
        # given
        df = pd.DataFrame(columns=["id_territoire"], data=[["nom_sans_espaces"]])
        # then
        verifier_nommage_ids_territoires(df, "id_territoire")

    def test_verifier_nommage_ids_territoires_ko(self):
        # given
        df = pd.DataFrame(
            columns=["id_territoire"],
            data=[
                ["nom_sans_espaces"],
                ["nom avec_1_espace"],
                ["nom avec plusieurs espaces"],
            ],
        )
        # then
        assert_error_message_raised(
            self,
            "ERREUR: des id_territoire sont problématiques : nom avec_1_espace, nom avec plusieurs espaces",
            verifier_nommage_ids_territoires,
            df=df,
            colonne_id="id_territoire",
        )


if __name__ == "__main__":
    unittest.main()
