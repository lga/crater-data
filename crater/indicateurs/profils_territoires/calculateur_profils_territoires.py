from pathlib import Path

import numpy as np
import pandas
from pandas import DataFrame, Series


from crater.commun import environnement
from crater.commun.calculs.outils_verification import verifier_valeurs_serie
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.exports.export_cartes import exporter_cartes
from crater.commun.exports.export_fichier import reinitialiser_dossier, exporter_df_indicateurs_par_territoires
from crater.commun.logger import log
from crater.indicateurs.otex.calculateur_otex_territoires import (
    OTEX_5_POSTES_VITICULTURE,
    OTEX_5_POSTES_GRANDES_CULTURES,
    OTEX_5_POSTES_FRUITS_LEGUMES,
    OTEX_5_POSTES_ELEVAGE,
)


def calculer_profils_territoires(
    df_territoires: DataFrame,
    fichier_population: Path,
    fichier_cheptels: Path,
    fichier_otex: Path,
    productions_besoins: Path,
    dossier_output: Path,
) -> Path:
    log.info("##### CALCUL PROFILS TERRITOIRES #####")

    df_territoires = charger_et_joindre_df_indicateurs(df_territoires, fichier_population)
    df_territoires = charger_et_joindre_df_indicateurs(df_territoires, fichier_cheptels)
    df_territoires = charger_et_joindre_df_indicateurs(df_territoires, fichier_otex)

    reinitialiser_dossier(dossier_output)

    df_profils = df_territoires.loc[:, ["nom_territoire", "categorie_territoire", "categorie_epci", "categorie_regroupement_communes"]].copy()

    df_profils["profil_agriculture"] = _calculer_profil_agriculture(
        df_territoires["otex_majoritaire_5_postes"], df_territoires["ugb_total_par_sau_ha"]
    )
    df_profils["profil_collectivite"] = _calculer_profil_collectivite(
        df_territoires.loc[
            :, ["categorie_territoire", "categorie_epci", "categorie_regroupement_communes", "densite_population_hab_par_km2", "code_densite"]
        ]
    )

    df_productions_besoins = pandas.read_csv(productions_besoins, sep=";").set_index("id_territoire")
    df_profils["profil_diagnostic"] = _calculer_profil_diagnostic(
        df_profils["profil_agriculture"], df_productions_besoins["taux_adequation_brut_assiette_actuelle_pourcent"]
    )

    df_profils = df_profils.loc[:, ["nom_territoire", "categorie_territoire", "profil_agriculture", "profil_collectivite", "profil_diagnostic"]]
    exporter_df_indicateurs_par_territoires(df_profils, dossier_output, "profils_territoires")

    exporter_cartes(
        df_profils,
        ["profil_agriculture", "profil_collectivite", "profil_diagnostic"],
        ["REGION", "DEPARTEMENT", "EPCI", "COMMUNE"],
        dossier_output / "cartes",
        bins=None,
        cmap="Dark2",
    )

    return dossier_output


def _calculer_profil_agriculture(otex_majoritaire_5_postes: Series, ugb_total_par_sau_ha: Series) -> Series:
    df = pandas.concat([otex_majoritaire_5_postes, ugb_total_par_sau_ha], axis=1)
    df["profil_agriculture"] = np.where(
        otex_majoritaire_5_postes == OTEX_5_POSTES_VITICULTURE,
        "VITICULTURE",
        np.where(
            otex_majoritaire_5_postes == OTEX_5_POSTES_GRANDES_CULTURES,
            "GRANDES_CULTURES",
            np.where(
                otex_majoritaire_5_postes == OTEX_5_POSTES_FRUITS_LEGUMES,
                "FRUITS_ET_LEGUMES",
                np.where(
                    otex_majoritaire_5_postes == OTEX_5_POSTES_ELEVAGE,
                    np.where(ugb_total_par_sau_ha > 1.5, "ELEVAGE_INTENSIF", "ELEVAGE_EXTENSIF"),
                    "NON_SPECIALISE",
                ),
            ),
        ),
    )
    return df["profil_agriculture"]


def _calculer_profil_collectivite(df_territoires: DataFrame) -> Series:
    SEUIL_DENSITE_POPULATION_INTERCOMMUNALITE_DENSE_HAB_PAR_KM2 = 1500
    SEUIL_DENSITE_POPULATION_INTERCOMMUNALITE_INTERMEDIAIRE_HAB_PAR_KM2 = 150

    dictionnaire_regroupements_communes = {
        "ND-69D": "DEPARTEMENT",
        "AT-CEA": "DEPARTEMENT",
    }

    df_profils_collectivites = df_territoires.copy()

    df_profils_collectivites["profil_collectivite"] = df_territoires.categorie_territoire
    df_profils_collectivites["profil_collectivite"] = np.where(
        df_territoires.categorie_territoire == "COMMUNE",
        df_territoires.code_densite,
        np.where(
            df_territoires.categorie_territoire == "REGROUPEMENT_COMMUNES",
            df_territoires.categorie_regroupement_communes,
            df_profils_collectivites["profil_collectivite"],
        ),
    )

    df_profils_collectivites["profil_collectivite"] = np.where(
        df_territoires.categorie_epci.isin(["METROPOLE"])
        | (
            (df_territoires.categorie_territoire.isin(["DEPARTEMENT", "EPCI", "REGROUPEMENT_COMMUNES"]))
            & (df_territoires.densite_population_hab_par_km2 >= SEUIL_DENSITE_POPULATION_INTERCOMMUNALITE_DENSE_HAB_PAR_KM2)
        ),
        df_profils_collectivites["profil_collectivite"] + "_URBAIN",
        np.where(
            (df_territoires.categorie_territoire.isin(["EPCI", "REGROUPEMENT_COMMUNES"]))
            & (df_territoires.densite_population_hab_par_km2 >= SEUIL_DENSITE_POPULATION_INTERCOMMUNALITE_INTERMEDIAIRE_HAB_PAR_KM2),
            df_profils_collectivites["profil_collectivite"] + "_INTERMEDIAIRE",
            np.where(
                df_territoires.categorie_territoire.isin(["EPCI", "REGROUPEMENT_COMMUNES"]),
                df_profils_collectivites["profil_collectivite"] + "_RURAL",
                df_profils_collectivites["profil_collectivite"],
            ),
        ),
    )
    df_profils_collectivites["profil_collectivite"].update(dictionnaire_regroupements_communes)  # type: ignore

    # catégories grisées = ne ressortent pas lors du calcul complet
    verifier_valeurs_serie(
        df_profils_collectivites["profil_collectivite"],
        [
            "PAYS",
            "REGION",
            "DEPARTEMENT",
            "DEPARTEMENT_URBAIN",
            "EPCI_URBAIN",
            "EPCI_INTERMEDIAIRE",
            "EPCI_RURAL",
            "BASSIN_DE_VIE_2022_URBAIN",
            "BASSIN_DE_VIE_2022_INTERMEDIAIRE",
            "BASSIN_DE_VIE_2022_RURAL",
            "PARC_NATIONAL_URBAIN",
            "PARC_NATIONAL_INTERMEDIAIRE",
            "PARC_NATIONAL_RURAL",
            # "PARC_NATUREL_REGIONAL_URBAIN",
            "PARC_NATUREL_REGIONAL_INTERMEDIAIRE",
            "PARC_NATUREL_REGIONAL_RURAL",
            # "PAYS_PETR_URBAIN",
            "PAYS_PETR_INTERMEDIAIRE",
            "PAYS_PETR_RURAL",
            "SCHEMA_COHERENCE_TERRITORIAL_URBAIN",
            "SCHEMA_COHERENCE_TERRITORIAL_INTERMEDIAIRE",
            "SCHEMA_COHERENCE_TERRITORIAL_RURAL",
            "PROJET_ALIMENTAIRE_TERRITORIAL_URBAIN",
            "PROJET_ALIMENTAIRE_TERRITORIAL_INTERMEDIAIRE",
            "PROJET_ALIMENTAIRE_TERRITORIAL_RURAL",
            # "AUTRE_TERRITOIRE_URBAIN",
            "AUTRE_TERRITOIRE_INTERMEDIAIRE",
            "AUTRE_TERRITOIRE_RURAL",
            "GRAND_CENTRE_URBAIN",
            "CENTRE_URBAIN_INTERMEDIAIRE",
            "PETITE_VILLE",
            "CEINTURE_URBAINE",
            "BOURG_RURAL",
            "COMMUNE_A_HABITAT_DISPERSE",
            "COMMUNE_A_HABITAT_TRES_DISPERSE",
        ],
        ignorer_valeurs_absentes=(environnement.ENVIRONNEMENT_EXECUTION != "PROD"),
    )

    return df_profils_collectivites["profil_collectivite"]


def _calculer_profil_diagnostic(profil_agriculture: Series, taux_adequation_brut_assiette_actuelle_pourcent: Series) -> Series:
    df = pandas.concat([profil_agriculture, taux_adequation_brut_assiette_actuelle_pourcent], axis=1)
    df["profil_diagnostic"] = np.where(
        taux_adequation_brut_assiette_actuelle_pourcent < 10,
        "URBAIN",
        np.where(profil_agriculture != "NON_SPECIALISE", profil_agriculture, "DEFAUT"),
    )

    return df["profil_diagnostic"]
