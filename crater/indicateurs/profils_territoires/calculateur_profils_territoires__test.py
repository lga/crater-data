import unittest
from pathlib import Path

import numpy as np
import pandas

from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.indicateurs.profils_territoires.calculateur_profils_territoires import (
    calculer_profils_territoires,
    _calculer_profil_agriculture,
    _calculer_profil_collectivite,
    _calculer_profil_diagnostic,
)
from crater.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas, assert_csv_files_are_equals

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestTypologie(unittest.TestCase):
    def test_calculer_profil_agriculture(self):
        df = pandas.DataFrame(
            columns=["otex_majoritaire_5_postes", "ugb_total_par_sau_ha", "profil_agriculture"],
            data=[
                ["GRANDES_CULTURES", np.nan, "GRANDES_CULTURES"],
                ["FRUITS_ET_LEGUMES", np.nan, "FRUITS_ET_LEGUMES"],
                ["VITICULTURE", np.nan, "VITICULTURE"],
                ["ELEVAGE", 1.4, "ELEVAGE_EXTENSIF"],
                ["ELEVAGE", 1.6, "ELEVAGE_INTENSIF"],
                ["POLYCULTURE_POLYELEVAGE", np.nan, "NON_SPECIALISE"],
                ["SANS_OTEX_MAJORITAIRE", np.nan, "NON_SPECIALISE"],
                ["NON_DEFINIE", np.nan, "NON_SPECIALISE"],
            ],
        )

        pandas.testing.assert_series_equal(
            _calculer_profil_agriculture(df["otex_majoritaire_5_postes"], df["ugb_total_par_sau_ha"]), df["profil_agriculture"], check_names=False
        )

    def test_calculer_profil_collectivite(self):
        df = pandas.DataFrame(
            columns=[
                "id_territoire",
                "categorie_territoire",
                "categorie_epci",
                "categorie_regroupement_communes",
                "densite_population_hab_par_km2",
                "code_densite",
                "profil_collectivite",
            ],
            data=[
                ["P-FR", "PAYS", np.nan, np.nan, np.nan, np.nan, "PAYS"],
                ["R-1", "REGION", np.nan, np.nan, np.nan, np.nan, "REGION"],
                ["D-1", "DEPARTEMENT", np.nan, np.nan, np.nan, np.nan, "DEPARTEMENT"],
                ["D-75", "DEPARTEMENT", np.nan, np.nan, 2000, np.nan, "DEPARTEMENT_URBAIN"],
                ["E-1", "EPCI", "METROPOLE", np.nan, 1.0, np.nan, "EPCI_URBAIN"],
                ["E-2", "EPCI", np.nan, np.nan, 200, np.nan, "EPCI_INTERMEDIAIRE"],
                ["E-3", "EPCI", np.nan, np.nan, 1, np.nan, "EPCI_RURAL"],
                ["BV-1", "REGROUPEMENT_COMMUNES", np.nan, "BASSIN_DE_VIE_2022", 2000, np.nan, "BASSIN_DE_VIE_2022_URBAIN"],
                ["BV-2", "REGROUPEMENT_COMMUNES", np.nan, "BASSIN_DE_VIE_2022", 200, np.nan, "BASSIN_DE_VIE_2022_INTERMEDIAIRE"],
                ["BV-3", "REGROUPEMENT_COMMUNES", np.nan, "BASSIN_DE_VIE_2022", np.nan, np.nan, "BASSIN_DE_VIE_2022_RURAL"],
                ["P-1", "REGROUPEMENT_COMMUNES", np.nan, "PARC_NATIONAL", 2000, np.nan, "PARC_NATIONAL_URBAIN"],
                ["P-2", "REGROUPEMENT_COMMUNES", np.nan, "PARC_NATIONAL", 200, np.nan, "PARC_NATIONAL_INTERMEDIAIRE"],
                ["P-3", "REGROUPEMENT_COMMUNES", np.nan, "PARC_NATIONAL", np.nan, np.nan, "PARC_NATIONAL_RURAL"],
                ["PNR-2", "REGROUPEMENT_COMMUNES", np.nan, "PARC_NATUREL_REGIONAL", 200, np.nan, "PARC_NATUREL_REGIONAL_INTERMEDIAIRE"],
                ["PNR-3", "REGROUPEMENT_COMMUNES", np.nan, "PARC_NATUREL_REGIONAL", np.nan, np.nan, "PARC_NATUREL_REGIONAL_RURAL"],
                ["PP-2", "REGROUPEMENT_COMMUNES", np.nan, "PAYS_PETR", 200, np.nan, "PAYS_PETR_INTERMEDIAIRE"],
                ["PP-3", "REGROUPEMENT_COMMUNES", np.nan, "PAYS_PETR", np.nan, np.nan, "PAYS_PETR_RURAL"],
                [
                    "SCOT-1",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    "SCHEMA_COHERENCE_TERRITORIAL",
                    2000,
                    np.nan,
                    "SCHEMA_COHERENCE_TERRITORIAL_URBAIN",
                ],
                [
                    "SCOT-2",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    "SCHEMA_COHERENCE_TERRITORIAL",
                    200,
                    np.nan,
                    "SCHEMA_COHERENCE_TERRITORIAL_INTERMEDIAIRE",
                ],
                ["SCOT-3", "REGROUPEMENT_COMMUNES", np.nan, "SCHEMA_COHERENCE_TERRITORIAL", np.nan, np.nan, "SCHEMA_COHERENCE_TERRITORIAL_RURAL"],
                [
                    "PAT-2",
                    "REGROUPEMENT_COMMUNES",
                    np.nan,
                    "PROJET_ALIMENTAIRE_TERRITORIAL",
                    200,
                    np.nan,
                    "PROJET_ALIMENTAIRE_TERRITORIAL_INTERMEDIAIRE",
                ],
                ["PAT-3", "REGROUPEMENT_COMMUNES", np.nan, "PROJET_ALIMENTAIRE_TERRITORIAL", np.nan, np.nan, "PROJET_ALIMENTAIRE_TERRITORIAL_RURAL"],
                ["AT-2", "REGROUPEMENT_COMMUNES", np.nan, "AUTRE_TERRITOIRE", 200, np.nan, "AUTRE_TERRITOIRE_INTERMEDIAIRE"],
                ["AT-3", "REGROUPEMENT_COMMUNES", np.nan, "AUTRE_TERRITOIRE", np.nan, np.nan, "AUTRE_TERRITOIRE_RURAL"],
                ["ND-69D", "REGROUPEMENT_COMMUNES", np.nan, "AUTRE_TERRITOIRE", np.nan, np.nan, "DEPARTEMENT"],
                ["C-1", "COMMUNE", np.nan, np.nan, np.nan, "GRAND_CENTRE_URBAIN", "GRAND_CENTRE_URBAIN"],
                ["C-2", "COMMUNE", np.nan, np.nan, np.nan, "CENTRE_URBAIN_INTERMEDIAIRE", "CENTRE_URBAIN_INTERMEDIAIRE"],
                ["C-3", "COMMUNE", np.nan, np.nan, np.nan, "PETITE_VILLE", "PETITE_VILLE"],
                ["C-4", "COMMUNE", np.nan, np.nan, np.nan, "CEINTURE_URBAINE", "CEINTURE_URBAINE"],
                ["C-5", "COMMUNE", np.nan, np.nan, np.nan, "BOURG_RURAL", "BOURG_RURAL"],
                ["C-6", "COMMUNE", np.nan, np.nan, np.nan, "COMMUNE_A_HABITAT_DISPERSE", "COMMUNE_A_HABITAT_DISPERSE"],
                ["C-7", "COMMUNE", np.nan, np.nan, np.nan, "COMMUNE_A_HABITAT_TRES_DISPERSE", "COMMUNE_A_HABITAT_TRES_DISPERSE"],
            ],
        ).set_index("id_territoire")

        pandas.testing.assert_series_equal(_calculer_profil_collectivite(df), df["profil_collectivite"], check_names=False)

    def test_calculer_profil_territoire(self):
        df = pandas.DataFrame(
            columns=["profil_agriculture", "taux_adequation_brut_assiette_actuelle_pourcent", "profil_territoire"],
            data=[
                ["GRANDES_CULTURES", 9, "URBAIN"],
                ["GRANDES_CULTURES", np.nan, "GRANDES_CULTURES"],
                ["VITICULTURE", np.nan, "VITICULTURE"],
                ["FRUITS_ET_LEGUMES", np.nan, "FRUITS_ET_LEGUMES"],
                ["ELEVAGE_EXTENSIF", np.nan, "ELEVAGE_EXTENSIF"],
                ["ELEVAGE_INTENSIF", np.nan, "ELEVAGE_INTENSIF"],
                ["NON_SPECIALISE", np.nan, "DEFAUT"],
            ],
        )

        pandas.testing.assert_series_equal(
            _calculer_profil_diagnostic(df["profil_agriculture"], df["taux_adequation_brut_assiette_actuelle_pourcent"]),
            df["profil_territoire"],
            check_names=False,
        )

    def test_calculer_profils_territoires(self):
        df_territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")

        calculer_profils_territoires(
            df_territoires,
            CHEMIN_INPUT_DATA / "population" / "population.csv",
            CHEMIN_INPUT_DATA / "cheptels" / "cheptels.csv",
            CHEMIN_INPUT_DATA / "otex" / "otex.csv",
            CHEMIN_INPUT_DATA / "production_besoins" / "productions_besoins.csv",
            CHEMIN_OUTPUT_DATA,
        )

        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "profils_territoires.csv",
            CHEMIN_OUTPUT_DATA / "profils_territoires.csv",
        )


if __name__ == "__main__":
    unittest.main()
