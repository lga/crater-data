from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.logger import log


def charger(chemin_fichier: Path) -> DataFrame:
    log.info("   => chargement flux azote depuis %s", chemin_fichier)

    df = pd.read_excel(
        chemin_fichier,
        sheet_name="Data",
        usecols="A:AB",
        names=[
            "code_insee",
            "bovins_ugb",
            "ovins_caprins_ugb",
            "porcins_ugb",
            "volailles_ugb",
            "besoin_plantes_kgN",
            "plantes_recoltees_kgN",
            "mineralisation_nette_azote_organique_sol_kgN",
            "deposition_atmosphérique_kgN",
            "fixation_symbiotique_kgN",
            "engrais_synthese_kgN",
            "boues_epuration_azote_mineral_uniquement_kgN",
            "excrements_animaux_elevage_kgN",
            "excrements_animaux_elevage_azote_mineral_uniquement_kgN",
            "residus_cultures_kgN",
            "residus_cultures_azote_mineral_uniquement_kgN",
            "plantes_recoltees_pour_animaux_kgN",
            "production_locale_concentres_energetiques_pour_animaux_kgN",
            "production_locale_fourrages_pour_animaux_kgN",
            "production_locale_concentres_proteiques_pour_animaux_kgN",
            "besoins_animaux_concentres_energetiques_kgN",
            "besoins_animaux_fourrages_kgN",
            "besoins_animaux_concentres_proteiques_kgN",
            "solde_import_export_concentres_energetiques_pour_animaux_kgN",
            "solde_import_export_fourrages_pour_animaux_kgN",
            "solde_import_export_concentres_proteiques_pour_animaux_kgN",
            "superficie_terres_arables_ha",
            "superficie_prairies_permanentes_ha",
        ],
        dtype={"code_insee": "str"},
        na_values=[""],
    )

    df.code_insee = traduire_code_insee_vers_id_commune_crater(df.code_insee)
    df.rename(columns={"code_insee": "id_commune"}, inplace=True)
    verifier_absence_doublons(df, "id_commune")

    return df
