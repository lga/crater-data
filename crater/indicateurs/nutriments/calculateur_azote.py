from pathlib import Path

import numpy as np
from pandas import DataFrame

from crater.indicateurs.nutriments.chargeur_flux_azote import charger as chargeur_flux_azote
from crater.commun.calculs.outils_territoires import ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales
from crater.commun.exports.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log
from crater.config.config_globale import IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX, ID_FRANCE
from crater.config.config_sources import ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_FLUX_AZOTE


def calculer_indicateurs_azote(
    territoires: DataFrame,
    chemin_fichier_flux_azote: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### CALCUL FLUX AZOTE #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_flux_azote = chargeur_flux_azote(chemin_fichier_flux_azote).rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")

    colonnes = (
        ["nom_territoire", "categorie_territoire"]
        + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
        + [
            "annee_dernier_mouvement_commune",
        ]
    )
    df_flux_azote = territoires[colonnes].join(df_flux_azote)

    for colonne in df_flux_azote.loc[:, slice("bovins_ugb", "superficie_prairies_permanentes_ha")].columns:
        df_flux_azote.loc[
            df_flux_azote.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_FLUX_AZOTE,
            colonne,
        ] = np.nan
        df_flux_azote = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_flux_azote, colonne)

    df_flux_azote = ajouter_colonnes_importations_depuis_etranger(df_flux_azote)

    return exporter_df_indicateurs_par_territoires(
        df_flux_azote.drop(columns=["annee_dernier_mouvement_commune"] + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX),
        chemin_dossier_output,
        "flux_azote",
    )


def ajouter_colonnes_importations_depuis_etranger(df_flux_azote: DataFrame) -> DataFrame:
    for c in ["concentres_energetiques", "fourrages", "concentres_proteiques"]:
        df_flux_azote[f"import_depuis_etranger_{c}_pour_animaux_kgN"] = 0
        solde_import_export_france = df_flux_azote.query(f"id_territoire=='{ID_FRANCE}'")[f"solde_import_export_{c}_pour_animaux_kgN"][0]
        if solde_import_export_france > 0:  # on importe (+) plus que l'on exporte (-)
            mask_communes_importatrices = (df_flux_azote[f"solde_import_export_{c}_pour_animaux_kgN"] > 0) & (
                df_flux_azote.categorie_territoire == "COMMUNE"
            )
            total_import_france = df_flux_azote.loc[mask_communes_importatrices, f"solde_import_export_{c}_pour_animaux_kgN"].sum()
            part_etranger = solde_import_export_france / total_import_france
            df_flux_azote.loc[mask_communes_importatrices, f"import_depuis_etranger_{c}_pour_animaux_kgN"] = (
                part_etranger * df_flux_azote.loc[mask_communes_importatrices, f"solde_import_export_{c}_pour_animaux_kgN"]
            )
            df_flux_azote = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
                df_flux_azote, f"import_depuis_etranger_{c}_pour_animaux_kgN"
            )

    return df_flux_azote
