import unittest
from pathlib import Path

from crater.indicateurs.consommation.calculateur_indicateurs_consommation import calculer_indicateurs_consommation
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestCalculateurTauxPauvrete(unittest.TestCase):
    def test_calcul_taux_pauvrete(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # when
        calculer_indicateurs_consommation(
            territoires,
            CHEMIN_INPUT_DATA / "population" / "population.csv",
            CHEMIN_INPUT_DATA / "insee" / "filosofi" / "indic-struct-distrib-revenu-2019-COMMUNES_csv",
            CHEMIN_INPUT_DATA / "insee" / "filosofi" / "indic-struct-distrib-revenu-2019-SUPRA_csv",
            CHEMIN_INPUT_DATA / "obsoalim" / "precarite_alimentaire.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "consommation.csv",
            CHEMIN_OUTPUT_DATA / "consommation.csv",
        )


if __name__ == "__main__":
    unittest.main()
