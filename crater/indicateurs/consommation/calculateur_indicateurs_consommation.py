from pathlib import Path

import pandas
import pandas as pd
from pandas import DataFrame

from crater.indicateurs.consommation import chargeur_taux_pauvrete
from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
    traduire_code_insee_vers_id_epci_crater,
    traduire_code_insee_vers_id_departement_crater,
    traduire_code_insee_vers_id_region_crater,
    ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales,
)
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.exports.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log
from crater.config.config_globale import ID_FRANCE
from crater.config.config_sources import (
    NOM_FICHIERS_TAUX_PAUVRETE,
    ANNEE_REFERENTIEL_TERRITOIRES_TAUX_PAUVRETE,
    ANNEE_REFERENTIEL_TERRITOIRES_RISQUE_PRECARITE_ALIMENTAIRE,
)

DICTIONNAIRE_NIVEAUX_RISQUE_PRECARITE_ALIMENTAIRE = {
    "1 - Très bas": "1_TRES_BAS",
    "2 - Bas": "2_BAS",
    "3 - Moyen": "3_MOYEN",
    "4 - Elevé": "4_ELEVE",
    "5 - Très élevé": "5_TRES_ELEVE",
}


def calculer_indicateurs_consommation(
    territoires: DataFrame,
    fichier_population: Path,
    chemin_dossier_filosofi_pauvrete_communes: Path,
    chemin_dossier_filosofi_pauvrete_supra: Path,
    fichier_obsoalim_risque_precarite_alimentaire_par_communes: Path,
    chemin_dossier_output: Path,
) -> None:
    reinitialiser_dossier(chemin_dossier_output)

    territoires = charger_et_joindre_df_indicateurs(territoires, fichier_population)

    df_indicateur_pauvrete = _calculer_indicateur_taux_pauvrete(
        territoires,
        chemin_dossier_filosofi_pauvrete_communes,
        chemin_dossier_filosofi_pauvrete_supra,
    )
    df_indicateur_risque_precarite_alimentaire = _calculer_indicateur_niveau_risque_precarite_alimentaire(
        territoires, fichier_obsoalim_risque_precarite_alimentaire_par_communes
    )
    df_consommation = df_indicateur_pauvrete.join(df_indicateur_risque_precarite_alimentaire)

    df_consommation = df_consommation.loc[
        :,
        ["nom_territoire", "categorie_territoire", "taux_pauvrete_60_pourcent", "code_niveau_risque_precarite_alimentaire"],
    ]

    exporter_df_indicateurs_par_territoires(df_consommation, chemin_dossier_output, "consommation")


def _calculer_indicateur_taux_pauvrete(
    territoires: DataFrame,
    chemin_dossier_filosofi_pauvrete_communes: Path,
    chemin_dossier_filosofi_pauvrete_supra: Path,
) -> pd.DataFrame:
    log.info("##### CALCUL TAUX PAUVRETE AU SEUIL DE 60 POUR CENT #####")

    df_source_taux_pauvrete_60_pourcent = _charger_sources_taux_pauvrete(
        chemin_dossier_filosofi_pauvrete_communes,
        chemin_dossier_filosofi_pauvrete_supra,
    )

    df_indicateur_pauvrete = territoires.copy().join(df_source_taux_pauvrete_60_pourcent.set_index("id_territoire"))
    df_indicateur_pauvrete = _vider_donnees_pauvrete_communes_avec_mouvement(df_indicateur_pauvrete)

    # Données de regroupements de communes non traités, ce serait non représentatif au vu du nombre de
    # communes dont les données manquent.
    return df_indicateur_pauvrete


def _calculer_indicateur_niveau_risque_precarite_alimentaire(
    territoires: DataFrame,
    fichier_obsoalim_risque_precarite_alimentaire_par_communes: Path,
) -> pd.DataFrame:
    log.info("##### CALCUL RISQUE DE PRECARITE ALIMENTAIRE #####")

    df_source_risque_precarite_alimentaire = pd.read_csv(fichier_obsoalim_risque_precarite_alimentaire_par_communes)

    df_source_risque_precarite_alimentaire["id_territoire"] = traduire_code_insee_vers_id_commune_crater(
        df_source_risque_precarite_alimentaire["code_commune"].str[3:]
    )
    df_indicateur_risque_precarite_alimentaire = territoires.copy().join(df_source_risque_precarite_alimentaire.set_index("id_territoire"))
    df_indicateur_risque_precarite_alimentaire = _vider_donnees_precarite_alimentaire_communes_avec_mouvement(
        df_indicateur_risque_precarite_alimentaire
    )

    df_indicateur_risque_precarite_alimentaire["indice_niveau_risque_precarite_alimentaire"] = df_indicateur_risque_precarite_alimentaire[
        "code_niveau_risque_precarite_alimentaire"
    ].map({k: i for i, k in enumerate(DICTIONNAIRE_NIVEAUX_RISQUE_PRECARITE_ALIMENTAIRE.keys())})

    df_indicateur_risque_precarite_alimentaire = ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales(
        df_indicateur_risque_precarite_alimentaire, "indice_niveau_risque_precarite_alimentaire", "population_totale_2017"
    )

    df_indicateur_risque_precarite_alimentaire["indice_niveau_risque_precarite_alimentaire"] = df_indicateur_risque_precarite_alimentaire[
        "indice_niveau_risque_precarite_alimentaire"
    ].round()

    df_indicateur_risque_precarite_alimentaire["code_niveau_risque_precarite_alimentaire"] = df_indicateur_risque_precarite_alimentaire[
        "indice_niveau_risque_precarite_alimentaire"
    ].map({i: v for i, v in enumerate(DICTIONNAIRE_NIVEAUX_RISQUE_PRECARITE_ALIMENTAIRE.values())})

    df_indicateur_risque_precarite_alimentaire = df_indicateur_risque_precarite_alimentaire.loc[
        :,
        ["code_niveau_risque_precarite_alimentaire"],
    ]

    return df_indicateur_risque_precarite_alimentaire


def _charger_sources_taux_pauvrete(chemin_dossier_pauvrete_communes: Path, chemin_dossier_pauvrete_supra: Path) -> DataFrame:
    log.info("##### CHARGEMENT DES DONNEES SOURCES TAUX PAUVRETE AU SEUIL DE 60 POUR CENT #####")

    df_taux_pauvrete_pays = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_supra / (NOM_FICHIERS_TAUX_PAUVRETE + "_METROPOLE.csv"))

    df_taux_pauvrete_pays["id_territoire"] = ID_FRANCE

    df_taux_pauvrete_region = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_supra / (NOM_FICHIERS_TAUX_PAUVRETE + "_REG.csv"))
    df_taux_pauvrete_region["id_territoire"] = traduire_code_insee_vers_id_region_crater(df_taux_pauvrete_region["id_territoire"])

    df_taux_pauvrete_departement = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_supra / (NOM_FICHIERS_TAUX_PAUVRETE + "_DEP.csv"))
    df_taux_pauvrete_departement["id_territoire"] = traduire_code_insee_vers_id_departement_crater(df_taux_pauvrete_departement["id_territoire"])

    df_taux_pauvrete_epci = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_supra / (NOM_FICHIERS_TAUX_PAUVRETE + "_EPCI.csv"))
    df_taux_pauvrete_epci["id_territoire"] = traduire_code_insee_vers_id_epci_crater(df_taux_pauvrete_epci["id_territoire"])

    df_taux_pauvrete_commune = chargeur_taux_pauvrete.charger(chemin_dossier_pauvrete_communes / (NOM_FICHIERS_TAUX_PAUVRETE + "_COM.csv"))
    df_taux_pauvrete_commune["id_territoire"] = traduire_code_insee_vers_id_commune_crater(df_taux_pauvrete_commune["id_territoire"])

    df_taux_pauvrete_60_pourcent = pd.concat(
        [
            df_taux_pauvrete_pays,
            df_taux_pauvrete_region,
            df_taux_pauvrete_departement,
            df_taux_pauvrete_epci,
            df_taux_pauvrete_commune,
        ]
    )

    return df_taux_pauvrete_60_pourcent


def _vider_donnees_pauvrete_communes_avec_mouvement(df):
    df.loc[
        df.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_TAUX_PAUVRETE,
        "taux_pauvrete_60_pourcent",
    ] = pandas.NA
    return df


def _vider_donnees_precarite_alimentaire_communes_avec_mouvement(df):
    df.loc[
        df.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_RISQUE_PRECARITE_ALIMENTAIRE,
        "niveau_risque_precarite_alimentaire",
    ] = pandas.NA
    return df
