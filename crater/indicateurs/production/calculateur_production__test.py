import unittest
from pathlib import Path

from crater.indicateurs.production.calculateur_production import calculer_production
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestProduction(unittest.TestCase):
    def test_production(self):
        # when
        calculer_production(
            CHEMIN_INPUT_DATA / "pratiques_agricoles.csv",
            CHEMIN_INPUT_DATA / "productions_besoins.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "production.csv",
            CHEMIN_OUTPUT_DATA / "production.csv",
        )


if __name__ == "__main__":
    unittest.main()
