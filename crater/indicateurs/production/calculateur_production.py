from pathlib import Path

import pandas as pd

from crater.commun.calculs.notes_et_messages import calculer_note_par_interpolation_2_seuils
from crater.commun.exports.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.exports.exporter_diagrammes import exporter_histogrammes
from crater.commun.logger import log
from crater.config.config_globale import ID_FRANCE


def calculer_production(
    chemin_fichier_pratiques_agricoles: Path,
    chemin_fichier_productions_besoins: Path,
    chemin_dossier_output: Path,
) -> None:
    log.info("##### CALCUL PRODUCTION #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_pratiques_agricoles = pd.read_csv(chemin_fichier_pratiques_agricoles, sep=";").set_index("id_territoire")
    df_pratiques_agricoles = df_pratiques_agricoles.loc[
        :,
        [
            "nom_territoire",
            "categorie_territoire",
            "hvn_score",
            "part_sau_bio_pourcent",
        ],
    ]
    df_productions_besoins = pd.read_csv(chemin_fichier_productions_besoins, sep=";").set_index("id_territoire")
    df_productions_besoins = df_productions_besoins.loc[:, ["taux_adequation_moyen_pondere_assiette_actuelle_pourcent"]]
    df_productions_besoins.columns = ["taux_adequation_moyen_pondere_pourcent"]  # type: ignore[assignment]  # TODO: open issue https://github.com/pandas-dev/pandas-stubs/issues/73

    df_production = df_pratiques_agricoles.join(df_productions_besoins)

    df_production = _ajouter_note(df_production)

    exporter_df_indicateurs_par_territoires(df_production, chemin_dossier_output, "production")
    exporter_histogrammes(
        df_production,
        [
            "note",
        ],
        chemin_dossier_output / "diagrammes",
    )


def _ajouter_note(df_production):
    df_production.loc[:, "hvn_note"] = df_production["hvn_score"] / 3

    df_production.loc[:, "part_sau_bio_note"] = calculer_note_par_interpolation_2_seuils(
        df_production["part_sau_bio_pourcent"],
        0,
        0,
        df_production.query(f"id_territoire=='{ID_FRANCE}'")["part_sau_bio_pourcent"][0],
        5,
    )

    df_production.loc[:, "taux_adequation_moyen_pondere_note"] = (df_production["taux_adequation_moyen_pondere_pourcent"] / 10).clip(0, 10)

    df_production.loc[:, "note"] = (
        ((((df_production["hvn_note"] + df_production["part_sau_bio_note"]) / 2) + (df_production["taux_adequation_moyen_pondere_note"])) / 2)
        .round(0)
        .astype("Int64")
    )
    return df_production
