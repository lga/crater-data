from pathlib import Path

import numpy as np
import pandas as pd
from pandas import DataFrame

import crater.indicateurs.politique_fonciere.chargeur_artificialisation as chargeur_artificialisation
import crater.indicateurs.politique_fonciere.chargeur_logements as chargeur_logements
from crater.indicateurs.politique_fonciere.evaluation_politique_amenagement import (
    ajouter_evaluation_politique_amenagement,
)
from crater.indicateurs.politique_fonciere.rythme_artificialisation_sau import (
    ajouter_rythme_artificialisation_sau,
)
from crater.indicateurs.politique_fonciere.sau_par_habitant import ajouter_sau_par_habitant
from crater.commun.calculs.outils_dataframes import DictDataFrames
from crater.commun.calculs.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
)
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.exports.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.exports.exporter_diagrammes import exporter_histogrammes
from crater.commun.logger import log
from crater.config.config_globale import IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
from crater.config.config_sources import ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_ARTIFICIALISATION, ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_NB_LOGEMENTS


def calculer_politique_fonciere(
    territoires: DataFrame,
    fichier_population: Path,
    fichier_occupation_sols: Path,
    chemin_fichier_artificialisation: Path,
    chemin_fichier_nb_logements: Path,
    chemin_fichier_nb_logements_vacants: Path,
    chemin_dossier_output: Path,
) -> None:
    log.info("##### CALCUL POLITIQUE FONCIERE #####")

    reinitialiser_dossier(chemin_dossier_output)

    territoires = charger_et_joindre_df_indicateurs(territoires, fichier_population)
    territoires = charger_et_joindre_df_indicateurs(territoires, fichier_occupation_sols)

    sources_politique_fonciere = _charger_sources(
        chemin_fichier_artificialisation,
        chemin_fichier_nb_logements,
        chemin_fichier_nb_logements_vacants,
    )

    donnees_politique_fonciere = _calculer_donnees_politique_fonciere(territoires, sources_politique_fonciere)
    donnees_politique_fonciere = ajouter_evaluation_politique_amenagement(donnees_politique_fonciere)
    donnees_politique_fonciere = ajouter_sau_par_habitant(donnees_politique_fonciere)
    donnees_politique_fonciere = ajouter_rythme_artificialisation_sau(donnees_politique_fonciere)
    donnees_politique_fonciere = _ajouter_note(donnees_politique_fonciere)

    exporter_df_indicateurs_par_territoires(
        donnees_politique_fonciere.drop(
            columns=["superficie_ha", "sau_productive_ha", "population_totale_2017", "annee_dernier_mouvement_commune"]
            + (IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX)
        ),
        chemin_dossier_output,
        "politique_fonciere",
    )
    exporter_histogrammes(
        donnees_politique_fonciere,
        [
            "note",
        ],
        chemin_dossier_output / "diagrammes",
    )


def _ajouter_note(
    donnees_politique_fonciere: DataFrame,
) -> DataFrame:
    # On fait un astype préalable sinon pandas mélange des pandas.NA et des numpy.nan dans le df résultat (à éviter !)
    donnees_politique_fonciere.loc[:, "note"] = (
        (
            donnees_politique_fonciere["sau_par_habitant_note"].astype("float64") + donnees_politique_fonciere["rythme_artificialisation_sau_note"]
        ).astype("float64")
        / 2
    ).round(0)

    donnees_politique_fonciere["note"] = donnees_politique_fonciere["note"].astype(pd.Int64Dtype())

    return donnees_politique_fonciere


def _calculer_donnees_politique_fonciere(territoires: DataFrame, sources_politique_fonciere: DictDataFrames) -> DataFrame:
    colonnes = (
        ["nom_territoire", "categorie_territoire"]
        + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
        + [
            "annee_dernier_mouvement_commune",
            "superficie_ha",
            "sau_productive_ha",
            "population_totale_2017",
        ]
    )
    donnees_politique_fonciere = territoires[colonnes]

    donnees_politique_fonciere = _ajout_sources_donnees(donnees_politique_fonciere, sources_politique_fonciere)
    donnees_politique_fonciere = _traiter_mouvements_communes(donnees_politique_fonciere)

    index_insertion = donnees_politique_fonciere.columns.get_loc("evolution_emplois_2013_2018") + 1  # type: ignore # erreur depuis l'upgrade de mypy vers 1.11.2
    donnees_menages_emplois = donnees_politique_fonciere["evolution_menages_2013_2018"] + donnees_politique_fonciere["evolution_emplois_2013_2018"]
    donnees_politique_fonciere.insert(index_insertion, "evolution_menages_emplois_2013_2018", donnees_menages_emplois)  # type: ignore # erreur depuis l'upgrade de mypy vers 1.11.2

    donnees_politique_fonciere = _ajouter_donnees_supracommunales(donnees_politique_fonciere)

    donnees_politique_fonciere["artificialisation_2013_2018_sur_superficie_pourcent"] = (
        donnees_politique_fonciere["artificialisation_2013_2018_ha"] / donnees_politique_fonciere["superficie_ha"] * 100
    )

    donnees_politique_fonciere["part_logements_vacants_2013_pourcent"] = round(
        donnees_politique_fonciere["nb_logements_vacants_2013"].astype("float64")
        / donnees_politique_fonciere["nb_logements_2013"].astype("float64")
        * 100,
        1,
    )

    donnees_politique_fonciere["part_logements_vacants_2018_pourcent"] = round(
        donnees_politique_fonciere["nb_logements_vacants_2018"].astype("float64")
        / donnees_politique_fonciere["nb_logements_2018"].astype("float64")
        * 100,
        1,
    )

    return donnees_politique_fonciere


def _ajout_sources_donnees(donnees_politique_fonciere: DataFrame, sources_politique_fonciere: DictDataFrames) -> DataFrame:
    donnees_artificialisation = (
        sources_politique_fonciere["artificialisation"].rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")
    )

    donnees_politique_fonciere = donnees_politique_fonciere.join(donnees_artificialisation)

    donnees_nb_logements = sources_politique_fonciere["nb_logements"].rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")
    donnees_nb_logements_vacants = (
        sources_politique_fonciere["nb_logements_vacants"].rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")
    )
    donnees_politique_fonciere = donnees_politique_fonciere.join(donnees_nb_logements.loc[:, slice("nb_logements_2013", "nb_logements_2018")])
    donnees_politique_fonciere = donnees_politique_fonciere.join(
        donnees_nb_logements_vacants.loc[:, slice("nb_logements_vacants_2013", "nb_logements_vacants_2018")]
    )

    return donnees_politique_fonciere


def _traiter_mouvements_communes(donnees_politique_fonciere: DataFrame) -> DataFrame:
    for colonne in [
        "artificialisation_2010_2015_ha",
        "artificialisation_2015_2020_ha",
        "artificialisation_2013_2018_ha",
        "evolution_menages_2013_2018",
        "evolution_emplois_2013_2018",
    ]:
        donnees_politique_fonciere.loc[
            donnees_politique_fonciere.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_ARTIFICIALISATION,
            colonne,
        ] = np.nan

    for colonne in ["nb_logements_2013", "nb_logements_2018", "nb_logements_vacants_2013", "nb_logements_vacants_2018"]:
        donnees_politique_fonciere.loc[
            donnees_politique_fonciere.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_NB_LOGEMENTS,
            colonne,
        ] = np.nan

    return donnees_politique_fonciere


def _ajouter_donnees_supracommunales(
    donnees_politique_fonciere: DataFrame,
) -> DataFrame:
    colonnes = (
        "artificialisation_2010_2015_ha",
        "artificialisation_2015_2020_ha",
        "artificialisation_2013_2018_ha",
        "evolution_menages_2013_2018",
        "evolution_emplois_2013_2018",
        "evolution_menages_emplois_2013_2018",
        "nb_logements_2013",
        "nb_logements_2018",
        "nb_logements_vacants_2013",
        "nb_logements_vacants_2018",
    )

    for colonne in colonnes:
        donnees_politique_fonciere = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(donnees_politique_fonciere, colonne)

    return donnees_politique_fonciere


def _charger_sources(
    chemin_fichier_artificialisation: Path,
    chemin_fichier_nb_logements: Path,
    chemin_fichier_nb_logements_vacants: Path,
) -> DictDataFrames:
    artificialisation = chargeur_artificialisation.charger(chemin_fichier_artificialisation)
    nb_logements = chargeur_logements.charger(chemin_fichier_nb_logements, "nb_logements")
    nb_logements_vacants = chargeur_logements.charger(chemin_fichier_nb_logements_vacants, "nb_logements_vacants")

    return {
        "artificialisation": artificialisation,
        "nb_logements": nb_logements,
        "nb_logements_vacants": nb_logements_vacants,
    }
