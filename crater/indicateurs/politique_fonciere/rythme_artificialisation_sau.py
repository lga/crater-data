from pandas import DataFrame

from crater.commun.calculs.notes_et_messages import calculer_note_par_interpolation_2_seuils
from crater.config.config_globale import ID_FRANCE


def ajouter_rythme_artificialisation_sau(
    donnees_politique_fonciere: DataFrame,
) -> DataFrame:
    donnees_politique_fonciere.loc[
        donnees_politique_fonciere["artificialisation_2013_2018_ha"] == 0,
        "rythme_artificialisation_sau_pourcent",
    ] = 0
    mask_donnees = donnees_politique_fonciere["sau_productive_ha"] > 0
    donnees_politique_fonciere.loc[mask_donnees, "rythme_artificialisation_sau_pourcent"] = (
        donnees_politique_fonciere.loc[mask_donnees, "artificialisation_2013_2018_ha"]
        / donnees_politique_fonciere.loc[mask_donnees, "sau_productive_ha"]
        * 100
    ).round(2)

    if donnees_politique_fonciere.query(f"id_territoire=='{ID_FRANCE}'")["rythme_artificialisation_sau_pourcent"][0] <= 0:
        raise ValueError("Attention, la valeur moyenne française pose problème vu la définition de calcul de la note !")

    donnees_politique_fonciere.loc[:, "rythme_artificialisation_sau_note"] = calculer_note_par_interpolation_2_seuils(
        donnees_politique_fonciere["rythme_artificialisation_sau_pourcent"],
        0,
        10,
        donnees_politique_fonciere.query(f"id_territoire=='{ID_FRANCE}'")["rythme_artificialisation_sau_pourcent"][0],
        0,
    )

    return donnees_politique_fonciere
