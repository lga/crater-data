import unittest

import numpy as np
import pandas as pd

from crater.indicateurs.politique_fonciere.calculateur_politique_fonciere import (
    _ajouter_note,
)
from crater.indicateurs.politique_fonciere.evaluation_politique_amenagement import (
    ajouter_evaluation_politique_amenagement,
)
from crater.indicateurs.politique_fonciere.parametres import (
    SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE,
    CODE_DONNEES_NON_DISPONIBLES,
    CODE_ARTIFICIALISATION_QUASI_NULLE_OU_NEGATIVE,
    CODE_ARTIFICIALISATION_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE,
    CODE_ARTIFICIALISATION_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE,
)
from crater.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas

augmenter_nombre_colonnes_affichees_pandas()

ARTIFICIALISATION_HA_POSITIVE = SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE + 1000


class TestUnitairePolitiqueFonciere(unittest.TestCase):
    def test_note_politique_fonciere(self):
        # given
        df_politique_fonciere = pd.DataFrame(
            columns=["sau_par_habitant_note", "rythme_artificialisation_sau_note"],
            data=[[9, 2], [np.nan, 2], [9, np.nan]],
        )
        resultat_attendu = pd.Series(data=[6, np.nan, np.nan], dtype="Int64")
        # when
        df_resultat = _ajouter_note(df_politique_fonciere)
        # then
        self.assertEqual(resultat_attendu.tolist(), df_resultat["note"].tolist())
        self.assertEqual(resultat_attendu.dtype, df_resultat["note"].dtype)

    def test_evaluation_artificialisation_politique_fonciere(self):
        # given
        df_politique_fonciere = pd.DataFrame(
            columns=[
                "evolution_menages_emplois_2013_2018",
                "artificialisation_2013_2018_ha",
            ],
            data=[
                [
                    1,
                    np.nan,
                ],
                [
                    np.nan,
                    SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE,
                ],
                [
                    10,
                    SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE + 1,
                ],
                [
                    -10,
                    SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE + 1,
                ],
            ],
        )

        # when
        df_resultat = ajouter_evaluation_politique_amenagement(df_politique_fonciere)

        # then
        resultat_attendu = pd.Series(
            data=[
                CODE_DONNEES_NON_DISPONIBLES,
                CODE_ARTIFICIALISATION_QUASI_NULLE_OU_NEGATIVE,
                CODE_ARTIFICIALISATION_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE,
                CODE_ARTIFICIALISATION_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE,
            ],
            dtype=("str"),
        )

        self.assertEqual(
            resultat_attendu.tolist(),
            df_resultat["evaluation_politique_amenagement"].tolist(),
        )
        self.assertEqual(
            resultat_attendu.dtype,
            df_resultat["evaluation_politique_amenagement"].dtype,
        )


if __name__ == "__main__":
    unittest.main()
