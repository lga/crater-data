import unittest
from pathlib import Path

from crater.indicateurs.politique_fonciere.calculateur_politique_fonciere import (
    calculer_politique_fonciere,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestPolitiqueFonciere(unittest.TestCase):
    def test_politique_fonciere(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # when
        calculer_politique_fonciere(
            territoires,
            CHEMIN_INPUT_DATA / "population" / "population.csv",
            CHEMIN_INPUT_DATA / "occupation_sols" / "occupation_sols.csv",
            CHEMIN_INPUT_DATA / "artificialisation_2010_2020.csv",
            CHEMIN_INPUT_DATA / "nombre_de_logements.xlsx",
            CHEMIN_INPUT_DATA / "nombre_de_logements_vacants.xlsx",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "politique_fonciere.csv",
            CHEMIN_OUTPUT_DATA / "politique_fonciere.csv",
        )


if __name__ == "__main__":
    unittest.main()
