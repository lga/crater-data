import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.logger import log


def charger(chemin_fichier):
    log.info("   => chargement artificialisation depuis %s", chemin_fichier)

    df = (
        pd.read_csv(
            chemin_fichier,
            sep=";",
            dtype={
                "idcom": "str",
                "naf10art11": "float",
                "naf11art12": "float",
                "naf12art13": "float",
                "naf13art14": "float",
                "naf14art15": "float",
                "naf15art16": "float",
                "naf16art17": "float",
                "naf17art18": "float",
                "naf18art19": "float",
                "naf19art20": "float",
                "men1318": "Int64",
                "emp1318": "Int64",
            },
        )
        .assign(naf10art15=lambda x: (x["naf10art11"] + x["naf11art12"] + x["naf12art13"] + x["naf13art14"] + x["naf14art15"]) / 1e4)
        .assign(naf13art18=lambda x: (x["naf13art14"] + x["naf14art15"] + x["naf15art16"] + x["naf16art17"] + x["naf17art18"]) / 1e4)
        .assign(naf15art20=lambda x: (x["naf15art16"] + x["naf16art17"] + x["naf17art18"] + x["naf18art19"] + x["naf19art20"]) / 1e4)
        .rename(
            columns={
                "idcom": "id_commune",
                "naf10art15": "artificialisation_2010_2015_ha",
                "naf13art18": "artificialisation_2013_2018_ha",
                "naf15art20": "artificialisation_2015_2020_ha",
                "men1318": "evolution_menages_2013_2018",
                "emp1318": "evolution_emplois_2013_2018",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_commune",
                "artificialisation_2010_2015_ha",
                "artificialisation_2013_2018_ha",
                "artificialisation_2015_2020_ha",
                "evolution_menages_2013_2018",
                "evolution_emplois_2013_2018",
            ]
        )
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_commune")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    return df
