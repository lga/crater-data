from pathlib import Path
from typing import List

import pandas
from pandas import DataFrame

from crater.indicateurs.pesticides.indicateurs_pesticides.calculateur_indicateurs_pesticides import (
    calculer_indicateurs_pesticides,
)
from crater.indicateurs.pesticides.synthese_pesticides.calculateur_synthese_pesticides import calculer_synthese_pesticides
from crater.commun.exports.export_cartes import exporter_cartes
from crater.commun.exports.export_fichier import (
    reinitialiser_dossier,
)
from crater.commun.exports.exporter_diagrammes import exporter_histogrammes
from crater.commun.logger import log

ANNEE_PLUS_RECENTE = 2020
NB_ANNEES_POUR_MOYENNE = 3


def lancer_pipeline_pesticides(
    territoires: DataFrame,
    fichier_occupation_sols: Path,
    classifications_substances_retenues: str,
    chemins_dossiers_bnvd: List[Path],
    chemin_fichier_du_2017: Path,
    chemin_fichier_du_2019: Path,
    chemin_fichier_usages_produits: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### CALCUL PIPELINE PESTICIDES #####")

    reinitialiser_dossier(chemin_dossier_output)

    fichier_pesticides_par_annnees = calculer_indicateurs_pesticides(
        territoires,
        fichier_occupation_sols,
        classifications_substances_retenues,
        chemins_dossiers_bnvd,
        chemin_fichier_du_2017,
        chemin_fichier_du_2019,
        chemin_fichier_usages_produits,
        NB_ANNEES_POUR_MOYENNE,
        chemin_dossier_output,
    )

    fichier_synthese_pesticides = calculer_synthese_pesticides(territoires, fichier_pesticides_par_annnees, ANNEE_PLUS_RECENTE, chemin_dossier_output)

    exporter_diagrammes(chemin_dossier_output, pandas.read_csv(fichier_synthese_pesticides, sep=";"))

    return fichier_synthese_pesticides


def exporter_diagrammes(chemin_dossier_output, df_synthese_pesticides):
    exporter_histogrammes(
        df_synthese_pesticides,
        ["NODU_normalise", "pesticides_note"],
        chemin_dossier_output / "diagrammes",
        bins=100,
    )
    exporter_cartes(
        df_synthese_pesticides,
        ["NODU_normalise", "pesticides_note"],
        ["REGION", "DEPARTEMENT", "EPCI", "COMMUNE"],
        chemin_dossier_output / "cartes",
    )
