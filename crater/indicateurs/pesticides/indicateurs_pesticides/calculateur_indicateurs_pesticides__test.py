import unittest
from pathlib import Path

from crater.indicateurs.pesticides.indicateurs_pesticides.calculateur_indicateurs_pesticides import (
    calculer_indicateurs_pesticides,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")

CLASSIFICATIONS_SUBSTANCES_RETENUES_CMR = "CMR"  # regex


class TestIndicateursPesticides(unittest.TestCase):
    def test_indicateurs_pesticides(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # when
        fichier_pesticides = calculer_indicateurs_pesticides(
            territoires,
            CHEMIN_INPUT_DATA / "occupation_sols" / "occupation_sols.csv",
            CLASSIFICATIONS_SUBSTANCES_RETENUES_CMR,
            [
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_ACHAT_2016",
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_ACHAT_2017",
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_ACHAT_2018",
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_ACHAT_2019",
                CHEMIN_INPUT_DATA / "bnvd" / "BNVD_2020_VENTE_2019",
            ],
            CHEMIN_INPUT_DATA / "extrait_arrete_2017_annexe5.csv",
            CHEMIN_INPUT_DATA / "DU_2016_2019.xlsx",
            CHEMIN_INPUT_DATA / "produits_usages_v3_utf8.csv",
            3,
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "pesticides_par_annees.csv",
            fichier_pesticides,
        )


if __name__ == "__main__":
    unittest.main()
