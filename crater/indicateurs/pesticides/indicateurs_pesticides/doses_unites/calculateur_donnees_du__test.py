import unittest
from pathlib import Path

from crater.indicateurs.pesticides.indicateurs_pesticides.doses_unites.calculateur_donnees_du import calculer_et_exporter_donnees_du
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")

CLASSIFICATIONS_SUBSTANCES_RETENUES_CMR = "CMR"  # regex


class TestCalculateurDonneesDU(unittest.TestCase):
    def test_calculer_du(self):
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        # when
        calculer_et_exporter_donnees_du(
            CHEMIN_INPUT_DATA / "extrait_arrete_2017_annexe5.csv",
            CHEMIN_INPUT_DATA / "DU_2016_2019.xlsx",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "doses_unites.csv",
            CHEMIN_OUTPUT_DATA / "doses_unites.csv",
        )


if __name__ == "__main__":
    unittest.main()
