from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import chronometre, log


@chronometre
def calculer_et_exporter_donnees_du(
    chemin_fichier_du_2017: Path,
    chemin_fichier_du_2019: Path,
    chemin_dossier_output: Path,
) -> DataFrame:
    donnees_du_2019 = _charger_source_du_2016_2018(chemin_fichier_du_2019)
    donnees_du_2019 = _nettoyer_noms_substances(donnees_du_2019)
    donnees_du_2019["dose_unite_kg_par_ha"] = donnees_du_2019["dose_unite_kg_par_ha_2018"]
    donnees_du_2019["annee_dose_unite"] = 2018

    donnees_du_2017 = _charger_source_du_2011_2015(chemin_fichier_du_2017)
    donnees_du_2017 = _nettoyer_noms_substances(donnees_du_2017)
    donnees_du_2017["dose_unite_kg_par_ha"] = donnees_du_2017["dose_unite_kg_par_ha_2015"]
    donnees_du_2017["annee_dose_unite"] = 2015

    donnees_du = pd.concat([donnees_du_2019, donnees_du_2017])
    donnees_du = donnees_du.loc[:, ["substance", "dose_unite_kg_par_ha", "annee_dose_unite"]]
    donnees_du = donnees_du.sort_values("annee_dose_unite", ascending=False).groupby(["substance"]).first().reset_index()
    donnees_du = donnees_du.sort_values("substance")
    donnees_du.to_csv(
        chemin_dossier_output / "doses_unites.csv",
        sep=";",
        index=False,
        float_format="%.9f",
    )

    return donnees_du


def _charger_source_du_2016_2018(chemin_fichier: Path) -> pd.DataFrame:
    log.info("   => chargement Doses Unités de 2016 à 2019 depuis %s", chemin_fichier)

    return pd.read_excel(
        chemin_fichier,
        usecols="A:D",
        names=[
            "substance",
            "dose_unite_kg_par_ha_2016",
            "dose_unite_kg_par_ha_2017",
            "dose_unite_kg_par_ha_2018",
        ],
        na_values=[""],
        dtype={
            "dose_unite_kg_par_ha_2016": "float64",
            "dose_unite_kg_par_ha_2017": "float64",
            "dose_unite_kg_par_ha_2018": "float64",
        },
    )


def _charger_source_du_2011_2015(chemin_fichier: Path) -> pd.DataFrame:
    # Non utilisé pour le moment mais gardé si besoin des DU de l'arrété 2017
    log.info("   => chargement Doses Unités de 2011 à 2015 depuis %s", chemin_fichier)

    return pd.read_csv(
        chemin_fichier,
        sep=";",
        header=3,
        names=[
            "substance",
            "dose_unite_kg_par_ha_2011",
            "dose_unite_kg_par_ha_2012",
            "dose_unite_kg_par_ha_2013",
            "dose_unite_kg_par_ha_2014",
            "dose_unite_kg_par_ha_2015",
        ],
        na_values="-",
        encoding="latin1",
        decimal=",",
        dtype={
            "dose_unite_kg_par_ha_2011": "float64",
            "dose_unite_kg_par_ha_2012": "float64",
            "dose_unite_kg_par_ha_2013": "float64",
            "dose_unite_kg_par_ha_2014": "float64",
            "dose_unite_kg_par_ha_2015": "float64",
        },
    )


def _nettoyer_noms_substances(df: DataFrame) -> DataFrame:
    df["substance"] = df["substance"].str.lower()
    remplacements = {"é": "e", "è": "e", "ê": "e", "à": "a"}
    for char in remplacements.keys():
        df["substance"] = df["substance"].str.replace(char, remplacements[char])

    return df
