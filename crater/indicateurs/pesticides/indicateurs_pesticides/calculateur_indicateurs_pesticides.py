from pathlib import Path

import numpy as np
import pandas as pd
from pandas import DataFrame

from crater.indicateurs.pesticides.indicateurs_pesticides import chargeur_donnees_pesticides
from crater.indicateurs.pesticides.indicateurs_pesticides.calculateur_statistiques import calculer_et_exporter_statistiques
from crater.indicateurs.pesticides.indicateurs_pesticides.doses_unites.calculateur_donnees_du import (
    calculer_et_exporter_donnees_du,
    _nettoyer_noms_substances,
)
from crater.commun.calculs.calcul_indicateurs import calculer_moyenne_glissante
from crater.commun.calculs.outils_dataframes import produit_cartesien
from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_departement_crater,
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    trier_territoires,
)
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.exports.export_fichier import reinitialiser_dossier, exporter_df_indicateurs_par_territoires
from crater.commun.logger import log, chronometre
from crater.config.config_globale import IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX


def calculer_indicateurs_pesticides(
    territoires: DataFrame,
    fichier_occupation_sols: Path,
    classifications_substances_retenues: str,
    chemins_dossiers_bnvd: list[Path],
    chemin_fichier_du_2017: Path,
    chemin_fichier_du_2019: Path,
    chemin_fichier_usages_produits: Path,
    nb_annees_pour_moyenne: int,
    chemin_dossier_output: Path,
) -> Path:

    territoires = charger_et_joindre_df_indicateurs(territoires, fichier_occupation_sols)

    colonnes = (
        ["nom_territoire", "categorie_territoire"]
        + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
        + ["annee_dernier_mouvement_commune", "sau_ra_2020_ha", "codes_postaux_commune"]
    )
    df_pesticides = territoires[colonnes]
    (
        df_pesticides_par_annees,
        nodu_substances,
    ) = _calculer_indicateurs_pesticides_par_annees(
        df_pesticides,
        classifications_substances_retenues,
        chemins_dossiers_bnvd,
        chemin_fichier_du_2017,
        chemin_fichier_du_2019,
        chemin_dossier_output,
    )
    df_pesticides_moyennes_glissantes = _calculer_moyennes_glissantes(df_pesticides_par_annees.drop(columns="NODU_normalise"), nb_annees_pour_moyenne)

    df_pesticides_moyennes_glissantes = _calculer_nodu_normalise(df_pesticides_moyennes_glissantes)

    df_pesticides_moyennes_glissantes = _vider_valeurs_communales(df_pesticides_moyennes_glissantes)
    df_pesticides_moyennes_glissantes = _assigner_nodu_normalise_epci_aux_communes(df_pesticides_moyennes_glissantes)
    df_pesticides_moyennes_glissantes = _assigner_valeurs_commune_paris_au_departement(df_pesticides_moyennes_glissantes)

    fichier_pesticides_par_annees_moyennes_glissantes: Path = exporter_df_indicateurs_par_territoires(
        df_pesticides_moyennes_glissantes.loc[
            :,
            [
                "nom_territoire",
                "categorie_territoire",
                "quantite_substance_sans_du_kg",
                "quantite_substance_avec_du_kg",
                "NODU_ha",
                "NODU_normalise",
            ],
        ].reset_index("annee"),
        chemin_dossier_output,
        "pesticides_par_annees",
        "annee",
    )

    calculer_et_exporter_statistiques(
        nodu_substances,
        chemin_fichier_usages_produits,
        chemin_dossier_output / "statistiques",
    )

    return fichier_pesticides_par_annees_moyennes_glissantes


@chronometre
def _calculer_indicateurs_pesticides_par_annees(
    df_pesticides: DataFrame,
    classifications_substances_retenues: str,
    chemins_dossiers_bnvd: list[Path],
    chemin_fichier_du_2017: Path,
    chemin_fichier_du_2019: Path,
    chemin_dossier_output: Path,
) -> tuple[DataFrame, DataFrame]:
    log.info("   => calcul des indicateurs pesticides")

    reinitialiser_dossier(chemin_dossier_output)

    du = calculer_et_exporter_donnees_du(chemin_fichier_du_2017, chemin_fichier_du_2019, chemin_dossier_output)

    bnvd = _calculer_donnees_bnvd(chemins_dossiers_bnvd)

    nodu_substances = _calculer_nodu_substances(bnvd, du)

    nodu_substances = _ajouter_colonne_substance_retenue(nodu_substances, classifications_substances_retenues)

    nodu_par_communes_et_departements = _calculer_nodu_par_communes_et_departements(df_pesticides, nodu_substances)

    df_pesticides = _ajouter_colonnes_nodu_par_communes_et_departements(df_pesticides, nodu_par_communes_et_departements)
    df_pesticides = _calculer_valeurs_supracommunales(df_pesticides)
    df_pesticides = _calculer_nodu_normalise(df_pesticides)
    df_pesticides = trier_territoires(df_pesticides)

    return df_pesticides, nodu_substances


@chronometre
def _calculer_donnees_bnvd(chemins_dossiers_bnvd: list[Path]) -> DataFrame:
    donnees_bnvd = chargeur_donnees_pesticides.charger_source_bnvd(chemins_dossiers_bnvd)
    donnees_bnvd = _nettoyer_noms_substances(donnees_bnvd)
    return donnees_bnvd


@chronometre
def _calculer_nodu_substances(donnees_bnvd: DataFrame, donnees_du: DataFrame) -> DataFrame:
    donnees_nodu = pd.merge(
        donnees_bnvd,
        donnees_du.loc[:, ["substance", "dose_unite_kg_par_ha"]],
        on="substance",
        how="left",
        validate="many_to_one",
    )
    donnees_nodu["NODU_ha"] = donnees_nodu["quantite_substance_kg"] / donnees_nodu["dose_unite_kg_par_ha"]

    return donnees_nodu


@chronometre
def _ajouter_colonne_substance_retenue(donnees_nodu, classifications_substances_retenues):
    donnees_nodu["substance_retenue"] = False
    donnees_nodu["substance_retenue"].mask(
        donnees_nodu.classification.str.contains(classifications_substances_retenues, regex=True, case=False, na=False),
        True,
        inplace=True,
    )
    return donnees_nodu


@chronometre
def _calculer_nodu_par_communes_et_departements(donnees_pesticides_agricoles: DataFrame, nodu_substances: DataFrame) -> DataFrame:
    nodu_substances_retenues = nodu_substances.loc[nodu_substances.substance_retenue & nodu_substances.fichier_source.str.contains("ACHAT")]
    nodu_substances_retenues = _calculer_quantite_substance_avec_et_sans_du(nodu_substances_retenues)

    nodu_substances_retenues = nodu_substances_retenues.loc[
        :,
        [
            "annee",
            "code_departement",
            "code_postal_acheteur",
            "quantite_substance_sans_du_kg",
            "quantite_substance_avec_du_kg",
            "NODU_ha",
        ],
    ]

    nodu_par_departements = (
        nodu_substances_retenues.groupby(by=["annee", "code_departement"]).sum().reset_index().rename(columns={"code_departement": "id_territoire"})
    )

    nodu_par_departements.id_territoire = traduire_code_insee_vers_id_departement_crater(nodu_par_departements.id_territoire)

    nodu_par_communes = _calculer_donnees_nodu_par_commune_acheteur(nodu_substances_retenues, donnees_pesticides_agricoles)

    return pd.concat([nodu_par_departements, nodu_par_communes]).set_index(["id_territoire", "annee"])


def _calculer_quantite_substance_avec_et_sans_du(nodu_substances):
    nodu_substances = nodu_substances.assign(quantite_substance_sans_du_kg=0)
    nodu_substances = nodu_substances.assign(quantite_substance_avec_du_kg=0)

    nodu_substances["quantite_substance_sans_du_kg"].mask(
        nodu_substances.dose_unite_kg_par_ha.isna(),
        nodu_substances.quantite_substance_kg,
        inplace=True,
    )
    nodu_substances["quantite_substance_avec_du_kg"].mask(
        ~nodu_substances.dose_unite_kg_par_ha.isna(),
        nodu_substances.quantite_substance_kg,
        inplace=True,
    )

    return nodu_substances


@chronometre
def _ajouter_colonnes_nodu_par_communes_et_departements(donnees_pesticides_agricoles: DataFrame, nodu_par_territoires: DataFrame) -> DataFrame:
    # on duplique chaque territoire pour chaque annee
    donnees_pesticides_agricoles = produit_cartesien(
        donnees_pesticides_agricoles.reset_index(),
        nodu_par_territoires.reset_index().loc[:, ["annee"]].drop_duplicates(),
    ).set_index(["id_territoire", "annee"])
    donnees_pesticides_agricoles["quantite_substance_sans_du_kg"] = 0.0
    donnees_pesticides_agricoles["quantite_substance_avec_du_kg"] = 0.0
    donnees_pesticides_agricoles["NODU_ha"] = 0.0
    donnees_pesticides_agricoles.update(nodu_par_territoires)
    return donnees_pesticides_agricoles


def _calculer_repartition_codes_postaux_dans_communes(
    territoires: DataFrame,
) -> DataFrame:
    df = territoires.loc[
        territoires.categorie_territoire == "COMMUNE",
        ["codes_postaux_commune", "sau_ra_2020_ha"],
    ].reset_index()
    df = df[df.codes_postaux_commune.notnull()]
    df.codes_postaux_commune = df.codes_postaux_commune.str.split("|")
    df = df.explode("codes_postaux_commune")
    df = df.rename(columns={"codes_postaux_commune": "code_postal_commune"})
    # on ajoute un tout petit peu de SAU pour gérer le cas où un code postal est composé de communes sans sau
    df["sau_ra_2020_ha"] += 0.001
    df2 = (
        df.loc[:, ["code_postal_commune", "sau_ra_2020_ha"]]
        .groupby("code_postal_commune")
        .sum()
        .rename(columns={"sau_ra_2020_ha": "sau_totale_par_code_postal_ha"})
    )
    df = df.join(df2, on="code_postal_commune", how="left")
    df["cle_repartition"] = round(df["sau_ra_2020_ha"] / df["sau_totale_par_code_postal_ha"], 5)

    return df.reset_index(drop=True).drop(columns=["sau_totale_par_code_postal_ha"])


def _calculer_donnees_nodu_par_commune_acheteur(nodu_par_code_postal: DataFrame, df_pesticides_agricoles: DataFrame) -> DataFrame:
    repartition_codes_postaux_dans_communes = _calculer_repartition_codes_postaux_dans_communes(df_pesticides_agricoles)

    nodu_par_communes = nodu_par_code_postal.groupby(by=["annee", "code_postal_acheteur"]).sum().reset_index()

    nodu_par_communes = nodu_par_communes.merge(
        repartition_codes_postaux_dans_communes.loc[:, ["id_territoire", "code_postal_commune", "cle_repartition"]],
        left_on="code_postal_acheteur",
        right_on="code_postal_commune",
        how="left",
    )
    nodu_par_communes["quantite_substance_sans_du_kg"] *= nodu_par_communes["cle_repartition"]
    nodu_par_communes["quantite_substance_avec_du_kg"] *= nodu_par_communes["cle_repartition"]
    nodu_par_communes["NODU_ha"] *= nodu_par_communes["cle_repartition"]
    nodu_par_communes.drop(columns=["cle_repartition"], inplace=True)
    nodu_par_communes = nodu_par_communes.groupby(by=["id_territoire", "annee"]).sum().reset_index()

    return nodu_par_communes


@chronometre
def _calculer_valeurs_supracommunales(donnees_pesticides_agricoles):
    for colonne in ["quantite_substance_sans_du_kg", "quantite_substance_avec_du_kg", "NODU_ha"]:
        donnees_pesticides_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            donnees_pesticides_agricoles,
            colonne,
            "COMMUNE",
            ["REGROUPEMENT_COMMUNES", "EPCI"],
        )
        donnees_pesticides_agricoles = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            donnees_pesticides_agricoles,
            colonne,
            "DEPARTEMENT",
            ["REGION", "PAYS"],
        )
    return donnees_pesticides_agricoles


@chronometre
def _calculer_nodu_normalise(df_pesticides):
    df_pesticides["NODU_normalise"] = df_pesticides["NODU_ha"] / df_pesticides["sau_ra_2020_ha"]
    df_pesticides["NODU_normalise"] = df_pesticides["NODU_normalise"].replace([np.inf, -np.inf], np.nan)
    df_pesticides.loc[df_pesticides["NODU_ha"] == 0, "NODU_normalise"] = 0
    return df_pesticides


def _calculer_moyennes_glissantes(df_pesticides: DataFrame, nb_annees_pour_moyenne: int) -> DataFrame:
    liste_champs_indicateurs = [
        "quantite_substance_sans_du_kg",
        "quantite_substance_avec_du_kg",
        "NODU_ha",
    ]
    indicateurs_pesticides_moyennes_glissantes = calculer_moyenne_glissante(
        df_pesticides.loc[:, liste_champs_indicateurs], "annee", nb_annees_pour_moyenne
    )
    masque_colonnes: list[str] = df_pesticides.columns.drop(liste_champs_indicateurs).tolist()
    return df_pesticides.loc[:, masque_colonnes].join(indicateurs_pesticides_moyennes_glissantes, how="right")


def _vider_valeurs_communales(df_pesticides: DataFrame) -> DataFrame:
    df_pesticides = df_pesticides.copy()
    mask = df_pesticides.categorie_territoire == "COMMUNE"
    df_pesticides.loc[mask, "quantite_substance_sans_du_kg"] = np.nan
    df_pesticides.loc[mask, "quantite_substance_avec_du_kg"] = np.nan
    df_pesticides.loc[mask, "NODU_ha"] = np.nan
    df_pesticides.loc[mask, "NODU_normalise"] = np.nan
    return df_pesticides


def _assigner_nodu_normalise_epci_aux_communes(df_pesticides: DataFrame) -> DataFrame:
    df_pesticides.update(
        pd.merge(
            df_pesticides.loc[:, "id_epci"].reset_index(),
            df_pesticides.loc[:, "NODU_normalise"].reset_index().rename(columns={"id_territoire": "id_epci"}),
            on=("id_epci", "annee"),
            how="inner",
        ).set_index(["id_territoire", "annee"])
    )
    return df_pesticides


def _assigner_valeurs_commune_paris_au_departement(df_pesticides: DataFrame) -> DataFrame:
    for colonne in ["quantite_substance_sans_du_kg", "quantite_substance_avec_du_kg", "NODU_ha", "NODU_normalise"]:
        # mypy génère une erreur de type, mais le code fonctionne (TODO : voir si on peut l'écrire dans une forme qui lui convienne)
        df_pesticides.loc[("D-75",), colonne] = df_pesticides.loc[("C-75056",), colonne].to_list()  # type: ignore
    return df_pesticides
