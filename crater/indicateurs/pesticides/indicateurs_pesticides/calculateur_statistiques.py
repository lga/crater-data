from pathlib import Path
from pandas import DataFrame

from crater.indicateurs.pesticides.indicateurs_pesticides import chargeur_donnees_pesticides
from crater.commun.exports.export_fichier import reinitialiser_dossier


def calculer_et_exporter_statistiques(
    nodu_substances: DataFrame,
    chemin_fichier_usages_produits: Path,
    chemin_dossier_output: Path,
) -> None:
    reinitialiser_dossier(chemin_dossier_output)

    usages_produits = _calculer_et_exporter_usages_produits(chemin_fichier_usages_produits, chemin_dossier_output)

    donnees_nodu = nodu_substances.copy()

    donnees_nodu["fichier_source"].mask(
        donnees_nodu.fichier_source == "DPT",
        "DPT-" + donnees_nodu.code_departement,
        inplace=True,
    )

    donnees_nodu["du_manquant"] = False
    donnees_nodu.loc[
        donnees_nodu.dose_unite_kg_par_ha.isnull() & ~donnees_nodu.quantite_substance_kg.isnull(),
        "du_manquant",
    ] = True
    donnees_nodu.loc[donnees_nodu.du_manquant, "quantite_substance_sans_du_kg"] = donnees_nodu.loc[donnees_nodu.du_manquant, "quantite_substance_kg"]
    donnees_nodu["du_present"] = ~donnees_nodu["du_manquant"]

    donnees_nodu_achats = donnees_nodu.loc[donnees_nodu.fichier_source.str.contains("ACHAT")]

    donnees_nodu_achats_avec_usages = donnees_nodu_achats.merge(usages_produits, on="amm", how="left")
    donnees_nodu_achats_avec_usages["usage_manquant"] = False
    donnees_nodu_achats_avec_usages.loc[donnees_nodu_achats_avec_usages["produit"].isnull(), "usage_manquant"] = True
    donnees_nodu_achats_avec_usages["usage_present"] = ~donnees_nodu_achats_avec_usages["usage_manquant"]

    _exporter_statistiques(donnees_nodu, chemin_dossier_output)
    _exporter_statistiques_par_departements(donnees_nodu, chemin_dossier_output)
    _exporter_statistiques_codes_postaux(donnees_nodu_achats, chemin_dossier_output)
    _exporter_classement_substances(donnees_nodu, chemin_dossier_output)

    _exporter_statistiques_substances_sans_du(donnees_nodu_achats, chemin_dossier_output)
    _exporter_substances_sans_du(donnees_nodu_achats, chemin_dossier_output)

    _exporter_statistiques_usages(donnees_nodu_achats_avec_usages, chemin_dossier_output)
    _exporter_substances_sans_usages(donnees_nodu_achats_avec_usages, chemin_dossier_output)


def _calculer_et_exporter_usages_produits(chemin_fichier_usages_produits: Path, chemin_dossier_output: Path) -> DataFrame:
    usages_produits = chargeur_donnees_pesticides.charger_usages_produits(chemin_fichier_usages_produits)
    usages_produits["usage_culture"] = usages_produits["usage"].str.split("*").str[0]
    usages_produits["usage_traitement"] = usages_produits["usage"].str.split("*").str[1]
    usages_produits["usage_cible"] = usages_produits["usage"].str.split("*").str[2]

    USAGES_CULTURES_HORS_AGRICOLE = [
        "Adjuvants",
        "Arbres et arbustes",
        "Gazons de graminées",
        "Jardin d'amateur",
        "Plantes d'intérieur",
        "Plantes d'intérieur et balcons",
    ]
    USAGES_TRAITEMENTS_HORS_AGRICOLE = ["Trt bois abattus", "Trt Troncs Charp. Branch."]
    USAGES_CIBLES_HORS_AGRICOLE = [
        "All. PJT, Abords non plant. (1)",
        "All. PJT, Cimet., Voies (1)",
        "Bord. Plans d'eau",
        "Locx Ordures Déchets",
        "Locx Struct. Matér. (POA)",
        "Locx Struct. Matér. (POV...)",
        "Matér. Transp. Ordures Déchets",
        "Sites Indust. (1)",
        "Voies ferrées",
    ]

    USAGES_CIBLES_SNCF = ["Voies ferrées"]

    USAGES_TRAITEMENTS_POST_RECOLTE = ["Trt Prod. Réc.", "Trt Prod.Réc."]

    usages_produits["usage_agricole"] = False
    usages_produits["usage_hors_agricole"] = False
    usages_produits["usage_sncf"] = False
    usages_produits["usage_post_recolte"] = False

    usages_produits["usage_hors_agricole"].mask(
        usages_produits["usage_culture"].isin(USAGES_CULTURES_HORS_AGRICOLE)
        | usages_produits["usage_traitement"].isin(USAGES_TRAITEMENTS_HORS_AGRICOLE)
        | usages_produits["usage_cible"].isin(USAGES_CIBLES_HORS_AGRICOLE),
        True,
        inplace=True,
    )
    usages_produits["usage_agricole"] = ~usages_produits["usage_hors_agricole"]

    usages_produits["usage_sncf"].mask(usages_produits["usage_cible"].isin(USAGES_CIBLES_SNCF), True, inplace=True)

    usages_produits["usage_post_recolte"].mask(
        usages_produits["usage_traitement"].isin(USAGES_TRAITEMENTS_POST_RECOLTE),
        True,
        inplace=True,
    )

    usages_produits["usage"] = True
    usages_produits = usages_produits.groupby(by=["amm", "produit"]).sum().reset_index()
    for i in [
        "usage_agricole",
        "usage_hors_agricole",
        "usage_sncf",
        "usage_post_recolte",
    ]:
        usages_produits[i] = round(usages_produits[i] / usages_produits["usage"] * 100, 0)
    usages_produits.drop(columns=["usage", "usage_culture", "usage_traitement", "usage_cible"], inplace=True)

    usages_produits.to_csv(
        chemin_dossier_output / "usages_produits.csv",
        sep=";",
        index=False,
        float_format="%.2f",
    )

    return usages_produits


def _exporter_statistiques_usages(donnees_nodu_avec_usages, chemin_dossier_output) -> None:
    donnees_nodu_avec_usages.reset_index().loc[:, ["annee", "amm", "produit", "usage_present", "usage_manquant"]].drop_duplicates().groupby(
        by=["annee"], dropna=False
    ).sum().reset_index().loc[:, ["annee", "usage_present", "usage_manquant"]].to_csv(
        chemin_dossier_output / "produits_sans_usages_statistiques.csv",
        sep=";",
        index=False,
    )

    donnees_nodu_avec_usages["usage"] = "agricole uniquement"
    donnees_nodu_avec_usages["usage"].mask(donnees_nodu_avec_usages.usage_hors_agricole > 0, "mixte", inplace=True)
    donnees_nodu_avec_usages["usage"].mask(
        donnees_nodu_avec_usages.usage_hors_agricole == 100,
        "hors agricole uniquement",
        inplace=True,
    )
    donnees_nodu_avec_usages["usage"].mask(donnees_nodu_avec_usages.usage_manquant, "manquant", inplace=True)
    donnees_nodu_avec_usages.query('fichier_source == "ACHAT_FR"').reset_index().loc[
        :,
        [
            "annee",
            "usage",
            "quantite_substance_kg",
            "NODU_ha",
            "quantite_substance_sans_du_kg",
        ],
    ].drop_duplicates().groupby(by=["annee", "usage"], dropna=False).sum().reset_index().to_csv(
        chemin_dossier_output / "produits_quantites_sans_usages_statistiques.csv",
        sep=";",
        index=False,
        float_format="%.2f",
    )


def _exporter_substances_sans_usages(donnees_nodu_avec_usages, chemin_dossier_output) -> None:
    donnees_nodu_avec_usages.loc[donnees_nodu_avec_usages.usage_manquant, ["amm"]].drop_duplicates().reset_index(drop=True).sort_values(
        ["amm"]
    ).to_csv(
        chemin_dossier_output / "produits_sans_usages.csv",
        sep=";",
        index=False,
        float_format="%.2f",
    )


def _exporter_statistiques_substances_sans_du(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu.reset_index().loc[:, ["annee", "substance", "cas", "classification", "du_present", "du_manquant"]].drop_duplicates().groupby(
        by=["annee", "classification"], dropna=False
    ).sum().reset_index().loc[:, ["annee", "classification", "du_present", "du_manquant"]].to_csv(
        chemin_dossier_output / "substances_sans_du_statistiques.csv",
        sep=";",
        index=False,
    )


def _exporter_substances_sans_du(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu.loc[donnees_nodu.du_manquant].reset_index()[["substance", "cas", "classification", "annee"]].drop_duplicates().groupby(
        by=["substance", "cas"], dropna=False
    ).agg(
        {
            "classification": lambda x: " | ".join(x.unique()),
            "annee": lambda x: " | ".join(sorted((str(i) for i in x))),
        }
    ).reset_index().sort_values(
        ["substance", "classification"]
    ).to_csv(
        chemin_dossier_output / "substances_sans_du.csv", sep=";", index=False
    )


def _exporter_classement_substances(donnees_nodu, chemin_dossier_output) -> None:
    classements_substances = (
        donnees_nodu[
            [
                "fichier_source",
                "annee",
                "substance",
                "classification",
                "quantite_substance_kg",
                "NODU_ha",
                "quantite_substance_sans_du_kg",
            ]
        ]
        .groupby(by=["fichier_source", "annee", "substance", "classification"], dropna=False)
        .sum()
    )
    classements_substances["rang_quantite"] = classements_substances.groupby(by=["fichier_source", "annee"])["quantite_substance_kg"].rank(
        method="min", ascending=False
    )
    classements_substances["rang_NODU"] = classements_substances.groupby(by=["fichier_source", "annee"])["NODU_ha"].rank(
        method="min", ascending=False
    )
    classements_substances.loc[(classements_substances.rang_quantite <= 10) | (classements_substances.rang_NODU <= 10)].to_csv(
        chemin_dossier_output / "resultats_classement_substances_top10.csv",
        sep=";",
        float_format="%.2f",
    )
    classements_substances.query("annee == 2020").to_csv(
        chemin_dossier_output / "resultats_classement_substances_complet_2020.csv",
        sep=";",
        float_format="%.2f",
    )


def _exporter_statistiques(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu[
        [
            "fichier_source",
            "annee",
            "classification",
            "quantite_substance_kg",
            "NODU_ha",
            "quantite_substance_sans_du_kg",
        ]
    ].groupby(by=["fichier_source", "annee", "classification"], dropna=False).sum().to_csv(
        chemin_dossier_output / "resultats_statistiques.csv",
        sep=";",
        float_format="%.2f",
    )


def _exporter_statistiques_par_departements(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu.loc[donnees_nodu.fichier_source.str.contains("DPT")][
        [
            "fichier_source",
            "annee",
            "code_departement",
            "classification",
            "quantite_substance_kg",
            "NODU_ha",
            "quantite_substance_sans_du_kg",
        ]
    ].groupby(
        by=["fichier_source", "code_departement", "annee", "classification"],
        dropna=False,
    ).sum().to_csv(
        chemin_dossier_output / "resultats_statistiques_par_departements.csv",
        sep=";",
        float_format="%.2f",
    )


def _exporter_statistiques_codes_postaux(donnees_nodu, chemin_dossier_output) -> None:
    donnees_nodu = donnees_nodu.assign(code_postal_acheteur_categorie="présent")
    donnees_nodu.loc[donnees_nodu["code_postal_acheteur"].isnull(), "code_postal_acheteur_categorie"] = "NA"
    donnees_nodu.loc[
        (donnees_nodu["code_postal_acheteur"] == "00000"),
        "code_postal_acheteur_categorie",
    ] = "00000"

    donnees_nodu[
        [
            "fichier_source",
            "annee",
            "code_postal_acheteur_categorie",
            "classification",
            "quantite_substance_kg",
            "NODU_ha",
            "quantite_substance_sans_du_kg",
        ]
    ].groupby(
        by=[
            "fichier_source",
            "annee",
            "code_postal_acheteur_categorie",
            "classification",
        ],
        dropna=False,
    ).sum().to_csv(
        chemin_dossier_output / "resultats_statistiques_codes_postaux.csv",
        sep=";",
        float_format="%.2f",
    )
