from pathlib import Path
import pandas as pd
from typing import TypedDict

from crater.commun.logger import log


def charger_source_bnvd(chemins_dossiers_bnvd: list[Path]) -> pd.DataFrame:
    # NOTA BENE
    # + les données BNVD sont composées de fichiers donnant les achats/ventes de produits et de substances. On regarde que les substances.
    # + il y a un dossier par année
    # + il y a 3 types de fichiers :
    #   + un fichier pour toute la France
    #   + un fichier par département avec un code département, potentiellement INDETERMINEE (= 00)
    #   + pour les achats, un fichier par région donnant le détail par code postal d'achat avec :
    #     + un code postal bien renseigné et des infos disponibles
    #     + un code postal bien renseigné et pas d'infos (valeurs = nc)
    #     (+ pour les données de 2020 : un code postal inconnu (= 00000) mais des infos bien renseignées)
    #     + un fichier spécifique pour une région dite INDETERMINEE avec les codes postaux = 00000
    # + chaque substance a une classification (eg. CMR, Autre, nc, etc.)

    df = pd.DataFrame()

    for dossier in chemins_dossiers_bnvd:
        log.info("   => chargement BNVD depuis %s", dossier)
        fichiers = [x for x in dossier.glob("*.csv") if x.is_file()]
        fichiers_a_charger = _extraire_fichiers_a_charger(fichiers)
        for fichier in fichiers_a_charger:
            df_fichier = pd.read_csv(
                fichier["chemin_fichier_substance"],
                sep=";",
                na_values=["", "nc", "NC"],
                dtype={
                    "annee": "int64",
                    "code_postal_acheteur": "str",
                    "code_departement": "str",
                    "amm": "str",
                    "quantite_substance": "float64",
                },
            ).rename(columns={"quantite_substance": "quantite_substance_kg"})
            df_fichier["fichier_source"] = fichier["fichier_source"]

            df = pd.concat([df, df_fichier])

    return df


class DictFichierSubstances(TypedDict):
    chemin_fichier_substance: Path
    fichier_source: str


def _extraire_fichiers_a_charger(fichiers: list[Path]) -> list[DictFichierSubstances]:
    fichiers_a_extraire: list[DictFichierSubstances] = []
    for chemin_fichier in fichiers:
        if "_ACHAT_" in chemin_fichier.name:
            type_fichier = "ACHAT"
        elif "_VENTE_" in chemin_fichier.name:
            type_fichier = "VENTE"
        if "_SUBSTANCE_" in chemin_fichier.name:
            if "_CP_" in chemin_fichier.name:
                nom_region = chemin_fichier.name.replace(".csv", "").split("_SUBSTANCE_")[1].split("_")[0]
                fichiers_a_extraire.append(
                    {
                        "chemin_fichier_substance": chemin_fichier,
                        "fichier_source": type_fichier + "_" + nom_region,
                    }
                )
            elif "_DPT_" in chemin_fichier.name:  # fichiers départements
                fichiers_a_extraire.append(
                    {
                        "chemin_fichier_substance": chemin_fichier,
                        "fichier_source": type_fichier + "_" + "DPT",
                    }
                )
            elif "_FR_" in chemin_fichier.name:  # fichier France
                fichiers_a_extraire.append(
                    {
                        "chemin_fichier_substance": chemin_fichier,
                        "fichier_source": type_fichier + "_" + "FR",
                    }
                )
    return fichiers_a_extraire


def charger_usages_produits(chemin_fichier: Path) -> pd.DataFrame:
    log.info("   => chargement des usages des produits depuis %s", chemin_fichier)

    df = pd.read_csv(
        chemin_fichier,
        sep=";",
        usecols=[0, 1, 2],
        dtype={"numero AMM": "str", "nom produit": "str", "identifiant usage": "str"},
        na_values=[""],
    ).rename(
        columns={
            "numero AMM": "amm",
            "nom produit": "produit",
            "identifiant usage": "usage",
        },
        errors="raise",
    )

    df.loc[:, "amm"] = df["amm"].str.replace(" ", "", regex=False).str.replace("+", " + ", regex=False)

    return df
