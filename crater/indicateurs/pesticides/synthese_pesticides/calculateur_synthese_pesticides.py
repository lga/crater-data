from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.calculs.notes_et_messages import calculer_note_par_interpolation_n_seuils
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires


def calculer_synthese_pesticides(
    territoire: DataFrame, chemin_pesticides_par_annees: Path, annee_plus_recente: int, chemin_dossier_output: Path
) -> Path:
    df_pesticides_par_annees = pandas.read_csv(chemin_pesticides_par_annees, sep=";")

    df_pesticides_annee_plus_recente = df_pesticides_par_annees.loc[df_pesticides_par_annees.annee == annee_plus_recente]
    df_pesticides_annee_plus_recente = df_pesticides_annee_plus_recente.set_index("id_territoire")

    df_synthese_pesticides = territoire.loc[:, ["nom_territoire", "categorie_territoire"]].copy()
    df_synthese_pesticides = df_synthese_pesticides.join(df_pesticides_annee_plus_recente.loc[:, ["NODU_normalise"]])
    df_synthese_pesticides = _ajouter_colonne_note(df_synthese_pesticides)

    fichier_resultat = exporter_df_indicateurs_par_territoires(
        df_synthese_pesticides,
        chemin_dossier_output,
        "synthese_pesticides",
    )

    return fichier_resultat


def _ajouter_colonne_note(df_pesticides: DataFrame) -> DataFrame:
    seuils = df_pesticides.loc[df_pesticides["categorie_territoire"] == "DEPARTEMENT", "NODU_normalise"].quantile([1 / 4, 1 / 2, 3 / 4])
    # On prend comme seuil pour la note 0, le 3e quartile et au minimum 2 si jamais Q3 < 2 ce qui n'arrive pas en pratique
    seuil_note_0 = max(seuils.values[2], 2)
    df_pesticides["pesticides_note"] = calculer_note_par_interpolation_n_seuils(df_pesticides["NODU_normalise"], [0, 1, seuil_note_0], [10, 5, 0])

    return df_pesticides
