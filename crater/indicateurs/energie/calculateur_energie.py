from pathlib import Path

from pandas import DataFrame

from crater.indicateurs.energie.postes.calculateur_energie_postes import (
    calculer_postes_energie,
)
from crater.indicateurs.energie.synthese.calculateur_synthese_energie import (
    calculer_synthese_energie,
)
from crater.commun.exports.export_cartes import exporter_cartes
from crater.commun.exports.export_fichier import (
    reinitialiser_dossier,
)
from crater.commun.exports.exporter_diagrammes import (
    exporter_histogrammes,
)
from crater.commun.logger import log


def calculer_energie(
    territoires: DataFrame,
    fichier_occupation_sols: Path,
    fichier_cheptels: Path,
    chemin_fichier_indicateurs_eau: Path,
    chemin_fichiers_sau_par_cultures_rpg: Path,
    chemin_fichier_source_serres: Path,
    chemin_fichier_flux_azote: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### CALCUL ENERGIE #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_postes_energie = calculer_postes_energie(
        territoires,
        chemin_fichier_indicateurs_eau,
        chemin_fichiers_sau_par_cultures_rpg,
        chemin_fichier_source_serres,
        fichier_cheptels,
        chemin_fichier_flux_azote,
        chemin_dossier_output,
    )

    (df_synthese_energie, chemin_fichier_resultat) = calculer_synthese_energie(
        territoires, fichier_occupation_sols, df_postes_energie, chemin_dossier_output
    )

    exporter_cartes(
        df_synthese_energie,
        ["energie_EP_GJ", "energie_EP_GJ_par_ha", "energie_note"],
        ["REGION", "DEPARTEMENT", "EPCI", "COMMUNE"],
        chemin_dossier_output / "cartes",
    )

    exporter_cartes(
        df_synthese_energie,
        [
            "poste_principal_energie",
        ],
        ["REGION", "DEPARTEMENT", "EPCI", "COMMUNE"],
        chemin_dossier_output / "cartes",
        bins=None,
        cmap="hsv",
    )

    exporter_histogrammes(
        df_synthese_energie,
        ["energie_EP_GJ_par_ha", "energie_note"],
        chemin_dossier_output / "diagrammes",
        bins=100,
    )

    return chemin_fichier_resultat
