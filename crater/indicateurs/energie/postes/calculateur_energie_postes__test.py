import unittest
from pathlib import Path

from crater.indicateurs.energie.postes.batiments_elevage import config as config_batiments_elevage
from crater.indicateurs.energie.postes.calculateur_energie_postes import calculer_postes_energie
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)
from crater.config import groupes_cultures as config_groupes_cultures

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestEnergiePostes(unittest.TestCase):
    def test_calculer_energie_postes(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        config_groupes_cultures.FICHIER_CONFIG_CORRESPONDANCES_RPG_VERS_CRATER = (
            CHEMIN_INPUT_DATA / "tracteurs_carburants_et_materiel" / "correspondance_nomenclature_rpg_vers_crater.csv"
        )
        config_batiments_elevage.FICHIER_CONFIG_COEFFICIENTS_BATIMENTS_ELEVAGE = (
            CHEMIN_INPUT_DATA / "batiments_elevage" / "coefficients_energie_batiments_elevage.csv"
        )
        # when
        calculer_postes_energie(
            territoires,
            CHEMIN_INPUT_DATA / "eau" / "synthese_eau.csv",
            CHEMIN_INPUT_DATA / "tracteurs_carburants_et_materiel" / "sau_par_commune_et_culture",
            CHEMIN_INPUT_DATA / "serres" / "synthese_etude_serres_ctifl_2016.xlsx",
            CHEMIN_INPUT_DATA / "cheptels" / "cheptels.csv",
            CHEMIN_INPUT_DATA / "nutriments" / "flux_azote.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "postes_energie.csv",
            CHEMIN_OUTPUT_DATA / "postes_energie.csv",
        )


if __name__ == "__main__":
    unittest.main()
