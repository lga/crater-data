FACTEUR_CONVERSION_GJ_PAR_kWh = 0.0036

# Source : tableur Climagri FRANCE onglet C5 - voir coefficients_energie_202404.xlsx (dossier energie/methodologie/coefficients du nuage)

# Coefficient de conversion energetique carburant litres -> GJ
COEFFICIENT_ENERGIE_PRIMAIRE_CARBURANT_FIOUL_GJ_PAR_LITRE = 0.048
COEFFICIENT_ENERGIE_FINALE_CARBURANT_FIOUL_GJ_PAR_LITRE = 0.036

# Coefficients de conversion energie finale vers energie primaire
COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_CARBURANTS = (
    COEFFICIENT_ENERGIE_PRIMAIRE_CARBURANT_FIOUL_GJ_PAR_LITRE / COEFFICIENT_ENERGIE_FINALE_CARBURANT_FIOUL_GJ_PAR_LITRE
)
COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_ELECTRICITE_MIX_FRANCE = 0.011 / 0.0036
COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_GAZ = 0.032 / 0.028  # On utilise les valeurs du Gaz naturel (mais Climagri distingue Gaz naturel,
# Propane et Butane dans les mix énergétiques)
COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_BIOMASSE = 6.3 / 6.3  # Voir source d'energie Bois
