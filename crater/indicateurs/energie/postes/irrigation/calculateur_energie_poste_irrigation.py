from pathlib import Path

import pandas
from pandas import DataFrame

from crater.indicateurs.energie.postes.config_energie import (
    FACTEUR_CONVERSION_GJ_PAR_kWh,
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_CARBURANTS,
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_ELECTRICITE_MIX_FRANCE,
)
from crater.commun.logger import log

# Source : tableur Climagri FRANCE onglets A5A et A7 - voir coefficients_energie_202404.xlsx (dossier energie/methodologie/coefficients du nuage)
COEFFICIENT_COUT_ENERGETIQUE_IRRIGATION_kWh_EF_PAR_M3 = 0.48
COEFFICIENT_COUT_ENERGETIQUE_IRRIGATION_GJ_EF_PAR_M3 = COEFFICIENT_COUT_ENERGETIQUE_IRRIGATION_kWh_EF_PAR_M3 * FACTEUR_CONVERSION_GJ_PAR_kWh
PART_CARBURANTS_DANS_MIX_ENERGETIQUE_IRRIGATION = 0.15
PART_ELECTRICITE_DANS_MIX_ENERGETIQUE_IRRIGATION = 0.85


def calculer_indicateurs_energie_irrigation(
    territoires: DataFrame,
    chemin_fichier_indicateurs_eau: Path,
) -> DataFrame:
    log.info("##### CALCUL ENERGIE IRRIGATION #####")

    df_eau = pandas.read_csv(chemin_fichier_indicateurs_eau, sep=";").set_index("id_territoire")

    df_energie_irrigation = territoires.loc[:, ["nom_territoire", "categorie_territoire"]].join(df_eau.loc[:, ["irrigation_hors_gravitaire_m3"]])

    df_energie_irrigation["irrigation_total_EF_GJ"] = (
        df_energie_irrigation["irrigation_hors_gravitaire_m3"] * COEFFICIENT_COUT_ENERGETIQUE_IRRIGATION_GJ_EF_PAR_M3
    )
    df_energie_irrigation["irrigation_total_EF_GJ"] = df_energie_irrigation["irrigation_total_EF_GJ"].fillna(0)

    df_energie_irrigation["irrigation_carburants_EP_GJ"] = (
        df_energie_irrigation["irrigation_total_EF_GJ"]
        * PART_CARBURANTS_DANS_MIX_ENERGETIQUE_IRRIGATION
        * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_CARBURANTS
    )
    df_energie_irrigation["irrigation_electricite_EP_GJ"] = (
        df_energie_irrigation["irrigation_total_EF_GJ"]
        * PART_ELECTRICITE_DANS_MIX_ENERGETIQUE_IRRIGATION
        * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_ELECTRICITE_MIX_FRANCE
    )
    df_energie_irrigation["irrigation_total_EP_GJ"] = (
        df_energie_irrigation["irrigation_carburants_EP_GJ"] + df_energie_irrigation["irrigation_electricite_EP_GJ"]
    )

    df_energie_irrigation = df_energie_irrigation.loc[
        :, ["nom_territoire", "categorie_territoire", "irrigation_carburants_EP_GJ", "irrigation_electricite_EP_GJ", "irrigation_total_EP_GJ"]
    ]

    return df_energie_irrigation
