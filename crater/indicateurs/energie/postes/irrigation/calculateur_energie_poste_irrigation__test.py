import unittest
from pathlib import Path

from crater.indicateurs.energie.postes.irrigation.calculateur_energie_poste_irrigation import calculer_indicateurs_energie_irrigation
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestEnergieIrrigation(unittest.TestCase):
    def test_calculer_indicateurs_energie_irrigation(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")
        # when
        calculer_indicateurs_energie_irrigation(
            territoires,
            CHEMIN_INPUT_DATA / "synthese_eau.csv",
        ).to_csv(
            CHEMIN_OUTPUT_DATA / "energie_irrigation.csv",
            sep=";",
            float_format="%.2f",
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "energie_irrigation.csv",
            CHEMIN_OUTPUT_DATA / "energie_irrigation.csv",
        )


if __name__ == "__main__":
    unittest.main()
