from pathlib import Path

import pandas
from pandas import DataFrame

from crater.indicateurs.energie.postes.config_energie import COEFFICIENT_ENERGIE_PRIMAIRE_CARBURANT_FIOUL_GJ_PAR_LITRE
from crater.donnees_sources.surface_agricole_utile import chargeur_sau_par_commune_et_culture
from crater.commun.calculs.outils_dataframes import merge_strict
from crater.commun.calculs.outils_territoires import ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales
from crater.commun.logger import log
from crater.config import groupes_cultures as config_groupes_cultures

# Source : tableur Climagri FRANCE onglet A4 - voir coefficients_energie_202404.xlsx (dossier energie/methodologie/coefficients du nuage)
DF_COEFFICIENTS_CONSOMMATION_CARBURANT_TRACTEURS_ET_MATERIEL = pandas.DataFrame(
    columns=[
        "categorie_culture_climagri",
        "coefficient_consommation_carburant_fioul_litres_par_ha",
        "coefficient_consommation_materiel_EP_GJ_par_ha",
    ],
    data=[
        ["culture_annuelle", 99.62, 1.53],
        ["prairie_temporaire", 65, 1.00],
        ["prairie_naturelle", 65, 1.00],
        ["prairie_naturelle_peu_productive", 5, 0.35],
        ["culture_permanente", 190, 2.30],
    ],
)


def calculer_indicateurs_energie_tracteurs_et_materiel(
    territoires: DataFrame,
    chemin_fichiers_sau_par_cultures_rpg: Path,
) -> DataFrame:
    log.info("##### CALCUL ENERGIE CARBURANT TRACTEURS & MATERIEL #####")

    df_sau_communes = chargeur_sau_par_commune_et_culture.charger(chemin_fichiers_sau_par_cultures_rpg)
    df_correspondance_nomenclature_rpg_vers_crater = pandas.read_csv(
        config_groupes_cultures.FICHIER_CONFIG_CORRESPONDANCES_RPG_VERS_CRATER, sep=";", encoding="utf-8"
    )
    df_energie_tracteurs_et_materiel = merge_strict(
        df_sau_communes,
        df_correspondance_nomenclature_rpg_vers_crater.loc[:, ["code_culture_rpg", "categorie_culture_climagri"]],
        on="code_culture_rpg",
        how="left",
    )

    df_energie_tracteurs_et_materiel = merge_strict(
        df_energie_tracteurs_et_materiel, DF_COEFFICIENTS_CONSOMMATION_CARBURANT_TRACTEURS_ET_MATERIEL, on="categorie_culture_climagri", how="left"
    )

    df_energie_tracteurs_et_materiel["tracteurs_carburants_EP_GJ"] = (
        df_energie_tracteurs_et_materiel["sau_ha"]
        * df_energie_tracteurs_et_materiel["coefficient_consommation_carburant_fioul_litres_par_ha"]
        * COEFFICIENT_ENERGIE_PRIMAIRE_CARBURANT_FIOUL_GJ_PAR_LITRE
    )

    df_energie_tracteurs_et_materiel["materiel_total_EP_GJ"] = (
        df_energie_tracteurs_et_materiel["sau_ha"] * df_energie_tracteurs_et_materiel["coefficient_consommation_materiel_EP_GJ_par_ha"]
    )

    df_energie_tracteurs_et_materiel = df_energie_tracteurs_et_materiel.groupby("id_commune", as_index=False)[
        ["tracteurs_carburants_EP_GJ", "materiel_total_EP_GJ"]
    ].sum()

    df_energie_tracteurs_et_materiel = (
        territoires.reset_index()
        .merge(df_energie_tracteurs_et_materiel, how="left", left_on="id_territoire", right_on="id_commune")
        .set_index("id_territoire")
    )
    df_energie_tracteurs_et_materiel["tracteurs_carburants_EP_GJ"] = df_energie_tracteurs_et_materiel["tracteurs_carburants_EP_GJ"].fillna(0)
    df_energie_tracteurs_et_materiel["materiel_total_EP_GJ"] = df_energie_tracteurs_et_materiel["materiel_total_EP_GJ"].fillna(0)

    df_energie_tracteurs_et_materiel = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        df_energie_tracteurs_et_materiel, "tracteurs_carburants_EP_GJ"
    )
    df_energie_tracteurs_et_materiel = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        df_energie_tracteurs_et_materiel, "materiel_total_EP_GJ"
    )

    df_energie_tracteurs_et_materiel["tracteurs_total_EP_GJ"] = df_energie_tracteurs_et_materiel["tracteurs_carburants_EP_GJ"]

    df_energie_tracteurs_et_materiel = df_energie_tracteurs_et_materiel.loc[
        :, ["nom_territoire", "categorie_territoire", "tracteurs_carburants_EP_GJ", "tracteurs_total_EP_GJ", "materiel_total_EP_GJ"]
    ]

    return df_energie_tracteurs_et_materiel
