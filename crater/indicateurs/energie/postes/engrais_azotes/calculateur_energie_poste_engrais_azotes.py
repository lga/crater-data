from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.logger import log

# Source : onglet engrais_azotes du fichier coefficients_energie_202404.xlsx (dossier energie/methodologie/coefficients du nuage)
CONSOMMATION_ENERGETIQUE_MOYENNE_PRODUCTION_ENGRAIS_AZOTES_GJ_EP_PAR_tN = 70.8


def calculer_indicateurs_energie_engrais_azotes(
    territoires: DataFrame,
    chemin_fichier_flux_azote: Path,
) -> DataFrame:
    log.info("##### CALCUL ENERGIE ENGRAIS AZOTES #####")

    df_flux_azote = pandas.read_csv(chemin_fichier_flux_azote, sep=";").set_index("id_territoire").loc[:, ["engrais_synthese_kgN"]]

    df_energie_engrais_azotes = territoires.loc[:, ["nom_territoire", "categorie_territoire"]].join(df_flux_azote)
    df_energie_engrais_azotes["engrais_azotes_total_EP_GJ"] = (
        df_energie_engrais_azotes["engrais_synthese_kgN"] / 1000 * CONSOMMATION_ENERGETIQUE_MOYENNE_PRODUCTION_ENGRAIS_AZOTES_GJ_EP_PAR_tN
    )
    df_energie_engrais_azotes = df_energie_engrais_azotes.loc[:, ["nom_territoire", "categorie_territoire", "engrais_azotes_total_EP_GJ"]]

    return df_energie_engrais_azotes
