from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.logger import log

# Source : onglet alimentation_animale du fichier coefficients_energie_202404.xlsx (dossier energie/methodologie/coefficients du nuage)
CONSOMMATION_ENERGETIQUE_MOYENNE_PRODUCTION_CONCENTRES_ENERGETIQUES_IMPORTES_GJ_EP_PAR_tN = 156.30
CONSOMMATION_ENERGETIQUE_MOYENNE_PRODUCTION_FOURRAGES_IMPORTES_GJ_EP_PAR_tN = 199.58
CONSOMMATION_ENERGETIQUE_MOYENNE_PRODUCTION_CONCENTRES_PROTEIQUES_IMPORTES_GJ_EP_PAR_tN = 81.54


def calculer_indicateurs_energie_alimentation_animale(
    territoires: DataFrame,
    chemin_fichier_flux_azote: Path,
) -> DataFrame:
    log.info("##### CALCUL ENERGIE ALIMENTATION ANIMALE IMPORTEE #####")

    df_flux_azote = (
        pandas.read_csv(chemin_fichier_flux_azote, sep=";")
        .set_index("id_territoire")
        .loc[
            :,
            [
                "import_depuis_etranger_concentres_energetiques_pour_animaux_kgN",
                "import_depuis_etranger_fourrages_pour_animaux_kgN",
                "import_depuis_etranger_concentres_proteiques_pour_animaux_kgN",
            ],
        ]
    )

    df_energie_alimentation_animale = territoires.loc[:, ["nom_territoire", "categorie_territoire"]].join(df_flux_azote)
    df_energie_alimentation_animale["alimentation_animale_total_EP_GJ"] = (
        df_energie_alimentation_animale["import_depuis_etranger_concentres_energetiques_pour_animaux_kgN"]
        / 1000
        * CONSOMMATION_ENERGETIQUE_MOYENNE_PRODUCTION_CONCENTRES_ENERGETIQUES_IMPORTES_GJ_EP_PAR_tN
        + df_energie_alimentation_animale["import_depuis_etranger_fourrages_pour_animaux_kgN"]
        / 1000
        * CONSOMMATION_ENERGETIQUE_MOYENNE_PRODUCTION_FOURRAGES_IMPORTES_GJ_EP_PAR_tN
        + df_energie_alimentation_animale["import_depuis_etranger_concentres_proteiques_pour_animaux_kgN"]
        / 1000
        * CONSOMMATION_ENERGETIQUE_MOYENNE_PRODUCTION_CONCENTRES_PROTEIQUES_IMPORTES_GJ_EP_PAR_tN
    )
    df_energie_alimentation_animale = df_energie_alimentation_animale.loc[
        :, ["nom_territoire", "categorie_territoire", "alimentation_animale_total_EP_GJ"]
    ]

    return df_energie_alimentation_animale
