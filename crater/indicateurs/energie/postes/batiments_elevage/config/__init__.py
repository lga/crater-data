# Chemin vers config, peut être redéfinie par les tests pour utiliser une config simplifiée
from pathlib import Path

FICHIER_CONFIG_COEFFICIENTS_BATIMENTS_ELEVAGE: Path = Path(__file__).parent / "coefficients_energie_batiments_elevage.csv"
