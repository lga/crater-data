from pathlib import Path

import pandas
from pandas import DataFrame

from crater.indicateurs.energie.postes.batiments_elevage import config
from crater.indicateurs.energie.postes.config_energie import (
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_CARBURANTS,
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_ELECTRICITE_MIX_FRANCE,
    COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_GAZ,
    COEFFICIENT_ENERGIE_FINALE_CARBURANT_FIOUL_GJ_PAR_LITRE,
    FACTEUR_CONVERSION_GJ_PAR_kWh,
)
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.logger import log

# Source: onglet correspondances_cheptels du fichier coefficients_energie_202404.xlsx (dossier energie/methodologie/coefficients du nuage)
PART_TRUIES_DANS_TRUIES_REPRODUCTRICES_50KG_OU_PLUS_YC_COCHETTES = 100 / 100
PART_PORCS_ENGRAISSEMENT_DANS_TOTAL_HORS_TRUIES_REPRODUCTRICES_50KG_OU_PLUS_YC_COCHETTES = 43 / 100

PART_VOLAILLES_PONDEUSES_DANS_TOTAL_VOLAILLES = 21 / 100
PART_VOLAILLES_CHAIR_DANS_TOTAL_VOLAILLES = 79 / 100

PART_VEAUX_BOUCHERIE_DANS_TOTAL_BOVINS_HORS_VACHES = 12 / 100


def calculer_indicateurs_energie_batiments_elevage(
    territoires: DataFrame,
    fichier_cheptels: Path,
) -> DataFrame:
    log.info("##### CALCUL ENERGIE BATIMENTS ELEVAGE #####")

    df_indicateurs = territoires.copy()
    df_indicateurs = charger_et_joindre_df_indicateurs(df_indicateurs, fichier_cheptels)
    df_indicateurs = _ajouter_nb_cheptels_selon_categorisation_climagri(df_indicateurs)

    df_coefficients = _charger_coefficients_batiments_elevage()

    df_indicateurs = _calculer_energies_finales(df_coefficients, df_indicateurs)
    df_indicateurs = _calculer_energies_primaires(df_indicateurs)

    return df_indicateurs


def _calculer_energies_primaires(df_indicateurs: DataFrame) -> DataFrame:
    df_indicateurs["batiments_elevage_carburants_EP_GJ"] = (
        df_indicateurs["batiments_elevage_chauffage_carburants_EF_GJ"] + df_indicateurs["batiments_elevage_carburants_EF_GJ"]
    ) * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_CARBURANTS
    df_indicateurs["batiments_elevage_electricite_EP_GJ"] = (
        df_indicateurs["batiments_elevage_chauffage_electricite_EF_GJ"] + df_indicateurs["batiments_elevage_electricite_EF_GJ"]
    ) * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_ELECTRICITE_MIX_FRANCE
    df_indicateurs["batiments_elevage_gaz_EP_GJ"] = (
        df_indicateurs["batiments_elevage_chauffage_gaz_EF_GJ"] * COEFFICIENT_ENERGIE_FINALE_VERS_PRIMAIRE_GAZ
    )

    df_indicateurs["batiments_elevage_total_EP_GJ"] = (
        df_indicateurs["batiments_elevage_carburants_EP_GJ"]
        + df_indicateurs["batiments_elevage_electricite_EP_GJ"]
        + df_indicateurs["batiments_elevage_gaz_EP_GJ"]
    )

    return df_indicateurs.loc[
        :,
        [
            "nom_territoire",
            "categorie_territoire",
            "batiments_elevage_carburants_EP_GJ",
            "batiments_elevage_electricite_EP_GJ",
            "batiments_elevage_gaz_EP_GJ",
            "batiments_elevage_total_EP_GJ",
        ],
    ]


def _calculer_energies_finales(df_coefficients: DataFrame, df_indicateurs: DataFrame) -> DataFrame:
    df_indicateurs["batiments_elevage_chauffage_carburants_EF_GJ"] = 0
    df_indicateurs["batiments_elevage_chauffage_electricite_EF_GJ"] = 0
    df_indicateurs["batiments_elevage_chauffage_gaz_EF_GJ"] = 0
    df_indicateurs["batiments_elevage_electricite_EF_GJ"] = 0
    df_indicateurs["batiments_elevage_carburants_EF_GJ"] = 0
    for nom_cheptel in df_coefficients.index:
        nom_colonne_nb_cheptel = "nb_" + nom_cheptel
        if nom_colonne_nb_cheptel in df_indicateurs.columns:
            chauffage = (
                df_coefficients.loc[nom_cheptel]["chauffage_kWh_par_tete"] * FACTEUR_CONVERSION_GJ_PAR_kWh * df_indicateurs[nom_colonne_nb_cheptel]
            )
            carburants_chauffage = df_coefficients.loc[nom_cheptel]["part_carburants_dans_chauffage"] * chauffage
            electricite_chauffage = df_coefficients.loc[nom_cheptel]["part_electricite_dans_chauffage"] * chauffage
            gaz_chauffage = df_coefficients.loc[nom_cheptel]["part_gaz_dans_chauffage"] * chauffage
            electricite = (
                df_coefficients.loc[nom_cheptel]["electricite_kWh_par_tete"] * FACTEUR_CONVERSION_GJ_PAR_kWh * df_indicateurs[nom_colonne_nb_cheptel]
            )
            carburants = (
                df_coefficients.loc[nom_cheptel]["fioul_litres_par_tete_et_jour"]
                * df_coefficients.loc[nom_cheptel]["temps_presence_moyen_jours"]
                * COEFFICIENT_ENERGIE_FINALE_CARBURANT_FIOUL_GJ_PAR_LITRE
                * df_indicateurs[nom_colonne_nb_cheptel]
            )

            df_indicateurs["batiments_elevage_chauffage_carburants_EF_GJ"] += carburants_chauffage
            df_indicateurs["batiments_elevage_chauffage_electricite_EF_GJ"] += electricite_chauffage
            df_indicateurs["batiments_elevage_chauffage_gaz_EF_GJ"] += gaz_chauffage
            df_indicateurs["batiments_elevage_electricite_EF_GJ"] += electricite
            df_indicateurs["batiments_elevage_carburants_EF_GJ"] += carburants

    return df_indicateurs


def _ajouter_nb_cheptels_selon_categorisation_climagri(df: DataFrame) -> DataFrame:
    df["nb_porcins_hors_truies_reproductrices_50kg_ou_plus_yc_cochettes"] = (
        df["nb_porcins"] - df["nb_porcins_truies_reproductrices_50kg_ou_plus_yc_cochettes"]
    )
    df["nb_truies"] = (
        PART_TRUIES_DANS_TRUIES_REPRODUCTRICES_50KG_OU_PLUS_YC_COCHETTES * df["nb_porcins_truies_reproductrices_50kg_ou_plus_yc_cochettes"]
    )
    df["nb_porcs_engraissement"] = (
        PART_PORCS_ENGRAISSEMENT_DANS_TOTAL_HORS_TRUIES_REPRODUCTRICES_50KG_OU_PLUS_YC_COCHETTES
        * df["nb_porcins_hors_truies_reproductrices_50kg_ou_plus_yc_cochettes"]
    )

    df["nb_volailles_pondeuses"] = PART_VOLAILLES_PONDEUSES_DANS_TOTAL_VOLAILLES * df["nb_volailles"]
    df["nb_volailles_chair"] = PART_VOLAILLES_CHAIR_DANS_TOTAL_VOLAILLES * df["nb_volailles"]

    df["nb_bovins_veaux_boucherie"] = PART_VEAUX_BOUCHERIE_DANS_TOTAL_BOVINS_HORS_VACHES * (
        df["nb_bovins"] - df["nb_bovins_vaches_laitieres"] - df["nb_bovins_vaches_allaitantes"]
    )

    return df


def _charger_coefficients_batiments_elevage():
    df_coefficients = pandas.read_csv(config.FICHIER_CONFIG_COEFFICIENTS_BATIMENTS_ELEVAGE, sep=";", encoding="utf-8").set_index("cheptel").fillna(0)
    somme_coefficients = (
        df_coefficients.part_carburants_dans_chauffage + df_coefficients.part_electricite_dans_chauffage + df_coefficients.part_gaz_dans_chauffage
    )
    if ((somme_coefficients != 1) & (df_coefficients.chauffage_kWh_par_tete > 0)).any():
        raise ValueError(
            f"ERREUR calcul indicateur energie batiments elevage : la somme des coefficients pour les différents types de "
            f"chauffage est différente de 1 (fichier {config.FICHIER_CONFIG_COEFFICIENTS_BATIMENTS_ELEVAGE})"
        )

    return df_coefficients
