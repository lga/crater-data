from pathlib import Path

import pandas
from pandas import DataFrame

from crater.indicateurs.energie.postes.alimentation_animale.calculateur_energie_poste_alimentation_animale import (
    calculer_indicateurs_energie_alimentation_animale,
)
from crater.indicateurs.energie.postes.batiments_elevage.calculateur_energie_poste_batiments_elevage import (
    calculer_indicateurs_energie_batiments_elevage,
)
from crater.indicateurs.energie.postes.engrais_azotes.calculateur_energie_poste_engrais_azotes import (
    calculer_indicateurs_energie_engrais_azotes,
)
from crater.indicateurs.energie.postes.irrigation.calculateur_energie_poste_irrigation import (
    calculer_indicateurs_energie_irrigation,
)
from crater.indicateurs.energie.postes.serres.calculateur_energie_poste_serres import (
    calculer_indicateurs_energie_serres,
)
from crater.indicateurs.energie.postes.tracteurs_et_materiel.calculateur_energie_poste_tracteurs_et_materiel import (
    calculer_indicateurs_energie_tracteurs_et_materiel,
)
from crater.commun.calculs.outils_territoires import trier_territoires
from crater.commun.exports.export_fichier import (
    exporter_df_indicateurs_par_territoires,
    reinitialiser_dossier,
)


def calculer_postes_energie(
    territoires: DataFrame,
    chemin_fichier_indicateurs_eau: Path,
    chemin_fichiers_sau_par_cultures_rpg: Path,
    chemin_fichier_source_serres: Path,
    chemin_fichier_cheptels: Path,
    chemin_fichier_flux_azote: Path,
    chemin_dossier_output: Path,
) -> DataFrame:
    reinitialiser_dossier(chemin_dossier_output)

    df_energie_en_colonnes = territoires.loc[:, ["nom_territoire", "categorie_territoire"]].copy()

    df_energie_en_colonnes = _ajouter_colonnes_tracteurs_et_materiel(
        territoires,
        df_energie_en_colonnes,
        chemin_fichiers_sau_par_cultures_rpg,
    )

    df_energie_en_colonnes = _ajouter_colonnes_irrigation(territoires, df_energie_en_colonnes, chemin_fichier_indicateurs_eau)

    df_energie_en_colonnes = _ajouter_colonnes_serres(territoires, df_energie_en_colonnes, chemin_fichier_source_serres)

    df_energie_en_colonnes = _ajouter_colonne_engrais(territoires, df_energie_en_colonnes, chemin_fichier_flux_azote)

    df_energie_en_colonnes = _ajouter_colonnes_batiments_elevage(
        territoires,
        chemin_fichier_cheptels,
        df_energie_en_colonnes,
    )

    df_energie_en_colonnes = _ajouter_colonne_alimentation_animale(territoires, df_energie_en_colonnes, chemin_fichier_flux_azote)

    df_energie_en_lignes = _creer_df_energie_en_lignes(df_energie_en_colonnes)

    df_energie_en_lignes = trier_territoires(df_energie_en_lignes, ["poste", "source"])

    exporter_df_indicateurs_par_territoires(df_energie_en_lignes, chemin_dossier_output, "postes_energie")

    return df_energie_en_lignes


def _creer_df_energie_en_lignes(df_energie_en_colonnes):
    df_energie_source_total = _extraire_df_en_lignes_par_source(
        df_energie_en_colonnes,
        "_total_EP_GJ",
    )
    df_energie_source_carburants = _extraire_df_en_lignes_par_source(
        df_energie_en_colonnes,
        "_carburants_EP_GJ",
    )
    df_energie_source_electricite = _extraire_df_en_lignes_par_source(
        df_energie_en_colonnes,
        "_electricite_EP_GJ",
    )
    df_energie_source_gaz = _extraire_df_en_lignes_par_source(df_energie_en_colonnes, "_gaz_EP_GJ")
    df_energie_source_biomasse = _extraire_df_en_lignes_par_source(df_energie_en_colonnes, "_biomasse_EP_GJ")
    df_energie_en_lignes = pandas.concat(
        [
            df_energie_source_total,
            df_energie_source_carburants,
            df_energie_source_electricite,
            df_energie_source_gaz,
            df_energie_source_biomasse,
        ]
    )

    mask_energies_indirectes = df_energie_en_lignes["poste"].isin(["ALIMENTATION_ANIMALE", "ENGRAIS_AZOTES", "MATERIEL"])
    df_energie_en_lignes["est_energie_directe"] = True
    df_energie_en_lignes.loc[mask_energies_indirectes, ["est_energie_directe"]] = False

    df_energie_en_lignes = df_energie_en_lignes.set_index("id_territoire").loc[
        :,
        [
            "nom_territoire",
            "categorie_territoire",
            "poste",
            "source",
            "est_energie_directe",
            "energie_EP_GJ",
        ],
    ]
    return df_energie_en_lignes


def _extraire_df_en_lignes_par_source(df_energie_en_colonnes, suffixe_nom_colonnes_a_inclure):
    noms_colonnes = [nc for nc in (df_energie_en_colonnes.columns.values) if suffixe_nom_colonnes_a_inclure in nc]
    df_energie_en_lignes = pandas.melt(
        df_energie_en_colonnes.reset_index(),
        id_vars=["id_territoire", "nom_territoire", "categorie_territoire"],
        value_vars=noms_colonnes,
        var_name="poste",
        value_name="energie_EP_GJ",
    )
    df_energie_en_lignes["poste"] = df_energie_en_lignes["poste"].str.replace(suffixe_nom_colonnes_a_inclure, "").str.upper()
    df_energie_en_lignes["source"] = suffixe_nom_colonnes_a_inclure.split("_")[1].upper()
    return df_energie_en_lignes


def _ajouter_colonne_alimentation_animale(territoires, df_energie, chemin_fichier_flux_azote):
    df_energie_alimentation_animale = calculer_indicateurs_energie_alimentation_animale(
        territoires,
        chemin_fichier_flux_azote,
    )
    df_energie = df_energie.join(df_energie_alimentation_animale.loc[:, ["alimentation_animale_total_EP_GJ"]])
    return df_energie


def _ajouter_colonnes_batiments_elevage(territoires: DataFrame, fichier_cheptels: Path, df_energie: DataFrame) -> DataFrame:
    df_energie_batiments_elevage = calculer_indicateurs_energie_batiments_elevage(
        territoires,
        fichier_cheptels,
    )
    df_energie = df_energie.join(
        df_energie_batiments_elevage.loc[
            :,
            [
                "batiments_elevage_total_EP_GJ",
                "batiments_elevage_carburants_EP_GJ",
                "batiments_elevage_electricite_EP_GJ",
                "batiments_elevage_gaz_EP_GJ",
            ],
        ]
    )
    return df_energie


def _ajouter_colonne_engrais(territoires, df_energie, chemin_fichier_flux_azote):
    df_energie_engrais_azotes = calculer_indicateurs_energie_engrais_azotes(
        territoires,
        chemin_fichier_flux_azote,
    )
    df_energie = df_energie.join(df_energie_engrais_azotes.loc[:, ["engrais_azotes_total_EP_GJ"]])
    return df_energie


def _ajouter_colonnes_serres(territoires, df_energie, chemin_fichier_source_serres):
    df_energie_serres = calculer_indicateurs_energie_serres(
        territoires,
        chemin_fichier_source_serres,
    )
    df_energie = df_energie.join(
        df_energie_serres.loc[
            :,
            [
                "serres_total_EP_GJ",
                "serres_electricite_EP_GJ",
                "serres_gaz_EP_GJ",
                "serres_biomasse_EP_GJ",
            ],
        ]
    )
    return df_energie


def _ajouter_colonnes_irrigation(territoires, df_energie, chemin_fichier_indicateurs_eau):
    df_energie_irrigation = calculer_indicateurs_energie_irrigation(territoires, chemin_fichier_indicateurs_eau)
    df_energie = df_energie.join(
        df_energie_irrigation.loc[
            :,
            [
                "irrigation_total_EP_GJ",
                "irrigation_carburants_EP_GJ",
                "irrigation_electricite_EP_GJ",
            ],
        ]
    )
    return df_energie


def _ajouter_colonnes_tracteurs_et_materiel(
    territoires,
    df_energie,
    chemin_fichiers_sau_par_cultures_rpg,
):
    df_energie_tracteurs = calculer_indicateurs_energie_tracteurs_et_materiel(
        territoires,
        chemin_fichiers_sau_par_cultures_rpg,
    )
    df_energie = df_energie.join(
        df_energie_tracteurs.loc[
            :,
            [
                "tracteurs_carburants_EP_GJ",
                "tracteurs_total_EP_GJ",
                "materiel_total_EP_GJ",
            ],
        ]
    )
    return df_energie
