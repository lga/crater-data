from pathlib import Path
from typing import Tuple

import numpy as np
from pandas import DataFrame

from crater.commun.calculs.notes_et_messages import calculer_note_par_interpolation_n_seuils
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.exports.export_fichier import (
    exporter_df_indicateurs_par_territoires,
)


def calculer_synthese_energie(
    territoires: DataFrame, fichier_occupation_sols: Path, df_postes_energie: DataFrame, chemin_dossier_output: Path
) -> Tuple[DataFrame, Path]:
    territoires = charger_et_joindre_df_indicateurs(territoires, fichier_occupation_sols)

    df_synthese_energie = territoires.loc[:, ["nom_territoire", "categorie_territoire"]].copy()

    df_postes_totaux = df_postes_energie.loc[df_postes_energie["source"] == "TOTAL"]

    df_synthese_energie = _ajouter_colonne_poste_principal_energie(df_synthese_energie, df_postes_totaux)
    df_synthese_energie = _ajouter_colonne_energie_totale(df_synthese_energie, df_postes_totaux)
    df_synthese_energie = _ajouter_colonne_energie_par_ha(df_synthese_energie, territoires)
    df_synthese_energie = _ajouter_colonne_energie_note(df_synthese_energie)

    chemin_fichier_resultat = exporter_df_indicateurs_par_territoires(df_synthese_energie, chemin_dossier_output, "synthese_energie")

    return (df_synthese_energie, chemin_fichier_resultat)


def _ajouter_colonne_energie_par_ha(df_synthese_energie, territoires) -> DataFrame:
    df_energie_par_ha = df_synthese_energie.loc[:, ["energie_EP_GJ"]].join(territoires.loc[:, ["sau_ha"]])

    df_energie_par_ha["energie_EP_GJ_par_ha"] = (df_energie_par_ha["energie_EP_GJ"] / df_energie_par_ha["sau_ha"]).round(2).replace({np.inf: np.nan})
    return df_synthese_energie.join(df_energie_par_ha.loc[:, ["energie_EP_GJ_par_ha"]])


def _ajouter_colonne_energie_note(df_synthese_energie):
    seuils_energie = df_synthese_energie.loc[df_synthese_energie["categorie_territoire"] == "DEPARTEMENT", "energie_EP_GJ_par_ha"].quantile(
        [1 / 4, 1 / 2, 3 / 4]
    )
    df_synthese_energie["energie_note"] = calculer_note_par_interpolation_n_seuils(
        df_synthese_energie["energie_EP_GJ_par_ha"],
        [
            0,
            seuils_energie.values[0],
            seuils_energie.values[1],
            seuils_energie.values[2],
            seuils_energie.values[2] + 1.5 * (seuils_energie.values[2] - seuils_energie.values[0]),
        ],
        [10, 7.5, 5, 2.5, 0],
    )
    return df_synthese_energie


def _ajouter_colonne_energie_totale(df_synthese_energie, df_postes_energie):
    df_energie_totale = df_postes_energie.reset_index().groupby("id_territoire")["energie_EP_GJ"].sum()

    return df_synthese_energie.join(df_energie_totale)


def _ajouter_colonne_poste_principal_energie(df_synthese_energie, df_postes_energie):
    df_poste_principal = df_postes_energie.reset_index()
    df_poste_principal = (
        df_poste_principal.loc[df_poste_principal.groupby("id_territoire")["energie_EP_GJ"].idxmax()].set_index("id_territoire").loc[:, ["poste"]]
    )

    df_poste_principal = df_poste_principal.rename(columns={"poste": "poste_principal_energie"})

    return df_synthese_energie.join(df_poste_principal)
