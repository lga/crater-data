import unittest
from pathlib import Path

import pandas

from crater.indicateurs.energie.synthese.calculateur_synthese_energie import calculer_synthese_energie
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestSyntheseEnergie(unittest.TestCase):
    def test_calculer_synthese_energie(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        df_energies = pandas.read_csv(CHEMIN_INPUT_DATA / "postes_energie.csv", sep=";").set_index("id_territoire")
        # when
        calculer_synthese_energie(
            territoires,
            CHEMIN_INPUT_DATA / "occupation_sols" / "occupation_sols.csv",
            df_energies,
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "synthese_energie.csv",
            CHEMIN_OUTPUT_DATA / "synthese_energie.csv",
        )


if __name__ == "__main__":
    unittest.main()
