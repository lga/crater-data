from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.exports.export_cartes import exporter_cartes
from crater.commun.exports.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.exports.exporter_diagrammes import exporter_histogrammes
from crater.commun.logger import log


def calculer_intrants(
    territoires: DataFrame,
    fichier_synthese_energie: Path,
    fichier_synthese_eau: Path,
    fichier_synthese_pesticides: Path,
    chemin_dossier_output: Path,
) -> tuple[Path, DataFrame]:
    log.info("##### CALCUL INTRANTS #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_energie = pd.read_csv(fichier_synthese_energie, sep=";").set_index("id_territoire")
    df_eau = pd.read_csv(fichier_synthese_eau, sep=";").set_index("id_territoire")
    df_pesticides = pd.read_csv(fichier_synthese_pesticides, sep=";").set_index("id_territoire")

    df_intrants = territoires.loc[:, ["nom_territoire", "categorie_territoire"]].copy()
    df_intrants = df_intrants.join(df_energie.loc[:, ["energie_note"]])
    df_intrants = df_intrants.join(df_eau.loc[:, ["eau_note"]])
    df_intrants = df_intrants.join(df_pesticides.loc[:, ["pesticides_note"]])
    df_intrants["intrants_note"] = (df_intrants["energie_note"] + df_intrants["eau_note"] + df_intrants["pesticides_note"]) / 3
    fichier_synthese_intrants = exporter_df_indicateurs_par_territoires(df_intrants, chemin_dossier_output, "synthese_intrants")

    exporter_cartes(
        df_intrants,
        ["intrants_note"],
        ["REGION", "DEPARTEMENT", "EPCI", "COMMUNE"],
        chemin_dossier_output / "cartes",
    )

    exporter_histogrammes(
        df_intrants,
        ["intrants_note"],
        chemin_dossier_output / "diagrammes",
        bins=100,
    )

    return fichier_synthese_intrants, df_intrants
