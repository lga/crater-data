import unittest
from pathlib import Path

from crater.indicateurs.intrants.calculateur_intrants import calculer_intrants
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestMaillonIntrants(unittest.TestCase):
    def test_calculer_intrants(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")
        # when
        calculer_intrants(
            territoires,
            CHEMIN_INPUT_DATA / "synthese_energie.csv",
            CHEMIN_INPUT_DATA / "synthese_eau.csv",
            CHEMIN_INPUT_DATA / "synthese_pesticides.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "synthese_intrants.csv",
            CHEMIN_OUTPUT_DATA / "synthese_intrants.csv",
        )


if __name__ == "__main__":
    unittest.main()
