CODE_COMMERCE_GENERALISTE = "COMMERCE_GENERALISTE"
CODE_BOULANGERIE_PATISSERIE = "BOULANGERIE_PATISSERIE"
CODE_BOUCHERIE_POISSONNERIE = "BOUCHERIE_POISSONNERIE"
CODE_COMMERCE_SPECIALISE = "COMMERCE_SPECIALISE"

#  75% des types de commerces = 3 catégories sur un total de 4 total
NB_CATEGORIES_COMMERCES_3EME_QUARTILE = 3

CODE_COMMERCE_TOUS_MOYENNE = "TOUS_MOYENNE"
CODE_COMMERCE_TOUS_PLUS_PROCHE = "TOUS_PLUS_PROCHE"
CODE_COMMERCE_TOUS_PLUS_LOIN = "TOUS_PLUS_LOIN"
CODE_COMMERCE_TOUS_PLUS_LOIN_3EME_QUARTILE = "TOUS_PLUS_LOIN_3EME_QUARTILE"

CODES_COMMERCES = [
    CODE_BOULANGERIE_PATISSERIE,
    CODE_BOUCHERIE_POISSONNERIE,
    CODE_COMMERCE_GENERALISTE,
    CODE_COMMERCE_SPECIALISE,
    CODE_COMMERCE_TOUS_MOYENNE,
    CODE_COMMERCE_TOUS_PLUS_LOIN_3EME_QUARTILE,
    CODE_COMMERCE_TOUS_PLUS_PROCHE,
    CODE_COMMERCE_TOUS_PLUS_LOIN,
]

# Voir  https://wiki.openstreetmap.org/wiki/FR:Key:shop pour le référentiel des codes commerces OSM
# Pas évident de faire ce mapping => les types ne sont pas normalisés dans OSM, plus de 1500 différents
# mais un value_counts() sur le dataframe, montre quand meme que les principaux types ressortent le plus souvent
# par contre pas de disctinction entre les tailles de supermarchés
MAPPING_CODES_COMMERCES_ALIMENTAIRES_OSM = {
    "supermarket": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Hypermarché"},
    "department_store": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Supermarché"},
    "convenience": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Supérette"},
    "deli": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Épicerie fine"},
    "bakery": {"code": CODE_BOULANGERIE_PATISSERIE, "libelle": "Boulangerie"},
    "pastry": {"code": CODE_BOULANGERIE_PATISSERIE, "libelle": "Patisserie"},
    "butcher": {
        "code": CODE_BOUCHERIE_POISSONNERIE,
        "libelle": "Boucherie-Charcuterie",
    },
    "seafood": {"code": CODE_BOUCHERIE_POISSONNERIE, "libelle": "Poissonnerie"},
    "frozen_food": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Produits surgelés"},
    "cheese": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Fromagerie"},
    "dairy": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Crèmerie"},
    "farm": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Vente à la ferme"},
    "greengrocer": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Primeur"},
    "pasta": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Pâtes"},
    "wholesale": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Vente en entrepôt"},
    "caterer": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Traiteur"},
    "cannery": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Conserverie"},
    "organic": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Épicerie bio"},
    "health_food": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Épicerie bio"},
}

MAPPING_CODES_COMMERCES_ALIMENTAIRES_BPE = {
    "B101": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Hypermarché"},
    "B102": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Supermarché"},
    "B201": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Supérette"},
    "B202": {"code": CODE_COMMERCE_GENERALISTE, "libelle": "Épicerie"},
    "B203": {"code": CODE_BOULANGERIE_PATISSERIE, "libelle": "Boulangerie-Patisserie"},
    "B204": {"code": CODE_BOUCHERIE_POISSONNERIE, "libelle": "Boucherie-Charcuterie"},
    "B205": {"code": CODE_COMMERCE_SPECIALISE, "libelle": "Produits surgelés"},
    "B206": {"code": CODE_BOUCHERIE_POISSONNERIE, "libelle": "Poissonnerie"},
}

RAYON_RECHERCHE_DOUBLONS_OSM_BPE_METRES = 80

DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_PIED_METRES = 1000
DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES = 2000
