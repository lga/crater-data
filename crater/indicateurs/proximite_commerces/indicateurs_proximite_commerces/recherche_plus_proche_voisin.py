import numpy as np
from geopandas import GeoDataFrame
from numpy import ndarray
from sklearn.neighbors import BallTree

from crater.commun.logger import log


def rechercher_plus_proche_voisin_selon_distance_euclidienne(points_input: GeoDataFrame, voisins_potentiels_input: GeoDataFrame) -> GeoDataFrame:
    log.info("        - Recherche du plus proche voisin")

    if (points_input.crs != "EPSG:2154") | (voisins_potentiels_input.crs != "EPSG:2154"):
        raise ValueError("Le CRS des GeoDataFrame des points et voisins potentiels doit être EPSG:2154")

    # Positionner des index numériques : nécessaire sur les voisins pour l'algo de recherche, et nécessaire sur les points pour la jointure finale
    points = points_input.copy().reset_index(drop=True)
    voisins_potentiels = voisins_potentiels_input.copy().reset_index(drop=True).rename(columns={"geometry": "geometry_voisin"})

    np_points = np.array([points["geometry"].x, points["geometry"].y]).transpose()
    np_voisins_potentiels = np.array(
        [
            voisins_potentiels["geometry_voisin"].x,
            voisins_potentiels["geometry_voisin"].y,
        ]
    ).transpose()

    (
        index_plus_proche_voisin,
        distances_m,
    ) = _rechercher_index_et_distance_plus_proches_voisins(points=np_points, voisins_potentiels=np_voisins_potentiels)

    plus_proche_voisin_par_points = voisins_potentiels.loc[index_plus_proche_voisin]

    # Reinitialiser l'index pour correspondre avec celui de points
    plus_proche_voisin_par_points = plus_proche_voisin_par_points.reset_index(drop=True)

    plus_proche_voisin_par_points["distance_m"] = distances_m

    return points.join(plus_proche_voisin_par_points)


def _rechercher_index_et_distance_plus_proches_voisins(points: ndarray, voisins_potentiels: ndarray):
    """
    Retourne 2 tableaux (index_plus_proche et distance), qui font la même dimension que le tableau des points
    Ils contiennent respectivement l'index du voisin le plus proche dans le tableau voisins_potentiels, et la distance entre le point et ce voisin
    """
    if voisins_potentiels.shape[0] == 0:
        return np.array([]), np.array([])

    tree = BallTree(voisins_potentiels, leaf_size=15, metric="euclidean")

    # k=1 car on ne recherche que le plus proche (avec k>1 on pourrait trouver les k plus proches pour chaque point)
    distances, indices = tree.query(points, k=1)

    distances = distances.transpose()
    indices = indices.transpose()

    index_plus_proche_voisin = indices[0]
    distances = distances[0]

    return (index_plus_proche_voisin, distances)
