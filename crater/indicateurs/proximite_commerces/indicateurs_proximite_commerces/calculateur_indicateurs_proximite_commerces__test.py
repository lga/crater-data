import unittest
from pathlib import Path

import geopandas as gpd

from crater.indicateurs.proximite_commerces.indicateurs_proximite_commerces.calculateur_indicateurs_par_carreau import (
    calculer_indicateurs_proximite_par_carreau,
)
from crater.indicateurs.proximite_commerces.indicateurs_proximite_commerces.calculateur_indicateurs_proximite_commerces import (
    calculer_indicateurs_proximite_commerces,
)
from crater.indicateurs.proximite_commerces.indicateurs_proximite_commerces.chargeur_carroyage_insee import (
    generer_et_charger_carroyage_insee_feather,
)
from crater.indicateurs.proximite_commerces.referentiel_commerces.chargeur_referentiel_commerces import charger_referentiel_commerces
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestIndicateursProximiteCommerces(unittest.TestCase):
    def test_generer_carroyage_feather(self):
        # when
        generer_et_charger_carroyage_insee_feather(
            CHEMIN_INPUT_DATA / "carroyage_insee.gpkg",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        gdf = gpd.read_feather(CHEMIN_OUTPUT_DATA / "carroyage_insee_simplifie.feather")
        gdf.to_csv(
            CHEMIN_OUTPUT_DATA / "carroyage_insee_simplifie.csv",
            sep=";",
            index=False,
        )

        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "carroyage_insee_simplifie.csv",
            CHEMIN_OUTPUT_DATA / "carroyage_insee_simplifie.csv",
        )

    def test_calculer_indicateurs_proximite_par_carreau(self):
        # given
        gdf_carroyage = generer_et_charger_carroyage_insee_feather(
            CHEMIN_INPUT_DATA / "carroyage_insee.gpkg",
            CHEMIN_OUTPUT_DATA,
        )
        gdf_commerces = charger_referentiel_commerces(
            CHEMIN_INPUT_DATA / "referentiel_commerces.csv",
        )
        # when
        gdf_indicateurs_par_carreau = calculer_indicateurs_proximite_par_carreau(gdf_carroyage, gdf_commerces)
        # then
        self.exporter_indicateurs_par_carreaux(gdf_indicateurs_par_carreau)
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "proximite_commerces_par_types_commerces_et_carreaux.csv",
            CHEMIN_OUTPUT_DATA / "proximite_commerces_par_types_commerces_et_carreaux.csv",
        )

    def exporter_indicateurs_par_carreaux(self, gdf):
        gdf["x"] = gdf.geometry.x
        gdf["y"] = gdf.geometry.y
        gdf.loc[
            :,
            [
                "id_carreau",
                "code_type_commerce",
                "population_carroyage",
                "distance_m",
                "population_acces_a_pied",
                "population_acces_a_velo",
                "population_dependante_voiture",
                "carreau_dependant_voiture",
                "x",
                "y",
            ],
        ].to_csv(
            CHEMIN_OUTPUT_DATA / "proximite_commerces_par_types_commerces_et_carreaux.csv",
            sep=";",
            index=False,
            float_format="%.2f",
        )

    def test_calculer_indicateurs_proximite_commerces(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")

        # when
        calculer_indicateurs_proximite_commerces(
            territoires,
            CHEMIN_INPUT_DATA / "referentiel_commerces.csv",
            CHEMIN_INPUT_DATA / "carroyage_insee.gpkg",
            CHEMIN_INPUT_DATA / "geometries_communes_test",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "proximite_commerces_par_types_commerces.csv",
            CHEMIN_OUTPUT_DATA / "proximite_commerces_par_types_commerces.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "proximite_commerces.csv",
            CHEMIN_OUTPUT_DATA / "proximite_commerces.csv",
        )


if __name__ == "__main__":
    unittest.main()
