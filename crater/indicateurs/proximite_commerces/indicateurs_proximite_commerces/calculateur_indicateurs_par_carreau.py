import pandas as pd
from geopandas import GeoDataFrame
from pandas import DataFrame
from typing import Optional

from crater.commun.logger import log
from crater.indicateurs.proximite_commerces.config_proximite_commerces import (
    CODE_BOULANGERIE_PATISSERIE,
    CODE_COMMERCE_SPECIALISE,
    CODE_COMMERCE_TOUS_PLUS_PROCHE,
    CODE_COMMERCE_TOUS_PLUS_LOIN,
    CODE_COMMERCE_GENERALISTE,
    CODE_BOUCHERIE_POISSONNERIE,
    DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_PIED_METRES,
    DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES,
    CODE_COMMERCE_TOUS_MOYENNE,
    CODE_COMMERCE_TOUS_PLUS_LOIN_3EME_QUARTILE,
    NB_CATEGORIES_COMMERCES_3EME_QUARTILE,
)
from crater.indicateurs.proximite_commerces.indicateurs_proximite_commerces.recherche_plus_proche_voisin import (
    rechercher_plus_proche_voisin_selon_distance_euclidienne,
)


def calculer_indicateurs_proximite_par_carreau(gdf_carroyage: GeoDataFrame, gdf_commerces: GeoDataFrame) -> GeoDataFrame:
    log.info("   => Calcul des indicateurs par carreau")

    gdf_indicateurs_par_carreaux = _ajouter_indicateurs_pour_type_commerce(gdf_carroyage, gdf_commerces, CODE_COMMERCE_GENERALISTE)
    gdf_indicateurs_par_carreaux = _ajouter_indicateurs_pour_type_commerce(
        gdf_carroyage,
        gdf_commerces,
        CODE_COMMERCE_SPECIALISE,
        gdf_indicateurs_par_carreaux,
    )
    gdf_indicateurs_par_carreaux = _ajouter_indicateurs_pour_type_commerce(
        gdf_carroyage,
        gdf_commerces,
        CODE_BOUCHERIE_POISSONNERIE,
        gdf_indicateurs_par_carreaux,
    )
    gdf_indicateurs_par_carreaux = _ajouter_indicateurs_pour_type_commerce(
        gdf_carroyage,
        gdf_commerces,
        CODE_BOULANGERIE_PATISSERIE,
        gdf_indicateurs_par_carreaux,
    )
    gdf_indicateurs_par_carreaux = _ajouter_indicateurs_pour_type_commerce_TOUS(gdf_carroyage, gdf_indicateurs_par_carreaux)

    gdf_indicateurs_par_carreaux = _ajouter_colonne_population_a_moins_de(
        gdf_indicateurs_par_carreaux,
        DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_PIED_METRES,
        "population_acces_a_pied",
    )
    gdf_indicateurs_par_carreaux = _ajouter_colonne_population_a_moins_de(
        gdf_indicateurs_par_carreaux,
        DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES,
        "population_acces_a_velo",
    )
    gdf_indicateurs_par_carreaux = _ajouter_colonnes_dependance_voiture(gdf_indicateurs_par_carreaux)

    gdf_indicateurs_par_carreaux = gdf_indicateurs_par_carreaux.sort_values(by=["id_carreau", "code_type_commerce"])
    return gdf_indicateurs_par_carreaux


def _ajouter_indicateurs_pour_type_commerce(gdf_carroyage, gdf_commerces, type_commerce, gdf_resultat: Optional[DataFrame] = None) -> GeoDataFrame:
    log.info(f"      - Calcul pour le type de point de distribution {type_commerce}")

    gdf_carreaux = _calculer_distance_plus_proche_commerce(gdf_carroyage, gdf_commerces, type_commerce)
    if gdf_resultat is None:
        return gdf_carreaux
    else:
        return pd.concat([gdf_resultat, gdf_carreaux], ignore_index=True)


def _calculer_distance_plus_proche_commerce(gdf_carroyage: GeoDataFrame, gdf_commerces: GeoDataFrame, type_commerce: str) -> GeoDataFrame:
    gdf_commerces = gdf_commerces.loc[(gdf_commerces["code_type_commerce"] == type_commerce), :]

    gdf_carreaux_avec_plus_proche_voisin_et_distance = rechercher_plus_proche_voisin_selon_distance_euclidienne(gdf_carroyage, gdf_commerces)
    return gdf_carreaux_avec_plus_proche_voisin_et_distance.loc[
        :,
        [
            "id_carreau",
            "code_type_commerce",
            "population_carroyage",
            "distance_m",
            "geometry",
        ],
    ]


def _ajouter_colonne_population_a_moins_de(df_carreaux: DataFrame, seuil_distance_m: int, nom_colonne: str) -> DataFrame:
    df_carreaux[nom_colonne] = df_carreaux["population_carroyage"]
    df_carreaux.loc[(df_carreaux["distance_m"] > seuil_distance_m), [nom_colonne]] = 0
    return df_carreaux


def _ajouter_colonnes_dependance_voiture(df_carreaux: DataFrame) -> DataFrame:
    df_carreaux["carreau_dependant_voiture"] = df_carreaux["distance_m"] > DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES
    df_carreaux["population_dependante_voiture"] = df_carreaux["population_carroyage"] - df_carreaux["population_acces_a_velo"]
    return df_carreaux


def _ajouter_indicateurs_pour_type_commerce_TOUS(gdf_carroyage, gdf_indicateurs_par_carreaux):
    log.info("      - Calcul pour les types TOUS_xxx")

    df_valeurs_tous_commerces = gdf_indicateurs_par_carreaux.loc[:, ["id_carreau", "distance_m"]].sort_values(by="distance_m", ascending=True)

    gdf_valeur_plus_proche = df_valeurs_tous_commerces.groupby("id_carreau").min().reset_index()
    gdf_valeur_plus_proche["code_type_commerce"] = CODE_COMMERCE_TOUS_PLUS_PROCHE
    gdf_valeur_plus_proche = gdf_valeur_plus_proche.merge(gdf_carroyage, on="id_carreau")

    gdf_valeur_plus_loin = df_valeurs_tous_commerces.groupby("id_carreau").max().reset_index()
    gdf_valeur_plus_loin["code_type_commerce"] = CODE_COMMERCE_TOUS_PLUS_LOIN
    gdf_valeur_plus_loin = gdf_valeur_plus_loin.merge(gdf_carroyage, on="id_carreau")

    gdf_valeur_moyenne = df_valeurs_tous_commerces.groupby("id_carreau").mean().reset_index()
    gdf_valeur_moyenne["code_type_commerce"] = CODE_COMMERCE_TOUS_MOYENNE
    gdf_valeur_moyenne = gdf_valeur_moyenne.merge(gdf_carroyage, on="id_carreau")

    gdf_valeur_plus_loin_3Q = (
        df_valeurs_tous_commerces.groupby("id_carreau").head(NB_CATEGORIES_COMMERCES_3EME_QUARTILE).groupby("id_carreau").max().reset_index()
    )
    gdf_valeur_plus_loin_3Q["code_type_commerce"] = CODE_COMMERCE_TOUS_PLUS_LOIN_3EME_QUARTILE
    gdf_valeur_plus_loin_3Q = gdf_valeur_plus_loin_3Q.merge(gdf_carroyage, on="id_carreau")

    gdf_indicateurs_par_carreaux = pd.concat([gdf_indicateurs_par_carreaux, gdf_valeur_moyenne])
    gdf_indicateurs_par_carreaux = pd.concat([gdf_indicateurs_par_carreaux, gdf_valeur_plus_proche])
    gdf_indicateurs_par_carreaux = pd.concat([gdf_indicateurs_par_carreaux, gdf_valeur_plus_loin])
    gdf_indicateurs_par_carreaux = pd.concat([gdf_indicateurs_par_carreaux, gdf_valeur_plus_loin_3Q])
    return gdf_indicateurs_par_carreaux
