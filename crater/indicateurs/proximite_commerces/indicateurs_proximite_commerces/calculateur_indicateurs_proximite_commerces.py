from pathlib import Path

from pandas import DataFrame

from crater.indicateurs.proximite_commerces.config_proximite_commerces import CODE_COMMERCE_TOUS_PLUS_LOIN_3EME_QUARTILE
from crater.indicateurs.proximite_commerces.indicateurs_proximite_commerces.calculateur_indicateurs_par_carreau import (
    calculer_indicateurs_proximite_par_carreau,
)
from crater.indicateurs.proximite_commerces.indicateurs_proximite_commerces.calculateur_indicateurs_par_territoire import (
    calculer_indicateurs_proximite_commerces_par_type_commerce_et_territoire,
)
from crater.indicateurs.proximite_commerces.indicateurs_proximite_commerces.chargeur_carroyage_insee import (
    generer_et_charger_carroyage_insee_feather,
)
from crater.indicateurs.proximite_commerces.referentiel_commerces.chargeur_referentiel_commerces import (
    charger_referentiel_commerces,
)
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires
from crater.commun.exports.exporter_diagrammes import exporter_histogrammes
from crater.commun.logger import log
from crater.config.config_globale import NOM_DOSSIER_CACHE


def calculer_indicateurs_proximite_commerces(
    territoires: DataFrame,
    chemin_fichier_referentiel_commerces: Path,
    chemin_fichier_carroyage_insee: Path,
    chemin_dossier_geometries_communes: Path,
    chemin_dossier_output: Path,
):
    log.info("##### CALCUL PROXIMITE AUX COMMERCES #####")

    gdf_carroyage = generer_et_charger_carroyage_insee_feather(
        chemin_fichier_carroyage_insee,
        chemin_dossier_output / NOM_DOSSIER_CACHE,
    )
    gdf_commerces = charger_referentiel_commerces(chemin_fichier_referentiel_commerces)

    gdf_indicateurs_par_carreaux = calculer_indicateurs_proximite_par_carreau(gdf_carroyage, gdf_commerces)

    df_indicateurs_par_types_commerces = calculer_indicateurs_proximite_commerces_par_type_commerce_et_territoire(
        territoires,
        chemin_dossier_geometries_communes,
        gdf_indicateurs_par_carreaux,
    )
    df_indicateurs = calculer_indicateurs_proximite_commerces_par_territoires(df_indicateurs_par_types_commerces)

    exporter_df_indicateurs_par_territoires(
        df_indicateurs_par_types_commerces,
        chemin_dossier_output,
        "proximite_commerces_par_types_commerces",
        nom_colonne_regroupement="code_type_commerce",
    )
    exporter_df_indicateurs_par_territoires(
        df_indicateurs,
        chemin_dossier_output,
        "proximite_commerces",
    )
    exporter_histogrammes(
        df_indicateurs,
        [
            "note",
        ],
        chemin_dossier_output / "diagrammes",
    )


def calculer_indicateurs_proximite_commerces_par_territoires(
    df_indicateurs_par_types_commerces: DataFrame,
) -> DataFrame:
    df_indicateurs = df_indicateurs_par_types_commerces.loc[
        df_indicateurs_par_types_commerces["code_type_commerce"] == CODE_COMMERCE_TOUS_PLUS_LOIN_3EME_QUARTILE,
        :,
    ].copy()
    note_population_independante_voiture = (100 - df_indicateurs["part_population_dependante_voiture_pourcent"]) / 10
    df_indicateurs["note"] = note_population_independante_voiture.round().astype("Int64")
    return df_indicateurs.loc[
        :,
        [
            "nom_territoire",
            "categorie_territoire",
            "part_territoire_dependant_voiture_pourcent",
            "part_population_dependante_voiture_pourcent",
            "note",
        ],
    ]
