import unittest
from pathlib import Path

from crater.indicateurs.proximite_commerces.referentiel_commerces.calculateur_referentiel_commerces import (
    calculer_referentiel_commerces,
)
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestReferentielCommerces(unittest.TestCase):
    def test_calculer_referentiel_commerces(self):
        # when
        calculer_referentiel_commerces(
            CHEMIN_INPUT_DATA / "osm-shop-fr.csv",
            CHEMIN_INPUT_DATA / "bpe20_ensemble_xy_csv.zip",
            "bpe20_ensemble_xy.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "referentiel_commerces.csv",
            CHEMIN_OUTPUT_DATA / "referentiel_commerces.csv",
        )


if __name__ == "__main__":
    unittest.main()
