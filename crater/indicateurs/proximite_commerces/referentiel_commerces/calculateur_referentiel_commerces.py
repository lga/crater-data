from pathlib import Path

import geopandas as gpd
import pandas as pd

from crater.indicateurs.proximite_commerces.config_proximite_commerces import RAYON_RECHERCHE_DOUBLONS_OSM_BPE_METRES
from crater.indicateurs.proximite_commerces.referentiel_commerces.chargeur_commerces_bpe import (
    charger_donnees_bpe,
)
from crater.indicateurs.proximite_commerces.referentiel_commerces.chargeur_commerces_osm import (
    charger_donnees_osm,
)
from crater.commun.exports.export_fichier import exporter_df, reinitialiser_dossier
from crater.commun.logger import log


def calculer_referentiel_commerces(
    chemin_fichier_commerces_osm: Path,
    chemin_fichier_zip_commerces_bpe: Path,
    nom_fichier_dans_zip_bpe: str,
    chemin_dossier_output: Path,
):
    log.info("##### CALCUL REFERENTIEL COMMERCES #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_osm = charger_donnees_osm(chemin_fichier_commerces_osm)
    df_bpe = charger_donnees_bpe(chemin_fichier_zip_commerces_bpe, nom_fichier_dans_zip_bpe)
    df_consolide = _construire_base_consolidee_osm_bpe(df_osm, df_bpe)
    df_consolide = _ajouter_colonne_id_commerce(df_consolide)
    df_consolide = _supprimer_doublons_osm_bpe(df_consolide)
    df_consolide = df_consolide.loc[
        :,
        [
            "id_commerce",
            "nom_commerce",
            "code_type_commerce",
            "libelle_type_commerce",
            "longitude",
            "latitude",
            "source",
        ],
    ]
    exporter_df(
        df_consolide,
        chemin_dossier_output,
        "referentiel_commerces",
        float_format="%.6f",
    )


def _construire_base_consolidee_osm_bpe(df_osm, df_bpe):
    df_osm["source"] = "OSM"
    df_bpe["source"] = "BPE"
    return pd.concat([df_bpe, df_osm])


def _ajouter_colonne_id_commerce(df_commerces):
    df_commerces = df_commerces.astype({"longitude": float, "latitude": float})
    df_commerces["hash_longitude"] = df_commerces["longitude"] * 100000
    df_commerces["hash_latitude"] = df_commerces["latitude"] * 100000
    df_commerces = df_commerces.astype({"hash_longitude": int, "hash_latitude": int})
    df_commerces["id_commerce"] = (
        df_commerces["source"]
        + "_"
        + df_commerces["code_type_commerce"]
        + "_"
        + df_commerces["hash_longitude"].map(str)
        + "_"
        + df_commerces["hash_latitude"].map(str)
    )
    return df_commerces


def _supprimer_doublons_osm_bpe(df_commerces):
    """
    Supprime les commerces en doublon entre OSM et BPE pour ne garder que ceux d'OSM
    Un commerce BPE est considéré comme doublon s'il existe un commerce de même type dans OSM dans un rayon de x mètres
    Voir la variable RAYON_RECHERCHE_DOUBLONS_OSM_BPE_METRES pour la valeur de x
    """
    gdf = gpd.GeoDataFrame(
        df_commerces,
        geometry=gpd.points_from_xy(df_commerces["longitude"], df_commerces["latitude"]),
        crs="EPSG:4326",
    )
    # Projection Lambert pour fiabiliser l'évaluation de la distance lors du buffer (1 unité = 1 mètre)
    gdf = gdf.to_crs("EPSG:2154")
    gdf_osm = gdf.loc[gdf.source == "OSM"]
    gdf_bpe = gdf.loc[gdf.source == "BPE"]

    perimetre_recherche_voisins_osm = (
        gdf_osm.copy()
        .loc[:, ["id_commerce", "code_type_commerce", "geometry"]]
        .rename(
            columns={
                "id_commerce": "id_commerce_osm",
                "code_type_commerce": "code_type_commerce_osm",
            }
        )
    )
    perimetre_recherche_voisins_osm["geometry"] = perimetre_recherche_voisins_osm.buffer(RAYON_RECHERCHE_DOUBLONS_OSM_BPE_METRES)

    voisins_osm = gpd.sjoin(perimetre_recherche_voisins_osm, gdf_bpe)
    id_doublons = voisins_osm.loc[
        voisins_osm["code_type_commerce_osm"] == voisins_osm["code_type_commerce"],
        ["id_commerce_osm", "id_commerce"],
    ]
    id_doublons["doublon_a_supprimer"] = True

    df_commerces = df_commerces.merge(id_doublons, how="left", on="id_commerce")

    df_commerces["doublon_a_supprimer"] = df_commerces["doublon_a_supprimer"].fillna(False)
    log.info(
        f"      - suppression de {df_commerces[df_commerces['doublon_a_supprimer'] == True].shape[0]} commerces BPE "  # noqa: E712
        f"en doublon de commerces OSM, sur un total de {df_commerces.shape[0]} commerces (OSM+BPE) "
    )
    df_commerces_sans_doublons = df_commerces.loc[~df_commerces.doublon_a_supprimer, :]

    return df_commerces_sans_doublons
