import pandas
from crater.indicateurs.productions_besoins import config
from crater.config import groupes_cultures as config_groupes_cultures


def charger_definition_cultures_et_groupes_cultures_besoins():
    return (
        pandas.read_csv(config_groupes_cultures.FICHIER_CONFIG_CULTURES_ET_GROUPES_CULTURES, sep=";")
        .query("type_culture != 'PRODUCTION'")
        .assign(alimentation_animale=lambda x: x.type_culture == "BESOIN_ANIMAUX")
        .drop("type_culture", axis=1)
    )


def charger_besoins_parcel(fichier_besoins_parcel):
    return pandas.read_csv(
        fichier_besoins_parcel,
        sep=";",
        dtype={
            "id_territoire_crater": "str",
            "id_produit_parcel": "str",
            "SurfaceArea": "float",
            "SurfaceAreaBio": "float",
            "Curseur_Min_Bio": "float",
        },
    ).loc[
        :,
        [
            "id_territoire_crater",
            "id_produit_parcel",
            "SurfaceArea",
            "SurfaceAreaBio",
            "Curseur_Min_Bio",
        ],
    ]


def charger_repartition_produits_elevage_sur_groupes_cultures():
    return pandas.read_csv(config.FICHIER_CONFIG_PRODUITS_ELEVAGE_VERS_CODES_CULTURES, sep=";", dtype="str").drop(columns=["nom_produit_parcel"])


def charger_produits_hors_elevage_vers_cultures_besoins_humains():
    return pandas.read_csv(
        config.FICHIER_CONFIG_PRODUITS_HORS_ELEVAGE_VERS_CODES_CULTURES,
        sep=";",
        dtype="str",
    )


def charger_definition_cultures_et_groupes_cultures_productions():
    nomenclature_cutlures_productions = (
        pandas.read_csv(config_groupes_cultures.FICHIER_CONFIG_CULTURES_ET_GROUPES_CULTURES, sep=";")
        .query("type_culture == 'PRODUCTION'")
        .drop("type_culture", axis=1)
    )
    return nomenclature_cutlures_productions
