import pandas

import crater.donnees_sources.surface_agricole_utile.chargeur_sau_par_commune_et_culture as chargeur_sau_par_commune_et_culture
from crater.indicateurs.productions_besoins.chargeur_sources_productions_besoins import (
    charger_definition_cultures_et_groupes_cultures_productions,
)
from crater.indicateurs.productions_besoins.export_productions_besoins import (
    exporter_sau_par_culture,
)
from crater.commun.calculs.outils_dataframes import produit_cartesien, merge_strict
from crater.commun.calculs.outils_territoires import (
    categories_territoires_hors_communes,
    traduire_categorie_territoire_vers_id_categorie_territoire,
    extraire_df_communes_appartenant_a_une_categorie_de_territoire,
)
from crater.commun.logger import log
from crater.config import groupes_cultures as config_groupes_cultures
from crater.config.config_globale import CATEGORIES_TERRITOIRES


def calculer_productions(
    territoires,
    fichiers_sau_productions,
    chemin_dossier_resultats,
):
    log.info("##### CALCUL PRODUCTION #####")

    df_productions_par_cultures = _initialiser_df_productions_par_cultures(territoires)
    df_productions_par_cultures = _ajouter_colonnes_productions(
        df_productions_par_cultures,
        fichiers_sau_productions,
    )
    df_productions_par_cultures = _aggreger_productions_sur_categories_territoires(df_productions_par_cultures)
    df_productions_par_cultures = _ajouter_colonnes_cultures_et_groupes_cultures(df_productions_par_cultures)
    productions_par_groupe_culture = (
        df_productions_par_cultures.groupby(["id_territoire", "code_groupe_culture"])["production_culture_ha"]
        .sum()
        .reset_index(name="production_groupe_culture_ha")
    )

    df_productions_par_cultures = merge_strict(
        df_productions_par_cultures,
        productions_par_groupe_culture,
        how="left",
        on=["id_territoire", "code_groupe_culture"],
    )

    df_productions_par_cultures = df_productions_par_cultures.loc[
        :,
        [
            "id_territoire",
            "categorie_territoire",
            "nom_territoire",
            "code_groupe_culture",
            "nom_groupe_culture",
            "production_groupe_culture_ha",
            "code_culture",
            "nom_culture",
            "production_culture_ha",
        ],
    ]

    log.info("   => export des fichiers résultat 'productions'")
    exporter_sau_par_culture(df_productions_par_cultures, chemin_dossier_resultats)
    return df_productions_par_cultures


def _initialiser_df_productions_par_cultures(territoires):
    df_productions_par_cultures = territoires.copy().reset_index()
    df_productions_par_cultures = _construire_produit_cartesien_avec_cultures(df_productions_par_cultures)
    return df_productions_par_cultures


def _construire_produit_cartesien_avec_cultures(df_productions_par_cultures):
    nomenclature_cultures_productions = charger_definition_cultures_et_groupes_cultures_productions()
    id_territoires_par_cultures = produit_cartesien(
        df_productions_par_cultures.loc[:, ["id_territoire"]],
        nomenclature_cultures_productions,
    )
    df_productions_par_cultures = merge_strict(id_territoires_par_cultures, df_productions_par_cultures, how="left", on="id_territoire")
    return df_productions_par_cultures


def _ajouter_colonnes_cultures_et_groupes_cultures(df_productions):
    definitions_cultures_et_groupes_cultures_productions = charger_definition_cultures_et_groupes_cultures_productions()
    df_productions = merge_strict(
        df_productions,
        definitions_cultures_et_groupes_cultures_productions,
        how="left",
        on="code_culture",
    )
    return df_productions


def _aggreger_productions_sur_categories_territoires(df_productions):
    for categorie_territoire in categories_territoires_hors_communes(CATEGORIES_TERRITOIRES):
        df_productions = _aggreger_productions_sur_categorie_territoire(df_productions, categorie_territoire)

    return df_productions.loc[
        :,
        [
            "id_territoire",
            "categorie_territoire",
            "nom_territoire",
            "code_culture",
            "production_culture_ha",
        ],
    ]


def _aggreger_productions_sur_categorie_territoire(df_productions, categorie_territoire):
    id_categorie_territoire = traduire_categorie_territoire_vers_id_categorie_territoire(categorie_territoire)
    df_productions_par_commune = extraire_df_communes_appartenant_a_une_categorie_de_territoire(df_productions, categorie_territoire)
    df_indicateur_aggrege = (
        df_productions_par_commune.groupby([id_categorie_territoire, "code_culture"], as_index=False)[["production_culture_ha"]]
        .sum()
        .set_index([id_categorie_territoire, "code_culture"])
    )
    df_productions = df_productions.set_index(["id_territoire", "code_culture"], drop=False)
    df_productions.update(df_indicateur_aggrege)
    df_productions = df_productions.reset_index(drop=True)
    return df_productions


def _ajouter_colonnes_productions(
    df_productions,
    fichiers_sau_productions,
):
    df_productions_communes_par_cultures = _calculer_productions_communes_par_culture(
        fichiers_sau_productions,
    )
    df_productions = pandas.merge(
        df_productions,
        df_productions_communes_par_cultures,
        how="left",
        left_on=["id_territoire", "code_culture"],
        right_on=["id_commune", "code_culture"],
        suffixes=(False, False),
    ).drop(columns=["id_commune"])
    df_productions.loc[(df_productions["categorie_territoire"] == "COMMUNE"), ["production_culture_ha"]] = df_productions.loc[
        (df_productions["categorie_territoire"] == "COMMUNE"), ["production_culture_ha"]
    ].fillna(0)
    return df_productions


def _calculer_productions_communes_par_culture(
    fichiers_sau_productions_par_culture_rpg,
):
    donnees_productions_par_cultures = chargeur_sau_par_commune_et_culture.charger(fichiers_sau_productions_par_culture_rpg)
    codes_rpg_vers_code_cultures = pandas.read_csv(config_groupes_cultures.FICHIER_CONFIG_CORRESPONDANCES_RPG_VERS_CRATER, sep=";", dtype="str")
    donnees_productions_par_cultures = merge_strict(donnees_productions_par_cultures, codes_rpg_vers_code_cultures, how="left", on="code_culture_rpg")
    donnees_productions_par_cultures = (
        donnees_productions_par_cultures.loc[:, ["id_commune", "sau_ha", "code_culture_crater"]]
        .rename(columns={"code_culture_crater": "code_culture"})
        .rename(columns={"sau_ha": "production_culture_ha"})
    )
    donnees_productions_par_cultures["production_culture_ha"] = donnees_productions_par_cultures["production_culture_ha"].astype("float64")

    donnees_productions_par_cultures = donnees_productions_par_cultures.groupby(["id_commune", "code_culture"], as_index=False).sum()
    return donnees_productions_par_cultures
