# Chemin vers config, peut être redéfinie par les tests pour utiliser une config simplifiée
from pathlib import Path

FICHIER_CONFIG_PRODUITS_ELEVAGE_VERS_CODES_CULTURES: Path = Path(__file__).parent / "produits_elevage_vers_codes_cultures.csv"
FICHIER_CONFIG_PRODUITS_HORS_ELEVAGE_VERS_CODES_CULTURES: Path = Path(__file__).parent / "produits_hors_elevage_vers_codes_cultures.csv"
