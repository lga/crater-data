import copy
import unittest
from pathlib import Path

import pandas

from crater.indicateurs.productions_besoins import config
from crater.indicateurs.productions_besoins.besoins import calculer_besoins
from crater.indicateurs.productions_besoins.calculateur_productions_besoins import (
    calculer_productions_besoins,
    _calculer_indicateurs_productions_besoins,
)
from crater.indicateurs.productions_besoins.productions import calculer_productions
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    augmenter_nombre_colonnes_affichees_pandas,
    assert_csv_files_are_equals,
)
from crater.config import groupes_cultures as config_groupes_cultures

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestProductionBesoins(unittest.TestCase):
    def test_calculer_besoins(self):
        CHEMIN_INPUT_DATA_TEST_COURANT = CHEMIN_INPUT_DATA / "commun"
        CHEMIN_EXPECTED_DATA_TEST_COURANT = CHEMIN_EXPECTED_DATA / "test_calculer_besoins"
        CHEMIN_OUTPUT_DATA_TEST_COURANT = CHEMIN_OUTPUT_DATA / "test_calculer_besoins"
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA_TEST_COURANT)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA_TEST_COURANT / "referentiel_territoires.csv")
        territoires_initial = copy.deepcopy(territoires)
        config.FICHIER_CONFIG_PRODUITS_ELEVAGE_VERS_CODES_CULTURES = CHEMIN_INPUT_DATA_TEST_COURANT / "produits_elevage_vers_codes_cultures.csv"
        config.FICHIER_CONFIG_PRODUITS_HORS_ELEVAGE_VERS_CODES_CULTURES = (
            CHEMIN_INPUT_DATA_TEST_COURANT / "produits_hors_elevage_vers_codes_cultures.csv"
        )
        config_groupes_cultures.FICHIER_CONFIG_CULTURES_ET_GROUPES_CULTURES = CHEMIN_INPUT_DATA_TEST_COURANT / "cultures_et_groupes_cultures.csv"
        # when
        calculer_besoins(
            territoires,
            CHEMIN_INPUT_DATA_TEST_COURANT / "reponse_api_parcel_besoins_sau_par_produits.csv",
            CHEMIN_INPUT_DATA_TEST_COURANT / "reponse_api_parcel_besoins_sau_par_produits_moins_50p.csv",
            CHEMIN_OUTPUT_DATA_TEST_COURANT,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_TEST_COURANT / "besoins_par_culture.csv",
            CHEMIN_OUTPUT_DATA_TEST_COURANT / "besoins_par_culture.csv",
        )
        pandas.testing.assert_frame_equal(territoires_initial, territoires)

    def test_calculer_production(self):
        CHEMIN_INPUT_DATA_TEST_COURANT = CHEMIN_INPUT_DATA / "commun"
        CHEMIN_EXPECTED_DATA_TEST_COURANT = CHEMIN_EXPECTED_DATA / "test_calculer_production"
        CHEMIN_OUTPUT_DATA_TEST_COURANT = CHEMIN_OUTPUT_DATA / "test_calculer_production"
        config_groupes_cultures.FICHIER_CONFIG_CULTURES_ET_GROUPES_CULTURES = CHEMIN_INPUT_DATA_TEST_COURANT / "cultures_et_groupes_cultures.csv"

        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA_TEST_COURANT)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA_TEST_COURANT / "referentiel_territoires.csv")
        territoires_initial = copy.deepcopy(territoires)
        config_groupes_cultures.FICHIER_CONFIG_CORRESPONDANCES_RPG_VERS_CRATER = CHEMIN_INPUT_DATA_TEST_COURANT / "rpg_vers_codes_cultures.csv"
        # when
        calculer_productions(
            territoires,
            CHEMIN_INPUT_DATA_TEST_COURANT / "sau_par_commune_et_culture",
            CHEMIN_OUTPUT_DATA_TEST_COURANT,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_TEST_COURANT / "sau_par_culture.csv",
            CHEMIN_OUTPUT_DATA_TEST_COURANT / "sau_par_culture.csv",
        )
        pandas.testing.assert_frame_equal(territoires_initial, territoires)

    def test_calculer_indicateurs_productions_besoins(self):
        CHEMIN_INPUT_DATA_TEST_COURANT = CHEMIN_INPUT_DATA / "test_calculer_indicateurs_production_besoins"
        CHEMIN_EXPECTED_DATA_TEST_COURANT = CHEMIN_EXPECTED_DATA / "test_calculer_indicateurs_production_besoins"
        CHEMIN_OUTPUT_DATA_TEST_COURANT = CHEMIN_OUTPUT_DATA / "test_calculer_indicateurs_production_besoins"

        reinitialiser_dossier(CHEMIN_OUTPUT_DATA_TEST_COURANT)

        besoins_par_culture = pandas.read_csv(CHEMIN_INPUT_DATA_TEST_COURANT / "besoins_par_culture.csv", sep=";")
        productions_par_culture = pandas.read_csv(CHEMIN_INPUT_DATA_TEST_COURANT / "production_par_culture.csv", sep=";")

        _calculer_indicateurs_productions_besoins(besoins_par_culture, productions_par_culture, CHEMIN_OUTPUT_DATA_TEST_COURANT)

        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_TEST_COURANT / "productions_besoins_par_groupe_culture.csv",
            CHEMIN_OUTPUT_DATA_TEST_COURANT / "productions_besoins_par_groupe_culture.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_TEST_COURANT / "productions_besoins.csv",
            CHEMIN_OUTPUT_DATA_TEST_COURANT / "productions_besoins.csv",
        )

    def test_calculer_productions_besoins(self):
        # given
        CHEMIN_INPUT_DATA_TEST_COURANT = CHEMIN_INPUT_DATA / "commun"
        CHEMIN_EXPECTED_DATA_TEST_COURANT = CHEMIN_EXPECTED_DATA / "test_calculer_production_besoins"
        CHEMIN_OUTPUT_DATA_TEST_COURANT = CHEMIN_OUTPUT_DATA / "test_calculer_production_besoins"

        reinitialiser_dossier(CHEMIN_OUTPUT_DATA_TEST_COURANT)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA_TEST_COURANT / "referentiel_territoires.csv")
        config.FICHIER_CONFIG_PRODUITS_ELEVAGE_VERS_CODES_CULTURES = CHEMIN_INPUT_DATA_TEST_COURANT / "produits_elevage_vers_codes_cultures.csv"
        config.FICHIER_CONFIG_PRODUITS_HORS_ELEVAGE_VERS_CODES_CULTURES = (
            CHEMIN_INPUT_DATA_TEST_COURANT / "produits_hors_elevage_vers_codes_cultures.csv"
        )
        config_groupes_cultures.FICHIER_CONFIG_CULTURES_ET_GROUPES_CULTURES = CHEMIN_INPUT_DATA_TEST_COURANT / "cultures_et_groupes_cultures.csv"
        config_groupes_cultures.FICHIER_CONFIG_CORRESPONDANCES_RPG_VERS_CRATER = CHEMIN_INPUT_DATA_TEST_COURANT / "rpg_vers_codes_cultures.csv"

        # when
        calculer_productions_besoins(
            territoires,
            CHEMIN_INPUT_DATA_TEST_COURANT / "reponse_api_parcel_besoins_sau_par_produits.csv",
            CHEMIN_INPUT_DATA_TEST_COURANT / "reponse_api_parcel_besoins_sau_par_produits_moins_50p.csv",
            CHEMIN_INPUT_DATA_TEST_COURANT / "sau_par_commune_et_culture",
            CHEMIN_OUTPUT_DATA_TEST_COURANT,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_TEST_COURANT / "productions_besoins_par_groupe_culture.csv",
            CHEMIN_OUTPUT_DATA_TEST_COURANT / "productions_besoins_par_groupe_culture.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA_TEST_COURANT / "productions_besoins.csv",
            CHEMIN_OUTPUT_DATA_TEST_COURANT / "productions_besoins.csv",
        )
