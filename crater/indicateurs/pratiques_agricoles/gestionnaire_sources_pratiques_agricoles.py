from pathlib import Path

import pandas as pd

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.logger import log


def charger_source_hvn(chemin_fichier: Path) -> pd.DataFrame:
    log.info("   => chargement HVN depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="Feuil1",
            usecols="A:J",
            dtype={
                "COD_COM2018": "str",
                "NOM_COM": "str",
                "HVN_Ind1_17": "float",
                "HVN_Ind2_17": "float",
                "HVN_Ind3_17": "float",
                "Score_HVN_2017": "float",
            },
        )
        .rename(
            columns={
                "COD_COM2018": "id_commune",
                "NOM_COM": "nom_commune",
                "HVN_Ind1_17": "hvn_indice1",
                "HVN_Ind2_17": "hvn_indice2",
                "HVN_Ind3_17": "hvn_indice3",
                "Score_HVN_2017": "hvn_score",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_commune",
                "hvn_indice1",
                "hvn_indice2",
                "hvn_indice3",
                "hvn_score",
            ]
        )
    )

    verifier_absence_doublons(df, "id_commune")

    return df


def importer_hvn(chemin_fichier_hvn: Path):
    hvn_communes = charger_source_hvn(chemin_fichier_hvn)
    hvn_communes.id_commune = traduire_code_insee_vers_id_commune_crater(hvn_communes.id_commune)
    return hvn_communes
