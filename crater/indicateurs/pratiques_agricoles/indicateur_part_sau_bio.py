from pathlib import Path

import pandas as pd


def ajouter_indicateur_part_sau_bio(donnees_pratiques_agricoles: pd.DataFrame, chemin_fichier_surfaces_bio: Path) -> pd.DataFrame:
    donnees_pratiques_agricoles = _ajouter_donnees_surfaces_bio(donnees_pratiques_agricoles, chemin_fichier_surfaces_bio)
    donnees_pratiques_agricoles = _calculer_part_sau_bio_pourcent(donnees_pratiques_agricoles)

    return donnees_pratiques_agricoles


def _ajouter_donnees_surfaces_bio(donnees_pratiques_agricoles: pd.DataFrame, chemin_fichier_surfaces_bio) -> pd.DataFrame:
    surfaces_bio = pd.read_csv(chemin_fichier_surfaces_bio, sep=";").set_index("id_territoire").loc[:, "surface_bio_ha"]
    donnees_pratiques_agricoles = donnees_pratiques_agricoles.join(surfaces_bio)

    return donnees_pratiques_agricoles


def _calculer_part_sau_bio_pourcent(donnees_pratiques_agricoles):
    mask_donnees = donnees_pratiques_agricoles["sau_productive_ha"] > 0
    donnees_pratiques_agricoles.loc[mask_donnees, "part_sau_bio_pourcent"] = (
        (donnees_pratiques_agricoles.loc[mask_donnees, "surface_bio_ha"] / donnees_pratiques_agricoles.loc[mask_donnees, "sau_productive_ha"] * 100)
        .clip(0, 100)
        .round(2)
    )

    return donnees_pratiques_agricoles
