from pathlib import Path

import numpy as np
import pandas as pd

from crater.indicateurs.pratiques_agricoles.gestionnaire_sources_pratiques_agricoles import (
    importer_hvn,
)
from crater.commun.calculs.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales,
)
from crater.config.config_sources import ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_HVN


def ajouter_indicateur_hvn(donnees_pratiques_agricoles: pd.DataFrame, chemin_fichier_hvn: Path) -> pd.DataFrame:
    hvn_communes = importer_hvn(chemin_fichier_hvn).rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")

    donnees_pratiques_agricoles = donnees_pratiques_agricoles.join(hvn_communes)

    donnees_pratiques_agricoles.loc[
        donnees_pratiques_agricoles.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_HVN,
        "hvn_indice1",
    ] = np.nan
    donnees_pratiques_agricoles.loc[
        donnees_pratiques_agricoles.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_HVN,
        "hvn_indice2",
    ] = np.nan
    donnees_pratiques_agricoles.loc[
        donnees_pratiques_agricoles.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_HVN,
        "hvn_indice3",
    ] = np.nan
    donnees_pratiques_agricoles.loc[
        donnees_pratiques_agricoles.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_HVN, "hvn_score"
    ] = np.nan
    donnees_pratiques_agricoles.loc["C-75056", slice("hvn_indice1", "hvn_score")] = np.nan  # HVN de Paris devrait etre NA

    donnees_pratiques_agricoles = ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales(
        donnees_pratiques_agricoles, "hvn_indice1", "sau_productive_ha"
    )
    donnees_pratiques_agricoles = ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales(
        donnees_pratiques_agricoles, "hvn_indice2", "sau_productive_ha"
    )
    donnees_pratiques_agricoles = ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales(
        donnees_pratiques_agricoles, "hvn_indice3", "sau_productive_ha"
    )
    donnees_pratiques_agricoles = ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales(
        donnees_pratiques_agricoles, "hvn_score", "sau_productive_ha"
    )

    return donnees_pratiques_agricoles
