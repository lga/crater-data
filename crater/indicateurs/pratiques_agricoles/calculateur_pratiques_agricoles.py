from pathlib import Path

from pandas import DataFrame

from crater.indicateurs.pratiques_agricoles.indicateur_hvn import ajouter_indicateur_hvn
from crater.indicateurs.pratiques_agricoles.indicateur_part_sau_bio import (
    ajouter_indicateur_part_sau_bio,
)
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.exports.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log
from crater.config.config_globale import IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX


def calculer_pratiques_agricoles(
    territoires: DataFrame,
    fichier_occupation_sols: Path,
    chemin_fichier_hvn: Path,
    chemin_fichier_surfaces_bio: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### CALCUL PRATIQUES AGRICOLES #####")

    reinitialiser_dossier(chemin_dossier_output)

    territoires = charger_et_joindre_df_indicateurs(territoires, fichier_occupation_sols)

    colonnes = (
        ["nom_territoire", "categorie_territoire"]
        + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
        + ["annee_dernier_mouvement_commune", "sau_productive_ha"]
    )
    donnees_pratiques_agricoles = territoires.loc[:, colonnes]

    donnees_pratiques_agricoles = ajouter_indicateur_hvn(donnees_pratiques_agricoles, chemin_fichier_hvn)
    donnees_pratiques_agricoles = ajouter_indicateur_part_sau_bio(donnees_pratiques_agricoles, chemin_fichier_surfaces_bio)

    return exporter_df_indicateurs_par_territoires(
        donnees_pratiques_agricoles.drop(
            columns=["sau_productive_ha", "annee_dernier_mouvement_commune"] + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX
        ).reset_index(),
        chemin_dossier_output,
        "pratiques_agricoles",
    )
