import unittest
from pathlib import Path

from crater.indicateurs.pratiques_agricoles.calculateur_pratiques_agricoles import (
    calculer_pratiques_agricoles,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()


CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestPratiquesAgricoles(unittest.TestCase):
    def test_pratiques_agricoles(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # when
        calculer_pratiques_agricoles(
            territoires,
            CHEMIN_INPUT_DATA / "occupation_sols" / "occupation_sols.csv",
            CHEMIN_INPUT_DATA / "hvn.xlsx",
            CHEMIN_INPUT_DATA / "donnees_sau_bio.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "pratiques_agricoles.csv",
            CHEMIN_OUTPUT_DATA / "pratiques_agricoles.csv",
        )


if __name__ == "__main__":
    unittest.main()
