import unittest
from pathlib import Path

from crater.indicateurs.analogue_climatique.calculateur_analogue_climatique_2050 import (
    calculer_analogues_climatiques_2050,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestCalculateurAnaloguesClimatiques(unittest.TestCase):
    def test_calcul_analogue_climatique(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")
        # when
        calculer_analogues_climatiques_2050(territoires, CHEMIN_INPUT_DATA / "cc-explorer", CHEMIN_OUTPUT_DATA)
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "analogues_climatiques.csv",
            CHEMIN_OUTPUT_DATA / "analogues_climatiques.csv",
        )


if __name__ == "__main__":
    unittest.main()
