from pathlib import Path

import pandas as pd
from pandas import DataFrame
from crater.commun.logger import log


from crater.commun.exports.export_fichier import (
    reinitialiser_dossier,
    exporter_df_indicateurs_par_territoires,
)
from crater.config.config_sources import NOM_FICHIER_CC_EXPLORER_ANALOGUES_CLIMATIQUES_2050


def calculer_analogues_climatiques_2050(
    territoires: DataFrame,
    fichier_ccexplorer_analogue_climatique_2050: Path,
    chemin_dossier_output: Path,
):
    reinitialiser_dossier(chemin_dossier_output)
    df_source_analogue_climatique_2050 = _calculer_analogue_climatique_2050(territoires, fichier_ccexplorer_analogue_climatique_2050)
    exporter_df_indicateurs_par_territoires(df_source_analogue_climatique_2050, chemin_dossier_output, "analogues_climatiques")


def _calculer_analogue_climatique_2050(territoires: DataFrame, chemin_dossier_ccexplorer_analogue_climatique_2050: Path) -> DataFrame:
    log.info("##### CHARGEMENT DES DONNEES SOURCES ANALOGUES CLIMATIQUES 2050 #####")

    df_source_analogue_climatique_2050 = pd.read_csv(
        chemin_dossier_ccexplorer_analogue_climatique_2050 / NOM_FICHIER_CC_EXPLORER_ANALOGUES_CLIMATIQUES_2050, sep=","
    )

    # Tri secondaire par nom de commune pour garantir la reproductibilité
    # rares cas d'analogues climatiques au climat identique (dû à l'extrême proximité géographique) - ex : Porto et Vila Nova de Gaia
    df_source_analogue_climatique_2050 = (
        df_source_analogue_climatique_2050.sort_values(by=["score_similarite", "nom_commune_analogue_climatique_2000_2050"], ascending=[False, True])
        .groupby("id_departement")
        .first()
        .reset_index()
    )
    df_analogues_climatiques = (
        territoires.copy().reset_index(drop=False).loc[:, ["id_territoire", "nom_territoire", "id_departement", "categorie_territoire"]]
    )

    df_analogues_climatiques["id_departement_reference"] = df_analogues_climatiques["id_departement"]

    df_analogues_climatiques.loc[df_analogues_climatiques["categorie_territoire"] == "DEPARTEMENT", "id_departement_reference"] = (
        df_analogues_climatiques["id_territoire"]
    )
    df_analogues_climatiques = df_analogues_climatiques.drop(columns=["id_departement"])
    df_analogues_climatiques = df_analogues_climatiques.merge(
        df_source_analogue_climatique_2050, left_on="id_departement_reference", right_on="id_departement", how="left"
    ).drop(columns=["score_similarite", "id_departement_reference", "id_departement"])

    df_analogues_climatiques = df_analogues_climatiques.rename(
        columns={
            "id_commune_cheflieu_departement": "id_commune_reference",
            "nom_commune_cheflieu_departement": "nom_commune_reference",
            "longitude_commune_cheflieu_departement": "longitude_commune_reference",
            "latitude_commune_cheflieu_departement": "latitude_commune_reference",
        }
    )
    return df_analogues_climatiques
