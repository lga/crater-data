from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.logger import log


def charger(chemin_fichier: Path) -> DataFrame:
    log.info("   => chargement historique population totale depuis %s", chemin_fichier)

    df = pd.read_excel(
        chemin_fichier,
        sheet_name="Grille_Densite",
        usecols="A:C",
        skiprows=4,
        dtype={
            "CODGEO": "str",
            "LIBGEO": "str",
            "DENS": "Int64",
        },
        na_values=[""],
    ).rename(
        columns={
            "CODGEO": "id_commune",
            "LIBGEO": "nom_commune",
            "DENS": "code_densite",
        },
        errors="raise",
    )

    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    verifier_absence_doublons(df, "id_commune")

    return df
