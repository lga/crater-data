from pathlib import Path

import pandas
from pandas import DataFrame

from crater.indicateurs.population import (
    chargeur_population_totale_historique,
    chargeur_grille_densite_communale,
)
from crater.indicateurs.population import chargeur_population_totale
from crater.referentiel_territoires.calculateur_referentiel_territoires import (
    ajouter_id_commune_de_arrondissement,
)
from crater.commun.calculs.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    obtenir_arrondissements_depuis_territoires,
    vider_colonne_selon_mouvements_communes,
)
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires, reinitialiser_dossier
from crater.commun.logger import log
from crater.config.config_globale import ANNEE_REFERENTIEL_TERRITOIRES_INSEE
from crater.config.config_sources import (
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE_HISTORIQUE,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_GRILLE_DENSITE_COMMUNALE,
)

DICTIONNAIRE_CODES_DENSITE = {
    1: "GRAND_CENTRE_URBAIN",
    2: "CENTRE_URBAIN_INTERMEDIAIRE",
    3: "CEINTURE_URBAINE",
    4: "PETITE_VILLE",
    5: "BOURG_RURAL",
    6: "COMMUNE_A_HABITAT_DISPERSE",
    7: "COMMUNE_A_HABITAT_TRES_DISPERSE",
}


def calculer_population(
    territoires: DataFrame,
    chemin_fichier_population_totale: Path,
    chemin_fichier_population_totale_historique: Path,
    chemin_fichier_occupation_sols: Path,
    chemin_fichier_grille_densite_communale: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### CALCUL DES DONNEES DE POPULATION #####")
    reinitialiser_dossier(chemin_dossier_output)

    population_totale = chargeur_population_totale.charger(chemin_fichier_population_totale)
    population_totale_historique = chargeur_population_totale_historique.charger(chemin_fichier_population_totale_historique)
    occupation_sols = pandas.read_csv(chemin_fichier_occupation_sols, sep=";").set_index("id_territoire")
    grille_densite_communale = chargeur_grille_densite_communale.charger(chemin_fichier_grille_densite_communale)

    territoires = _ajouter_colonne_population_totale(territoires, population_totale, ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE)
    territoires = _ajouter_colonne_population_totale_historique(
        territoires, population_totale_historique, ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE_HISTORIQUE
    )
    territoires = _ajouter_colonne_densite_population(territoires, occupation_sols)
    territoires = _ajouter_colonne_categorie_densite(territoires, grille_densite_communale)
    territoires = territoires.loc[
        :,
        [
            "nom_territoire",
            "categorie_territoire",
            "population_totale_2017",
            "population_totale_2010",
            "population_totale_1990",
            "densite_population_hab_par_km2",
            "code_densite",
        ],
    ]

    return exporter_df_indicateurs_par_territoires(territoires, chemin_dossier_output, "population")


def _ajouter_colonne_population_totale(territoires: DataFrame, population_totale: DataFrame, annee: int) -> DataFrame:
    # les populations totales sont donnés par arrondissements ou communes quand celle-ci n'a pas d'arrondissements
    # on doit donc remplacer l'id arrondissement par l'id de sa commune le cas échéant
    population_totale_final = population_totale.copy()
    arrondissements = obtenir_arrondissements_depuis_territoires(territoires)
    population_totale_final = (
        ajouter_id_commune_de_arrondissement(population_totale_final, arrondissements)
        .groupby(["id_commune"], as_index=False)
        .agg({"population_totale_2017": "sum"})
        .rename(columns={"id_commune": "id_territoire"})
        .set_index("id_territoire")
    )

    territoires = territoires.join(population_totale_final)
    territoires = vider_colonne_selon_mouvements_communes(territoires, annee, "population_totale_2017")
    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "population_totale_2017")

    return territoires


def _ajouter_colonne_population_totale_historique(territoires: DataFrame, population_totale_historique: DataFrame, annee: int) -> DataFrame:
    population_totale_historique_final = (
        population_totale_historique.rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire").drop(columns="nom_commune")
    )

    territoires = territoires.join(population_totale_historique_final)

    territoires = vider_colonne_selon_mouvements_communes(territoires, annee, "population_totale_2010")
    territoires = vider_colonne_selon_mouvements_communes(territoires, annee, "population_totale_1990")

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "population_totale_2010")
    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "population_totale_1990")

    return territoires


def _ajouter_colonne_densite_population(territoires: DataFrame, occupations_sols: DataFrame) -> DataFrame:
    territoires["densite_population_hab_par_km2"] = territoires["population_totale_2017"] / (occupations_sols["superficie_ha"] / 100)

    return territoires


def _ajouter_colonne_categorie_densite(territoires: DataFrame, grille_densite_communale: DataFrame) -> DataFrame:
    grille_densite_communale_final = (
        grille_densite_communale.rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire").drop(columns="nom_commune")
    )
    if ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_GRILLE_DENSITE_COMMUNALE == ANNEE_REFERENTIEL_TERRITOIRES_INSEE:
        territoires = territoires.join(grille_densite_communale_final)
    else:
        raise ValueError("ERREUR lors de l'ajout de la grille communale de densité.\nL'année du référentiel de communes INSEE n'est pas reconnue.")
    territoires["code_densite"] = territoires["code_densite"].map(DICTIONNAIRE_CODES_DENSITE)

    return territoires
