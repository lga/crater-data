import unittest
from pathlib import Path

from crater.indicateurs.population.calculateur_population import calculer_population
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestPopulation(unittest.TestCase):
    def test_calculer_population(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")
        # when
        calculer_population(
            territoires,
            CHEMIN_INPUT_DATA / "population_totale_2017.csv",
            CHEMIN_INPUT_DATA / "population_totale_historique_2015.xlsx",
            CHEMIN_INPUT_DATA / "occupation_sols.csv",
            CHEMIN_INPUT_DATA / "grille_densite_7_niveaux.xlsx",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "population.csv",
            CHEMIN_OUTPUT_DATA / "population.csv",
        )


if __name__ == "__main__":
    unittest.main()
