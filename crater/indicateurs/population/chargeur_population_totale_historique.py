from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.logger import log


def charger(chemin_fichier: Path) -> DataFrame:
    log.info("   => chargement historique population totale depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="COM_2015",
            usecols="A:K",
            skiprows=5,
            dtype={
                "CODGEO": "str",
                "LIBGEO": "str",
                "P10_POP": "Int64",
                "D90_POP": "Int64",
            },
            na_values=[""],
        )
        .rename(
            columns={
                "CODGEO": "id_commune",
                "LIBGEO": "nom_commune",
                "P10_POP": "population_totale_2010",
                "D90_POP": "population_totale_1990",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_commune",
                "nom_commune",
                "population_totale_2010",
                "population_totale_1990",
            ]
        )
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_commune")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    return df
