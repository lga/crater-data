from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
)
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires, reinitialiser_dossier
from crater.commun.logger import log
from crater.config.config_sources import ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2020

OTEFDA_CODE_DESCRIPTION = {
    1516: "nb_communes_otex_grandes_cultures",
    2829: "nb_communes_otex_maraichage_horticulture",
    3500: "nb_communes_otex_viticulture",
    3900: "nb_communes_otex_fruits",
    4500: "nb_communes_otex_bovin_lait",
    4600: "nb_communes_otex_bovin_viande",
    4700: "nb_communes_otex_bovin_mixte",
    4800: "nb_communes_otex_ovins_caprins_autres_herbivores",
    5074: "nb_communes_otex_porcins_volailles",
    6184: "nb_communes_otex_polyculture_polyelevage",
    9000: "nb_communes_otex_non_classees",
    0000: "nb_communes_otex_sans_exploitations",
}

OTEX_TERRITOIRES_COLONNES = ["nom_territoire", "categorie_territoire"]

OTEX_5_POSTES_GRANDES_CULTURES = "GRANDES_CULTURES"
OTEX_5_POSTES_VITICULTURE = "VITICULTURE"
OTEX_5_POSTES_FRUITS_LEGUMES = "FRUITS_ET_LEGUMES"
OTEX_5_POSTES_ELEVAGE = "ELEVAGE"
OTEX_5_POLYCULTURE_POLYELEVAGE = "POLYCULTURE_POLYELEVAGE"
OTEX_5_POSTES_NON_DEFINIE = "NON_DEFINIE"
OTEX_5_POSTES_SANS_OTEX_MAJORITAIRE = "SANS_OTEX_MAJORITAIRE"


def calculer_otex_territoires(territoires: DataFrame, chemin_fichier_agreste_2020: Path, chemin_dossier_output: Path) -> Path:
    log.info("##### CALCUL DES OTEX TERRITOIRES #####")
    reinitialiser_dossier(chemin_dossier_output)

    df_otex_communes = pd.read_csv(chemin_fichier_agreste_2020, sep=";", skiprows=2, usecols=[0, 3], dtype={"Code": "str"})

    df_otex_communes.columns = ["id_territoire", "OTEFDA"]  # type: ignore[assignment]  # TODO: open issue https://github.com/pandas-dev/pandas-stubs/issues/73
    df_otex_communes["id_territoire"] = traduire_code_insee_vers_id_commune_crater(df_otex_communes["id_territoire"])

    df_otex_territoires = (
        territoires.copy().merge(df_otex_communes, how="left", left_on="id_territoire", right_on="id_territoire").set_index("id_territoire")
    )

    df_otex_territoires_categorie = pd.get_dummies(df_otex_territoires["OTEFDA"], dtype=int)
    df_otex_territoires_categorie = df_otex_territoires_categorie.rename(columns=OTEFDA_CODE_DESCRIPTION)

    df_otex_territoires = df_otex_territoires.join(df_otex_territoires_categorie)

    df_otex_territoires["nb_communes_otex_na"] = 0
    mask_communes_otex_na = df_otex_territoires.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2020
    df_otex_territoires.loc[mask_communes_otex_na, "nb_communes_otex_na"] = 1

    colonnes_a_garder = OTEX_TERRITOIRES_COLONNES.copy()

    for code_libelle_otex in OTEFDA_CODE_DESCRIPTION.items():
        libelle_otex = code_libelle_otex[1]
        if libelle_otex in df_otex_territoires.columns:
            df_otex_territoires = df_otex_territoires.astype(dtype={libelle_otex: "Int64"})
            df_otex_territoires.loc[mask_communes_otex_na, libelle_otex] = 0
            df_otex_territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_otex_territoires, libelle_otex)
            colonnes_a_garder.append(libelle_otex)
    df_otex_territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(df_otex_territoires, "nb_communes_otex_na")

    df_otex_territoires = df_otex_territoires.loc[:, colonnes_a_garder + ["nb_communes_otex_na"]]

    df_otex_territoires = _ajouter_otex_majoritaire_5_postes(df_otex_territoires)

    return exporter_df_indicateurs_par_territoires(df_otex_territoires.reset_index(), chemin_dossier_output, "otex_territoires")


def _ajouter_otex_majoritaire_5_postes(df_otex_territoires: DataFrame) -> DataFrame:
    otex_5_postes_majoritaire = df_otex_territoires.loc[:, []].copy()

    otex_5_postes_majoritaire[OTEX_5_POSTES_GRANDES_CULTURES] = df_otex_territoires["nb_communes_otex_grandes_cultures"]
    otex_5_postes_majoritaire[OTEX_5_POSTES_VITICULTURE] = df_otex_territoires["nb_communes_otex_viticulture"]
    otex_5_postes_majoritaire[OTEX_5_POSTES_FRUITS_LEGUMES] = (
        df_otex_territoires["nb_communes_otex_maraichage_horticulture"] + df_otex_territoires["nb_communes_otex_fruits"]
    )
    otex_5_postes_majoritaire[OTEX_5_POSTES_ELEVAGE] = (
        df_otex_territoires["nb_communes_otex_bovin_lait"]
        + df_otex_territoires["nb_communes_otex_bovin_viande"]
        + df_otex_territoires["nb_communes_otex_bovin_mixte"]
        + df_otex_territoires["nb_communes_otex_ovins_caprins_autres_herbivores"]
        + df_otex_territoires["nb_communes_otex_porcins_volailles"]
    )
    otex_5_postes_majoritaire[OTEX_5_POLYCULTURE_POLYELEVAGE] = df_otex_territoires["nb_communes_otex_polyculture_polyelevage"]
    otex_5_postes_majoritaire[OTEX_5_POSTES_NON_DEFINIE] = (
        df_otex_territoires["nb_communes_otex_non_classees"]
        + df_otex_territoires["nb_communes_otex_sans_exploitations"]
        + df_otex_territoires["nb_communes_otex_na"]
    )

    otex_5_postes_majoritaire = otex_5_postes_majoritaire.div(otex_5_postes_majoritaire.sum(axis=1), axis=0)
    df_otex_territoires["otex_majoritaire_5_postes"] = otex_5_postes_majoritaire.idxmax(axis=1)
    df_otex_territoires.loc[otex_5_postes_majoritaire.max(axis=1) < 0.5, "otex_majoritaire_5_postes"] = OTEX_5_POSTES_SANS_OTEX_MAJORITAIRE

    return df_otex_territoires
