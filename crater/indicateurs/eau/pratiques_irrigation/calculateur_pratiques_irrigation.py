from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    trier_territoires,
)
from crater.commun.calculs.outils_verification import (
    verifier_absence_doublons,
    verifier_absence_na,
)
from crater.commun.exports.export_fichier import exporter_csv
from crater.commun.chargeurs.chargeur_agreste import (
    charger_agreste,
    filtrer_et_ajouter_colonne_id_territoire,
)
from crater.commun.logger import log
from crater.referentiel_territoires.chargeur_regions import charger_correspondances_anciennes_nouvelles_regions

# On ne garde que le niveau non agrégé des cultures
CATEGORIES_CULTURES_AGREGEES_A_EXCLURE = [
    "Superficie agricole utilisée (SAU)",
    "Céréales",
    "Cultures industrielles (oléo-protéagineux, plantes à fibres, autres)",
    "Fourrages",
    "Cultures permanentes",
    "Légumes",
]

FICHIER_CONFIG_CORRESPONDANCE_CULTURES = Path(__file__).parent / "config/correspondances_irrigation_cultures_agreste_vers_crater.csv"


def calculer_pratiques_irrigation(
    territoires: DataFrame,
    fichier_correspondances_anciennes_nouvelles_regions: Path,
    dossier_agreste: Path,
    chemin_fichier_surfaces_riz_par_departements: Path,
    chemin_dossier_output: Path,
) -> DataFrame:
    log.info("##### CALCUL PRATIQUES IRRIGATION #####")

    df_correspondance_cultures_agreste_crater = pandas.read_csv(FICHIER_CONFIG_CORRESPONDANCE_CULTURES, sep=";")

    df_pratiques_irrigation = _initialiser_df_pratiques_irrigation(
        territoires, dossier_agreste, fichier_correspondances_anciennes_nouvelles_regions, df_correspondance_cultures_agreste_crater
    )

    df_pratiques_irrigation = _integrer_surfaces_riz(
        territoires,
        df_pratiques_irrigation,
        chemin_fichier_surfaces_riz_par_departements,
    )

    df_pratiques_irrigation = _ajouter_colonnes_irrigation_gravitaire_et_hors_gravitaire(
        df_pratiques_irrigation, df_correspondance_cultures_agreste_crater
    )

    df_pratiques_irrigation = trier_territoires(
        df_pratiques_irrigation.loc[
            :,
            [
                "id_territoire",
                "nom_territoire",
                "categorie_territoire",
                "categorie_culture",
                "culture",
                "superficie_ha",
                "superficie_irriguee_ha",
                "irrigation_gravitaire_modelisee_m3",
                "irrigation_hors_gravitaire_modelisee_m3",
            ],
        ],
        ["categorie_culture", "culture"],
    )

    exporter_csv(
        df_pratiques_irrigation,
        chemin_dossier_output / "pratiques_irrigation.csv",
        float_format="%.0f",
    )
    return df_pratiques_irrigation


def _initialiser_df_pratiques_irrigation(
    territoires: DataFrame,
    dossier_agreste: Path,
    fichier_correspondances_anciennes_nouvelles_regions: Path,
    df_correspondance_cultures_agreste_crater: DataFrame,
) -> DataFrame:
    df_agreste_irrigation_anciennes_regions = _charger_agreste_pratiques_irrigation(dossier_agreste)

    df_correspondances_anciennes_nouvelles_regions = charger_correspondances_anciennes_nouvelles_regions(
        fichier_correspondances_anciennes_nouvelles_regions
    )
    df_agreste_pratiques_irrigation = _agreger_anciennes_regions_en_nouvelles_regions(
        df_correspondances_anciennes_nouvelles_regions, df_agreste_irrigation_anciennes_regions
    )

    df_pratiques_irrigation = _creer_df_pratiques_irrigation(df_agreste_pratiques_irrigation, df_correspondance_cultures_agreste_crater, territoires)

    verifier_absence_na(df_pratiques_irrigation, "id_territoire", "culture")
    verifier_absence_doublons(df_pratiques_irrigation, ["id_territoire", "culture"])

    return df_pratiques_irrigation


def _charger_agreste_pratiques_irrigation(dossier: Path) -> DataFrame:
    log.info("   => chargement des pratiques d'irrigation")

    df = (
        charger_agreste({"code": "G_0051", "annee": "2010", "dossier": dossier})
        .rename(
            columns={
                "G_0051_LIB_DIM1": "taille_exploitation",
                "G_0051_LIB_DIM2": "otex",
                "G_0051_LIB_DIM3": "culture",
                "G_0051_LIB_DIM4": "indicateur",
                "VALEUR": "valeur",
                "QUALITE": "qualite",
            },
            errors="raise",
        )
        .query(
            'taille_exploitation  == "Ensemble des exploitations (hors pacages collectifs)" & otex == "Ensemble" & '
            'indicateur.isin(["Superficie de la culture (hectares)", "Superficie de la culture irriguée (hectares)"]) '
            "& ~culture.isin(@CATEGORIES_CULTURES_AGREGEES_A_EXCLURE)"
        )
        .reindex(
            columns=[
                "FRANCE",
                "FRDOM",
                "REGION",
                "DEP",
                "COM",
                "taille_exploitation",
                "otex",
                "culture",
                "indicateur",
                "valeur",
                "qualite",
            ]
        )
        .astype({"valeur": "float64"})
    )

    df = filtrer_et_ajouter_colonne_id_territoire(df, communes_absentes=True)
    df = df.pivot(index=["id_territoire", "culture"], columns="indicateur", values="valeur").reset_index()
    df = df.rename(
        columns={
            "culture": "culture_agreste",
            "Superficie de la culture (hectares)": "superficie_ha",
            "Superficie de la culture irriguée (hectares)": "superficie_irriguee_ha",
        }
    )
    return df


def _creer_df_pratiques_irrigation(df_agreste_pratiques_irrigation, df_correspondance_cultures_agreste_crater, territoires):
    df_agreste_pratiques_irrigation["culture"] = df_agreste_pratiques_irrigation["culture_agreste"].map(
        df_correspondance_cultures_agreste_crater.set_index("culture_agreste")["culture_crater"]
    )
    df_agreste_pratiques_irrigation = df_agreste_pratiques_irrigation.loc[:, ["id_territoire", "culture", "superficie_ha", "superficie_irriguee_ha"]]

    index = pandas.MultiIndex.from_product(
        [
            df_agreste_pratiques_irrigation["id_territoire"].drop_duplicates().tolist(),
            df_correspondance_cultures_agreste_crater["culture_crater"].drop_duplicates().tolist(),
        ],
        names=["id_territoire", "culture"],
    )
    df_pratiques_irrigation = pandas.DataFrame(index=index)
    df_pratiques_irrigation = df_pratiques_irrigation.join(df_agreste_pratiques_irrigation.set_index(["id_territoire", "culture"])).reset_index()
    df_pratiques_irrigation["categorie_culture"] = df_pratiques_irrigation["culture"].map(
        df_correspondance_cultures_agreste_crater.set_index("culture_crater")["categorie_culture_crater"]
    )
    df_pratiques_irrigation["superficie_ha"] = df_pratiques_irrigation["superficie_ha"].fillna(0)
    df_pratiques_irrigation["superficie_irriguee_ha"] = df_pratiques_irrigation["superficie_irriguee_ha"].fillna(0)
    df_pratiques_irrigation = (
        df_pratiques_irrigation.set_index("id_territoire").join(territoires.loc[:, ["nom_territoire", "categorie_territoire"]]).reset_index()
    )
    return df_pratiques_irrigation


def _agreger_anciennes_regions_en_nouvelles_regions(
    df_correspondances_anciennes_nouvelles_regions: DataFrame,
    df_pratiques_irrigation: DataFrame,
) -> DataFrame:
    mask_regions = df_pratiques_irrigation.id_territoire.str[0] == "R"
    df_pratiques_irrigation.loc[mask_regions, "id_territoire"] = df_pratiques_irrigation.loc[mask_regions, "id_territoire"].map(
        df_correspondances_anciennes_nouvelles_regions["nouveau_code"]
    )
    df_pratiques_irrigation = df_pratiques_irrigation.groupby(["id_territoire", "culture_agreste"]).sum().reset_index()

    return df_pratiques_irrigation


def _integrer_surfaces_riz(
    territoires: DataFrame,
    df_pratiques_irrigation: DataFrame,
    chemin_fichier_surfaces_riz_par_departements: Path,
):
    df_surfaces_riz = _calculer_surfaces_riz_pour_pays_regions_departements(territoires, chemin_fichier_surfaces_riz_par_departements)

    df_pratiques_irrigation = _ajouter_lignes_pratiques_irrigation_riz(df_pratiques_irrigation, df_surfaces_riz)

    df_pratiques_irrigation = _retrancher_surfaces_riz_aux_surfaces_cereales_autres(df_pratiques_irrigation, df_surfaces_riz)
    return df_pratiques_irrigation


def _calculer_surfaces_riz_pour_pays_regions_departements(territoires, chemin_fichier_surfaces_riz_par_departements):
    df_surfaces_riz_par_departements = (
        pandas.read_csv(chemin_fichier_surfaces_riz_par_departements, sep=";").set_index("id_departement").loc[:, ["superficie_riz_ha"]]
    )
    df_surfaces_riz = territoires.loc[
        (territoires["categorie_territoire"].isin(["DEPARTEMENT", "REGION", "PAYS"])),
        [
            "nom_territoire",
            "categorie_territoire",
            "ids_regroupements_communes",
            "id_epci",
            "id_departement",
            "id_region",
            "id_pays",
        ],
    ].join(df_surfaces_riz_par_departements)
    df_surfaces_riz = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        territoires=df_surfaces_riz,
        colonne_a_sommer="superficie_riz_ha",
        categorie_territoire_a_sommer="DEPARTEMENT",
        categories_territoires_cibles=["REGION", "PAYS"],
    )
    df_surfaces_riz = df_surfaces_riz.reset_index().loc[:, ["id_territoire", "superficie_riz_ha"]]
    return df_surfaces_riz


def _ajouter_lignes_pratiques_irrigation_riz(df_pratiques_irrigation, df_surfaces_riz):
    df_surfaces_riz["culture"] = "Céréales : riz"
    df_surfaces_riz = df_surfaces_riz.set_index(["id_territoire", "culture"]).rename(columns={"superficie_riz_ha": "superficie_ha"})
    df_surfaces_riz["superficie_irriguee_ha"] = df_surfaces_riz["superficie_ha"]  # hypothèse : tout le riz est irrigué

    df_pratiques_irrigation = df_pratiques_irrigation.set_index(["id_territoire", "culture"])
    df_pratiques_irrigation.update(df_surfaces_riz)

    return df_pratiques_irrigation.reset_index()


def _retrancher_surfaces_riz_aux_surfaces_cereales_autres(df_pratiques_irrigation, df_surfaces_riz):
    df_surfaces_irrigees_a_retrancher = (
        df_surfaces_riz.reset_index()
        .loc[:, ["id_territoire", "superficie_riz_ha"]]
        .rename(columns={"superficie_riz_ha": "superficie_a_retrancher_ha"})
    )
    df_surfaces_irrigees_a_retrancher["culture"] = "Céréales : autres"
    df_pratiques_irrigation = df_pratiques_irrigation.merge(df_surfaces_irrigees_a_retrancher, how="left", on=["id_territoire", "culture"])
    df_pratiques_irrigation["superficie_a_retrancher_ha"] = df_pratiques_irrigation["superficie_a_retrancher_ha"].fillna(0)
    df_pratiques_irrigation["superficie_irriguee_ha"] = (
        df_pratiques_irrigation["superficie_irriguee_ha"] - df_pratiques_irrigation["superficie_a_retrancher_ha"]
    )
    df_pratiques_irrigation["superficie_ha"] = df_pratiques_irrigation["superficie_ha"] - df_pratiques_irrigation["superficie_a_retrancher_ha"]
    df_pratiques_irrigation = df_pratiques_irrigation.drop(columns=["superficie_a_retrancher_ha"])
    return df_pratiques_irrigation


def _ajouter_colonnes_irrigation_gravitaire_et_hors_gravitaire(df_pratiques_irrigation, df_correspondance_cultures_agreste_crater):
    df_pratiques_irrigation["coefficients_irrigation_m3_par_ha_et_an"] = df_pratiques_irrigation["culture"].map(
        df_correspondance_cultures_agreste_crater.set_index("culture_crater")["coefficient_irrigation_gravitaire_m3_par_ha_et_an"]
    )
    df_pratiques_irrigation["coefficients_irrigation_hors_gravitaire_m3_ha_an"] = df_pratiques_irrigation["culture"].map(
        df_correspondance_cultures_agreste_crater.set_index("culture_crater")["coefficient_irrigation_hors_gravitaire_m3_par_ha_et_an"]
    )
    df_pratiques_irrigation["irrigation_gravitaire_modelisee_m3"] = (
        df_pratiques_irrigation["superficie_irriguee_ha"] * df_pratiques_irrigation["coefficients_irrigation_m3_par_ha_et_an"]
    )
    df_pratiques_irrigation["irrigation_hors_gravitaire_modelisee_m3"] = (
        df_pratiques_irrigation["superficie_irriguee_ha"] * df_pratiques_irrigation["coefficients_irrigation_hors_gravitaire_m3_ha_an"]
    )
    return df_pratiques_irrigation
