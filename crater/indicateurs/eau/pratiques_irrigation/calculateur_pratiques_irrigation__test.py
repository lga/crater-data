import unittest
from pathlib import Path

from crater.indicateurs.eau.pratiques_irrigation.calculateur_pratiques_irrigation import calculer_pratiques_irrigation
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas, assert_csv_files_are_equals

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestCalculateurEau(unittest.TestCase):
    reinitialiser_dossier(CHEMIN_OUTPUT_DATA, reinitialiser_cache=True)

    def test_calculer_pratiques_irrigation(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")
        # when
        calculer_pratiques_irrigation(
            territoires,
            CHEMIN_INPUT_DATA / "correspondance_anciennes_nouvelles_regions.csv",
            CHEMIN_INPUT_DATA / "agreste",
            CHEMIN_INPUT_DATA / "surfaces_riz_par_departements.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "pratiques_irrigation.csv",
            CHEMIN_OUTPUT_DATA / "pratiques_irrigation.csv",
        )


if __name__ == "__main__":
    unittest.main()
