from pathlib import Path

import numpy as np
import pandas
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    initialiser_df_indicateur_par_annees,
    calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales,
)
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires
from crater.commun.logger import log
from crater.donnees_sources.hubeau_irrigation.calcul_hubeau_irrigation import ResultatsDonneesHubeauIrrigation


def calculer_irrigation_par_annees(
    territoires: DataFrame, fichier_occupation_sols: Path, donnees_irrigation: list[ResultatsDonneesHubeauIrrigation], dossier_output: Path
) -> DataFrame:
    log.info("   => calcul irrigation par annees")

    territoires = charger_et_joindre_df_indicateurs(territoires, fichier_occupation_sols)

    df_irrigation = _charger_donnees_irrigation(donnees_irrigation)

    df_irrigation_par_annees = initialiser_df_indicateur_par_annees(territoires, df_irrigation.reset_index("annee")["annee"])
    df_irrigation_par_annees = _ajouter_colonne_irrigation_m3(df_irrigation_par_annees, df_irrigation)
    df_irrigation_par_annees = _ajouter_colonne_irrigation_mm(df_irrigation_par_annees)
    exporter_df_indicateurs_par_territoires(df_irrigation_par_annees, dossier_output, "irrigation_par_annees")
    return df_irrigation_par_annees


def _charger_donnees_irrigation(donnees_irrigation: list[ResultatsDonneesHubeauIrrigation]):
    df = pandas.DataFrame()
    for d in donnees_irrigation:
        df_annee = pandas.read_csv(d.chemin, sep=";")
        df_annee["annee"] = d.annee
        df = pandas.concat([df, df_annee])
    return df.set_index(["id_territoire", "annee"])


def _ajouter_colonne_irrigation_m3(df_irrigation_par_annees, df_irrigation):
    df_irrigation_par_annees = df_irrigation_par_annees.join(df_irrigation.drop(columns=["nom_territoire", "categorie_territoire"]))

    mask_communes_avec_epci = (df_irrigation_par_annees["categorie_territoire"] == "COMMUNE") & (~df_irrigation_par_annees["id_epci"].isnull())
    df_irrigation_par_annees.loc[mask_communes_avec_epci, "irrigation_m3"] = np.nan
    df_irrigation_par_annees = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
        df_irrigation_par_annees, "irrigation_m3", "sau_productive_hors_prairies_ha", "COMMUNE", "EPCI"
    )

    df_irrigation_par_annees.loc[df_irrigation_par_annees["categorie_territoire"] == "REGROUPEMENT_COMMUNES", "irrigation_m3"] = np.nan
    df_irrigation_par_annees = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        df_irrigation_par_annees, "irrigation_m3", "COMMUNE", ["REGROUPEMENT_COMMUNES"]
    )

    df_irrigation_par_annees = df_irrigation_par_annees.reset_index("annee")
    df_irrigation_par_annees = df_irrigation_par_annees.loc[
        :,
        ["nom_territoire", "categorie_territoire", "annee", "sau_productive_hors_prairies_ha", "irrigation_m3"],
    ]
    return df_irrigation_par_annees


def _ajouter_colonne_irrigation_mm(df_irrigation_par_annees: DataFrame) -> DataFrame:
    M2_PAR_HA = 10000
    MM_PAR_M = 1000

    df_irrigation_par_annees["irrigation_mm"] = (
        (df_irrigation_par_annees["irrigation_m3"] / (df_irrigation_par_annees["sau_productive_hors_prairies_ha"].astype("float64") * M2_PAR_HA))
        * MM_PAR_M
    ).replace([np.inf, -np.inf], np.nan)
    df_irrigation_par_annees.loc[df_irrigation_par_annees["irrigation_m3"] == 0, "irrigation_mm"] = 0

    df_irrigation_par_annees = df_irrigation_par_annees.drop(columns="sau_productive_hors_prairies_ha")

    return df_irrigation_par_annees
