import unittest
from pathlib import Path

from crater.donnees_sources.hubeau_irrigation.calcul_hubeau_irrigation import ResultatsDonneesHubeauIrrigation
from crater.indicateurs.eau.irrigation.calculateur_eau_irrigation import calculer_irrigation_par_annees
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestCalculateurIrrigation(unittest.TestCase):
    def test_calculer_indicateurs_irrigation(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # when
        calculer_irrigation_par_annees(
            territoires,
            CHEMIN_INPUT_DATA / "occupation_sols" / "occupation_sols.csv",
            [
                ResultatsDonneesHubeauIrrigation(2019, CHEMIN_INPUT_DATA / "irrigation" / "donnees_irrigation_2019.csv"),
                ResultatsDonneesHubeauIrrigation(2020, CHEMIN_INPUT_DATA / "irrigation" / "donnees_irrigation_2020.csv"),
            ],
            CHEMIN_OUTPUT_DATA,
        )

        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "irrigation_par_annees.csv",
            CHEMIN_OUTPUT_DATA / "irrigation_par_annees.csv",
        )


if __name__ == "__main__":
    unittest.main()
