import unittest
from pathlib import Path

import numpy as np
import pandas
import pandas as pd

from crater.indicateurs.eau.synthese_eau.calculateur_eau import (
    _ajouter_taux_impact_arretes_secheresse_pourcent,
    _ajouter_colonnes_notes_eau,
    calculer_synthese_eau,
    _ajouter_colonnes_irrigation_m3_et_irrigation_mm,
    _ajouter_colonnes_irrigation_gravitaire,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_df_indicateurs
from crater.commun.outils_pour_tests import (
    augmenter_nombre_colonnes_affichees_pandas,
    assert_csv_files_are_equals,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestCalculateurEau(unittest.TestCase):
    reinitialiser_dossier(CHEMIN_OUTPUT_DATA, reinitialiser_cache=True)

    def test_ajouter_colonne_taux_impact_arretes_secheresse(self):
        # given
        df_arretes = pd.DataFrame(
            columns=[
                "id_territoire",
                "annee_mois",
                "part_territoire_en_arrete_pourcent",
            ],
            data=[
                ["C-1", "2018-07", 99.9],
                ["C-1", "2019-01", 99.9],
                ["C-1", "2019-07", 10],
                ["C-1", "2019-08", 20],
                ["C-1", "2020-07", 30],
                ["C-1", "2020-08", 40],
                ["C-2", "2020-08", 80],
            ],
        )
        df_arretes["annee_mois"] = df_arretes["annee_mois"].astype("datetime64[ns]")
        df_eau = pd.DataFrame(
            columns=["id_territoire"],
            data=[
                ["C-1"],
                ["C-2"],
            ],
        ).set_index("id_territoire")
        df_eau_attendu = pd.DataFrame(
            columns=["id_territoire", "taux_impact_arretes_secheresse_pourcent"],
            data=[
                ["C-1", 25.0],
                ["C-2", 20.0],
            ],
        ).set_index("id_territoire")
        # when
        df_eau_resultat = _ajouter_taux_impact_arretes_secheresse_pourcent(df_eau, df_arretes, 2020, 2)
        # then
        pd.testing.assert_frame_equal(df_eau_attendu, df_eau_resultat)

    def test_ajouter_colonnes_irrigation(self):
        # given
        df_irrigation = pd.DataFrame(
            columns=["id_territoire", "annee", "irrigation_m3", "irrigation_mm"],
            data=[
                ["C-1", 2018, 999, 99],
                ["C-1", 2019, 100.0, 10.0],
                ["C-1", 2020, 80, 8],
                ["C-2", 2019, 50, 10],
                ["C-2", 2020, np.nan, 0],
            ],
        )
        df_eau = pd.DataFrame(
            columns=["id_territoire"],
            data=[
                ["C-1"],
                ["C-2"],
            ],
        ).set_index("id_territoire")
        df_eau_attendu = pd.DataFrame(
            columns=["id_territoire", "irrigation_m3", "irrigation_mm"],
            data=[
                ["C-1", 90.0, 9.0],
                ["C-2", 50, 5],
            ],
        ).set_index("id_territoire")
        # when
        df_eau_resultat = _ajouter_colonnes_irrigation_m3_et_irrigation_mm(df_eau, df_irrigation, 2020, 2)
        # then
        pd.testing.assert_frame_equal(df_eau_attendu, df_eau_resultat)

    def test_ajouter_colonnes_irrigation_gravitaire(self):
        # given
        df_pratiques_irrigation = pd.DataFrame(
            columns=["id_territoire", "culture", "irrigation_gravitaire_modelisee_m3", "irrigation_hors_gravitaire_modelisee_m3"],
            data=[
                ["R-1", "Culture 1 avec irrigation gravitaire", 10, 0],
                ["R-1", "Culture 2 avec irrigation gravitaire", 20, 0],
                ["D-1", "Culture 1 avec irrigation gravitaire", 1, 0],
                ["D-1", "Culture 2 avec irrigation gravitaire", 2, 0],
                ["D-1", "Culture avec irrigation non gravitaire", 0, 3],
            ],
        )
        df_eau = pd.DataFrame(
            columns=["id_territoire", "id_epci", "ids_regroupements_communes", "id_departement", "categorie_territoire", "irrigation_m3"],
            data=[
                ["R-1", pandas.NA, pandas.NA, pandas.NA, "REGION", 100],
                ["D-1", pandas.NA, pandas.NA, pandas.NA, "DEPARTEMENT", 100],
                ["RC-1", pandas.NA, pandas.NA, "D-1", "REGROUPEMENT_COMMUNES", 100],
                ["E-1", pandas.NA, pandas.NA, "D-1", "EPCI", 100],
                ["C-1", "E-1", pandas.NA, "D-1", "COMMUNE", 10],
                ["C-2", "E-1", "RC-1", "D-1", "COMMUNE", 10],
            ],
        ).set_index("id_territoire")

        irrigation_gravitaire_attendue = pd.Series([100.0, 50, 50, 50, 5, 5])
        irrigation_hors_gravitaire_attendue = pd.Series([0.0, 50, 50, 50, 5, 5])
        # when
        df_eau_resultat = _ajouter_colonnes_irrigation_gravitaire(df_eau, df_pratiques_irrigation)
        # then
        pd.testing.assert_series_equal(
            irrigation_hors_gravitaire_attendue,
            df_eau_resultat["irrigation_hors_gravitaire_m3"],
            check_names=False,
            check_dtype=True,
            check_index=False,
        )
        pd.testing.assert_series_equal(
            irrigation_gravitaire_attendue,
            df_eau_resultat["irrigation_gravitaire_m3"],
            check_names=False,
            check_dtype=True,
            check_index=False,
        )

    def test_ajouter_colonnes_notes_eau(self):
        # given
        df_eau = pd.DataFrame(
            columns=[
                "id_territoire",
                "categorie_territoire",
                "irrigation_mm",
                "taux_impact_arretes_secheresse_pourcent",
            ],
            data=[
                ["D-1", "DEPARTEMENT", 0, 30.0],
                ["D-2", "DEPARTEMENT", 10, 90],
                ["C-1", "COMMUNE", 2, np.nan],
                ["C-2", "COMMUNE", 5, 20],
                ["C-3", "COMMUNE", 15, 60],
                ["C-4", "COMMUNE", pd.NA, 100],
            ],
        ).set_index("id_territoire")
        df_eau["irrigation_mm"] = df_eau["irrigation_mm"].astype("Int64")
        irrigation_notes_attendues = pd.Series([10, 1.67, 8, 5, 0.0, np.nan]).astype("float64")
        arretes_secheresse_notes_attendues = pd.Series([7, 1, np.nan, 8, 4, 0]).astype("float64")
        eau_notes_attendues = pd.Series([(10 + 7) / 2, (1.67 + 1) / 2, np.nan, (5 + 8) / 2, (0 + 4) / 2, np.nan])

        # when
        df_eau_resultat = _ajouter_colonnes_notes_eau(df_eau)

        # then
        pd.testing.assert_series_equal(
            irrigation_notes_attendues,
            df_eau_resultat["irrigation_note"],
            check_names=False,
            check_dtype=True,
            check_index=False,
        )
        pd.testing.assert_series_equal(
            arretes_secheresse_notes_attendues,
            df_eau_resultat["arretes_secheresse_note"],
            check_names=False,
            check_dtype=True,
            check_index=False,
        )
        pd.testing.assert_series_equal(
            eau_notes_attendues,
            df_eau_resultat["eau_note"],
            check_names=False,
            check_dtype=True,
            check_index=False,
        )

    def test_calculer_synthese_eau(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        df_irrigation_par_annees = charger_df_indicateurs(CHEMIN_INPUT_DATA / "irrigation_par_annees.csv")
        df_arretes_secheresse_par_mois_annee = charger_df_indicateurs(
            CHEMIN_INPUT_DATA / "arretes_secheresse_par_annees_mois.csv",
            parse_dates=["annee_mois"],
        )
        df_pratiques_irrigation = charger_df_indicateurs(CHEMIN_INPUT_DATA / "pratiques_irrigation.csv").reset_index()
        # when
        calculer_synthese_eau(
            territoires,
            df_irrigation_par_annees,
            df_arretes_secheresse_par_mois_annee,
            2020,
            1,
            df_pratiques_irrigation,
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "synthese_eau.csv",
            CHEMIN_OUTPUT_DATA / "synthese_eau.csv",
        )


if __name__ == "__main__":
    unittest.main()
