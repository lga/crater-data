from pathlib import Path

from pandas import DataFrame

from crater.commun.calculs.notes_et_messages import (
    calculer_note_par_interpolation_n_seuils,
)
from crater.commun.exports.export_fichier import (
    exporter_df_indicateurs_par_territoires,
)
from crater.commun.logger import log

COEFFICIENT_IRRIGATION_GRAVITAIRE_M3_PAR_HA_PAR_AN = 20000  # Source Climagri France, onglet A5.a colonne F

# Codes cultures utilisés dans pratiques_irrigation.csv
CULTURES_A_IRRIGATION_GRAVITAIRE = [
    "Céréales : riz",
    "Fourrages : superficies toujours en herbe",
]


def calculer_synthese_eau(
    territoires: DataFrame,
    df_irrigation_par_annees: DataFrame,
    df_arretes_secheresse_par_annees_mois: DataFrame,
    annee_plus_recente: int,
    nb_annees_pour_moyenne: int,
    df_pratiques_irrigation: DataFrame,
    dossier_output: Path,
) -> Path:
    log.info("  => calcul synthese eau")

    df_synthese_eau = territoires.loc[
        :,
        [
            "nom_territoire",
            "id_epci",
            "ids_regroupements_communes",
            "id_departement",
            "categorie_territoire",
        ],
    ].copy()

    df_synthese_eau = _ajouter_colonnes_irrigation_m3_et_irrigation_mm(
        df_synthese_eau,
        df_irrigation_par_annees,
        annee_plus_recente,
        nb_annees_pour_moyenne,
    )

    df_synthese_eau = _ajouter_colonnes_irrigation_gravitaire(df_synthese_eau, df_pratiques_irrigation)

    df_synthese_eau = _ajouter_taux_impact_arretes_secheresse_pourcent(
        df_synthese_eau,
        df_arretes_secheresse_par_annees_mois,
        annee_plus_recente,
        nb_annees_pour_moyenne,
    )
    df_synthese_eau = _ajouter_colonnes_notes_eau(df_synthese_eau)

    df_synthese_eau = df_synthese_eau.drop(columns=["id_epci", "ids_regroupements_communes", "id_departement"])

    return exporter_df_indicateurs_par_territoires(df_synthese_eau, dossier_output, "synthese_eau")


def _ajouter_colonnes_irrigation_m3_et_irrigation_mm(df_eau, df_irrigation, annee_plus_recente, nb_annees_pour_moyenne):
    df_irrigation = df_irrigation.reset_index().set_index("id_territoire")

    df_indicateurs_annees_pour_moyenne = df_irrigation.loc[
        (df_irrigation["annee"] <= annee_plus_recente) & (df_irrigation["annee"] > annee_plus_recente - nb_annees_pour_moyenne),
        ["irrigation_m3", "irrigation_mm"],
    ]
    # Par construction, le df_irrigation contient une ligne pour tous les couples territoires-annees => ok pour mean()
    df_irrigation_valeurs_moyennes = df_indicateurs_annees_pour_moyenne.groupby("id_territoire").mean()
    df_eau = df_eau.join(df_irrigation_valeurs_moyennes)
    return df_eau


def _ajouter_colonnes_irrigation_gravitaire(df_eau, df_pratiques_irrigation: DataFrame):
    df_eau = df_eau.join(
        _calculer_part_irrigation_gravitaire_par_departements_regions_pays(df_pratiques_irrigation).loc[:, ["part_irrigation_gravitaire"]]
    )

    df_eau.update(_calculer_part_irrigation_gravitaire_pour_infra_departements(df_eau))
    df_eau["irrigation_gravitaire_m3"] = df_eau["irrigation_m3"] * df_eau["part_irrigation_gravitaire"]
    df_eau["irrigation_hors_gravitaire_m3"] = df_eau["irrigation_m3"] * (1 - df_eau["part_irrigation_gravitaire"])
    return df_eau.drop(columns=["part_irrigation_gravitaire"])


def _calculer_part_irrigation_gravitaire_par_departements_regions_pays(
    df_pratiques_irrigation,
):
    df_part_irrigation_gravitaire_par_departements_regions_pays = (
        df_pratiques_irrigation.loc[
            :,
            [
                "id_territoire",
                "irrigation_gravitaire_modelisee_m3",
                "irrigation_hors_gravitaire_modelisee_m3",
            ],
        ]
        .groupby(["id_territoire"])
        .sum()
    )
    df_part_irrigation_gravitaire_par_departements_regions_pays[
        "part_irrigation_gravitaire"
    ] = df_part_irrigation_gravitaire_par_departements_regions_pays["irrigation_gravitaire_modelisee_m3"] / (
        df_part_irrigation_gravitaire_par_departements_regions_pays["irrigation_gravitaire_modelisee_m3"]
        + df_part_irrigation_gravitaire_par_departements_regions_pays["irrigation_hors_gravitaire_modelisee_m3"]
    )
    return df_part_irrigation_gravitaire_par_departements_regions_pays


def _calculer_part_irrigation_gravitaire_pour_infra_departements(df_eau):
    df_part_irrigation_gravitaire_par_departements = df_eau.loc[df_eau["categorie_territoire"] == "DEPARTEMENT", :].copy()
    df_part_irrigation_gravitaire_par_departements = df_part_irrigation_gravitaire_par_departements.loc[:, ["part_irrigation_gravitaire"]].fillna(0)
    df_eau_infra_departements = (
        df_eau.loc[
            df_eau["categorie_territoire"].isin(["COMMUNE", "EPCI", "REGROUPEMENT_COMMUNES"]),
            ["id_departement"],
        ]
        .copy()
        .reset_index()
    )
    df_eau_infra_departements = df_eau_infra_departements.merge(
        df_part_irrigation_gravitaire_par_departements,
        how="left",
        left_on="id_departement",
        right_on="id_territoire",
    )
    df_eau_infra_departements = df_eau_infra_departements.set_index("id_territoire")
    return df_eau_infra_departements.loc[:, ["part_irrigation_gravitaire"]]


def _ajouter_taux_impact_arretes_secheresse_pourcent(
    df_eau: DataFrame,
    df_arretes: DataFrame,
    annee_plus_recente: int,
    nb_annees_pour_moyenne: int,
) -> DataFrame:
    MOIS_RETENUS = [7, 8]  # Juillet et Aout
    nb_valeurs_pour_moyenne = nb_annees_pour_moyenne * len(MOIS_RETENUS)

    df_arretes_pour_moyenne = df_arretes.loc[
        (
            (df_arretes["annee_mois"].dt.month.isin(MOIS_RETENUS))
            & (df_arretes["annee_mois"].dt.year <= annee_plus_recente)
            & (df_arretes["annee_mois"].dt.year > annee_plus_recente - nb_annees_pour_moyenne)
        ),
        :,
    ]
    df_arretes_pour_moyenne = df_arretes_pour_moyenne.groupby("id_territoire")[["part_territoire_en_arrete_pourcent"]].sum() / (
        nb_valeurs_pour_moyenne
    )
    df_eau = df_eau.join(df_arretes_pour_moyenne.rename(columns={"part_territoire_en_arrete_pourcent": "taux_impact_arretes_secheresse_pourcent"}))

    # Pas de ligne dans le df_arretes pour les territoires qui n'ont pas été sous arrêté au moins un des mois
    # pris en compte dans la moyenne => taux = 0 si NaN
    df_eau.loc[:, ["taux_impact_arretes_secheresse_pourcent"]] = df_eau.loc[:, ["taux_impact_arretes_secheresse_pourcent"]].fillna(0).round(0)
    return df_eau


def _ajouter_colonnes_notes_eau(df_indicateurs_eau):
    seuils_irrigation = df_indicateurs_eau.loc[df_indicateurs_eau["categorie_territoire"] == "DEPARTEMENT", "irrigation_mm"].quantile(
        [1 / 4, 1 / 2, 3 / 4]
    )

    df_indicateurs_eau["irrigation_note"] = calculer_note_par_interpolation_n_seuils(
        df_indicateurs_eau["irrigation_mm"],
        [
            0,
            seuils_irrigation.values[0],
            seuils_irrigation.values[1],
            seuils_irrigation.values[2],
            seuils_irrigation.values[2] + 1.5 * (seuils_irrigation.values[2] - seuils_irrigation.values[0]),
        ],
        [10, 7.5, 5, 2.5, 0],
    )

    df_indicateurs_eau["arretes_secheresse_note"] = calculer_note_par_interpolation_n_seuils(
        df_indicateurs_eau["taux_impact_arretes_secheresse_pourcent"],
        [0, 100],
        [10, 0],
    )

    df_indicateurs_eau["eau_note"] = (df_indicateurs_eau["irrigation_note"] + df_indicateurs_eau["arretes_secheresse_note"]) / 2

    return df_indicateurs_eau
