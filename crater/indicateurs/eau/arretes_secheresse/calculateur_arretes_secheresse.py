from pathlib import Path

import pandas
from pandas import DataFrame

from crater.indicateurs.eau.arretes_secheresse.precalculateur_arretes_secheresse import pretraiter_donnees_arretes_secheresse
from crater.commun.calculs.outils_dataframes import produit_cartesien
from crater.commun.calculs.outils_territoires import ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales, trier_territoires
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires
from crater.commun.logger import log
from crater.config.config_globale import NOM_DOSSIER_CACHE, IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX


def calculer_indicateurs_arretes_secheresse(
    territoires: DataFrame,
    fichier_occupation_sols: Path,
    chemin_geometries_communes: Path,
    chemin_geometries_zones_alerte: Path,
    chemins_arretes_secheresse: list[Path],
    chemin_dossier_output: Path,
) -> DataFrame:
    log.info("##### CALCUL DES INDICATEURS D'ARRETES SECHERESSE #####")

    df_arretes_secheresse, df_intersection_communes_et_zones_alertes = _generer_et_charger_donnees_sources_pretraitees(
        territoires, chemin_geometries_communes, chemin_geometries_zones_alerte, chemins_arretes_secheresse, chemin_dossier_output
    )

    df_arretes_secheresse_par_annees_mois = _calculer_part_des_annees_mois_en_arrete_secheresse_par_zones_alerte(
        df_arretes_secheresse,
        ["Alerte", "Alerte renforcée", "Crise"],
        "SUPERFICIELLE",
    )

    df_territoires_en_arretes_secheresse_par_annees_mois = _calculer_parts_territoires_en_arrete_par_annees_mois(
        territoires, fichier_occupation_sols, df_arretes_secheresse_par_annees_mois, df_intersection_communes_et_zones_alertes
    )

    exporter_df_indicateurs_par_territoires(
        df_territoires_en_arretes_secheresse_par_annees_mois, chemin_dossier_output, "arretes_secheresse_par_annees_mois"
    )

    return df_territoires_en_arretes_secheresse_par_annees_mois


def _generer_et_charger_donnees_sources_pretraitees(
    territoires, chemin_geometries_communes, chemin_geometries_zones_alerte, chemins_arretes_secheresse, chemin_dossier_output
):
    if not (
        (chemin_dossier_output / NOM_DOSSIER_CACHE / "arretes_secheresse.csv").exists()
        and (chemin_dossier_output / NOM_DOSSIER_CACHE / "intersection_communes_et_zones_alertes.csv").exists()
    ):
        log.info("    - les fichiers prétraités des arrêtés sont absents, génération...")
        pretraiter_donnees_arretes_secheresse(
            territoires,
            chemin_geometries_zones_alerte,
            chemins_arretes_secheresse,
            chemin_geometries_communes,
            chemin_dossier_output / NOM_DOSSIER_CACHE,
        )
    df_arretes_secheresse = pandas.read_csv(chemin_dossier_output / NOM_DOSSIER_CACHE / "arretes_secheresse.csv", sep=";")
    df_intersection_communes_et_zones_alertes = pandas.read_csv(
        chemin_dossier_output / NOM_DOSSIER_CACHE / "intersection_communes_et_zones_alertes.csv", sep=";"
    )

    return df_arretes_secheresse, df_intersection_communes_et_zones_alertes


def _calculer_part_des_annees_mois_en_arrete_secheresse_par_zones_alerte(
    df_arretes_secheresse: DataFrame, niveaux_alerte_retenus: list[str], type_de_zones: str
) -> DataFrame:
    df_arretes_secheresse_par_annees_mois = df_arretes_secheresse.loc[
        df_arretes_secheresse.niveau_alerte.isin(niveaux_alerte_retenus) & (df_arretes_secheresse.type_zone_alerte == type_de_zones),
        ["numero_arrete", "id_zone_alerte", "niveau_alerte", "date_debut", "date_fin"],
    ].copy()

    df_arretes_secheresse_par_annees_mois = _calculer_nombre_de_jours_par_mois_en_arrete_secheresse(df_arretes_secheresse_par_annees_mois)
    df_arretes_secheresse_par_annees_mois["nb_jours_totaux"] = df_arretes_secheresse_par_annees_mois["annee_mois"].dt.days_in_month
    df_arretes_secheresse_par_annees_mois["part_annee_mois_en_arrete_pourcent"] = round(
        100 * df_arretes_secheresse_par_annees_mois["nb_jours_en_arrete"] / df_arretes_secheresse_par_annees_mois["nb_jours_totaux"], 1
    )

    return df_arretes_secheresse_par_annees_mois


def _calculer_nombre_de_jours_par_mois_en_arrete_secheresse(df_arretes_secheresse_par_annees_mois) -> DataFrame:
    # les arretés courent de date_debut inclus à date_fin inclus => inclusive="both"
    df_arretes_secheresse_par_annees_mois["dates"] = df_arretes_secheresse_par_annees_mois.apply(
        lambda x: pandas.date_range(x.date_debut, x.date_fin, inclusive="both").to_list(), axis=1
    )
    df_arretes_secheresse_par_annees_mois = (
        df_arretes_secheresse_par_annees_mois.explode("dates").drop(columns=["date_debut", "date_fin"]).rename(columns={"dates": "date"})
    )
    df_arretes_secheresse_par_annees_mois = df_arretes_secheresse_par_annees_mois.loc[:, ["id_zone_alerte", "date"]].drop_duplicates()
    df_arretes_secheresse_par_annees_mois["annee_mois"] = df_arretes_secheresse_par_annees_mois["date"].values.astype("datetime64[M]")
    df_arretes_secheresse_par_annees_mois = (
        df_arretes_secheresse_par_annees_mois.loc[:, ["id_zone_alerte", "annee_mois", "date"]]
        .groupby(["id_zone_alerte", "annee_mois"], as_index=False)
        .count()
        .rename(columns={"date": "nb_jours_en_arrete"})
    )
    return df_arretes_secheresse_par_annees_mois


def _calculer_parts_territoires_en_arrete_par_annees_mois(
    territoires: DataFrame,
    fichier_occupation_sols: Path,
    df_arretes_secheresse_par_annees_mois: DataFrame,
    df_intersection_communes_et_zones_alertes: DataFrame,
) -> DataFrame:

    territoires = charger_et_joindre_df_indicateurs(territoires, fichier_occupation_sols)

    df_communes_en_arrete_par_annees_mois = _calculer_parts_communes_en_arrete_par_annees_mois(
        df_arretes_secheresse_par_annees_mois, df_intersection_communes_et_zones_alertes
    )

    colonnes_territoires = ["nom_territoire", "categorie_territoire"] + IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX + ["superficie_ha"]
    df_territoires_en_arrete_par_annees_mois = produit_cartesien(
        territoires.loc[:, colonnes_territoires].reset_index(),
        pandas.DataFrame({"annee_mois": df_communes_en_arrete_par_annees_mois["annee_mois"].drop_duplicates().sort_values()}),
    ).set_index(["id_territoire", "annee_mois"])
    df_territoires_en_arrete_par_annees_mois = pandas.merge(
        df_territoires_en_arrete_par_annees_mois,
        df_communes_en_arrete_par_annees_mois.loc[:, ["id_commune", "annee_mois", "part_commune_en_arrete_pourcent"]]
        .rename(columns={"id_commune": "id_territoire", "part_commune_en_arrete_pourcent": "part_territoire_en_arrete_pourcent"})
        .set_index(["id_territoire", "annee_mois"]),
        left_index=True,
        right_index=True,
        how="left",
    )
    df_territoires_en_arrete_par_annees_mois["part_territoire_en_arrete_pourcent"] = df_territoires_en_arrete_par_annees_mois[
        "part_territoire_en_arrete_pourcent"
    ].fillna(0)

    df_territoires_en_arrete_par_annees_mois = (
        ajouter_donnees_supraterritoriales_par_moyenne_ponderee_donnees_territoriales(
            df_territoires_en_arrete_par_annees_mois, "part_territoire_en_arrete_pourcent", "superficie_ha"
        )
        .reset_index()
        .drop(columns=IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX)
    )
    df_territoires_en_arrete_par_annees_mois["part_territoire_en_arrete_pourcent"] = df_territoires_en_arrete_par_annees_mois[
        "part_territoire_en_arrete_pourcent"
    ].round(0)

    # Suppression des lignes avec valeur nulle ou faibles, sinon le fichier csv dépasse les 100 Mo
    df_territoires_en_arrete_par_annees_mois = df_territoires_en_arrete_par_annees_mois.loc[
        df_territoires_en_arrete_par_annees_mois.part_territoire_en_arrete_pourcent >= 1,
        ["id_territoire", "nom_territoire", "categorie_territoire", "annee_mois", "part_territoire_en_arrete_pourcent"],
    ]
    df_territoires_en_arrete_par_annees_mois = trier_territoires(df_territoires_en_arrete_par_annees_mois)

    return df_territoires_en_arrete_par_annees_mois


def _calculer_parts_communes_en_arrete_par_annees_mois(df_arretes_secheresse_par_annees_mois, df_intersection_communes_et_zones_alertes) -> DataFrame:
    df_communes_en_arrete_par_annees_mois = pandas.merge(
        df_intersection_communes_et_zones_alertes.loc[:, ["id_commune", "id_zone_alerte", "part_commune_dans_zone_alerte"]],
        df_arretes_secheresse_par_annees_mois.loc[:, ["id_zone_alerte", "annee_mois", "part_annee_mois_en_arrete_pourcent"]],
        on="id_zone_alerte",
        how="inner",
    )
    df_communes_en_arrete_par_annees_mois["part_commune_en_arrete_pourcent"] = (
        df_communes_en_arrete_par_annees_mois["part_commune_dans_zone_alerte"]
        * df_communes_en_arrete_par_annees_mois["part_annee_mois_en_arrete_pourcent"]
    )
    df_communes_en_arrete_par_annees_mois = (
        df_communes_en_arrete_par_annees_mois.loc[:, ["id_commune", "annee_mois", "part_commune_en_arrete_pourcent"]]
        .groupby(["id_commune", "annee_mois"], as_index=False)
        .sum()
        .round(2)
    )

    if (df_communes_en_arrete_par_annees_mois["part_commune_en_arrete_pourcent"] > 300).any():
        raise ValueError(
            "ERREUR calcul df_communes_en_arrete_par_annees_mois : part_commune_en_arrete_pourcent > 300, à cause d'arrêtés "
            "concomitant spatialement et temporellement !)"
        )
    elif (df_communes_en_arrete_par_annees_mois["part_commune_en_arrete_pourcent"] > 100).any():
        log.warning(
            "WARNING calcul df_communes_en_arrete_par_annees_mois : part_commune_en_arrete_pourcent > 100, à cause d'arrêtés "
            "concomitant spatialement et temporellement !)"
        )
        df_communes_en_arrete_par_annees_mois["part_commune_en_arrete_pourcent"] = df_communes_en_arrete_par_annees_mois[
            "part_commune_en_arrete_pourcent"
        ].clip(0, 100)

    return df_communes_en_arrete_par_annees_mois
