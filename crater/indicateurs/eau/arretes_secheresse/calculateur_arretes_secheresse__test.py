import unittest
from pathlib import Path

import pandas

from crater.indicateurs.eau.arretes_secheresse.calculateur_arretes_secheresse import (
    _calculer_part_des_annees_mois_en_arrete_secheresse_par_zones_alerte,
    _calculer_parts_territoires_en_arrete_par_annees_mois,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data_calculateur"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestCalculateurArretesSecheresse(unittest.TestCase):
    reinitialiser_dossier(CHEMIN_OUTPUT_DATA, reinitialiser_cache=True)

    def test_calculer_part_des_annees_mois_en_arrete_secheresse_par_zones_alerte(self):
        df_arretes_secheresse = pandas.read_csv(CHEMIN_INPUT_DATA / "arretes_secheresse.csv", sep=";")
        # when
        _calculer_part_des_annees_mois_en_arrete_secheresse_par_zones_alerte(
            df_arretes_secheresse,
            ["Alerte", "Alerte renforcée", "Crise"],
            "SUP",
        ).to_csv(CHEMIN_OUTPUT_DATA / "arretes_secheresse_par_annees_mois.csv", sep=";", encoding="utf-8", index=False)
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "arretes_secheresse_par_annees_mois.csv",
            CHEMIN_OUTPUT_DATA / "arretes_secheresse_par_annees_mois.csv",
        )

    def test_calculer_parts_territoires_en_arrete_par_annees_mois(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # when
        _calculer_parts_territoires_en_arrete_par_annees_mois(
            territoires,
            CHEMIN_INPUT_DATA / "occupation_sols" / "occupation_sols.csv",
            pandas.read_csv(CHEMIN_EXPECTED_DATA / "arretes_secheresse_par_annees_mois.csv", sep=";"),
            pandas.read_csv(CHEMIN_INPUT_DATA / "intersection_zones_alerte_communes.csv", sep=";"),
        ).to_csv(CHEMIN_OUTPUT_DATA / "territoires_en_arrete_par_annees_mois.csv", sep=";", encoding="utf-8", float_format="%.2f", index=False)
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "territoires_en_arrete_par_annees_mois.csv",
            CHEMIN_OUTPUT_DATA / "territoires_en_arrete_par_annees_mois.csv",
        )


if __name__ == "__main__":
    unittest.main()
