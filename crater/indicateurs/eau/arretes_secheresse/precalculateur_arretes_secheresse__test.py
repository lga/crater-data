import unittest
from pathlib import Path

import geopandas
import pandas

from crater.indicateurs.eau.arretes_secheresse.precalculateur_arretes_secheresse import (
    _changer_zones_alerte_des_arretes_absentes_des_geometries,
    pretraiter_donnees_arretes_secheresse,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
    assert_error_message_raised,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data_precalculateur"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestPrecalculateurArretesSecheresse(unittest.TestCase):
    reinitialiser_dossier(CHEMIN_OUTPUT_DATA, reinitialiser_cache=True)

    def test_assigner_zones_alerte_arretes_absentes_des_geometries(self):
        # given
        df_arretes = pandas.DataFrame(
            columns=["id_zone_alerte", "categorie_zone_alerte", "superficie_zone_alerte_ha"],
            data=[
                ["A-1", "A", 1],
                ["A-101", "A", 101],
                ["A-100", "A", 100],
                ["A-99", "A", 99],
                ["A-150", "A", 150],
                ["A-151", "A", 151],
                ["A-1000", "A", 1000],
                ["B-1000", "B", 1000],
            ],
        )
        df_zones = geopandas.GeoDataFrame(
            columns=["id_zone_alerte", "categorie_zone_alerte", "superficie_zone_alerte_ha"],
            data=[
                ["A-1", "A", 1],
                ["A-200", "A", 200],
                ["A-100", "A", 100],
                ["B-10", "B", 10],
            ],
        )
        df_arretes_final_attendu = pandas.DataFrame(
            columns=["id_zone_alerte", "superficie_zone_alerte_ha"],
            data=[
                ["A-1", 1],
                ["A-100", 101],
                ["A-100", 100],
                ["A-100", 99],
                ["A-100", 150],
                ["A-200", 151],
                ["A-200", 1000],
                ["B-10", 1000],
            ],
        )
        # when
        df_arretes_final = _changer_zones_alerte_des_arretes_absentes_des_geometries(df_arretes, df_zones)
        # then
        pandas.testing.assert_frame_equal(df_arretes_final_attendu, df_arretes_final)

    def test_pretraiter_donnees_arretes_secheresse(self):
        # given
        referentiel_territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # when
        pretraiter_donnees_arretes_secheresse(
            referentiel_territoires,
            CHEMIN_INPUT_DATA / "geometries_zones_alerte_test",
            [
                CHEMIN_INPUT_DATA / "propluvia_test_2013.csv",
                CHEMIN_INPUT_DATA / "propluvia_test_2021.csv",
                CHEMIN_INPUT_DATA / "propluvia_test_2022.csv",
            ],
            CHEMIN_INPUT_DATA / "geometries_communes_test",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "arretes_secheresse.csv",
            CHEMIN_OUTPUT_DATA / "arretes_secheresse.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "intersection_communes_et_zones_alertes.csv",
            CHEMIN_OUTPUT_DATA / "intersection_communes_et_zones_alertes.csv",
        )

    def test_precalculer_donnees_arretes_secheresse_ko(self):
        # given
        referentiel_territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # then
        assert_error_message_raised(
            self,
            "ERREUR: le(s) 1 élément(s) suivant(s) de la serie 1 sont absents de la série 2 : 1-D47-SUPERFICIELLE-3780",
            pretraiter_donnees_arretes_secheresse,
            referentiel_territoires=referentiel_territoires,
            chemin_geometries_zones_alerte=CHEMIN_INPUT_DATA / "geometries_zones_alerte_test",
            chemins_arretes_secheresse=[
                CHEMIN_INPUT_DATA / "propluvia_test_2013_avec_zones_sans_geometries.csv",
                CHEMIN_INPUT_DATA / "propluvia_test_2021.csv",
                CHEMIN_INPUT_DATA / "propluvia_test_2022.csv",
            ],
            chemin_geometries_communes=CHEMIN_INPUT_DATA / "geometries_communes_test",
            chemin_dossier_output=CHEMIN_OUTPUT_DATA,
        )


if __name__ == "__main__":
    unittest.main()
