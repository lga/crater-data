from pathlib import Path

import geopandas as gpd
import numpy as np
import pandas
from geopandas import GeoDataFrame
from pandas import DataFrame

from crater.commun.calculs.outils_geodataframes import calculer_colonne_superficie_en_hectares
from crater.commun.calculs.outils_verification import verifier_elements_colonne1_presents_dans_colonne2, verifier_absence_doublons
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.chargeurs.chargeur_geometries_communes import charger_geometries_communes
from crater.commun.logger import log, chronometre
from crater.config.config_globale import CRS_PROJET


def pretraiter_donnees_arretes_secheresse(
    referentiel_territoires: DataFrame,
    chemin_geometries_zones_alerte: Path,
    chemins_arretes_secheresse: list[Path],
    chemin_geometries_communes: Path,
    chemin_dossier_output: Path,
) -> None:
    log.info("    - précalcul données arrêtés sècheresses")

    reinitialiser_dossier(chemin_dossier_output)

    df_arretes_secheresse = _charger_arretes_secheresse_toutes_annees(chemins_arretes_secheresse, referentiel_territoires)
    gdf_zones_alerte = _charger_geometries_zones_alerte(chemin_geometries_zones_alerte)

    df_arretes_secheresse, gdf_zones_alerte = _ajouter_correspondance_des_zones_alerte_entre_arretes_et_geometries_des_zones_alertes(
        df_arretes_secheresse, gdf_zones_alerte, referentiel_territoires
    )

    df_intersection_communes_et_zones_alertes = _calculer_intersection_communes_et_zones_alerte(chemin_geometries_communes, gdf_zones_alerte)

    df_arretes_secheresse.to_csv(chemin_dossier_output / "arretes_secheresse.csv", index=False, sep=";")
    df_intersection_communes_et_zones_alertes.to_csv(chemin_dossier_output / "intersection_communes_et_zones_alertes.csv", index=False, sep=";")


def _ajouter_correspondance_des_zones_alerte_entre_arretes_et_geometries_des_zones_alertes(
    df_arretes_secheresse: DataFrame, gdf_zones_alerte: GeoDataFrame, referentiel_territoires: DataFrame
) -> tuple[DataFrame, GeoDataFrame]:
    df_arretes_secheresse = _ajouter_colonnes_id_zone_alerte_aux_arretes_secheresse(df_arretes_secheresse, referentiel_territoires)
    gdf_zones_alerte = _ajouter_colonnes_id_zone_alerte_aux_zones_alerte_et_supprimer_doublons(gdf_zones_alerte)

    df_arretes_secheresse = _changer_zones_alerte_des_arretes_absentes_des_geometries(df_arretes_secheresse, gdf_zones_alerte)

    verifier_elements_colonne1_presents_dans_colonne2(
        df_arretes_secheresse["id_zone_alerte"].drop_duplicates(), gdf_zones_alerte["id_zone_alerte"].drop_duplicates()
    )

    return df_arretes_secheresse, gdf_zones_alerte


def _changer_zones_alerte_des_arretes_absentes_des_geometries(df_arretes_secheresse, gdf_zones_alerte) -> DataFrame:
    df_arretes_secheresse = df_arretes_secheresse.copy()
    df_zones_alerte_des_arretes_a_changer = (
        df_arretes_secheresse[~df_arretes_secheresse.id_zone_alerte.isin(gdf_zones_alerte.id_zone_alerte)]
        .loc[:, ["id_zone_alerte", "categorie_zone_alerte"]]
        .drop_duplicates()
    )
    if df_zones_alerte_des_arretes_a_changer.shape[0] > 0:
        df_correspondances_zones_a_changer = pandas.DataFrame()
        for index, row in df_zones_alerte_des_arretes_a_changer.iterrows():
            df_correspondances_zones_a_changer = pandas.concat(
                [
                    df_correspondances_zones_a_changer,
                    pandas.merge_asof(
                        df_arretes_secheresse.loc[df_arretes_secheresse.id_zone_alerte == row["id_zone_alerte"]],
                        gdf_zones_alerte.loc[gdf_zones_alerte.categorie_zone_alerte == row["categorie_zone_alerte"]]
                        .drop(columns=["categorie_zone_alerte"])
                        .rename(columns={"id_zone_alerte": "id_zone_alerte_dans_df_zones_alerte"})
                        .sort_values(by="superficie_zone_alerte_ha"),
                        on="superficie_zone_alerte_ha",
                        direction="nearest",
                    ),
                ]
            )

        df_arretes_secheresse = pandas.merge(
            df_arretes_secheresse.drop(columns=["categorie_zone_alerte"]),
            df_correspondances_zones_a_changer.loc[:, ["id_zone_alerte", "id_zone_alerte_dans_df_zones_alerte"]],
            on="id_zone_alerte",
            how="left",
        )
        df_arretes_secheresse.loc[~df_arretes_secheresse.id_zone_alerte_dans_df_zones_alerte.isna(), "id_zone_alerte"] = df_arretes_secheresse.loc[
            ~df_arretes_secheresse.id_zone_alerte_dans_df_zones_alerte.isna(), "id_zone_alerte_dans_df_zones_alerte"
        ]
        df_arretes_secheresse.drop(columns=["id_zone_alerte_dans_df_zones_alerte"], inplace=True)

    return df_arretes_secheresse


def _ajouter_colonnes_id_zone_alerte_aux_arretes_secheresse(df_arretes_secheresse: DataFrame, referentiel_territoires: DataFrame) -> DataFrame:
    departements = referentiel_territoires.loc[referentiel_territoires.categorie_territoire == "DEPARTEMENT", ["nom_territoire"]].copy()
    departements["code_departement"] = departements.index.str[2:4]
    departements.loc[departements.code_departement == "75", "nom_territoire"] = "Paris"

    df_arretes_secheresse = pandas.merge(
        df_arretes_secheresse,
        departements.rename(columns={"code_departement": "code_departement_zone_alerte", "nom_territoire": "nom_departement_zone_alerte"}),
        on="nom_departement_zone_alerte",
        how="left",
    )

    _calculer_colonnes_id_zone_alerte(df_arretes_secheresse)

    return df_arretes_secheresse


def _ajouter_colonnes_id_zone_alerte_aux_zones_alerte_et_supprimer_doublons(gdf_zones_alerte: GeoDataFrame) -> GeoDataFrame:
    _calculer_colonnes_id_zone_alerte(gdf_zones_alerte)

    gdf_zones_alerte = gdf_zones_alerte.groupby("id_zone_alerte").head(1).reset_index(drop=True)

    verifier_absence_doublons(gdf_zones_alerte, "id_zone_alerte")

    return gdf_zones_alerte


def _calculer_colonnes_id_zone_alerte(df: DataFrame) -> None:
    # certaines zones d’alerte avec des codesZA ‘-‘ et pas ‘_’
    df["categorie_zone_alerte"] = (
        df["code_zone_alerte"].str.replace("-", "_").replace("_", "") + "-D" + df["code_departement_zone_alerte"] + "-" + df["type_zone_alerte"]
    )

    df["id_zone_alerte"] = df["categorie_zone_alerte"] + "-" + df["superficie_zone_alerte_ha"].astype("int64").astype("str")


def _calculer_intersection_communes_et_zones_alerte(
    chemin_geometries_communes: Path,
    geometries_zones_alertes: GeoDataFrame,
) -> DataFrame:
    log.info("##### CALCUL INTERSECTION ZONES D'ALERTE ET COMMUNES #####")

    geometries_communes = charger_geometries_communes(chemin_geometries_communes)
    df_intersection_zones_alertes_et_communes = _intersecter_zones_alertes_et_communes(geometries_communes, geometries_zones_alertes)
    df_intersection_zones_alertes_et_communes = df_intersection_zones_alertes_et_communes.loc[
        :,
        [
            "id_commune",
            "nom_commune",
            "superficie_commune_ha",
            "id_zone_alerte",
            "code_zone_alerte",
            "version_zone_alerte",
            "id_geometrie_zone_alerte",
            "type_zone_alerte",
            "superficie_intersection_ha",
            "part_commune_dans_zone_alerte",
        ],
    ]

    return df_intersection_zones_alertes_et_communes


@chronometre
def _intersecter_zones_alertes_et_communes(geometries_communes: GeoDataFrame, geometries_zones_alertes: GeoDataFrame) -> DataFrame:
    gdf = (
        gpd.overlay(geometries_communes, geometries_zones_alertes, how="intersection")
        .rename(
            columns={
                "superficie_ha": "superficie_commune_ha",
                "codeZA": "code_zone_alerte",
                "idZone": "id_geometrie_zone_alerte",
                "type": "type_zone_alerte",
                "version": "version_zone_alerte",
            }
        )
        .sort_values(by=["id_commune", "code_zone_alerte"])
        .reset_index(drop=True)
    )
    gdf["superficie_intersection_ha"] = calculer_colonne_superficie_en_hectares(gdf).round(1)
    gdf["part_commune_dans_zone_alerte"] = (gdf["superficie_intersection_ha"] / gdf["superficie_commune_ha"]).round(2).clip(0, 1)

    return gdf


def _charger_arretes_secheresse_toutes_annees(fichiers_source_arretes_secheresse: list[Path], referentiel_territoires: DataFrame):
    df_arretes_secheresse = pandas.DataFrame()
    for f in fichiers_source_arretes_secheresse:
        df_arretes_secheresse = pandas.concat([df_arretes_secheresse, _charger_source_arretes_secheresse(f)])
    df_arretes_secheresse = df_arretes_secheresse.loc[
        :, ["Numero_AR", "Code_ZA", "Libelle_ZA", "Surface", "Departement", "NIVEAU", "Date_debut", "Date_fin", "Type_de_zone"]
    ].rename(
        columns={
            "Numero_AR": "numero_arrete",
            "Code_ZA": "code_zone_alerte",
            "Libelle_ZA": "libelle_zone_alerte",
            "Surface": "superficie_zone_alerte_km2",
            "Departement": "nom_departement_zone_alerte",
            "NIVEAU": "niveau_alerte",
            "Date_debut": "date_debut",
            "Date_fin": "date_fin",
            "Type_de_zone": "type_zone_alerte",
        },
    )
    df_arretes_secheresse.rename(columns={"superficie_zone_alerte_km2": "superficie_zone_alerte_ha"}, inplace=True)
    df_arretes_secheresse["superficie_zone_alerte_ha"] = (df_arretes_secheresse["superficie_zone_alerte_ha"] * 100).round(0).astype("int64")
    # zones d'alerte à surface nulle (eg idZone = 7626)
    df_arretes_secheresse = df_arretes_secheresse.loc[df_arretes_secheresse["superficie_zone_alerte_ha"] > 0]

    df_arretes_secheresse["type_zone_alerte"] = np.where(df_arretes_secheresse["type_zone_alerte"] == "Superficielle", "SUPERFICIELLE", "SOUTERRAINE")

    df_arretes_secheresse["date_debut"] = df_arretes_secheresse["date_debut"].str.replace("0022-06-09", "2022-06-09")

    # les arretés courent de date_debut inclus à date_fin inclus
    df_arretes_secheresse["duree_jours"] = (
        pandas.to_datetime(df_arretes_secheresse["date_fin"]) - pandas.to_datetime(df_arretes_secheresse["date_debut"])
    ).dt.days + 1
    # certains arrêtés sont sur une durée nulle ou négative...
    df_arretes_secheresse = df_arretes_secheresse.loc[df_arretes_secheresse.duree_jours > 0]
    df_arretes_secheresse.drop(columns=["duree_jours"], inplace=True)

    # les arrêtés à cheval sur deux années apparaissent dans les 2 fichiers annuels
    df_arretes_secheresse.drop_duplicates(inplace=True)

    return df_arretes_secheresse


def _charger_source_arretes_secheresse(
    fichier_source_arretes_secheresse: Path,
) -> DataFrame:
    df_arretes_secheresse = pandas.read_csv(
        fichier_source_arretes_secheresse,
        sep=",",
    )
    return df_arretes_secheresse


def _charger_geometries_zones_alerte(chemin_dossier: Path, crs=CRS_PROJET) -> GeoDataFrame:
    log.info("   => chargement des géométries des zones d'alerte depuis %s", chemin_dossier)

    gdf_zones_alerte = gpd.read_file(chemin_dossier).set_crs(crs, allow_override=True).sort_values(by=["codeZA", "version"])
    gdf_zones_alerte = gdf_zones_alerte.loc[:, ["codeZA", "codeDep", "idZone", "type", "version", "geometry"]]
    gdf_zones_alerte.rename(
        columns={
            "codeZA": "code_zone_alerte",
            "codeDep": "code_departement_zone_alerte",
            "idZone": "id_geometrie_zone_alerte",
            "type": "type_zone_alerte",
            "version": "version_zone_alerte",
        },
        inplace=True,
    )
    gdf_zones_alerte["geometry"] = gdf_zones_alerte.geometry.buffer(0.001)
    gdf_zones_alerte["superficie_zone_alerte_ha"] = calculer_colonne_superficie_en_hectares(gdf_zones_alerte).round(0)
    # zones d'alerte à surface nulle (eg idZone = 7626)
    gdf_zones_alerte = gdf_zones_alerte.loc[gdf_zones_alerte["superficie_zone_alerte_ha"] > 0]
    gdf_zones_alerte["superficie_zone_alerte_ha"] = gdf_zones_alerte["superficie_zone_alerte_ha"].astype("int64")

    gdf_zones_alerte["type_zone_alerte"] = np.where(gdf_zones_alerte["type_zone_alerte"] == "SUP", "SUPERFICIELLE", "SOUTERRAINE")

    return gdf_zones_alerte
