from pathlib import Path

from pandas import DataFrame

from crater.donnees_sources.hubeau_irrigation.calcul_hubeau_irrigation import ResultatsDonneesHubeauIrrigation
from crater.indicateurs.eau.arretes_secheresse.calculateur_arretes_secheresse import calculer_indicateurs_arretes_secheresse
from crater.indicateurs.eau.irrigation.calculateur_eau_irrigation import calculer_irrigation_par_annees
from crater.indicateurs.eau.pratiques_irrigation.calculateur_pratiques_irrigation import calculer_pratiques_irrigation
from crater.indicateurs.eau.synthese_eau.calculateur_eau import calculer_synthese_eau
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.logger import log

ANNEE_PLUS_RECENTE = 2020
NB_ANNEES_POUR_MOYENNE = 5


def lancer_pipeline_eau(
    territoires: DataFrame,
    fichier_occupation_sols: Path,
    donnees_irrigation: list[ResultatsDonneesHubeauIrrigation],
    fichier_correspondances_anciennes_nouvelles_regions: Path,
    dossier_agreste: Path,
    chemin_geometries_communes: Path,
    chemin_geometries_zones_alerte: Path,
    chemins_arretes_secheresse: list[Path],
    fichier_surfaces_riz_par_departements: Path,
    dossier_output: Path,
) -> Path:
    log.info("##### CALCUL PIPELINE EAU #####")

    reinitialiser_dossier(dossier_output)
    irrigation_par_annees: DataFrame = calculer_irrigation_par_annees(territoires, fichier_occupation_sols, donnees_irrigation, dossier_output)
    arretes_secheresse_par_annees_mois: DataFrame = calculer_indicateurs_arretes_secheresse(
        territoires,
        fichier_occupation_sols,
        chemin_geometries_communes,
        chemin_geometries_zones_alerte,
        chemins_arretes_secheresse,
        dossier_output,
    )
    df_pratiques_irrigation = calculer_pratiques_irrigation(
        territoires,
        fichier_correspondances_anciennes_nouvelles_regions,
        dossier_agreste,
        fichier_surfaces_riz_par_departements,
        dossier_output,
    )

    return calculer_synthese_eau(
        territoires,
        irrigation_par_annees,
        arretes_secheresse_par_annees_mois,
        ANNEE_PLUS_RECENTE,
        NB_ANNEES_POUR_MOYENNE,
        df_pratiques_irrigation,
        dossier_output,
    )

    # FIXME : code commenté car il prenait pas mal de temps
    # Il faudrait trouver un moyen pour le désactiver par défaut (exec github, ou exec classique sur nos postes), et l'activer à la demande
    # exporter_cartes(
    #     synthese_eau,
    #     [
    #         "irrigation_mm",
    #         "irrigation_gravitaire_m3",
    #         "irrigation_hors_gravitaire_m3",
    #         "taux_impact_arretes_secheresse_pourcent",
    #         "irrigation_note",
    #         "arretes_secheresse_note",
    #         "eau_note",
    #     ],
    #     ["REGION", "DEPARTEMENT", "EPCI", "COMMUNE"],
    #     dossier_output / "cartes",
    # )
    # exporter_histogrammes(
    #     synthese_eau,
    #     [
    #         "irrigation_mm",
    #         "irrigation_m3",
    #         "irrigation_gravitaire_m3",
    #         "irrigation_hors_gravitaire_m3",
    #         "taux_impact_arretes_secheresse_pourcent",
    #         "irrigation_note",
    #         "arretes_secheresse_note",
    #         "eau_note",
    #     ],
    #     dossier_output / "diagrammes",
    #     bins=100,
    # )
