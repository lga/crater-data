import unittest
from pathlib import Path

from crater.indicateurs.occupation_sols.calculateur_occupation_sols import calculer_occupation_sols
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)
from crater.config import groupes_cultures as config_groupes_cultures

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestDonneesSols(unittest.TestCase):
    def test_calculer_occupation_sols(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")
        config_groupes_cultures.FICHIER_CONFIG_CORRESPONDANCES_RPG_VERS_CRATER = CHEMIN_INPUT_DATA / "correspondance_nomenclature_rpg_vers_crater.csv"
        # when
        calculer_occupation_sols(
            territoires,
            CHEMIN_INPUT_DATA / "geometries_communes_test",
            CHEMIN_INPUT_DATA / "sau_par_commune_et_culture",
            CHEMIN_INPUT_DATA / "agreste" / "2010",
            CHEMIN_INPUT_DATA / "agreste" / "2020" / "cartostat.csv",
            CHEMIN_INPUT_DATA / "clc" / "donnees_clc_2018.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "occupation_sols.csv",
            CHEMIN_OUTPUT_DATA / "occupation_sols.csv",
        )


if __name__ == "__main__":
    unittest.main()
