from pathlib import Path

import geopandas as gpd
import numpy as np
import pandas as pd
from pandas import DataFrame

from crater.donnees_sources.surface_agricole_utile import chargeur_sau_par_commune_et_culture
from crater.indicateurs.occupation_sols.chargeur_sau_ra_agreste import charger_sau_ra_2010, charger_sau_ra_2020
from crater.commun.calculs.outils_dataframes import merge_strict
from crater.commun.calculs.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    vider_colonne_si_est_estimee,
)
from crater.commun.calculs.outils_verification import (
    verifier_coherence_referentiel_avec_geometries_communes,
)
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires, reinitialiser_dossier
from crater.commun.chargeurs import chargeur_geometries_communes
from crater.commun.logger import log
from crater.config import groupes_cultures as config_groupes_cultures
from crater.config.config_sources import (
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2020,
)

CODE_CATEGORIE_SAU_PRODUCTIVE_HORS_PRAIRIES = "PRODUCTIVE_HORS_PRAIRIES"
CODE_CATEGORIE_SAU_PRODUCTIVE_PRAIRIES = "PRODUCTIVE_PRAIRIES"
CODE_CATEGORIE_SAU_PEU_PRODUCTIVE = "PEU_PRODUCTIVE"


def calculer_occupation_sols(
    referentiel_territoires: DataFrame,
    chemin_fichiers_geometries_communes: Path,
    chemin_fichiers_sau: Path,
    chemin_dossier_agreste_2010: Path,
    chemin_fichier_agreste_2020: Path,
    chemin_fichier_donnees_clc: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### CALCUL DE L'OCCUPATION DES SOLS #####")

    reinitialiser_dossier(chemin_dossier_output)

    geometries_communes = chargeur_geometries_communes.charger_geometries_communes(chemin_fichiers_geometries_communes)

    verifier_coherence_referentiel_avec_geometries_communes(referentiel_territoires, geometries_communes)

    sau_rpg = chargeur_sau_par_commune_et_culture.charger(chemin_fichiers_sau)
    correspondance_nomenclature_rpg_vers_crater = pd.read_csv(
        config_groupes_cultures.FICHIER_CONFIG_CORRESPONDANCES_RPG_VERS_CRATER, sep=";", dtype="str"
    )

    sau_ra_2010 = charger_sau_ra_2010(chemin_dossier_agreste_2010)
    sau_ra_2020 = charger_sau_ra_2020(chemin_fichier_agreste_2020)

    territoires_enrichi = _ajouter_colonnes(
        referentiel_territoires,
        geometries_communes,
        sau_rpg,
        correspondance_nomenclature_rpg_vers_crater,
        sau_ra_2010,
        sau_ra_2020,
        chemin_fichier_donnees_clc,
    )

    territoires_enrichi = territoires_enrichi.loc[
        :,
        [
            "nom_territoire",
            "categorie_territoire",
            "superficie_ha",
            "sau_ha",
            "sau_productive_ha",
            "sau_productive_hors_prairies_ha",
            "sau_peu_productive_ha",
            "sau_ra_2010_ha",
            "sau_ra_2020_ha",
            "superficie_artificialisee_clc_ha",
            "superficie_agricole_clc_ha",
            "superficie_naturelle_ou_forestiere_clc_ha",
        ],
    ]

    return exporter_df_indicateurs_par_territoires(territoires_enrichi, chemin_dossier_output, "occupation_sols")


def _ajouter_colonnes(
    referentiel_territoires: DataFrame,
    geometries_communes: DataFrame,
    sau_rpg: DataFrame,
    correspondance_nomenclature_rpg_vers_crater: DataFrame,
    sau_ra_2010: DataFrame,
    sau_ra_2020: DataFrame,
    chemin_fichier_donnees_clc: Path,
) -> DataFrame:
    territoires = referentiel_territoires.copy()

    territoires = _ajouter_colonne_superficie_totale(territoires, geometries_communes)
    territoires = _ajouter_colonnes_sau_rpg(
        territoires,
        sau_rpg,
        correspondance_nomenclature_rpg_vers_crater,
    )
    territoires = _ajouter_colonnes_sau_ra_2010(territoires, sau_ra_2010)
    territoires = _ajouter_colonne_sau_ra_2020(territoires, sau_ra_2020)
    territoires = _ajouter_colonnes_donnees_clc(territoires, chemin_fichier_donnees_clc)

    return territoires


def _ajouter_colonne_superficie_totale(territoires: DataFrame, geometries_communes: gpd.GeoDataFrame) -> DataFrame:
    geometries_communes = geometries_communes.rename(columns={"id_commune": "id_territoire"}).set_index("id_territoire")
    territoires = territoires.join(geometries_communes[["superficie_ha"]])

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, "superficie_ha")

    return territoires


def _ajouter_colonnes_sau_rpg(
    territoires: DataFrame,
    sau_par_commune_et_code_rpg: DataFrame,
    correspondance_nomenclature_rpg_vers_crater: DataFrame,
) -> DataFrame:
    sau_par_commune_et_categorie = merge_strict(
        sau_par_commune_et_code_rpg.loc[:, ["id_commune", "code_culture_rpg", "sau_ha"]],
        correspondance_nomenclature_rpg_vers_crater.loc[:, ["code_culture_rpg", "categorie_sau"]],
        on="code_culture_rpg",
        how="left",
    )

    sau_par_commune_et_categorie["sau_productive_ha"] = sau_par_commune_et_categorie.loc[
        sau_par_commune_et_categorie["categorie_sau"].isin([CODE_CATEGORIE_SAU_PRODUCTIVE_HORS_PRAIRIES, CODE_CATEGORIE_SAU_PRODUCTIVE_PRAIRIES]),
        "sau_ha",
    ]

    sau_par_commune_et_categorie["sau_productive_hors_prairies_ha"] = sau_par_commune_et_categorie.loc[
        sau_par_commune_et_categorie["categorie_sau"] == CODE_CATEGORIE_SAU_PRODUCTIVE_HORS_PRAIRIES,
        "sau_ha",
    ]

    sau_par_commune_et_categorie["sau_peu_productive_ha"] = sau_par_commune_et_categorie.loc[
        sau_par_commune_et_categorie["categorie_sau"] == CODE_CATEGORIE_SAU_PEU_PRODUCTIVE,
        "sau_ha",
    ]

    territoires = _ajouter_colonne_sau(territoires, sau_par_commune_et_categorie, "sau_ha")
    territoires = _ajouter_colonne_sau(territoires, sau_par_commune_et_categorie, "sau_productive_ha")
    territoires = _ajouter_colonne_sau(territoires, sau_par_commune_et_categorie, "sau_productive_hors_prairies_ha")
    territoires = _ajouter_colonne_sau(territoires, sau_par_commune_et_categorie, "sau_peu_productive_ha")

    return territoires


def _ajouter_colonne_sau(territoires, sau_enrichie_nomenclature_crater, nom_colonne_sau_resultat) -> DataFrame:
    sau_par_commune = (
        sau_enrichie_nomenclature_crater.loc[:, ["id_commune", nom_colonne_sau_resultat]]
        .groupby(["id_commune"], as_index=False)
        .sum()
        .rename(columns={"id_commune": "id_territoire"})
        .set_index("id_territoire")
    )

    territoires = territoires.join(sau_par_commune)

    territoires.update(territoires.loc[territoires.categorie_territoire == "COMMUNE", nom_colonne_sau_resultat].fillna(0))

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(territoires, nom_colonne_sau_resultat)

    return territoires


def _ajouter_colonnes_sau_ra_2010(territoires, sau_ra_2010):
    territoires = territoires.join(sau_ra_2010.set_index("id_territoire").loc[:, ["sau_ra_2010_ha"]])

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        territoires,
        "sau_ra_2010_ha",
        categorie_territoire_a_sommer="COMMUNE",
        categories_territoires_cibles=["EPCI", "REGROUPEMENT_COMMUNES"],
    )

    territoires.loc[
        territoires.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010,
        "sau_ra_2010_ha",
    ] = np.nan

    return territoires


def _ajouter_colonne_sau_ra_2020(territoires, sau_ra_2020):
    territoires = territoires.join(sau_ra_2020.set_index("id_territoire"))

    territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
        territoires,
        "sau_ra_2020_ha",
    )
    territoires.loc[territoires.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2020, "sau_ra_2020_ha"] = np.nan

    return territoires


def _ajouter_colonnes_donnees_clc(territoires: DataFrame, chemin_fichier_donnees_clc: Path) -> DataFrame:

    df_donnees_clc = pd.read_csv(chemin_fichier_donnees_clc, sep=";").set_index("id_territoire")

    territoires = territoires.join(df_donnees_clc.drop(columns=["nom_territoire", "categorie_territoire"]))

    territoires["superficie_totale_clc_ha"] = (
        territoires["superficie_artificialisee_clc_ha"]
        + territoires["superficie_agricole_clc_ha"]
        + territoires["superficie_naturelle_ou_forestiere_clc_ha"]
    )

    noms_colonnes = ["superficie_artificialisee_clc_ha", "superficie_agricole_clc_ha", "superficie_naturelle_ou_forestiere_clc_ha"]

    for colonne in noms_colonnes:
        territoires[colonne] = territoires["superficie_ha"] * (territoires[colonne] / territoires["superficie_totale_clc_ha"])
        territoires = vider_colonne_si_est_estimee(territoires, colonne)

    territoires = territoires.drop(
        columns=[
            "superficie_totale_clc_ha",
        ]
    )
    return territoires


def _filtrer_annee_2018(df_clc: DataFrame) -> DataFrame:
    df_clc = df_clc[df_clc.annee == 2018]
    df_clc = df_clc.drop(columns="annee")
    return df_clc
