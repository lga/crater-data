import unittest
from pathlib import Path

from crater.indicateurs.population_agricole.calculateur_population_agricole import (
    calculer_population_agricole,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestPopulationAgricole(unittest.TestCase):
    def test_population_agricole(self):
        # given
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # when
        calculer_population_agricole(
            territoires,
            CHEMIN_INPUT_DATA / "population" / "population.csv",
            CHEMIN_INPUT_DATA,
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "population_agricole.csv",
            CHEMIN_OUTPUT_DATA / "population_agricole.csv",
        )


if __name__ == "__main__":
    unittest.main()
