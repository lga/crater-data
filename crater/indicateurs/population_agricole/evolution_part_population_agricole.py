from pandas import DataFrame

from crater.commun.calculs.notes_et_messages import calculer_note_par_interpolation_2_seuils
from crater.config.config_globale import ID_FRANCE


def ajouter_colonne_evolution_part_population_agricole(
    population_agricole: DataFrame,
) -> DataFrame:
    population_agricole.loc[:, "evolution_part_population_agricole_pt_de_pourcent"] = (
        (
            (
                population_agricole["actifs_agricoles_permanents_2010"] / population_agricole["population_totale_2010"]
                - population_agricole["actifs_agricoles_permanents_1988"] / population_agricole["population_totale_1990"]
            )
            * 100
            #   on caste explicitement en float64 sinon le type attribué par pandas est Float64
            #   Float64 est expérimental et ne fonctionne pas encore avec to_csv (format %.2f non respecté)
            #   (voir https://pandas.pydata.org/pandas-docs/dev/user_guide/missing_data.html#missing-data-na) ou https://pandas.pydata.org/pandas-docs/dev/whatsnew/v1.2.0.html?highlight=float64dtype
        )
        .round(2)
        .astype("float64")
    )

    seuil_france = population_agricole.at[ID_FRANCE, "evolution_part_population_agricole_pt_de_pourcent"]

    population_agricole.loc[:, "evolution_part_population_agricole_note"] = calculer_note_par_interpolation_2_seuils(
        population_agricole["evolution_part_population_agricole_pt_de_pourcent"],
        2 * seuil_france,
        0,
        seuil_france,
        5,
    )

    return population_agricole
