import unittest

import numpy as np
import pandas as pd

from crater.indicateurs.population_agricole.calculateur_population_agricole import (
    _ajouter_note,
)
from crater.indicateurs.population_agricole.evolution_part_population_agricole import (
    ajouter_colonne_evolution_part_population_agricole,
)
from crater.indicateurs.population_agricole.part_population_agricole_1988 import (
    ajouter_colonne_part_population_agricole_1988,
)
from crater.indicateurs.population_agricole.part_population_agricole_2010 import (
    ajouter_colonne_part_population_agricole_2010,
)
from crater.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas

augmenter_nombre_colonnes_affichees_pandas()


class TestUnitairePopulationAgricole(unittest.TestCase):
    def test_part_population_agricole_2010_pourcent(self):
        df = pd.DataFrame(
            columns=[
                "id_territoire",
                "categorie_territoire",
                "id_departement",
                "cheptel",
            ],
            data=[
                ["D-1", "DEPARTEMENT", np.nan, 100],
                ["D-2", "DEPARTEMENT", np.nan, 20],
                ["C-1", "COMMUNE", "D-1", 10],
                ["C-2", "COMMUNE", "D-1", np.nan],
                ["C-3", "COMMUNE", "D-1", np.nan],
                ["C-4", "COMMUNE", "D-2", np.nan],
            ],
        ).set_index("id_territoire")

        cheptels_par_departements = df[df["categorie_territoire"] == "DEPARTEMENT"].groupby("id_territoire")["cheptel"].transform("sum")
        cheptels_par_departements_par_sommes_donnees_communales = (
            df[~df["cheptel"].isnull() & (df["categorie_territoire"] == "COMMUNE")]
            .reset_index()
            .set_index("id_departement")
            .groupby("id_departement")["cheptel"]
            .transform("sum")
        )
        cheptels_a_repartir_par_departements = (
            (cheptels_par_departements - cheptels_par_departements_par_sommes_donnees_communales)
            .reset_index()
            .rename(columns={"index": "id_departement", "cheptel": "cheptel_a_repartir"})
        )

        mask_communes_sans_cheptel = df["cheptel"].isnull() & (df["categorie_territoire"] == "COMMUNE")
        df = (
            df.loc[mask_communes_sans_cheptel]
            .reset_index()
            .merge(cheptels_a_repartir_par_departements, on="id_departement", how="outer")
            .set_index("id_territoire")
        )

        nb_communes_sans_cheptels_par_departements = df[mask_communes_sans_cheptel].groupby("id_departement")["id_departement"].transform("count")
        df.loc[mask_communes_sans_cheptel, "cheptel_a_repartir"] = (
            df.loc[mask_communes_sans_cheptel, "cheptel_a_repartir"] / nb_communes_sans_cheptels_par_departements
        )

        # given
        df_population_agricole = pd.DataFrame(
            columns=[
                "id_territoire",
                "actifs_agricoles_permanents_2010",
                "population_totale_2010",
            ],
            data=[["P-FR", 12, 100], ["C-1", 6, 100]],
        ).set_index("id_territoire")
        resultat_attendu = pd.DataFrame(
            columns=[
                "part_population_agricole_2010_pourcent",
                "part_population_agricole_2010_note",
            ],
            data=[[12.0, 5], [6.0, 2.5]],
        )
        # when
        df_resultat = ajouter_colonne_part_population_agricole_2010(df_population_agricole)
        df_resultat = df_resultat.reset_index()
        # then
        pd.testing.assert_series_equal(
            df_resultat["part_population_agricole_2010_pourcent"],
            resultat_attendu["part_population_agricole_2010_pourcent"],
            check_names=False,
        )
        pd.testing.assert_series_equal(
            df_resultat["part_population_agricole_2010_note"],
            resultat_attendu["part_population_agricole_2010_note"],
            check_names=False,
        )

    def test_part_population_agricole_1988_pourcent(self):
        # given
        df_population_agricole = pd.DataFrame(
            columns=[
                "id_territoire",
                "actifs_agricoles_permanents_1988",
                "population_totale_1990",
            ],
            data=[["P-FR", 12, 100], ["C-1", 6, 100]],
        ).set_index("id_territoire")
        resultat_attendu = pd.DataFrame(
            columns=[
                "part_population_agricole_1988_pourcent",
                "part_population_agricole_1988_note",
            ],
            data=[[12.0, 5], [6.0, 2.5]],
        )
        # when
        df_resultat = ajouter_colonne_part_population_agricole_1988(df_population_agricole)
        df_resultat = df_resultat.reset_index()
        # then
        pd.testing.assert_series_equal(
            df_resultat["part_population_agricole_1988_pourcent"],
            resultat_attendu["part_population_agricole_1988_pourcent"],
            check_names=False,
        )
        pd.testing.assert_series_equal(
            df_resultat["part_population_agricole_1988_note"],
            resultat_attendu["part_population_agricole_1988_note"],
            check_names=False,
        )

    def test_evolution_part_population_agricole(self):
        # given
        df_population_agricole = pd.DataFrame(
            columns=[
                "id_territoire",
                "population_totale_2010",
                "population_totale_1990",
                "actifs_agricoles_permanents_2010",
                "actifs_agricoles_permanents_1988",
            ],
            data=[
                ["P-FR", 100, 100, 5, 10],
                ["C-1", 10, 10, 2, 1],
                ["C-2", 10, 10, 0, 2],
            ],
        ).set_index("id_territoire")
        resultat_attendu = pd.DataFrame(
            columns=[
                "evolution_part_population_agricole_pt_de_pourcent",
                "evolution_part_population_agricole_note",
            ],
            data=[[-5.0, 5.0], [10.0, 10.0], [-20.0, 0.0]],
        )
        # when
        df_resultat = ajouter_colonne_evolution_part_population_agricole(df_population_agricole)
        df_resultat = df_resultat.reset_index()
        # then
        pd.testing.assert_series_equal(
            df_resultat["evolution_part_population_agricole_pt_de_pourcent"],
            resultat_attendu["evolution_part_population_agricole_pt_de_pourcent"],
            check_names=False,
        )
        pd.testing.assert_series_equal(
            df_resultat["evolution_part_population_agricole_note"],
            resultat_attendu["evolution_part_population_agricole_note"],
            check_names=False,
        )

    def test_note(self):
        # given
        df_population_agricole = pd.DataFrame(
            columns=[
                "id_territoire",
                "part_population_agricole_2010_note",
                "evolution_part_population_agricole_note",
                "part_population_agricole_2010_pourcent",
                "evolution_part_population_agricole_pt_de_pourcent",
            ],
            data=[
                ["P-FR", 5, 5, 10.0, -5.0],
                ["C-1", np.nan, np.nan, np.nan, np.nan],
                ["C-2", 0, 0, 0.0, -15.0],
                ["C-3", 10, 10, 20.0, 10.0],
            ],
        ).set_index("id_territoire")
        resultat_attendu = pd.DataFrame(
            columns=["note"],
            data=[
                [5],
                [np.nan],
                [0],
                [10],
            ],
        ).astype({"note": "Int64"})
        # when
        df_resultat = _ajouter_note(df_population_agricole)
        df_resultat = df_resultat.reset_index()
        # then
        pd.testing.assert_series_equal(
            df_resultat["note"],
            resultat_attendu["note"],
            check_names=False,
        )


if __name__ == "__main__":
    unittest.main()
