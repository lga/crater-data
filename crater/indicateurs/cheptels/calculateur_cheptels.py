from pathlib import Path

import numpy as np
from pandas import DataFrame

from crater.indicateurs.cheptels import chargeur_cheptels_agreste
from crater.commun.calculs.outils_dataframes import update_colonne_depuis_series_incluant_NAs
from crater.commun.calculs.outils_territoires import (
    ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales,
    calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales,
)
from crater.commun.chargeurs.chargeur_df_indicateurs import charger_et_joindre_df_indicateurs
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires, reinitialiser_dossier
from crater.commun.logger import log
from crater.config.config_sources import ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010

UGB_PAR_TETE_BOVINS = 0.86
UGB_PAR_TETE_PORCINS = 0.26
UGB_PAR_TETE_OVINS = 0.15
UGB_PAR_TETE_CAPRINS = 0.24
UGB_PAR_TETE_VOLAILLES = 0.013


def calculer_cheptels(
    territoires: DataFrame, chemin_dossier_agreste_2010: Path, chemin_fichier_occupation_sols: Path, chemin_dossier_output: Path
) -> Path:
    log.info("##### CALCUL DES CHEPTELS #####")
    reinitialiser_dossier(chemin_dossier_output)

    territoires = charger_et_joindre_df_indicateurs(territoires.copy(), chemin_fichier_occupation_sols)

    cheptels = chargeur_cheptels_agreste.charger_cheptels_agreste(chemin_dossier_agreste_2010)
    noms_cheptels = cheptels.columns.to_list()[1:]

    df_cheptels = _ajouter_colonnes_cheptels(territoires, cheptels, noms_cheptels)
    df_cheptels = _completer_colonnes_cheptels(df_cheptels, noms_cheptels)
    df_cheptels = _ajouter_colonnes_ugb(df_cheptels)

    df_cheptels = df_cheptels.drop(columns=df_cheptels.loc[:, slice("id_pays", "sau_ra_2020_ha")].columns)

    return exporter_df_indicateurs_par_territoires(df_cheptels.reset_index(), chemin_dossier_output, "cheptels")


def _ajouter_colonnes_cheptels(territoires: DataFrame, cheptels: DataFrame, noms_cheptels: list[str]) -> DataFrame:
    cheptels = cheptels.set_index("id_territoire")
    territoires = territoires.join(cheptels)

    for nom_cheptel in noms_cheptels:
        territoires.loc[
            territoires.annee_dernier_mouvement_commune > ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010,
            nom_cheptel,
        ] = np.nan

        territoires = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            territoires=territoires,
            colonne_a_sommer=nom_cheptel,
            categorie_territoire_a_sommer="COMMUNE",
            categories_territoires_cibles=["EPCI", "REGROUPEMENT_COMMUNES"],
            compter_part_valeurs_na=True,
        )
    return territoires


def _completer_colonnes_cheptels(territoires: DataFrame, noms_cheptels: list[str]) -> DataFrame:
    cheptels_estimes = territoires.copy()

    for nom_cheptel in noms_cheptels:
        cheptel = territoires.loc[
            :,
            [
                "categorie_territoire",
                "id_pays",
                "id_region",
                "id_departement",
                "sau_ra_2020_ha",
                nom_cheptel,
            ],
        ]
        cheptel = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
            cheptel, nom_cheptel, "sau_ra_2020_ha", "REGION", "PAYS"
        )
        cheptel = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
            cheptel,
            nom_cheptel,
            "sau_ra_2020_ha",
            "DEPARTEMENT",
            "REGION",
        )
        cheptel = calculer_donnees_infraterritoriales_par_repartition_donnees_supraterritoriales(
            cheptel,
            nom_cheptel,
            "sau_ra_2020_ha",
            "COMMUNE",
            "DEPARTEMENT",
        )
        cheptels_estimes = update_colonne_depuis_series_incluant_NAs(cheptels_estimes, nom_cheptel, cheptel[nom_cheptel])
        cheptels_estimes = cheptels_estimes.join(cheptel[nom_cheptel + "_est_estime"])

        cheptels_estimes.loc[
            (cheptels_estimes[nom_cheptel + "_part_na_pourcent"] > 0)
            & cheptels_estimes["categorie_territoire"].isin(["EPCI", "REGROUPEMENT_COMMUNES"]),
            nom_cheptel + "_est_estime",
        ] = True
        cheptels_estimes = ajouter_donnees_supraterritoriales_par_somme_donnees_territoriales(
            territoires=cheptels_estimes,
            colonne_a_sommer=nom_cheptel,
            categorie_territoire_a_sommer="COMMUNE",
            categories_territoires_cibles=["EPCI", "REGROUPEMENT_COMMUNES"],
        )
        cheptels_estimes.drop(columns=nom_cheptel + "_part_na_pourcent", inplace=True)

    noms_cheptels_ordonnes = sorted(noms_cheptels, key=chargeur_cheptels_agreste.CHEPTELS.index)
    colonnes_cheptels_ordonnees: list[str] = sum([[i, i + "_est_estime"] for i in noms_cheptels_ordonnes], [])
    colonnes_hors_cheptels = cheptels_estimes.loc[:, slice("nom_territoire", "sau_ra_2020_ha")].columns
    cheptels_estimes = cheptels_estimes.reindex(columns=[*colonnes_hors_cheptels, *colonnes_cheptels_ordonnees])

    return cheptels_estimes


def _ajouter_colonnes_ugb(territoires: DataFrame) -> DataFrame:
    territoires = territoires.copy()

    territoires["ugb_bovins"] = territoires["nb_bovins"] * UGB_PAR_TETE_BOVINS
    territoires["ugb_porcins"] = territoires["nb_porcins"] * UGB_PAR_TETE_PORCINS
    territoires["ugb_ovins"] = territoires["nb_ovins"] * UGB_PAR_TETE_OVINS
    territoires["ugb_caprins"] = territoires["nb_caprins"] * UGB_PAR_TETE_CAPRINS
    territoires["ugb_volailles"] = territoires["nb_volailles"] * UGB_PAR_TETE_VOLAILLES
    territoires["ugb_total"] = (
        territoires["ugb_bovins"] + territoires["ugb_porcins"] + territoires["ugb_ovins"] + territoires["ugb_caprins"] + territoires["ugb_volailles"]
    )

    territoires["ugb_total_par_sau_ha"] = (territoires["ugb_total"] / territoires["sau_ha"]).replace([np.inf, -np.inf], np.nan)

    return territoires
