from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.chargeurs.chargeur_agreste import charger_agreste, filtrer_et_ajouter_colonne_id_territoire

DICTIONNAIRE_CHEPTELS = {
    "nb_animaux": "Elevages (total hors apiculture)(1)",
    "nb_herbivores": "Herbivores(1)",
    "nb_granivores": "Granivores(1)",
    "nb_bovins": "Total Bovins",
    "nb_bovins_vaches_laitieres": "Vaches laitières",
    "nb_bovins_vaches_allaitantes": "Vaches allaitantes",
    "nb_equides": "Total Equidés",
    "nb_caprins": "Total Caprins",
    "nb_caprins_chevres": "Chèvres",
    "nb_ovins": "Total Ovins",
    "nb_ovins_brebis_laitieres": "Brebis laitières",
    "nb_ovins_brebis_nourrices": "Brebis nourrices",
    "nb_porcins": "Total Porcins",
    "nb_porcins_truies_reproductrices_50kg_ou_plus_yc_cochettes": "Truies reproductrices de 50 kg ou plus",
    "nb_volailles": "Volailles",
    "nb_lapines_meres": "Lapines-mères",
}

CHEPTELS = list(DICTIONNAIRE_CHEPTELS.keys())


def charger_cheptels_agreste(chemin_dossier):
    df = (
        charger_agreste({"code": "G_2141", "annee": "2010", "dossier": chemin_dossier})
        .rename(
            columns={
                "G_2141_LIB_DIM1": "taille_exploitation",
                "G_2141_LIB_DIM2": "cheptel",
                "G_2141_LIB_DIM3": "indicateur",
                "VALEUR": "valeur",
                "QUALITE": "qualite",
            },
            errors="raise",
        )
        .query('taille_exploitation == "Ensemble des exploitations (hors pacages collectifs)"' + ' & indicateur == "Cheptel correspondant (têtes)"')
        .reindex(
            columns=[
                "FRANCE",
                "FRDOM",
                "REGION",
                "DEP",
                "COM",
                "taille_exploitation",
                "cheptel",
                "indicateur",
                "valeur",
                "qualite",
            ]
        )
        .astype({"valeur": "Int64"})
    )

    df = filtrer_et_ajouter_colonne_id_territoire(df)
    df = df.pivot(index="id_territoire", columns=["indicateur", "cheptel"], values="valeur").reset_index()
    df.columns = df.columns.droplevel(0)
    df.rename(columns={"": "id_territoire"}, inplace=True)
    for i in DICTIONNAIRE_CHEPTELS:
        df.rename(columns={DICTIONNAIRE_CHEPTELS[i]: i}, inplace=True)
    verifier_absence_doublons(df, "id_territoire")

    return df
