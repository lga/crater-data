import unittest
from pathlib import Path

from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.indicateurs.cheptels.calculateur_cheptels import calculer_cheptels
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestCheptels(unittest.TestCase):
    def test_calculer_cheptels(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "territoires" / "referentiel_territoires.csv")
        # when
        calculer_cheptels(
            territoires,
            CHEMIN_INPUT_DATA,
            CHEMIN_INPUT_DATA / "occupation_sols.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "cheptels.csv",
            CHEMIN_OUTPUT_DATA / "cheptels.csv",
        )


if __name__ == "__main__":
    unittest.main()
