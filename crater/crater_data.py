import argparse
from datetime import datetime

from crater.collecteurs.hubeau.collecteur_prelevements_eau import (
    collecter_donnees_prelevements_eau,
)
from crater.collecteurs.parcel import collecteur_besoins_api_parcel
from crater.collecteurs.parcel.collecteur_besoins_api_parcel import (
    ID_ASSIETTE_ACTUELLE,
    ID_ASSIETTE_MOINS_50_POURCENT,
)
from crater.collecteurs.parcel.generateur_correspondance_id_territoires import (
    generer_fichier_correspondance_territoires_parcel_crater,
)
from crater.collecteurs.rpg import collecteur_rpg
from crater.collecteurs.vigieau.collecteur_vigieau import collecter_donnees_vigieau
from crater.commun import environnement
from crater.config.config_sources import (
    URL_SOURCE_RPG,
    NOMS_FICHIERS_RPG,
    CHEMIN_FICHIER_CORRESPONDANCE_TERRITOIRES_PARCEL_CRATER,
    CHEMIN_SOURCE_HUBEAU,
    CHEMIN_SOURCE_RPG,
    CHEMIN_SOURCE_PARCEL,
    CHEMIN_SOURCE_VIGIEAU_ANNEE_COURANTE,
    CHEMIN_SOURCE_VIGIEAU_OUTPUT_COLLECTEUR,
)
from crater.commun.logger import log
from crater.dagster.assets_territoires import CHEMIN_RESULTAT_REFERENTIEL_TERRITOIRES

# Quand ce script est exécuté directement, on est en environnement de PROD et pas TEST
# Cela permet d'activer la génération des diagrammes, cartes,...
environnement.ENVIRONNEMENT_EXECUTION = "PROD"


def lancer_telechargement_parcel_id_territoires():
    generer_fichier_correspondance_territoires_parcel_crater(
        CHEMIN_SOURCE_PARCEL / "communes-parcel-juin-2020.csv",
        CHEMIN_RESULTAT_REFERENTIEL_TERRITOIRES / "referentiel_territoires.csv",
        datetime.today(),
        CHEMIN_SOURCE_PARCEL / "output_collecteur" / "territoires_a_valider",
    )


def lancer_telechargement_parcel_besoins_assiette_actuelle():
    collecteur_besoins_api_parcel.collecter_donnees_besoins_parcel(
        ID_ASSIETTE_ACTUELLE,
        # FIXME : ne fonctionne pas avec la dernière version du fichier correspondance fourni par Mathieu
        # mais devrait fonctionner si on utilise correspondance_territoires_parcel_crater_VALIDE.csv)
        # il faudrait investiguer côté PARCEL + faire évoluer le collecteur
        CHEMIN_FICHIER_CORRESPONDANCE_TERRITOIRES_PARCEL_CRATER,
        datetime.today(),
        CHEMIN_SOURCE_PARCEL / "output_collecteur" / "besoins",
    )


def lancer_telechargement_parcel_besoins_assiette_moins_50p():
    collecteur_besoins_api_parcel.collecter_donnees_besoins_parcel(
        ID_ASSIETTE_MOINS_50_POURCENT,
        # FIXME : ne fonctionne pas avec la dernière version du fichier correspondance fourni par Mathieu
        # mais devrait fonctionner si on utilise correspondance_territoires_parcel_crater_VALIDE.csv)
        # il faudrait investiguer côté PARCEL + faire évoluer le collecteur
        CHEMIN_FICHIER_CORRESPONDANCE_TERRITOIRES_PARCEL_CRATER,
        datetime.today(),
        CHEMIN_SOURCE_PARCEL / "output_collecteur" / "besoins",
    )


def lancer_telechargement_rpg():
    collecteur_rpg.telecharger_rpg_toutes_les_regions(
        URL_SOURCE_RPG,
        NOMS_FICHIERS_RPG,
        2017,
        CHEMIN_SOURCE_RPG,
    )


def lancer_telechargement_prelevements_eau(annee: int) -> None:
    collecter_donnees_prelevements_eau(
        annee,
        CHEMIN_SOURCE_HUBEAU,
        datetime.today(),
    )


def lancer_transformation_fichier_vigieau() -> None:
    collecter_donnees_vigieau(
        CHEMIN_SOURCE_VIGIEAU_ANNEE_COURANTE / "historique-communes-20250305.zip",
        [2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024],
        CHEMIN_SOURCE_VIGIEAU_OUTPUT_COLLECTEUR,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        usage="%(prog)s [OPTIONS] ETAPE_CALCUL\n"
        + "Permet de lancer certains traitements de crater-data\n"
        + "Valeurs possible pour ETAPE_CALCUL :\n"
        + "\ttelecharger-parcel-id-territoires\t\tgénérer le fichier des correspondances d'id de territoires entre PARCEL et CRATer\n"
        + (
            "\ttelecharger-parcel-besoins-assiette-actuelle\tgénérer le fichier des besoins en SAU à partir de l'api PARCEL, "
            "pour la consommation actuelle\n"
        )
        + (
            "\ttelecharger-parcel-besoins-assiette-moins-50p\tgénérer le fichier des besoins en SAU à partir de l'api PARCEL, "
            "pour une consommation avec -50 pour cent de protéines animales\n"
        )
        + "\ttelecharger-rpg\t\t\t\t\ttélécharger les fichiers régionals du RPG\n"
        + "\ttelecharger-prelevements-eau\t\t\ttélécharger les fichiers de prélèvement d'eau pour l'irrigation\n"
        + "\ttraiter-fichier-vigieau\t\t\ttransformer le fichier zip/json vigieau en csv\n"
        + "\n"
        + "Toutes les autres étapes de calcul sont prises en charge par dagster, à lancer de la manière suivante :\n"
        + "\t'./web-dagster.sh' : lancer l'interface web dagster\n"
        + "\t'./run-dagster.sh' : lancer dagster en ligne de commande\n"
    )
    parser.add_argument(
        "ETAPE_CALCUL",
        help="Nom de l'étape à exécuter",
        choices=[
            "telecharger-parcel-id-territoires",
            "telecharger-parcel-besoins-assiette-actuelle",
            "telecharger-parcel-besoins-assiette-moins-50p",
            "telecharger-rpg",
            "telecharger-prelevements-eau",
            "traiter-fichier-vigieau",
        ],
    )

    parser.add_argument("-a", "--annee", action="store", default=None, type=int)
    args = parser.parse_args()

    annee = args.annee

    if args.ETAPE_CALCUL == "telecharger-parcel-id-territoires":
        lancer_telechargement_parcel_id_territoires()
    elif args.ETAPE_CALCUL == "telecharger-parcel-besoins-assiette-actuelle":
        lancer_telechargement_parcel_besoins_assiette_actuelle()
    elif args.ETAPE_CALCUL == "telecharger-parcel-besoins-assiette-moins-50p":
        lancer_telechargement_parcel_besoins_assiette_moins_50p()
    elif args.ETAPE_CALCUL == "telecharger-rpg":
        lancer_telechargement_rpg()
    elif args.ETAPE_CALCUL == "telecharger-prelevements-eau":
        if annee is None:
            log.error("Erreur : le paramètre --annee doit être spécifiée dans la ligne de commande pour cette étape")
            exit(1)
        lancer_telechargement_prelevements_eau(annee)
    elif args.ETAPE_CALCUL == "traiter-fichier-vigieau":
        lancer_transformation_fichier_vigieau()
    else:
        parser.print_help()
