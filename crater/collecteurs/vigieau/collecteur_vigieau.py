import os
from pathlib import Path

# pas de stub mypy trouvé pour ijson
import ijson  # type: ignore
import zipfile
import csv


from crater.commun.logger import log


def collecter_donnees_vigieau(
    chemin_fichier_source_vigieau: Path,
    annees: list[int],
    chemin_dossier_output: Path,
) -> None:
    log.info("Collecte des données Vigieau")

    for annee in annees:
        log.info(f"  - extraction des données pour l'année {annee}")
        chemin_fichier_csv_annee_n = collecter_donnees_vigieau_pour_une_annee(
            chemin_fichier_source_vigieau,
            annee,
            chemin_dossier_output,
        )
        log.info(f"  - zip du fichier {chemin_fichier_csv_annee_n}")
        with zipfile.ZipFile(chemin_fichier_csv_annee_n.with_suffix(".zip"), "w", compression=zipfile.ZIP_DEFLATED) as zf:
            zf.write(chemin_fichier_csv_annee_n, arcname=chemin_fichier_csv_annee_n.name)
        os.remove(chemin_fichier_csv_annee_n)


def collecter_donnees_vigieau_pour_une_annee(
    chemin_fichier_source_vigieau: Path,
    annee: int,
    chemin_dossier_output: Path,
) -> Path:

    annee_str = str(annee)

    with zipfile.ZipFile(chemin_fichier_source_vigieau, "r") as fichier_zip:
        if len(fichier_zip.namelist()) != 1:
            raise ValueError(f"Le fichier {chemin_fichier_source_vigieau} doit être un fichier zip, contenant un seul fichier json.")
        else:
            nom_fichier_source_dans_zip = fichier_zip.namelist()[0]

    chemin_fichier_csv_resultat = chemin_dossier_output / f"{chemin_fichier_source_vigieau.stem}_annee_{annee_str}.csv"

    log.info(f"  - transformation de {chemin_fichier_source_vigieau} en {chemin_fichier_csv_resultat}")

    with zipfile.ZipFile(chemin_fichier_source_vigieau, "r") as fichier_zip:
        with fichier_zip.open(nom_fichier_source_dans_zip) as fichier_json_dans_zip, open(
            chemin_fichier_csv_resultat, "w", newline="", encoding="utf-8"
        ) as fichier_csv:
            writer = csv.writer(fichier_csv, delimiter=";")
            writer.writerow(["code_insee_commune", "date_restriction", "restriction_AEP", "restriction_SOU", "restriction_SUP"])

            parser = ijson.items(fichier_json_dans_zip, "item")
            nb_communes_traitees = 0
            for item in parser:
                commune = item.get("commune", {})
                code_insee_commune = commune.get("code", "")

                nb_communes_traitees += 1
                if nb_communes_traitees % 1000 == 0:
                    log.info(f"  - nb communes traitees={nb_communes_traitees}")

                for restriction in item.get("restrictions", []):
                    if restriction.get("date", "").startswith(annee_str):
                        writer.writerow(
                            [
                                code_insee_commune,
                                restriction.get("date", ""),
                                restriction.get("AEP", ""),
                                restriction.get("SOU", ""),
                                restriction.get("SUP", ""),
                            ]
                        )

    log.info(f"  -  création du fichier {chemin_fichier_csv_resultat} terminée, {nb_communes_traitees} communes ajoutées")
    return chemin_fichier_csv_resultat
