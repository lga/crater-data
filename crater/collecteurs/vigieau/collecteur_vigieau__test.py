import unittest
from pathlib import Path

from crater.collecteurs.vigieau.collecteur_vigieau import collecter_donnees_vigieau_pour_une_annee, collecter_donnees_vigieau
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestCollecteurVigieau(unittest.TestCase):

    def test_collecter_donnees_vigieau_pour_une_annee(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        # when
        collecter_donnees_vigieau_pour_une_annee(
            CHEMIN_INPUT_DATA / "historique-vigieau-recupere-le-01-01-2015.zip",
            2013,
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "historique-vigieau-recupere-le-01-01-2015_annee_2013.csv",
            CHEMIN_OUTPUT_DATA / "historique-vigieau-recupere-le-01-01-2015_annee_2013.csv",
        )

    def test_collecter_donnees_vigieau(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        # when
        collecter_donnees_vigieau(
            CHEMIN_INPUT_DATA / "historique-vigieau-recupere-le-01-01-2015.zip",
            [2013, 2014],
            CHEMIN_OUTPUT_DATA,
        )
        # then
        self.assertTrue(
            Path(CHEMIN_OUTPUT_DATA / "historique-vigieau-recupere-le-01-01-2015_annee_2013.zip").is_file(), "Le fichier zip n'a pas été créé"
        )
        self.assertTrue(Path(CHEMIN_OUTPUT_DATA / "historique-vigieau-recupere-le-01-01-2015_annee_2014.zip").is_file())


if __name__ == "__main__":
    unittest.main()
