import unittest
from datetime import datetime
from pathlib import Path

from crater.collecteurs.hubeau.collecteur_prelevements_eau import collecter_donnees_prelevements_eau
from crater.commun.outils_pour_tests import assert_csv_files_are_equals

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestHubEau(unittest.TestCase):
    @unittest.skip(
        "Collecte des données de prélèvement en eau depuis l'api HUBEAU. "
        "Test desactivé par défaut, qui permet de vérifier si l'api est toujours compatible"
    )
    def test_collecter_source_prelevements_eau(self):
        collecter_donnees_prelevements_eau(
            2020,
            CHEMIN_OUTPUT_DATA,
            datetime(2022, 1, 1),
            nb_items_par_page=3,
            nb_max_pages=2,
        )

        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "2020" / "prelevements_eau_par_communes_20220101.csv",
            CHEMIN_OUTPUT_DATA / "2020" / "prelevements_eau_par_communes_20220101.csv",
            skiprows=6,
        )


if __name__ == "__main__":
    unittest.main()
