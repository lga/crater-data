import unittest
from pathlib import Path

from crater.collecteurs.rpg import collecteur_rpg
from crater.commun.outils_pour_tests import augmenter_nombre_colonnes_affichees_pandas
from crater.config.config_sources import URL_SOURCE_RPG

augmenter_nombre_colonnes_affichees_pandas()

NOMS_FICHIERS_RPG = {2017: {"R-1": "RPG_2-0__SHP_UTM20W84GUAD_R01-2017_2017-01-01.7z.001"}}
CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")


class TestRPG(unittest.TestCase):
    @unittest.skip("Mecanisme de collecte du RPG sur le site IGN peu fiable et exécuté ponctuellement donc on ne l'éxécute pas automatiquement")
    def test_telecharger_rpg_regions(self):
        annee = 2017
        collecteur_rpg.telecharger_rpg_toutes_les_regions(URL_SOURCE_RPG, NOMS_FICHIERS_RPG, annee, CHEMIN_OUTPUT_DATA)
        for code_region in NOMS_FICHIERS_RPG[annee]:
            fichier_est_telecharge = (CHEMIN_OUTPUT_DATA / str(annee) / NOMS_FICHIERS_RPG[annee][code_region]).is_file()
            self.assertTrue(fichier_est_telecharge)


if __name__ == "__main__":
    unittest.main()
