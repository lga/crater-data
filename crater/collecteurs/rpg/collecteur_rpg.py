import urllib.error
import urllib.request
from pathlib import Path
from typing import TypeAlias

from crater.commun.logger import log

# Type alias pour NOMS_FICHIERS_RPG: [année -> [id -> nom de fichier]]
DictFichiersRpg: TypeAlias = dict[int, dict[str, str]]


def telecharger_rpg_toutes_les_regions(url_ign: str, noms_fichiers: DictFichiersRpg, annee: int, dossier_rpg: Path) -> None:
    log.info(f"Téléchargement du RPG de {annee}")

    for nom_fichier in _liste_fichiers_manquant(dossier_rpg, annee, noms_fichiers):
        telecharger_rpg_region(url_ign, nom_fichier, dossier_rpg / str(annee))

    if len(_liste_fichiers_manquant(dossier_rpg, annee, noms_fichiers)) != 0:
        raise ValueError("ERREUR: tous les fichiers RPG de l'année n'ont pas été téléchargés !")
    else:
        log.info("   Tous les fichiers sont présents !")


def telecharger_rpg_region(url_ign: str, nom_fichier: str, dossier_telechargement: Path) -> None:
    log.info("Téléchargement du fichier " + nom_fichier)

    url = str(f"{url_ign}{nom_fichier}")
    dossier_telechargement.mkdir(parents=True, exist_ok=True)
    chemin_fichier_output = dossier_telechargement / nom_fichier
    fichier = urllib.request.urlopen(url).read()
    with open(str(chemin_fichier_output), "wb") as f:
        f.write(fichier)


def _liste_fichiers_manquant(dossier_rpg: Path, annee: int, noms_fichiers: DictFichiersRpg) -> list[str]:
    return [f for f in noms_fichiers[annee].values() if not Path(dossier_rpg / str(annee) / f).is_file()]
