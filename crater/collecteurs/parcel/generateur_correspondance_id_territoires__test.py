import unittest
from datetime import datetime
from pathlib import Path

from crater.collecteurs.parcel.generateur_correspondance_id_territoires import generer_fichier_correspondance_territoires_parcel_crater
from crater.commun.outils_pour_tests import assert_csv_files_are_equals
from crater.commun.outils_pour_tests import (
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data_generateur"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestApiParcelTerritoiresAValider(unittest.TestCase):
    @unittest.skip(
        "Mecanisme de génération du fichier de correspondance des ids territoires parcel crater, pas utilisé en continu "
        "car fait appel à l'api Parcel et nécessite une rectification manuelle "
        "(correspondance sur le nom pas suffisament fiable pour etre automatisée)"
    )
    def test_generer_fichier_correspondance_id_territoires_parcel_crater(self):
        generer_fichier_correspondance_territoires_parcel_crater(
            CHEMIN_INPUT_DATA / "parcel_communes.csv",
            CHEMIN_INPUT_DATA / "referentiel_territoires.csv",
            datetime(2022, 1, 1),
            CHEMIN_OUTPUT_DATA,
        )

        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "correspondance_territoires_parcel_crater_20220101_A_VALIDER.csv",
            CHEMIN_OUTPUT_DATA / "correspondance_territoires_parcel_crater_20220101_A_VALIDER.csv",
        )


if __name__ == "__main__":
    unittest.main()
