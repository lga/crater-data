import requests
from pandas import DataFrame
from pandas import Series
from unidecode import unidecode

from crater.collecteurs.parcel.collecteur_besoins_api_parcel import URL_BASE_API_PARCEL
from crater.commun.logger import log


def collecter_territoire_par_nom(df_territoires: DataFrame):
    log.info(f"   => collecte des IDs via l'API PARCEL pour {df_territoires.shape[0]} territoires")
    df_territoires = df_territoires.copy().reset_index(drop=True)
    df_territoires["nom_territoire"] = _normaliser_chaine_caractere(df_territoires["nom_territoire"])

    df_territoires = df_territoires.apply(_ajouter_donnees_territoire_parcel, axis=1)
    return df_territoires


def _normaliser_chaine_caractere(serie: Series) -> Series:
    return (
        serie.replace(to_replace="\(.*\)", value="", regex=True)
        .replace(to_replace="-|,|/", value=" ", regex=True)
        .replace(to_replace="\s\s+", value=" ", regex=True)
        .apply(unidecode)
        .str.upper()
        .str.strip()
    )


def _ajouter_donnees_territoire_parcel(ligne_territoire):
    critere_recherche = ligne_territoire["nom_territoire"]
    if ligne_territoire["categorie_territoire"] == "PAYS":
        critere_recherche = "France métropolitaine"
    if ligne_territoire["categorie_territoire"] == "REGION":
        critere_recherche = critere_recherche + " (Region)"
    if ligne_territoire["categorie_territoire"] == "DEPARTEMENT":
        critere_recherche = critere_recherche + " ("
    if ligne_territoire["categorie_territoire"] == "EPCI":
        critere_recherche = "EPCI/" + critere_recherche

    (
        id_territoire_parcel,
        nom_territoire_parcel,
        pourcentage_confiance_resultat,
        code_retour,
        autres_resultats,
    ) = _requeter_id_territoire_parcel(critere_recherche, ligne_territoire.name)

    ligne_territoire["id_territoire_parcel"] = id_territoire_parcel
    ligne_territoire["nom_territoire_parcel"] = nom_territoire_parcel
    ligne_territoire["pourcentage_confiance_resultat"] = pourcentage_confiance_resultat
    ligne_territoire["code_retour"] = code_retour
    ligne_territoire["autres_resultats"] = autres_resultats
    return ligne_territoire


def _requeter_id_territoire_parcel(nom_territoire_crater, numero_ligne_courante):
    if str(numero_ligne_courante).endswith("0"):
        log.info("Requete PARCEL numéro %s", str(numero_ligne_courante))

    r = requests.get(f"{URL_BASE_API_PARCEL}/GeoData/{nom_territoire_crater}")

    # Re tenter jusqu'à 3 fois
    if r.status_code not in range(200, 299):
        r = requests.get(f"{URL_BASE_API_PARCEL}/GeoData/{nom_territoire_crater}")

    if r.status_code not in range(200, 299):
        r = requests.get(f"{URL_BASE_API_PARCEL}/GeoData/{nom_territoire_crater}")

    if r.status_code not in range(200, 299):
        log.info(
            "ERREUR : Parcel a retourné une erreur : requete numéro %s, code retour= %s, territoire=%s, details=%s",
            numero_ligne_courante,
            r.status_code,
            nom_territoire_crater,
            r.content,
        )
        return "ERREUR", "Code retour <> 2xx", 0, r.status_code
    try:
        results = r.json()
    except Exception:
        log.info(
            "ERREUR : Impossible de lire la réponse Parcel : requete numéro %s, code retour= %s, territoire=%s, details=%s",
            numero_ligne_courante,
            r.status_code,
            nom_territoire_crater,
            r.content,
        )
        return "ERREUR", "Impossible de lire les données", 0, 1000

    if not results:
        return (
            "",
            "Pas de territoire parcel correspondant",
            0,
            "PAS_DE_CORRESPONDANCE",
            "",
        )
    id_territoire_parcel = results[0]["localeKey"]
    nom_territoire_parcel = results[0]["localeName"]
    pourcentage_confiance_resultat = 100 / len(results)
    code_retour = "OK_API" if (pourcentage_confiance_resultat == 100) else "A_VERIFIER"
    autres_resultats = ", ".join([f"{r['localeName']}({r['localeKey']})" for r in results])
    return (
        id_territoire_parcel,
        nom_territoire_parcel,
        pourcentage_confiance_resultat,
        code_retour,
        autres_resultats,
    )
