import unittest
from datetime import datetime
from pathlib import Path

from crater.collecteurs.parcel import collecteur_besoins_api_parcel, config
from crater.collecteurs.parcel.collecteur_besoins_api_parcel import ID_ASSIETTE_ACTUELLE
from crater.commun.outils_pour_tests import assert_csv_files_are_equals
from crater.commun.outils_pour_tests import (
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data_collecteur"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestApiParcelBesoins(unittest.TestCase):
    @unittest.skip("Ce test dépend des données PARCEL - permet de voir si elles ont changé ou pas. Pas lancé de " "manière automatique")
    def test_collecter_reponse_api_parcel_besoins_sau_par_produits(self):
        # given
        config.FICHIER_CONFIG_PRODUITS_ET_CATEGORIES_PARCEL = CHEMIN_INPUT_DATA / "parcel_produits_et_categories_202008.csv"
        # when
        collecteur_besoins_api_parcel.collecter_donnees_besoins_parcel(
            ID_ASSIETTE_ACTUELLE,
            CHEMIN_INPUT_DATA / "correspondance_territoires_parcel_crater_VALIDE.csv",
            datetime(2022, 1, 1),
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            expected_file_path=CHEMIN_EXPECTED_DATA / "reponse_api_parcel_besoins_sau_par_produits_assiette_actuelle_20220101.csv",
            result_file_path=CHEMIN_OUTPUT_DATA / "reponse_api_parcel_besoins_sau_par_produits_assiette_actuelle_20220101.csv",
            colonnes_a_exclure=["date_import"],
        )


if __name__ == "__main__":
    unittest.main()
