from datetime import datetime
from io import StringIO
from pathlib import Path

import pandas
import requests

from crater.collecteurs.parcel import config
from crater.commun.calculs.outils_dataframes import merge_strict
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.logger import log

URL_BASE_API_PARCEL = "http://convertisseur.azurewebsites.net/API"
ID_ASSIETTE_ACTUELLE = "0"
ID_ASSIETTE_MOINS_25_POURCENT = "1"
ID_ASSIETTE_MOINS_50_POURCENT = "2"


def collecter_donnees_besoins_parcel(
    id_assiette: str,
    fichier_territoires_parcel: Path,
    date_extraction: datetime,
    dossier_resultat: Path,
) -> None:
    log.info("Collecte des besoins via l'API PARCEL pour les territoires CRATer")

    reinitialiser_dossier(dossier_resultat)

    df_territoires_crater = pandas.read_csv(fichier_territoires_parcel, sep=";", dtype=object)
    log.info(f"   => {df_territoires_crater.shape[0]} territoires dans le referentiel CRATer")

    df_territoires_connus_de_parcel = df_territoires_crater.loc[
        ~df_territoires_crater["id_territoire_parcel"].isna(),
        [
            "id_territoire",
            "id_territoire_parcel",
        ],
    ]
    log.info(f"   => {df_territoires_connus_de_parcel.shape[0]} territoires ont un ID PARCEL")

    resultat = pandas.DataFrame()
    for index, row in df_territoires_connus_de_parcel.iterrows():
        if str(index).endswith("0"):
            log.info(f"Récupération données surface via API PARCEL, territoire numéro {str(index)} / {df_territoires_connus_de_parcel.shape[0]}")

        parcel_produits = _recuperer_donnees_surfaces_parcel(row.id_territoire_parcel, id_assiette)
        parcel_produits.insert(0, "id_territoire_crater", row.id_territoire)
        resultat = pandas.concat([resultat, parcel_produits])

    log.info(
        f"  => FIN de l'import des données, bilan des retours "
        f"http = \n{str(resultat.groupby('id_territoire_crater').first()['resultat_import'].value_counts())}"
    )

    nom_assiette = (
        "assiette_moins_25p"
        if (id_assiette == ID_ASSIETTE_MOINS_25_POURCENT)
        else "assiette_moins_50p" if (id_assiette == ID_ASSIETTE_MOINS_50_POURCENT) else "assiette_actuelle"
    )

    fichier_resultat = dossier_resultat / f"reponse_api_parcel_besoins_sau_par_produits_{nom_assiette}_{date_extraction.strftime('%Y%m%d')}.csv"

    resultat.to_csv(fichier_resultat, sep=";", index=False)


def _recuperer_donnees_surfaces_parcel(id_territoire_parcel, id_assiette):
    parcel_produits = pandas.read_csv(config.FICHIER_CONFIG_PRODUITS_ET_CATEGORIES_PARCEL)
    parcel_produits.insert(0, "id_territoire_parcel", id_territoire_parcel)
    parcel_produits = parcel_produits.drop(columns=["nom_produit_parcel", "nom_categorie_parcel"])
    try:
        r = requests.get(f"{URL_BASE_API_PARCEL}/LandArea/GetAllLandAreabyCategory?GeoLocale=[{id_territoire_parcel}]")

        parcel_category_results = pandas.read_json(StringIO(r.content.decode("utf-8"))[["ID", "Curseur_Min_Bio"]])
        r = requests.get(
            f"{URL_BASE_API_PARCEL}/LandArea/GetAllLandAreabyProduct?GeoLocale=[{id_territoire_parcel}]&ID_Category_CDTL=99&ID_Assiette={id_assiette}"
        )
        parcel_product_results = pandas.read_json(StringIO(r.content.decode("utf-8"))[["ID", "SurfaceArea", "SurfaceAreaBio"]])
        parcel_produits = merge_strict(
            parcel_produits,
            parcel_product_results,
            how="left",
            left_on="id_produit_parcel",
            right_on="ID",
        )
        parcel_produits = parcel_produits.drop(columns=["ID"])

        parcel_produits = merge_strict(
            parcel_produits,
            parcel_category_results,
            how="left",
            left_on="id_categorie_parcel",
            right_on="ID",
        )
        parcel_produits = parcel_produits.drop(columns=["ID"])

        parcel_produits["resultat_import"] = str(r.status_code) + " " + r.reason
        parcel_produits["date_import"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    except Exception:
        log.info(
            "ERREUR : Erreur lors de la collecte des données de surface Parcel pour le territoire %s",
            id_territoire_parcel,
        )
        parcel_produits["resultat_import"] = "KO"
        parcel_produits["date_import"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    return parcel_produits
