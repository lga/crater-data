# Chemin vers config, peut être redéfinie par les tests pour utiliser une config simplifiée
from pathlib import Path

FICHIER_CONFIG_PRODUITS_ET_CATEGORIES_PARCEL: Path = Path(__file__).parent / "parcel_produits_et_categories_202008.csv"
