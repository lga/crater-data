#!/bin/bash

BOUNDING_BOX_FRANCE=-5.4,39.2,10.1,52.2
DATE=$(date +%Y%m%d)
MAX_ZOOM=13 # voir https://wiki.openstreetmap.org/wiki/FR:Zoom_levels
CARTE_SOURCE=https://build.protomaps.com/${DATE}.pmtiles
CARTE_CIBLE=../../../../crater-data-sources/protomaps/france-${DATE}.pmtiles

echo "### Génération du fond de carte"
echo "      => Téléchargement et extraction de la carte depuis ${CARTE_SOURCE} dans ${CARTE_CIBLE}.pmtiles"
echo "      => Commande : pmtiles extract ${CARTE_SOURCE} ${CARTE_CIBLE} --bbox=${BOUNDING_BOX_FRANCE} --maxzoom=${MAX_ZOOM}"

pmtiles extract ${CARTE_SOURCE} ${CARTE_CIBLE} --bbox=${BOUNDING_BOX_FRANCE} --maxzoom=${MAX_ZOOM}
