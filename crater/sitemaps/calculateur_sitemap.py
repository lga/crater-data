from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.logger import log


def generer_sitemaps(territoires, chemin_dossier_output) -> None:
    log.info("##### EXPORT DES SITEMAPS #####")

    reinitialiser_dossier(chemin_dossier_output)

    mask_territoires = territoires["categorie_territoire"].isin(["PAYS", "REGION", "DEPARTEMENT", "EPCI"]) | (
        (territoires["categorie_territoire"] == "REGROUPEMENT_COMMUNES")
        & (~territoires["categorie_regroupement_communes"].isin(["BASSIN_DE_VIE_2012", "PAYS_PETR"]))
    )
    territoires_sitemap = territoires.loc[mask_territoires, :]

    url_territoires = []
    for t in territoires_sitemap["id_nom_territoire"]:
        url_territoires.append(
            "".join(
                [
                    "\n\t<url>",
                    f"\n\t\t<loc>https://crater.resiliencealimentaire.org/diagnostic/{t}</loc>",
                    "\n\t</url>",
                ]
            )
        )
    fichier = open(chemin_dossier_output / "sitemap-diagnostics-territoires.xml", "w")
    fichier.writelines(
        [
            '<?xml version="1.0" encoding="UTF-8"?>',
            '\n<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">',
            "".join(url_territoires),
            "\n</urlset>",
        ]
    )
    fichier.close()
