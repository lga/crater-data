import pandas

# Forcer pandas à lever une exception si on fait une affectation sur une copie d'un dataframe
pandas.set_option("mode.chained_assignment", "raise")
