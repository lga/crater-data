import os
import sys
from pathlib import Path
from pandas import DataFrame
import psutil
from dagster import asset

from crater.config.config_resultats import CHEMIN_RESULTAT_DONNEES_SOURCES
from crater.donnees_sources.agence_bio_surfaces.calcul_agence_bio_surfaces import calculer_donnees_agence_bio_surfaces
from crater.donnees_sources.clc.calcul_clc import calculer_donnees_clc
from crater.donnees_sources.hubeau_irrigation.calcul_hubeau_irrigation import calculer_donnees_hubeau_irrigation, ResultatsDonneesHubeauIrrigation
from crater.donnees_sources.surface_agricole_utile import calculateur_surface_agricole_utile

from crater.config.config_globale import ANNEE_REFERENTIEL_TERRITOIRES_INSEE
from crater.config.config_sources import (
    CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
    NOMS_FICHIERS_RPG,
    CHEMIN_SOURCE_OCCUPATION_SOLS_CLC,
    CHEMIN_SOURCE_MOUVEMENTS_COMMUNES,
    ANNEE_SOURCE_OCCUPATION_SOLS_CLC,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_OCCUPATION_SOLS_CLC,
    CHEMIN_SOURCE_RPG,
    CHEMIN_SOURCE_BIO,
    ANNEE_SOURCE_BIO,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_BIO,
    CHEMINS_SOURCES_PRELEVEMENTS_EAU_IRRIGATION,
    ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_PRELEVEMENTS_EAU_IRRIGATION,
)
from crater.commun.logger import log

NOM_GROUPE_ASSETS = "donnees_sources"


@asset(group_name=NOM_GROUPE_ASSETS, tags={"est_inactif_par_defaut": "true"})
def surface_agricole_utile(referentiel_territoires: DataFrame) -> Path:
    """Ensemble des fichiers de surface agricole utile par commune et par culture
    Calcul fait région par région (1 fichier résultat par région)
    """

    # Nécessaire pour pouvoir utiliser multiprocess.Pool avec dagster dans calculateur_surface_agricole_utile.py
    # sinon on a une erreur ModuleNotFoundError sur le module crater
    # on est obligé de retrouver le chemin racine du projet a partir de os.path.dirname(__file__)
    DOSSIER_RACINE_CRATER = os.path.dirname(__file__).split("/crater-data/")[0] + "/crater-data"
    sys.path.append(DOSSIER_RACINE_CRATER)
    log.info("Ajout du dossier racine crater-data dans le syspath :")
    log.info("    - DOSSIER_RACINE_CRATER = " + str(DOSSIER_RACINE_CRATER))
    log.info("    - sys.path = " + str(sys.path))

    return calculateur_surface_agricole_utile.calculer_surface_agricole_utile_par_commune_et_culture(
        2023,
        referentiel_territoires,
        CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
        CHEMIN_SOURCE_RPG,
        NOMS_FICHIERS_RPG,
        CHEMIN_RESULTAT_DONNEES_SOURCES / "surface_agricole_utile",
        psutil.cpu_count(logical=False),
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def donnees_clc(referentiel_territoires: DataFrame) -> Path:
    """Fichiers de données Corin Land Cover pour l'occupation des sols"""
    return calculer_donnees_clc(
        referentiel_territoires,
        ANNEE_REFERENTIEL_TERRITOIRES_INSEE,
        CHEMIN_SOURCE_OCCUPATION_SOLS_CLC,
        ANNEE_SOURCE_OCCUPATION_SOLS_CLC,
        ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_OCCUPATION_SOLS_CLC,
        CHEMIN_SOURCE_MOUVEMENTS_COMMUNES,
        CHEMIN_RESULTAT_DONNEES_SOURCES / "clc",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def donnees_agence_bio_surfaces(referentiel_territoires: DataFrame) -> Path:
    """Fichiers de données Agence Bio pour les surfaces bio"""
    return calculer_donnees_agence_bio_surfaces(
        referentiel_territoires,
        ANNEE_REFERENTIEL_TERRITOIRES_INSEE,
        CHEMIN_SOURCE_BIO,
        ANNEE_SOURCE_BIO,
        ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_BIO,
        CHEMIN_SOURCE_MOUVEMENTS_COMMUNES,
        CHEMIN_RESULTAT_DONNEES_SOURCES / "bio",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def donnees_hubeau_irrigation(referentiel_territoires: DataFrame) -> list[ResultatsDonneesHubeauIrrigation]:
    """Fichiers de données BNPE des prélèvements pour l'irrigation"""
    return calculer_donnees_hubeau_irrigation(
        referentiel_territoires,
        ANNEE_REFERENTIEL_TERRITOIRES_INSEE,
        CHEMINS_SOURCES_PRELEVEMENTS_EAU_IRRIGATION,
        ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_PRELEVEMENTS_EAU_IRRIGATION,
        CHEMIN_SOURCE_MOUVEMENTS_COMMUNES,
        CHEMIN_RESULTAT_DONNEES_SOURCES / "irrigation",
    )
