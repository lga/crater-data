from pathlib import Path

from dagster import asset
from pandas import DataFrame

from crater.config.config_resultats import CHEMIN_RESULTAT_CONTOURS
from crater.contours.geojson.calculateur_contours_geojson import ResultatsCalculerContoursGeojson, calculer_contours_geojson
from crater.contours.pmtiles.calculateur_contours_pmtiles import calculer_contours_pmtiles
from crater.contours.donnees_contours.calculateur_donnees_contours import calculer_donnees_contours
from crater.config.config_sources import (
    CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
    CHEMIN_SOURCE_GEOMETRIES_EPCIS,
    CHEMIN_SOURCE_GEOMETRIES_DEPARTEMENTS,
    CHEMIN_SOURCE_GEOMETRIES_REGIONS,
)

NOM_GROUPE_ASSETS = "contours"


@asset(group_name=NOM_GROUPE_ASSETS, tags={"est_inactif_par_defaut": "true"})
def contours_geojson(referentiel_territoires: DataFrame) -> ResultatsCalculerContoursGeojson:
    """Contours des territoires (régions, départements, EPCI) au format GeoJSON, avec géométries simplifiés pour une utilisation web"""
    return calculer_contours_geojson(
        referentiel_territoires,
        CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
        CHEMIN_SOURCE_GEOMETRIES_EPCIS,
        CHEMIN_SOURCE_GEOMETRIES_DEPARTEMENTS,
        CHEMIN_SOURCE_GEOMETRIES_REGIONS,
        CHEMIN_RESULTAT_CONTOURS / "geojson",
    )


@asset(group_name=NOM_GROUPE_ASSETS, tags={"est_inactif_par_defaut": "true"})
def contours_pmtiles(contours_geojson: ResultatsCalculerContoursGeojson) -> Path:
    """Contours des territoires (régions, départements, EPCI) sous forme de fichiers PMTiles. Rq : ce site permet de visualiser les PMTiles, voir https://protomaps.github.io/PMTiles"""
    return calculer_contours_pmtiles(
        contours_geojson["dossier_contours_geojson_temporaires"],
        CHEMIN_RESULTAT_CONTOURS / "pmtiles",
    )


@asset(group_name=NOM_GROUPE_ASSETS, tags={"est_inactif_par_defaut": "true"})
def donnees_contours(referentiel_territoires: DataFrame) -> Path:
    """Fichier csv contenant des données relatives aux contours des territoires"""
    return calculer_donnees_contours(
        referentiel_territoires,
        CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
        CHEMIN_SOURCE_GEOMETRIES_EPCIS,
        CHEMIN_SOURCE_GEOMETRIES_DEPARTEMENTS,
        CHEMIN_SOURCE_GEOMETRIES_REGIONS,
        CHEMIN_RESULTAT_CONTOURS / "donnees_contours",
    )
