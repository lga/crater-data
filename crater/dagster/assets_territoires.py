from dagster import asset
from pandas import DataFrame

from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.config.config_resultats import CHEMIN_RESULTAT_REFERENTIEL_TERRITOIRES
from crater.config.config_sources import (
    CHEMIN_SOURCE_COMMUNES_ET_ARRONDISSEMENTS,
    CHEMIN_SOURCE_FUSIONS_SCISSIONS_COMMUNES,
    CHEMIN_SOURCE_EPCIS,
    CHEMIN_SOURCE_DEPARTEMENTS,
    CHEMIN_SOURCE_REGIONS,
    CHEMIN_SOURCE_CODES_POSTAUX,
    FICHIERS_REGROUPEMENTS_COMMUNES_SOURCES,
    CHEMIN_FICHIER_CORRESPONDANCE_TERRITOIRES_PARCEL_CRATER,
    CHEMIN_SOURCE_DEFINITIONS_PAT,
    CHEMIN_SOURCE_CONTOURS_PAT,
)
from crater.dagster.assets_init import supprimer_donnees_et_caches
from crater.referentiel_territoires.calculateur_referentiel_territoires import (
    calculer_referentiel_territoires,
)

NOM_GROUPE_ASSETS = "territoires"


@asset(group_name=NOM_GROUPE_ASSETS, deps={supprimer_donnees_et_caches.key})
def referentiel_territoires() -> DataFrame:
    """referentiel_territoires.csv contenant la liste des territoires et les relations entre eux"""
    fichier_referentiel_territoires = calculer_referentiel_territoires(
        CHEMIN_SOURCE_COMMUNES_ET_ARRONDISSEMENTS,
        CHEMIN_SOURCE_FUSIONS_SCISSIONS_COMMUNES,
        CHEMIN_SOURCE_EPCIS,
        CHEMIN_SOURCE_DEPARTEMENTS,
        CHEMIN_SOURCE_REGIONS,
        CHEMIN_SOURCE_CODES_POSTAUX,
        FICHIERS_REGROUPEMENTS_COMMUNES_SOURCES,
        CHEMIN_SOURCE_DEFINITIONS_PAT,
        CHEMIN_SOURCE_CONTOURS_PAT,
        CHEMIN_FICHIER_CORRESPONDANCE_TERRITOIRES_PARCEL_CRATER,
        CHEMIN_RESULTAT_REFERENTIEL_TERRITOIRES,
    )

    return charger_referentiel_territoires(fichier_referentiel_territoires)
