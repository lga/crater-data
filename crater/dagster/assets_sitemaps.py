from dagster import asset
from pandas import DataFrame

from crater.config.config_resultats import CHEMIN_RESULTAT_SITEMAPS
from crater.sitemaps.calculateur_sitemap import generer_sitemaps

NOM_GROUPE_ASSETS = "sitemaps"


@asset(group_name=NOM_GROUPE_ASSETS)
def sitemaps(referentiel_territoires: DataFrame):
    """Fichiers de sitemap.xml pour le référencement des urls du site web pour certains territoires"""

    generer_sitemaps(
        referentiel_territoires,
        CHEMIN_RESULTAT_SITEMAPS,
    )
