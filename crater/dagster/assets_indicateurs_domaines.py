from pathlib import Path

from dagster import asset
from pandas import DataFrame

from crater.config.config_resultats import CHEMIN_RESULTAT_INDICATEURS
from crater.donnees_sources.hubeau_irrigation.calcul_hubeau_irrigation import ResultatsDonneesHubeauIrrigation
from crater.indicateurs.analogue_climatique.calculateur_analogue_climatique_2050 import calculer_analogues_climatiques_2050
from crater.indicateurs.consommation.calculateur_indicateurs_consommation import (
    calculer_indicateurs_consommation,
)
from crater.indicateurs.eau.pipeline_eau import lancer_pipeline_eau
from crater.indicateurs.energie.calculateur_energie import calculer_energie
from crater.indicateurs.intrants.calculateur_intrants import calculer_intrants
from crater.indicateurs.nutriments.calculateur_azote import calculer_indicateurs_azote
from crater.indicateurs.pesticides.indicateurs_pesticides.config_pesticides import CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES_HORS_AUTRE
from crater.indicateurs.pesticides.pipeline_pesticides import (
    lancer_pipeline_pesticides,
)
from crater.indicateurs.politique_fonciere.calculateur_politique_fonciere import (
    calculer_politique_fonciere,
)
from crater.indicateurs.population_agricole.calculateur_population_agricole import (
    calculer_population_agricole,
)
from crater.indicateurs.pratiques_agricoles.calculateur_pratiques_agricoles import (
    calculer_pratiques_agricoles,
)
from crater.indicateurs.production.calculateur_production import calculer_production
from crater.indicateurs.productions_besoins.calculateur_productions_besoins import (
    calculer_productions_besoins,
)
from crater.indicateurs.profils_territoires.calculateur_profils_territoires import (
    calculer_profils_territoires,
)
from crater.indicateurs.proximite_commerces.indicateurs_proximite_commerces.calculateur_indicateurs_proximite_commerces import (
    calculer_indicateurs_proximite_commerces,
)
from crater.indicateurs.proximite_commerces.referentiel_commerces.calculateur_referentiel_commerces import (
    calculer_referentiel_commerces,
)
from crater.config.config_sources import (
    CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
    CHEMIN_SOURCE_AGRESTE_RA_2010,
    CHEMIN_SOURCE_CORRESPONDANCES_ANCIENNES_NOUVELLES_REGIONS,
    CHEMIN_SOURCE_GEOMETRIES_ZONES_ALERTE_SECHERESSE,
    CHEMINS_SOURCES_ARRETES_SECHERESSE,
    CHEMIN_SOURCE_FLUX_AZOTE,
    CHEMIN_SOURCE_ARTIFICIALISATION,
    CHEMIN_SOURCE_NB_LOGEMENTS,
    CHEMIN_SOURCE_NB_LOGEMENTS_VACANTS,
    CHEMIN_SOURCE_HVN,
    CHEMINS_SOURCES_BNVD,
    CHEMIN_SOURCE_DU_2017,
    CHEMIN_SOURCE_DU_2019,
    CHEMIN_SOURCE_USAGES_PRODUITS,
    CHEMIN_SOURCE_COMMERCES_OSM,
    CHEMIN_SOURCE_COMMERCES_BPE,
    NOM_FICHIER_SOURCE_COMMERCES_BPE,
    CHEMIN_SOURCE_CARROYAGE_INSEE,
    CHEMIN_SOURCE_CTIFL_SERRES,
    CHEMIN_SOURCE_TAUX_PAUVRETE_COMMUNES,
    CHEMIN_SOURCE_TAUX_PAUVRETE_SUPRA_COMMUNES,
    CHEMIN_SOURCE_RISQUE_PRECARITE_ALIMENTAIRE,
    CHEMIN_SOURCE_SURFACES_RIZ_PAR_DEPARTEMENTS,
    CHEMIN_FICHIER_CC_EXPLORER_ANALOGUES_CLIMATIQUES_2050,
    CHEMIN_FICHIER_BESOINS_PARCEL_ASSIETTE_ACTUELLE,
    CHEMIN_FICHIER_BESOINS_PARCEL_ASSIETTE_50P,
)

NOM_GROUPE_ASSETS = "indicateurs_domaines"


@asset(group_name=NOM_GROUPE_ASSETS)
def eau(referentiel_territoires: DataFrame, occupation_sols: Path, donnees_hubeau_irrigation: list[ResultatsDonneesHubeauIrrigation]) -> Path:
    """Ensemble de fichiers sur l'eau : pratiques d'irrigation, irrigation par années,
    arrêtés sècheresse par années et mois, indicateurs de synthèse,..."""

    return lancer_pipeline_eau(
        referentiel_territoires,
        occupation_sols,
        donnees_hubeau_irrigation,
        CHEMIN_SOURCE_CORRESPONDANCES_ANCIENNES_NOUVELLES_REGIONS,
        CHEMIN_SOURCE_AGRESTE_RA_2010,
        CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
        CHEMIN_SOURCE_GEOMETRIES_ZONES_ALERTE_SECHERESSE,
        CHEMINS_SOURCES_ARRETES_SECHERESSE,
        CHEMIN_SOURCE_SURFACES_RIZ_PAR_DEPARTEMENTS,
        CHEMIN_RESULTAT_INDICATEURS / "eau",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def nutriments(referentiel_territoires: DataFrame) -> Path:
    """Fichier flux_azote.csv"""

    return calculer_indicateurs_azote(
        referentiel_territoires,
        CHEMIN_SOURCE_FLUX_AZOTE,
        CHEMIN_RESULTAT_INDICATEURS / "nutriments",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def population_agricole(referentiel_territoires: DataFrame, population: Path):
    """Fichier population_agricole.csv"""

    calculer_population_agricole(
        referentiel_territoires,
        population,
        CHEMIN_SOURCE_AGRESTE_RA_2010,
        CHEMIN_RESULTAT_INDICATEURS / "population_agricole",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def politique_fonciere(
    referentiel_territoires: DataFrame,
    population: Path,
    occupation_sols: Path,
):
    """Fichier politique_fonciere.csv"""

    calculer_politique_fonciere(
        referentiel_territoires,
        population,
        occupation_sols,
        CHEMIN_SOURCE_ARTIFICIALISATION,
        CHEMIN_SOURCE_NB_LOGEMENTS,
        CHEMIN_SOURCE_NB_LOGEMENTS_VACANTS,
        CHEMIN_RESULTAT_INDICATEURS / "politique_fonciere",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def pratiques_agricoles(referentiel_territoires: DataFrame, occupation_sols: Path, donnees_agence_bio_surfaces: Path) -> Path:
    """Fichier pratiques_agricoles.csv"""

    return calculer_pratiques_agricoles(
        referentiel_territoires, occupation_sols, CHEMIN_SOURCE_HVN, donnees_agence_bio_surfaces, CHEMIN_RESULTAT_INDICATEURS / "pratiques_agricoles"
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def productions_besoins(referentiel_territoires: DataFrame, surface_agricole_utile: Path) -> Path:
    """Ensemble de fichiers sur l'adéquation entre production et besoin : sau_par_culture.csv,
    production_besoins_par_groupe_culture.csv, besoins_par_culture.csv, production_besoins.csv
    """

    return calculer_productions_besoins(
        referentiel_territoires,
        CHEMIN_FICHIER_BESOINS_PARCEL_ASSIETTE_ACTUELLE,
        CHEMIN_FICHIER_BESOINS_PARCEL_ASSIETTE_50P,
        surface_agricole_utile,
        CHEMIN_RESULTAT_INDICATEURS / "productions_besoins",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def production(pratiques_agricoles: Path, productions_besoins: Path):
    """Fichier production.csv (indicateurs pour le maillon production)"""
    calculer_production(
        pratiques_agricoles,
        productions_besoins,
        CHEMIN_RESULTAT_INDICATEURS / "production",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def energie(
    referentiel_territoires: DataFrame, occupation_sols: Path, cheptels: Path, surface_agricole_utile: Path, eau: Path, nutriments: Path
) -> Path:
    """Fichier energie.csv"""

    return calculer_energie(
        referentiel_territoires,
        occupation_sols,
        cheptels,
        eau,
        surface_agricole_utile,
        CHEMIN_SOURCE_CTIFL_SERRES,
        nutriments,
        CHEMIN_RESULTAT_INDICATEURS / "energie",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def pesticides(referentiel_territoires: DataFrame, occupation_sols: Path) -> Path:
    """Ensemble de fichiers sur l'utilisation de pesticides : doses_unites.csv, pesticides_par_annees.csv, synthese_pesticides.csv"""

    return lancer_pipeline_pesticides(
        referentiel_territoires,
        occupation_sols,
        CLASSIFICATIONS_SUBSTANCES_RETENUES_TOUTES_HORS_AUTRE,
        CHEMINS_SOURCES_BNVD,
        CHEMIN_SOURCE_DU_2017,
        CHEMIN_SOURCE_DU_2019,
        CHEMIN_SOURCE_USAGES_PRODUITS,
        CHEMIN_RESULTAT_INDICATEURS / "pesticides",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def intrants(referentiel_territoires: DataFrame, eau: Path, energie: Path, pesticides: Path):
    """Fichier intrants.csv (indicateurs du maillon intrants)"""

    calculer_intrants(
        referentiel_territoires,
        energie,
        eau,
        pesticides,
        CHEMIN_RESULTAT_INDICATEURS / "intrants",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def proximite_commerces(referentiel_territoires: DataFrame):
    """Ensemble de fichiers sur la proximité aux commerces : referentiel_commerces.csv,
    proximite_commerces_par_types_commerces.csv, proximite_commerces.csv"""

    CHEMIN_DOSSIER_PROXIMITE_COMMERCES = CHEMIN_RESULTAT_INDICATEURS / "proximite_commerces"

    calculer_referentiel_commerces(
        CHEMIN_SOURCE_COMMERCES_OSM,
        CHEMIN_SOURCE_COMMERCES_BPE,
        NOM_FICHIER_SOURCE_COMMERCES_BPE,
        CHEMIN_DOSSIER_PROXIMITE_COMMERCES,
    )

    calculer_indicateurs_proximite_commerces(
        referentiel_territoires,
        CHEMIN_DOSSIER_PROXIMITE_COMMERCES / "referentiel_commerces.csv",
        CHEMIN_SOURCE_CARROYAGE_INSEE,
        CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
        CHEMIN_DOSSIER_PROXIMITE_COMMERCES,
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def consommation(referentiel_territoires: DataFrame, population: Path):
    """Fichier consommation.csv"""

    calculer_indicateurs_consommation(
        referentiel_territoires,
        population,
        CHEMIN_SOURCE_TAUX_PAUVRETE_COMMUNES,
        CHEMIN_SOURCE_TAUX_PAUVRETE_SUPRA_COMMUNES,
        CHEMIN_SOURCE_RISQUE_PRECARITE_ALIMENTAIRE,
        CHEMIN_RESULTAT_INDICATEURS / "consommation",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def profils_territoires(referentiel_territoires: DataFrame, population: Path, cheptels: Path, otex: Path, productions_besoins: Path):
    """Fichier profils_territoires.csv"""

    calculer_profils_territoires(
        referentiel_territoires,
        population,
        cheptels,
        otex,
        productions_besoins,
        CHEMIN_RESULTAT_INDICATEURS / "profils_territoires",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def analogues_climatiques(referentiel_territoires: DataFrame):
    calculer_analogues_climatiques_2050(
        referentiel_territoires,
        CHEMIN_FICHIER_CC_EXPLORER_ANALOGUES_CLIMATIQUES_2050,
        CHEMIN_RESULTAT_INDICATEURS / "climat",
    )
