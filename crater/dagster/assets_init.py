from dagster import asset

from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.config.config_resultats import CHEMIN_RESULTAT_DATA

NOM_GROUPE_ASSETS = "init"


@asset(group_name=NOM_GROUPE_ASSETS, tags={"est_inactif_par_defaut": "true"})
def supprimer_donnees_et_caches():
    """Supprime toutes les données crater-data-resultat"""
    reinitialiser_dossier(CHEMIN_RESULTAT_DATA, True)
