from pathlib import Path

from dagster import asset
from pandas import DataFrame

from crater.config.config_resultats import CHEMIN_RESULTAT_INDICATEURS
from crater.indicateurs.otex.calculateur_otex_territoires import calculer_otex_territoires
from crater.indicateurs.cheptels.calculateur_cheptels import calculer_cheptels
from crater.indicateurs.occupation_sols.calculateur_occupation_sols import calculer_occupation_sols
from crater.indicateurs.population.calculateur_population import calculer_population
from crater.config.config_sources import (
    CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
    CHEMIN_SOURCE_AGRESTE_RA_2010,
    CHEMIN_SOURCE_AGRESTE_RA_2020,
    CHEMIN_SOURCE_GRILLE_DENSITE_COMMUNALE,
    CHEMIN_SOURCE_POPULATION_TOTALE,
    CHEMIN_SOURCE_POPULATION_TOTALE_HISTORIQUE,
)

NOM_GROUPE_ASSETS = "indicateurs_base"


@asset(group_name=NOM_GROUPE_ASSETS)
def occupation_sols(referentiel_territoires: DataFrame, donnees_clc: Path, surface_agricole_utile: Path) -> Path:
    """Fichier occupation_sols.csv"""

    return calculer_occupation_sols(
        referentiel_territoires,
        CHEMIN_SOURCE_GEOMETRIES_COMMUNES,
        surface_agricole_utile,
        CHEMIN_SOURCE_AGRESTE_RA_2010,
        CHEMIN_SOURCE_AGRESTE_RA_2020,
        donnees_clc,
        CHEMIN_RESULTAT_INDICATEURS / "occupation_sols",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def population(referentiel_territoires: DataFrame, occupation_sols: Path) -> Path:
    """Fichier population.csv"""

    return calculer_population(
        referentiel_territoires,
        CHEMIN_SOURCE_POPULATION_TOTALE,
        CHEMIN_SOURCE_POPULATION_TOTALE_HISTORIQUE,
        occupation_sols,
        CHEMIN_SOURCE_GRILLE_DENSITE_COMMUNALE,
        CHEMIN_RESULTAT_INDICATEURS / "population",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def cheptels(referentiel_territoires: DataFrame, occupation_sols: Path) -> Path:
    """Fichier cheptels.csv"""

    return calculer_cheptels(
        referentiel_territoires,
        CHEMIN_SOURCE_AGRESTE_RA_2010,
        occupation_sols,
        CHEMIN_RESULTAT_INDICATEURS / "cheptels",
    )


@asset(group_name=NOM_GROUPE_ASSETS)
def otex(referentiel_territoires: DataFrame) -> Path:
    """Fichier cheptels.csv"""

    return calculer_otex_territoires(
        referentiel_territoires,
        CHEMIN_SOURCE_AGRESTE_RA_2020,
        CHEMIN_RESULTAT_INDICATEURS / "otex",
    )
