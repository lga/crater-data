from typing import List, cast

from dagster import (
    Definitions,
    load_assets_from_modules,
    define_asset_job,
    AssetSelection,
    AssetsDefinition,
)

from crater.commun import environnement
from crater.dagster import (
    assets_init,
    assets_donnees_sources,
    assets_sitemaps,
    assets_territoires,
    assets_indicateurs_base,
    assets_indicateurs_domaines,
    assets_contours,
)

# Quand ce script est exécuté directement, on est en environnement de PROD et pas TEST
# Cela permet d'activer la génération des diagrammes, cartes,...
environnement.ENVIRONNEMENT_EXECUTION = "PROD"

assets_tous: List[AssetsDefinition] = cast(
    List[AssetsDefinition],
    load_assets_from_modules(
        [
            assets_init,
            assets_territoires,
            assets_contours,
            assets_sitemaps,
            assets_donnees_sources,
            assets_indicateurs_base,
            assets_indicateurs_domaines,
        ]
    ),
)

assets_rapides: List[AssetsDefinition] = [asset for asset in assets_tous if asset.get_asset_spec().tags.get("est_inactif_par_defaut") != "true"]

job_complet = define_asset_job(
    name="job_complet",
    selection=AssetSelection.assets(*assets_tous),
    description="Générer l'ensemble des fichiers résultat crater-data. Supprime l'ensemble des données et caches puis exécute toutes les étapes",
)

job_incomplet_rapide = define_asset_job(
    name="job_incomplet_rapide",
    selection=AssetSelection.assets(*assets_rapides),
    description="Générer certains fichiers résultat crater-data. N'exécute qu'une partie des étapes",
)

defs = Definitions(
    jobs=[job_complet, job_incomplet_rapide],
    assets=assets_tous,
)
