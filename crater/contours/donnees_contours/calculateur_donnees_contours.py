from pathlib import Path

import pandas
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import calculer_contours_regroupements_communes
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires, reinitialiser_dossier
from crater.commun.chargeurs.chargeur_cartes import (
    charger_contours_communes,
    charger_contours_epcis,
    charger_contours_departements,
    charger_contours_regions,
)
from crater.commun.logger import log
from crater.config.config_globale import ID_FRANCE


def calculer_donnees_contours(
    territoires: DataFrame,
    chemin_geometries_communes: Path,
    chemin_geometries_epcis: Path,
    chemin_geometries_departements: Path,
    chemin_geometries_regions: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### CALCUL DES DONNEES CONTOURS #####")

    reinitialiser_dossier(chemin_dossier_output)

    territoires = territoires.copy()
    df_donnees_geographies = territoires.loc[:, ["nom_territoire", "categorie_territoire"]]
    contours_communes = charger_contours_communes(chemin_geometries_communes).to_crs("EPSG:4326")
    contours_regroupements_communes = calculer_contours_regroupements_communes(territoires, contours_communes)
    contours_epcis = charger_contours_epcis(chemin_geometries_epcis).to_crs("EPSG:4326")
    contours_departements = charger_contours_departements(chemin_geometries_departements).to_crs("EPSG:4326")
    contours_regions = charger_contours_regions(chemin_geometries_regions).to_crs("EPSG:4326")
    contours_pays = contours_regions.copy()
    contours_pays["id_territoire"] = ID_FRANCE
    contours_pays = contours_pays.dissolve("id_territoire").reset_index()

    contours = pandas.concat(
        [contours_communes, contours_regroupements_communes, contours_epcis, contours_departements, contours_regions, contours_pays]
    )
    contours = contours.set_index("id_territoire")

    df_donnees_geographies = df_donnees_geographies.join(contours.bounds)
    df_donnees_geographies = df_donnees_geographies.rename(
        columns={
            "minx": "longitude_min",
            "miny": "latitude_min",
            "maxx": "longitude_max",
            "maxy": "latitude_max",
        }
    )

    return exporter_df_indicateurs_par_territoires(df_donnees_geographies, chemin_dossier_output, "donnees_contours", nombre_chiffres_apres_virgule=3)
