#!/bin/bash

DOSSIER_SRC_GEOJSON=../../../../crater-data-resultats/data/crater/cartes/geojson
# Dossier qui a été utilisé pour stocker les fichiers temporaires avec les données indicateurs
DOSSIER_RESULTATS_INTERMEDIAIRES=../../../../crater-data-resultats/data/crater/cartes/.tmp
DOSSIER_RESULTATS=../../../../crater-data-resultats/data/crater/cartes/vectorielles

./tile-join --no-tile-size-limit -o ${DOSSIER_RESULTATS}/population.pmtiles -c ${DOSSIER_RESULTATS_INTERMEDIAIRES}/population.csv ${DOSSIER_RESULTATS}/contours.pmtiles --force
./tile-join --no-tile-size-limit -o ${DOSSIER_RESULTATS}/intrants.pmtiles -c ${DOSSIER_RESULTATS_INTERMEDIAIRES}/intrants.csv ${DOSSIER_RESULTATS}/contours.pmtiles --force

