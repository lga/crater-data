import csv
import os
import subprocess
from pathlib import Path

import pandas as pd

from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.logger import log


def calculer_contours_pmtiles(
    dossier_contours_geojson_tmp: Path,
    dossier_resultat: Path,
) -> Path:
    log.info("##### CALCUL DES CONTOURS PMTILES #####")

    reinitialiser_dossier(dossier_resultat)

    _generer_contours(dossier_contours_geojson_tmp, dossier_resultat)

    # FIXME : Pas utile, mais on garde pour l'instant, dépendra des perfs et du fonctionnement retenu sur l'ui
    # _generer_contours_avec_indicateurs(fichier_population, fichier_synthese_intrants, dossier_resultats_temporaires)

    return dossier_resultat


def _generer_contours_avec_indicateurs(fichier_population, fichier_synthese_intrants, dossier_resultats_temporaires):
    # TODO : code pas nettoyé car temporaire, a voir si on le garde

    #  Generation des fichiers csv intermédiaires, dans un format compréhensible par tile-oin
    population_df = pd.read_csv(fichier_population, sep=";", encoding="utf-8").drop(columns=["nom_territoire"])
    population_df.to_csv(
        dossier_resultats_temporaires / "population.csv",
        index=False,
        quoting=csv.QUOTE_NONNUMERIC,
    )
    intrants_df = pd.read_csv(fichier_synthese_intrants, sep=";", encoding="utf-8").drop(columns=["nom_territoire"])
    intrants_df.to_csv(
        dossier_resultats_temporaires / "intrants.csv",
        index=False,
        quoting=csv.QUOTE_NONNUMERIC,
    )

    # TODO : a ce niveau, il faudrait appeler le script creer_cartes_indicateurs
    #     subprocess.run(xxx)


def _generer_contours(dossier_contours_geojson: Path, dossier_contours_pmtiles: Path):
    current_dir = os.path.dirname(os.path.abspath(__file__))
    commande_script = Path(current_dir) / "creer_pmtiles_contours.sh"
    log.info(f" current dir : {current_dir}")
    log.info(f" Path(current dir) : {str(Path(current_dir))}")
    log.info(f"    => lancement du script {str(commande_script)}")
    resultat = subprocess.run(["/bin/sh", commande_script, dossier_contours_geojson, dossier_contours_pmtiles])
    if resultat.returncode == 0:
        log.info("    => fin du script de génération des contours")
    else:
        log.error("ERREUR : erreur lors de l'exécution du script de création des contours pmtiles")
        raise Exception("ERREUR : erreur lors de l'exécution du script de création des contours pmtiles")
