#!/bin/bash

echo "Exécution du script creer_pmtiles_contours.sh"


DOSSIER_SRC_GEOJSON=$1
DOSSIER_RESULTATS=$2
# Valeurs pour lancer manuellement le script
#DOSSIER_SRC_GEOJSON=../../../../crater-data-resultats/data/crater/cartes/geojson
#DOSSIER_RESULTATS=../../../../crater-data-resultats/data/crater/cartes/vectorielles

MAX_ZOOM=13
COEF_SIMPLIFICATION=10 # A augmenter si les fichiers sont trop lourds, controler la perte de précision sur zoom faible

echo "creer_carte_contours.sh $1 $2"
if [ $# -ne 2 ]; then
  echo 1>&2 "$0: Erreur, merci de renseigner les 2 paramètres obligatoires, dans l'ordre : DOSSIER_SRC_GEOJSON et DOSSIER_RESULTATS"
  exit 2
fi

echo "Création de la carte des contours ${DOSSIER_RESULTATS}/contours.pmtiles depuis les geojson de ${DOSSIER_SRC_GEOJSON}"

echo "  => contours régions"

tippecanoe -o ${DOSSIER_RESULTATS}/regions.pmtiles  --force --detect-shared-borders --maximum-zoom=${MAX_ZOOM} \
-L"{\"file\":\"${DOSSIER_SRC_GEOJSON}/regions.geojson\", \"layer\":\"contours\", \"description\":\"Contours régions\"}"

echo "  => contours départements"

tippecanoe -o ${DOSSIER_RESULTATS}/departements.pmtiles  --force --detect-shared-borders --maximum-zoom=${MAX_ZOOM} \
-L"{\"file\":\"${DOSSIER_SRC_GEOJSON}/departements.geojson\", \"layer\":\"contours\", \"description\":\"Contours départements\"}" \

echo "  => contours epcis"

tippecanoe -o ${DOSSIER_RESULTATS}/epcis.pmtiles --force --simplification=10 --simplify-only-low-zooms --detect-shared-borders --maximum-zoom=${MAX_ZOOM} \
-L"{\"file\":\"${DOSSIER_SRC_GEOJSON}/epcis.geojson\", \"layer\":\"contours\", \"description\":\"Contours ECPIs\"}" \

echo "  => contours regroupements de communes"

tippecanoe -o ${DOSSIER_RESULTATS}/regroupements_communes.pmtiles --force --simplification=10 --simplify-only-low-zooms --detect-shared-borders \
--maximum-zoom=${MAX_ZOOM} -L"{\"file\":\"${DOSSIER_SRC_GEOJSON}/regroupements_communes.geojson\", \"layer\":\"contours\",
\"description\":\"Contours regroupements de communes\"}" \

echo "  => contours communes"

tippecanoe -o ${DOSSIER_RESULTATS}/communes.pmtiles --force --simplification=30 --simplify-only-low-zooms --no-tile-size-limit --no-feature-limit --detect-shared-borders --maximum-zoom=${MAX_ZOOM} \
-L"{\"file\":\"${DOSSIER_SRC_GEOJSON}/communes.geojson\", \"layer\":\"contours\", \"description\":\"Contours communes\"}"


# Pas utilisé pour l'instant - on pourra l'activer si besoin d'une carte avec changement de niveau de contour lors du zoom
#echo "  => assemblage des couches dans ${DOSSIER_RESULTATS}/contours.pmtiles"
#tile-join --no-tile-size-limit -o ${DOSSIER_RESULTATS}/contours.pmtiles ${DOSSIER_RESULTATS}/regions.pmtiles ${DOSSIER_RESULTATS}/departements.pmtiles ${DOSSIER_RESULTATS}/epcis.pmtiles ${DOSSIER_RESULTATS}/communes.pmtiles --force

