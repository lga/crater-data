import unittest
from pathlib import Path

from crater.contours.geojson.calculateur_contours_geojson import calculer_contours_geojson
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    augmenter_nombre_colonnes_affichees_pandas,
    assert_list_of_json_files_are_equals,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestContours(unittest.TestCase):
    def test_calculer_contours(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")
        # when
        calculer_contours_geojson(
            territoires,
            CHEMIN_INPUT_DATA / "communes_test",
            CHEMIN_INPUT_DATA / "epcis_test",
            CHEMIN_INPUT_DATA / "departements_test",
            CHEMIN_INPUT_DATA / "regions_test",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_list_of_json_files_are_equals(
            self,
            CHEMIN_EXPECTED_DATA,
            CHEMIN_OUTPUT_DATA,
            [
                "communes-crater.geojson",
                "epcis-crater.geojson",
                "departements-crater.geojson",
                "regions-crater.geojson",
            ],
        )


if __name__ == "__main__":
    unittest.main()
