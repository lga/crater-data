from pathlib import Path
from typing import Tuple, Dict

import geopandas
import pandas
import topojson

from crater.commun.calculs.outils_territoires import calculer_contours_regroupements_communes
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.chargeurs.chargeur_cartes import (
    charger_contours_regions,
    charger_contours_departements,
    charger_contours_epcis,
    charger_contours_communes,
)
from crater.commun.logger import log

SEUIL_SIMPLIFICATION_CRATER = 0.02

ResultatsCalculerContoursGeojson = Dict[str, Path]

# TODO : TypeDict et Literal ne sont supportés par Dagster 1.8, voir si cela fonctionne avec une version plus récente
# ResultatsCalculerContoursGeojson = Dict[Literal["dossier_contours_geojson", "dossier_contours_geojson_temporaires"], Path]
#  ou
# class ResultatsCalculerContoursGeojson(TypedDict):
#     dossier_contours_geojson: Path
#     dossier_contours_geojson_temporaires: Path


def calculer_contours_geojson(
    territoires: pandas.DataFrame,
    chemin_fichiers_geometries_communes: Path,
    chemin_fichiers_geometries_epcis: Path,
    chemin_fichiers_geometries_departements: Path,
    chemin_fichiers_geometries_regions: Path,
    dossier_resultat: Path,
) -> ResultatsCalculerContoursGeojson:
    log.info("##### CALCUL DES CONTOURS GEOJSON #####")

    dossier_contours_geojson = dossier_resultat
    dossier_contours_geojson_temporaires = dossier_resultat / ".tmp"

    reinitialiser_dossier(dossier_contours_geojson)
    reinitialiser_dossier(dossier_contours_geojson_temporaires)

    communes_gdf, communes_topology_simplifiee = _calculer_contours_communes(
        territoires, chemin_fichiers_geometries_communes, dossier_contours_geojson, dossier_contours_geojson_temporaires
    )
    _calculer_contours_communes_par_departements(
        communes_topology_simplifiee,
        territoires,
        dossier_contours_geojson,
    )

    _calculer_contours_regroupements_communes(territoires, communes_gdf, dossier_contours_geojson_temporaires)

    _calculer_contours_epcis(territoires, chemin_fichiers_geometries_epcis, dossier_contours_geojson, dossier_contours_geojson_temporaires)
    _calculer_contours_departements(
        territoires, chemin_fichiers_geometries_departements, dossier_contours_geojson, dossier_contours_geojson_temporaires
    )
    _calculer_contours_regions(territoires, chemin_fichiers_geometries_regions, dossier_contours_geojson, dossier_contours_geojson_temporaires)

    return {
        "dossier_contours_geojson": dossier_contours_geojson,
        "dossier_contours_geojson_temporaires": dossier_contours_geojson_temporaires,
    }


def _calculer_contours_regions(
    territoires: pandas.DataFrame,
    chemin_fichiers_geometries_regions: Path,
    dossier_resultat_geojson: Path,
    dossier_resultats_temporaires: Path,
):
    log.info("   => régions")
    regions_gdf = charger_contours_regions(chemin_fichiers_geometries_regions)
    _calculer_contours(territoires, regions_gdf, "REGION", dossier_resultat_geojson, dossier_resultats_temporaires)


def _calculer_contours_departements(
    territoires: pandas.DataFrame,
    chemin_fichiers_geometries_departements: Path,
    dossier_resultat_geojson: Path,
    dossier_resultats_temporaires: Path,
):
    log.info("   => départements")
    departements_gdf = charger_contours_departements(chemin_fichiers_geometries_departements)
    _calculer_contours(territoires, departements_gdf, "DEPARTEMENT", dossier_resultat_geojson, dossier_resultats_temporaires)


def _calculer_contours_epcis(
    territoires: pandas.DataFrame,
    chemin_fichiers_geometries_epcis: Path,
    dossier_resultat_geojson: Path,
    dossier_resultats_temporaires: Path,
):
    log.info("   => epcis")
    epcis_gdf = charger_contours_epcis(chemin_fichiers_geometries_epcis)
    _calculer_contours(territoires, epcis_gdf, "EPCI", dossier_resultat_geojson, dossier_resultats_temporaires)


def _calculer_contours_communes(
    territoires: pandas.DataFrame,
    chemin_fichiers_geometries_communes: Path,
    dossier_resultat_geojson: Path,
    dossier_resultats_temporaires: Path,
) -> Tuple[geopandas.GeoDataFrame, topojson.Topology]:
    log.info("   => communes")

    communes_gdf = charger_contours_communes(chemin_fichiers_geometries_communes)
    communes_topology_simplifiee_crater = _calculer_contours(
        territoires, communes_gdf, "COMMUNE", dossier_resultat_geojson, dossier_resultats_temporaires
    )

    return communes_gdf, communes_topology_simplifiee_crater


def _calculer_contours_regroupements_communes(
    territoires: pandas.DataFrame,
    communes_gdf: geopandas.GeoDataFrame,
    dossier_resultats_temporaires: Path,
):
    log.info("   => regroupements de communes")
    regroupements_communes_gdf = calculer_contours_regroupements_communes(territoires, communes_gdf)
    regroupements_communes_gdf = _preparer_gdf(territoires, "REGROUPEMENT_COMMUNES", regroupements_communes_gdf)
    regroupements_communes_gdf.to_file(dossier_resultats_temporaires / "regroupements_communes.geojson", driver="GeoJSON")


def _calculer_contours(
    territoires: pandas.DataFrame,
    gdf: geopandas.GeoDataFrame,
    categorie_territoire: str,
    dossier_resultat_geojson: Path,
    dossier_resultats_temporaires: Path,
) -> topojson.Topology:
    gdf = _preparer_gdf(territoires, categorie_territoire, gdf)

    base_nom_fichier = categorie_territoire.lower() + "s"

    _exporter_contours_pleine_resolution(gdf, base_nom_fichier, dossier_resultats_temporaires)

    topology = _creer_topology(gdf)
    topology_simplifiee_crater = _simplifier_topology(topology, SEUIL_SIMPLIFICATION_CRATER)
    _exporter_contours_crater(topology_simplifiee_crater, dossier_resultat_geojson, base_nom_fichier + "-crater")
    return topology_simplifiee_crater


def _calculer_contours_communes_par_departements(
    topology_communes_simplifiees: topojson.Topology,
    territoires: pandas.DataFrame,
    dossier_resultat: Path,
):
    log.info("   => communes par départements")
    communes_gdf_simplifie = topology_communes_simplifiees.to_gdf().set_crs(epsg=4326, allow_override=True)
    communes_gdf_simplifie = communes_gdf_simplifie.join(territoires.loc[:, ["id_departement"]])

    dossier_resultat_contours_par_departements = dossier_resultat / "communes-crater"
    reinitialiser_dossier(dossier_resultat_contours_par_departements)

    for d in sorted(communes_gdf_simplifie["id_departement"].unique()):
        log.info(f"   => communes du département {d}")

        id_nom_d = territoires.loc[d].id_nom_territoire

        communes_departement_gdf = communes_gdf_simplifie[communes_gdf_simplifie.id_departement == d].drop(columns=["id_departement"])

        communes_departement_topology = topojson.Topology(communes_departement_gdf)
        _exporter_contours_crater(
            communes_departement_topology,
            dossier_resultat_contours_par_departements,
            f"communes_du_departement_{id_nom_d}",
        )


def _preparer_gdf(
    territoires: pandas.DataFrame,
    categorie_territoire: str,
    gdf: geopandas.GeoDataFrame,
) -> geopandas.GeoDataFrame:
    gdf_avant_simplification = (
        gdf.loc[:, ["id_territoire", "geometry"]]
        .merge(
            territoires.loc[
                territoires.categorie_territoire == categorie_territoire,
                ["id_nom_territoire", "nom_territoire"],
            ],
            on="id_territoire",
            how="inner",
        )
        .set_index("id_territoire")
    ).to_crs(epsg=4326)

    _verifier_taille_df(territoires, categorie_territoire, gdf_avant_simplification)

    return gdf_avant_simplification


def _creer_topology(gdf: geopandas.GeoDataFrame) -> topojson.Topology:
    log.info("      => création topology")
    # prequantize est mis à False car sinon cela provoque des géométries invalides (voir commune C-50143, frontière nord).
    # On pourrait le fixer à une valeur supérieure à 5e5 pour résoudre le problème tout en diminuant
    # la taille du fichier de sortie mais le calcul est très instable
    topology_resultat = topojson.Topology(gdf, prequantize=False)
    return topology_resultat


def _simplifier_topology(topology: topojson.Topology, seuil: float) -> topojson.Topology:
    log.info("      => simplification topology")
    topology_simplifiee = topology.toposimplify(seuil, prevent_oversimplify=True)
    return topology_simplifiee


def _exporter_contours_crater(topology, dossier_resultat, base_nom_fichier):
    log.info(f"      => export des cartes pour crater, {base_nom_fichier} dans {dossier_resultat}")
    topology.to_geojson(dossier_resultat / f"{base_nom_fichier}.geojson", decimals=6)
    topology.to_json(dossier_resultat / f"{base_nom_fichier}.topojson")


def _exporter_contours_pleine_resolution(gdf, base_nom_fichier, dossier_resultat):
    log.info(f"      => export des cartes en pleine résolution, {base_nom_fichier} dans {dossier_resultat}")
    gdf.to_file(dossier_resultat / f"{base_nom_fichier}.geojson", driver="GeoJSON")


def _verifier_taille_df(territoires, categorie_territoire, gdf_avant_simplification):
    nb_territoires_attendus_dans_carte = territoires.loc[territoires.categorie_territoire == categorie_territoire].shape[0]
    nb_territoires_dans_gdf_a_simplifier = gdf_avant_simplification.shape[0]
    if nb_territoires_attendus_dans_carte != nb_territoires_dans_gdf_a_simplifier:
        raise ValueError(
            f"""Attention, problème de cohérence entre le référentiel de territoires et les géometries à simplifier :
            catégorie territoire : {categorie_territoire} 
            nb de territoires de cette catégorie dans le référentiel : {nb_territoires_attendus_dans_carte}
            nb de territoires trouvés dans les géometries : {nb_territoires_dans_gdf_a_simplifier}
            """
        )
