from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.mouvements_communes.outils_mouvements_communes import calculer_donnees_territoires_annee_cible
from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
)
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires, reinitialiser_dossier
from crater.commun.logger import log


def calculer_donnees_clc(
    referentiel_territoires: DataFrame,
    annee_referentiel_territoire: int,
    chemin_fichier_occupation_sols_clc: Path,
    annee_donnees_clc: int,  # Millésime CLC pour lequel générer les données
    annee_referentiel_territoires_donnees_clc: int,  # Année référentiel territoires utilisée par le millésime
    # (par ex le millésime CLC 2018 est basée sur les communes de 2016)
    chemin_fichier_source_mouvements_communes: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### PRECALCUL DONNEES CLC #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_source_clc_communes_annee_origine = _charger_fichier_source_clc(chemin_fichier_occupation_sols_clc, annee_donnees_clc)

    df_source_clc_communes_annee_origine["superficie_naturelle_ou_forestiere_clc_ha"] = (
        df_source_clc_communes_annee_origine["superficie_forets_et_milieux_semi_naturels_clc_ha"]
        + df_source_clc_communes_annee_origine["superficie_zones_humides_clc_ha"]
        + df_source_clc_communes_annee_origine["superficie_eau_clc_ha"]
    )

    df_donnees_clc_annee_cible = calculer_donnees_territoires_annee_cible(
        referentiel_territoires,
        annee_referentiel_territoire,
        df_source_clc_communes_annee_origine,
        ["superficie_artificialisee_clc_ha", "superficie_agricole_clc_ha", "superficie_naturelle_ou_forestiere_clc_ha"],
        annee_referentiel_territoires_donnees_clc,
        chemin_fichier_source_mouvements_communes,
    )

    return exporter_df_indicateurs_par_territoires(df_donnees_clc_annee_cible, chemin_dossier_output, f"donnees_clc_{annee_donnees_clc}")


def _charger_fichier_source_clc(chemin_fichier_occupation_sols_clc, annee: int):
    df_clc_communes = pd.read_csv(chemin_fichier_occupation_sols_clc, sep=";", skiprows=3, usecols=[0, 1, 3, 4, 5, 6, 7], dtype={"NUM_COM": "str"})
    df_clc_communes.columns = [
        "id_commune",
        "annee",
        "superficie_artificialisee_clc_ha",
        "superficie_agricole_clc_ha",
        "superficie_forets_et_milieux_semi_naturels_clc_ha",
        "superficie_zones_humides_clc_ha",
        "superficie_eau_clc_ha",
    ]  # type: ignore[assignment]  # TODO: open issue https://github.com/pandas-dev/pandas-stubs/issues/73
    df_clc_communes = _filtrer_annee(df_clc_communes, annee)
    df_clc_communes["id_commune"] = traduire_code_insee_vers_id_commune_crater(df_clc_communes["id_commune"])
    return df_clc_communes


def _filtrer_annee(df_clc: DataFrame, annee: int) -> DataFrame:
    df_clc = df_clc[df_clc.annee == annee]
    df_clc = df_clc.drop(columns="annee")
    return df_clc
