from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.logger import log


def charger(chemin_fichiers: Path) -> DataFrame:
    log.info(f"   => chargement sau depuis {chemin_fichiers}")

    dfs = DataFrame()

    for chemin_fichier in [f for f in chemin_fichiers.iterdir() if f.is_file()]:
        df = pd.read_csv(chemin_fichier, sep=";", encoding="utf-8")
        dfs = pd.concat([dfs, df])

    return dfs
