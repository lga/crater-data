import unittest
from pathlib import Path

from crater.donnees_sources.surface_agricole_utile.calculateur_surface_agricole_utile import (
    calculer_surface_agricole_utile_par_commune_et_culture,
)
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")

NOMS_FICHIERS_RPG_INPUT = {
    2017: {
        "R-11": "RPG_R-53.7z",
        "R-53": "RPG_R-53.7z",
        "R-75": "RPG_R-53.7z",
        "R-76": "RPG_R-53.7z",
    }
}


class TestSurfaceAgricoleUtile(unittest.TestCase):
    def test_calculer_surface_agricole_utile_par_commune_et_culture(self):
        referentiel_territoires = charger_referentiel_territoires(CHEMIN_INPUT_DATA / "referentiel_territoires.csv")
        calculer_surface_agricole_utile_par_commune_et_culture(
            2017,
            referentiel_territoires,
            CHEMIN_INPUT_DATA / "geometries_communes_test",
            CHEMIN_INPUT_DATA / "rpg",
            NOMS_FICHIERS_RPG_INPUT,
            CHEMIN_OUTPUT_DATA,
            1,
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-53.csv",
            CHEMIN_OUTPUT_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-53.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-11.csv",
            CHEMIN_OUTPUT_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-11.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-75.csv",
            CHEMIN_OUTPUT_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-75.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-76.csv",
            CHEMIN_OUTPUT_DATA / "sau_par_commune_et_culture" / "sau_rpg_communes_R-76.csv",
        )
