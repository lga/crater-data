from functools import partial
from multiprocessing.pool import Pool
from pathlib import Path

import geopandas as gpd
import pandas as pd
from pandas import DataFrame

import crater.donnees_sources.surface_agricole_utile.chargeur_rpg as chargeur_rpg
from crater.collecteurs.rpg.collecteur_rpg import DictFichiersRpg
from crater.commun.calculs.outils_dataframes import merge_strict
from crater.commun.calculs.outils_verification import (
    verifier_coherence_referentiel_avec_geometries_communes,
)
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.chargeurs.chargeur_geometries_communes import charger_geometries_communes
from crater.commun.logger import log, chronometre


def calculer_surface_agricole_utile_par_commune_et_culture(
    annee_rpg: int,
    referentiel_territoires: pd.DataFrame,
    chemin_fichiers_geometries_communes: Path,
    dossier_rpg: Path,
    noms_fichiers_rpg: DictFichiersRpg,
    chemin_dossier_output: Path,
    nombre_traitements_paralleles: int | None = 1,
) -> Path:
    log.info("##### CALCUL SURFACES AGRICOLES PAR COMMUNE ET CULTURE #####")

    dossier_output_sau_par_commune_et_culture = chemin_dossier_output / "sau_par_commune_et_culture"
    reinitialiser_dossier(dossier_output_sau_par_commune_et_culture)

    # NB: les fichiers RPG par région ne sont pas exclusifs (une parcelle à la frontière de 2 régions peut se trouver
    # dans les fichiers des 2 régions)

    geometries_communes = _importer_communes(referentiel_territoires, chemin_fichiers_geometries_communes)

    ids_regions = (
        referentiel_territoires.reset_index()
        .query("categorie_territoire == 'REGION'")
        .drop(columns=["id_region"])
        .rename(columns={"id_territoire": "id_region"})["id_region"]
    )

    if (nombre_traitements_paralleles is None) | (nombre_traitements_paralleles == 1):
        for id_region in ids_regions:
            _calculer_surface_agricole_utile_par_region(
                annee_rpg,
                geometries_communes,
                dossier_rpg,
                noms_fichiers_rpg,
                dossier_output_sau_par_commune_et_culture,
                id_region,
            )
    else:
        partial_calculer_surface_agricole_utile_par_region = partial(
            _calculer_surface_agricole_utile_par_region,
            annee_rpg,
            geometries_communes,
            dossier_rpg,
            noms_fichiers_rpg,
            dossier_output_sau_par_commune_et_culture,
        )

        with Pool(processes=nombre_traitements_paralleles) as pool:
            pool.map(partial_calculer_surface_agricole_utile_par_region, ids_regions)

    return dossier_output_sau_par_commune_et_culture


def _importer_communes(referentiel_territoires: DataFrame, chemin_fichiers_geometries_communes: Path) -> gpd.GeoDataFrame:
    geometries_communes = charger_geometries_communes(chemin_fichiers_geometries_communes)
    verifier_coherence_referentiel_avec_geometries_communes(referentiel_territoires, geometries_communes)
    geometries_communes = merge_strict(
        geometries_communes,
        referentiel_territoires.loc[referentiel_territoires.categorie_territoire == "COMMUNE", "id_region"].reset_index(),
        left_on="id_commune",
        right_on="id_territoire",
    )
    return geometries_communes


def _calculer_surface_agricole_utile_par_region(
    annee_rpg: int,
    geometries_communes: gpd.GeoDataFrame,
    dossier_rpg: Path,
    noms_fichiers_rpg: DictFichiersRpg,
    chemin_dossier_output: Path,
    id_region: str,
):
    rpg = chargeur_rpg.charger_rpg(dossier_rpg / str(annee_rpg) / noms_fichiers_rpg[annee_rpg][id_region])

    log.info("Intersection RPG - Communes de la région " + str(id_region))
    intersection = _intersecter_rpg_communes(geometries_communes.query("id_region == @id_region"), rpg)
    _exporter_surface_agricole_utile_region_par_commune_et_culture(id_region, intersection, chemin_dossier_output)


def _exporter_surface_agricole_utile_region_par_commune_et_culture(id_region: str, intersection: pd.DataFrame, chemin_dossier_output: Path) -> None:
    log.info("=> Export région " + id_region)

    intersection.to_csv(
        chemin_dossier_output / ("sau_rpg_communes_" + id_region + ".csv"),
        sep=";",
        encoding="utf-8",
        index=False,
        float_format="%.2f",
    )


@chronometre
def _intersecter_rpg_communes(communes: gpd.GeoDataFrame, rpg: gpd.GeoDataFrame) -> pd.DataFrame:
    try:
        return (
            gpd.overlay(communes, rpg, how="intersection")
            .assign(
                superficie_ha=lambda x: x.geometry.area * 10**-4,
                nombre_parcelles=1,
            )
            .groupby(["id_commune", "nom_commune", "CODE_CULTU"])[["superficie_ha", "nombre_parcelles"]]
            .sum()
            .reset_index()
            .rename(
                columns={
                    "CODE_CULTU": "code_culture_rpg",
                    "superficie_ha": "sau_ha",
                }
            )
            .assign(sau_moyenne_par_parcelle_ha=lambda x: round(x.sau_ha / x.nombre_parcelles, 2))
        )
    except Exception as e:
        log.error(f"Une exception de type {type(e).__name__} a été levée avec le message : {str(e)}")
        return pd.DataFrame()
