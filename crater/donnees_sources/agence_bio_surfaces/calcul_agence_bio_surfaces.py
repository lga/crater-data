from pathlib import Path

import numpy as np
import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.mouvements_communes.outils_mouvements_communes import (
    calculer_donnees_territoires_annee_cible,
)
from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
)
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires, reinitialiser_dossier
from crater.commun.logger import log
from crater.config.config_globale import NUMEROS_DEPARTEMENTS_DROM


def calculer_donnees_agence_bio_surfaces(
    referentiel_territoires: DataFrame,
    annee_referentiel_territoire: int,
    chemin_fichier_source_bio: Path,
    annee_donnees_bio: int,  # Millésime BIO pour lequel générer les données
    annee_referentiel_territoires_source_bio: int,  # Année référentiel territoires utilisée par le millésime
    chemin_fichier_source_mouvements_communes: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### CALCUL DONNEES SURFACES AGENCE BIO #####")

    reinitialiser_dossier(chemin_dossier_output)

    df_source_sau_bio_communes_annee_origine = _charger_surfaces_bio(chemin_fichier_source_bio, annee_donnees_bio)

    colonnes = [
        "nombre_exploitations",
        "surface_AB_ha",
        "surface_en_conversion_1ere_annee_ha",
        "surface_en_conversion_2e_annee_ha",
        "surface_en_conversion_3e_annee_ha",
        "surface_en_conversion_ha",
        "surface_bio_ha",
    ]
    df_donnees_sau_bio_annee_cible = calculer_donnees_territoires_annee_cible(
        referentiel_territoires,
        annee_referentiel_territoire,
        df_source_sau_bio_communes_annee_origine,
        colonnes,
        annee_referentiel_territoires_source_bio,
        chemin_fichier_source_mouvements_communes,
    )

    mask_territoires_sous_secret_statistique = (df_donnees_sau_bio_annee_cible.nombre_exploitations > 0) & (
        df_donnees_sau_bio_annee_cible.nombre_exploitations < 3
    )
    for col in colonnes:
        df_donnees_sau_bio_annee_cible[col] = df_donnees_sau_bio_annee_cible[col].fillna(0)  # Les NA correspondent à la valeur 0 car le fichier
        # source ne contient que les communes qui ont une sau bio > 0. Aucune commune n'a donc de sau inconnue.
        df_donnees_sau_bio_annee_cible.loc[mask_territoires_sous_secret_statistique, col] = np.nan

    return exporter_df_indicateurs_par_territoires(df_donnees_sau_bio_annee_cible, chemin_dossier_output, "donnees_sau_bio")


def _charger_surfaces_bio(chemin_fichier: Path, annee: int) -> pd.DataFrame:
    log.info("   => chargement des surfaces bio depuis %s", chemin_fichier)

    df = pd.read_excel(
        chemin_fichier,
        sheet_name="SAU",
        usecols="A,H,K:Q",
        na_values="c",
        dtype={
            "annee": "int",
            "codeinseecommune": "str",
            "nb_exp": "int",
            "surfab": "float",
            "surfc1": "float",
            "surfc2": "float",
            "surfc3": "float",
            "surfc123": "float",
            "surfbio": "float",
        },
    ).rename(
        columns={
            "annee": "annee",
            "codeinseecommune": "id_commune",
            "nb_exp": "nombre_exploitations",
            "surfab": "surface_AB_ha",
            "surfc1": "surface_en_conversion_1ere_annee_ha",
            "surfc2": "surface_en_conversion_2e_annee_ha",
            "surfc3": "surface_en_conversion_3e_annee_ha",
            "surfc123": "surface_en_conversion_ha",
            "surfbio": "surface_bio_ha",
        },
        errors="raise",
    )

    # suppression des outre-mer
    df = df.loc[~(df["id_commune"].str[0:2].isin(NUMEROS_DEPARTEMENTS_DROM)), :]
    # suppression communes inconnues
    df = df.loc[~(df["id_commune"] == "00000"), :]

    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    df = _filtrer_annee(df, annee)

    # TODO provisoire, devrait être corrigé dans une nouvelle version du fichier (pb agrégation de différents OC suite erreur saisie departement)
    df = df.groupby(["id_commune"]).agg("sum").reset_index()

    verifier_absence_doublons(df, "id_commune")

    return df


def _filtrer_annee(df: DataFrame, annee: int) -> DataFrame:
    df = df[df.annee == annee]
    df = df.drop(columns="annee")
    return df
