from pathlib import Path
from typing import Tuple

import pandas
from pandas import DataFrame

from crater.commun.calculs.mouvements_communes.outils_mouvements_communes import (
    calculer_donnees_territoires_annee_cible,
)
from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
)
from crater.commun.exports.export_fichier import exporter_df_indicateurs_par_territoires, reinitialiser_dossier
from crater.commun.logger import log
from crater.config.config_globale import NUMEROS_DEPARTEMENTS_DROM


class ResultatsDonneesHubeauIrrigation:
    def __init__(self, annee: int, chemin: Path):
        self.annee = annee
        self.chemin = chemin


def calculer_donnees_hubeau_irrigation(
    referentiel_territoires: DataFrame,
    annee_referentiel_territoire: int,
    fichiers_source_prelevements_eau: list[Path],
    annee_referentiel_territoires_source_irrigation: int,  # Année référentiel territoires utilisée par le millésime
    fichier_source_mouvements_communes: Path,
    dossier_output: Path,
) -> list[ResultatsDonneesHubeauIrrigation]:
    log.info("##### CALCUL DONNEES IRRIGATION HUBEAU #####")

    reinitialiser_dossier(dossier_output)
    resultats = []

    for f in fichiers_source_prelevements_eau:
        annee, df_irrigation_annee_origine = _charger_source_prelevements_ouvrages_eau(f)
        df_irrigation_annee_cible = calculer_donnees_territoires_annee_cible(
            referentiel_territoires,
            annee_referentiel_territoire,
            df_irrigation_annee_origine,
            ["irrigation_m3"],
            annee_referentiel_territoires_source_irrigation,
            fichier_source_mouvements_communes,
        )
        df_irrigation_annee_cible["irrigation_m3"] = df_irrigation_annee_cible["irrigation_m3"].fillna(0)
        fichier_output = exporter_df_indicateurs_par_territoires(df_irrigation_annee_cible, dossier_output, f"donnees_irrigation_{annee}")
        resultats.append(ResultatsDonneesHubeauIrrigation(annee, fichier_output))
        log.info(f"  - export {fichier_output}")

    return resultats


def _charger_source_prelevements_ouvrages_eau(
    fichier_source_prelevements_eau: Path,
) -> Tuple[int, DataFrame]:
    df_prelevements_eau = pandas.read_csv(
        fichier_source_prelevements_eau,
        skiprows=9,
        sep=";",
        dtype={"code_commune_insee": "str", "volume": "float64"},
    )
    df_prelevements_eau = df_prelevements_eau.loc[~(df_prelevements_eau["code_commune_insee"].str[0:2].isin(NUMEROS_DEPARTEMENTS_DROM)), :]
    # filtre sur l'usage irrigation (double sécurité car on requête déjà l'API en filtrant cet usage)
    df_prelevements_eau_irrigation = df_prelevements_eau.loc[df_prelevements_eau["code_usage"] == "IRR", :].copy()
    df_prelevements_eau_irrigation["id_commune"] = traduire_code_insee_vers_id_commune_crater(df_prelevements_eau_irrigation["code_commune_insee"])
    df_prelevements_eau_irrigation = df_prelevements_eau_irrigation.rename(columns={"volume": "irrigation_m3"})
    annee = int(df_prelevements_eau_irrigation.annee[0])
    df_prelevements_eau_irrigation = df_prelevements_eau_irrigation.loc[:, ["id_commune", "irrigation_m3"]]
    return annee, df_prelevements_eau_irrigation
