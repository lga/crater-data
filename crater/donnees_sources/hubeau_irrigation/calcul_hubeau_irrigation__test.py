import unittest
from pathlib import Path

from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)
from crater.donnees_sources.hubeau_irrigation.calcul_hubeau_irrigation import calculer_donnees_hubeau_irrigation

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_COMMUN_INPUT_DATA = Path(__file__).parent.parent.parent / "commun" / "test_data" / "input_precalculs" / "referentiel_simple"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestCalculHubeauIrrigation(unittest.TestCase):
    def test_calculer_donnees_hubeau_irrigation(self):
        # given
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)
        territoires = charger_referentiel_territoires(CHEMIN_COMMUN_INPUT_DATA / "referentiel_territoires.csv")
        # when
        calculer_donnees_hubeau_irrigation(
            territoires,
            2024,
            [
                CHEMIN_INPUT_DATA / "2019" / "prelevements_eau_par_communes.csv",
                CHEMIN_INPUT_DATA / "2020" / "prelevements_eau_par_communes.csv",
            ],
            2016,
            CHEMIN_COMMUN_INPUT_DATA / "table_passage_annuelle_test",
            CHEMIN_OUTPUT_DATA,
        )
        # then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "donnees_irrigation_2019.csv",
            CHEMIN_OUTPUT_DATA / "donnees_irrigation_2019.csv",
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "donnees_irrigation_2020.csv",
            CHEMIN_OUTPUT_DATA / "donnees_irrigation_2020.csv",
        )


if __name__ == "__main__":
    unittest.main()
