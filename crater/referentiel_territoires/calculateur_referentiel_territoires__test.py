import unittest
from pathlib import Path

import pandas as pd

from crater.referentiel_territoires import config
from crater.referentiel_territoires.calculateur_referentiel_territoires import (
    _modifier_noms_epcis,
    calculer_referentiel_territoires,
    _ajouter_genres_nombres_prepositions_noms,
)
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestReferentielTerritoires(unittest.TestCase):
    def test_modifier_noms_epcis(self):
        # given
        df = pd.Series(
            [
                "CA ca CA",
                "CC cc CC",
                "CU cu CU",
                "blabla (ABC) toto (DEF)",
            ]
        )
        # when
        df_resultat = _modifier_noms_epcis(df)
        # then
        df_resultat_attendu = pd.Series(
            [
                "Communauté d'agglomération ca CA",
                "Communauté de communes cc CC",
                "Communauté urbaine cu CU",
                "blabla toto",
            ]
        )
        pd.testing.assert_series_equal(df_resultat, df_resultat_attendu)

    def test_ajouter_genres_nombres_prepositions_noms(self):
        # given
        territoires = pd.DataFrame(
            columns=["id_territoire", "nom_territoire", "categorie_territoire"],
            data=[
                ["P-FR", "France", "PAYS"],
                ["C-2", "Nantes", "COMMUNE"],
                ["E-1", "Nantes Métropole", "EPCI"],
                ["E-2", "Métropole du Grand Paris", "EPCI"],
                ["E-3", "Communauté urbaine Angers Loire Métropole", "EPCI"],
                ["R-1", "PAT de Nantes", "REGROUPEMENT_COMMUNES"],
            ],
        ).set_index("id_territoire")
        genre_nombres = pd.DataFrame(
            columns=["id_territoire", "genre_nombre", "preposition"],
            data=[
                ["P-FR", "FEMININ_SINGULIER", "en"],
            ],
        )
        # when
        df_resultat = _ajouter_genres_nombres_prepositions_noms(territoires, genre_nombres)
        # then
        df_resultat_attendu = pd.DataFrame(
            columns=["id_territoire", "nom_territoire", "categorie_territoire", "genre_nombre", "preposition"],
            data=[
                ["P-FR", "France", "PAYS", "FEMININ_SINGULIER", "en"],
                ["C-2", "Nantes", "COMMUNE", "FEMININ_SINGULIER", "à"],
                ["E-1", "Nantes Métropole", "EPCI", "FEMININ_SINGULIER", "à"],
                ["E-2", "Métropole du Grand Paris", "EPCI", "FEMININ_SINGULIER", "dans"],
                ["E-3", "Communauté urbaine Angers Loire Métropole", "EPCI", "FEMININ_SINGULIER", "dans"],
                ["R-1", "PAT de Nantes", "REGROUPEMENT_COMMUNES", "MASCULIN_SINGULIER", "dans"],
            ],
        ).set_index("id_territoire")
        pd.testing.assert_frame_equal(df_resultat, df_resultat_attendu)

    def test_calculer_referentiel_territoires(self):
        # Given
        config.FICHIER_CONFIG_GENRES_NOMBRES_PREPOSITIONS_TERRITOIRES = CHEMIN_INPUT_DATA / "genres_nombres_prepositions_territoires.csv"

        # When
        calculer_referentiel_territoires(
            CHEMIN_INPUT_DATA / "communes_et_arrondissements.xlsx",
            CHEMIN_INPUT_DATA / "table_passage_communes.xlsx",
            CHEMIN_INPUT_DATA / "epcis.xlsx",
            CHEMIN_INPUT_DATA / "departements.csv",
            CHEMIN_INPUT_DATA / "regions.csv",
            CHEMIN_INPUT_DATA / "codes_postaux.csv",
            [
                {
                    "code": "PNR",
                    "fichiers_sources": [CHEMIN_INPUT_DATA / "regroupements_geographiques" / "pnr.xlsx"],
                },
                {
                    "code": "SCOT",
                    "fichiers_sources": [CHEMIN_INPUT_DATA / "regroupements_geographiques" / "scot.xlsx"],
                },
            ],
            CHEMIN_INPUT_DATA / "regroupements_geographiques" / "pats.csv",
            CHEMIN_INPUT_DATA / "regroupements_geographiques" / "pats-simplifie.geojson",
            CHEMIN_INPUT_DATA / "correspondance_territoires_parcel_crater.csv",
            CHEMIN_OUTPUT_DATA,
        )
        # Then
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "referentiel_territoires.csv",
            CHEMIN_OUTPUT_DATA / "referentiel_territoires.csv",
        )


if __name__ == "__main__":
    unittest.main()
