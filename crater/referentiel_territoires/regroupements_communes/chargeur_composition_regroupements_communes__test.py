import unittest
from pathlib import Path

import pandas

from crater.referentiel_territoires.regroupements_communes.chargeur_composition_regroupements_communes import (
    _charger_composition_regroupements_communes_sources,
    _charger_composition_regroupements_communes_pats,
    _ajouter_id_regroupement_aux_noms_des_regroupements_communes_non_uniques,
    charger_composition_regroupements_communes,
    _charger_composition_regroupements_communes_particuliers,
)
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data_composition_regroupements_communes"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")

FICHIERS_SOURCES_REGROUPEMENTS_COMMUNES_TEST = [
    {
        "code": "BV2022",
        "fichiers_sources": [CHEMIN_INPUT_DATA / "bassins_de_vie_2022.xlsx"],
    },
    {
        "code": "PAYS_PETR",
        "fichiers_sources": [CHEMIN_INPUT_DATA / "pays_petr.xlsx"],
    },
]
REGROUPEMENTS_COMMUNES_PARTICULIERS_TEST = [
    {
        "nom": "Regroupement communes particulier 1",
        "id": "AT-1",
        "categorie": "AUTRE_TERRITOIRES",
        "requete": """(id_departement == "D-1")""",
    },
    {
        "nom": "Regroupement communes particulier 2",
        "id": "AT-2",
        "categorie": "AUTRE_TERRITOIRES",
        "requete": """(id_departement == "D-2")""",
    },
]


class TestChargeurRegroupementsCommunes(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)

    def test_charger_composition_regroupements_communes(self):
        # Given
        df_communes = pandas.read_csv(CHEMIN_INPUT_DATA / "communes.csv", sep=";")
        # When
        df_composition_regroupement_communes = charger_composition_regroupements_communes(
            FICHIERS_SOURCES_REGROUPEMENTS_COMMUNES_TEST, CHEMIN_INPUT_DATA / "pats.csv", df_communes, REGROUPEMENTS_COMMUNES_PARTICULIERS_TEST
        )
        # Then
        df_composition_regroupement_communes.to_csv(
            CHEMIN_OUTPUT_DATA / "composition_regroupements_communes.csv", sep=";", encoding="utf-8", index=False
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "composition_regroupements_communes.csv",
            CHEMIN_OUTPUT_DATA / "composition_regroupements_communes.csv",
        )

    def test_charger_composition_regroupements_communes_sources(self):
        # When
        df_composition_regroupement_communes = _charger_composition_regroupements_communes_sources(FICHIERS_SOURCES_REGROUPEMENTS_COMMUNES_TEST)
        # Then
        df_composition_regroupement_communes.to_csv(
            CHEMIN_OUTPUT_DATA / "composition_regroupements_communes_sources.csv", sep=";", encoding="utf-8", index=False
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "composition_regroupements_communes_sources.csv",
            CHEMIN_OUTPUT_DATA / "composition_regroupements_communes_sources.csv",
        )

    def test_charger_composition_regroupements_communes_pat(self):
        # When
        df_composition_pats = _charger_composition_regroupements_communes_pats(CHEMIN_INPUT_DATA / "pats.csv")
        # Then
        df_composition_pats.to_csv(CHEMIN_OUTPUT_DATA / "composition_regroupements_communes_pats.csv", sep=";", encoding="utf-8", index=False)
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "composition_regroupements_communes_pats.csv",
            CHEMIN_OUTPUT_DATA / "composition_regroupements_communes_pats.csv",
        )

    def test_charger_composition_regroupements_communes_particuliers(self):
        # Given
        df_communes = pandas.read_csv(CHEMIN_INPUT_DATA / "communes.csv", sep=";")
        # When
        df_composition_regroupement_communes = _charger_composition_regroupements_communes_particuliers(
            df_communes, REGROUPEMENTS_COMMUNES_PARTICULIERS_TEST
        )
        # Then
        df_composition_regroupement_communes.to_csv(
            CHEMIN_OUTPUT_DATA / "composition_regroupements_communes_particuliers.csv", sep=";", encoding="utf-8", index=False
        )
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "composition_regroupements_communes_particuliers.csv",
            CHEMIN_OUTPUT_DATA / "composition_regroupements_communes_particuliers.csv",
        )

    def test_ajouter_id_regroupement_aux_noms_des_regroupements_communes_non_uniques(self):
        # given
        df = pandas.DataFrame(
            columns=["id_commune", "nom_commune", "id_regroupement_communes", "nom_regroupement_communes"],
            data=[
                ["56000", "A", "BV1", "A"],
                ["56001", "X", "BV1", "A"],
                ["29000", "C", "BV3", "C"],
                ["31000", "C", "BV4", "C"],
            ],
        )
        # when
        df_resultat = _ajouter_id_regroupement_aux_noms_des_regroupements_communes_non_uniques(df)
        # then
        df_resultat_attendu = pandas.DataFrame(
            columns=["id_commune", "nom_commune", "id_regroupement_communes", "nom_regroupement_communes"],
            data=[
                ["56000", "A", "BV1", "A"],
                ["56001", "X", "BV1", "A"],
                ["29000", "C", "BV3", "C (BV3)"],
                ["31000", "C", "BV4", "C (BV4)"],
            ],
        )
        pandas.testing.assert_frame_equal(df_resultat, df_resultat_attendu)


if __name__ == "__main__":
    unittest.main()
