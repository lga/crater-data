from pathlib import Path

from pandas import DataFrame
import geopandas

from crater.referentiel_territoires.regroupements_communes.outils_pats import (
    charger_source_definitions_pats,
    traduire_id_france_pat_vers_id_pat_crater,
)


def charger_donnees_pats(fichier_definitions_pats: Path, fichier_contours_pats: Path) -> DataFrame:
    df_pat = charger_source_definitions_pats(fichier_definitions_pats)
    df_pat = df_pat.set_index("id_pat").loc[:, ["niveaux_de_labelisation"]]

    df_url_pats = geopandas.read_file(fichier_contours_pats, columns=["id", "url"], ignore_geometry=True)
    df_url_pats["id_pat"] = traduire_id_france_pat_vers_id_pat_crater(df_url_pats["id"].astype(str))
    df_url_pats = df_url_pats.set_index("id_pat").loc[:, ["url"]]

    df_pat = df_pat.join(df_url_pats).reset_index()
    return df_pat
