from pathlib import Path

import pandas
from pandas import Series, DataFrame

from crater.referentiel_territoires.regroupements_communes.config.config_regroupements_communes import (
    CATEGORIES_REGROUPEMENTS_COMMUNES,
)


def charger_source_definitions_pats(fichier_definitions_pats: Path) -> DataFrame:
    df_pats = pandas.read_csv(
        fichier_definitions_pats, sep=";", dtype={"id": "str", "nom_administratif": "str", "nom_usuel": "str", "communes_code_insee": "str"}
    )
    mask_pat_valide = (
        df_pats["id"].str.isnumeric()
        & (df_pats["communes_code_insee"].str.len() >= 5)
        & ((df_pats["nom_administratif"] != "?") | (df_pats["nom_usuel"] != "?"))
    )
    df_pats = df_pats.loc[mask_pat_valide, :]

    df_pats["id_pat"] = traduire_id_france_pat_vers_id_pat_crater(df_pats["id"])
    df_pats = df_pats.drop(columns=["id"])

    return df_pats


def traduire_id_france_pat_vers_id_pat_crater(df_colonne: Series) -> Series:
    return CATEGORIES_REGROUPEMENTS_COMMUNES["PAT"]["prefixe_id"] + "-" + df_colonne
