import unittest
from pathlib import Path


from crater.referentiel_territoires.regroupements_communes.chargeur_donnees_pat import charger_donnees_pats
from crater.commun.exports.export_fichier import reinitialiser_dossier
from crater.commun.outils_pour_tests import (
    assert_csv_files_are_equals,
    augmenter_nombre_colonnes_affichees_pandas,
)

augmenter_nombre_colonnes_affichees_pandas()

CHEMIN_TEST_DATA = Path(__file__).parent / "test_data_donnees_pats"
CHEMIN_INPUT_DATA = Path(CHEMIN_TEST_DATA / "input")
CHEMIN_OUTPUT_DATA = Path(CHEMIN_TEST_DATA / "output")
CHEMIN_EXPECTED_DATA = Path(CHEMIN_TEST_DATA / "expected")


class TestChargeurRegroupementsCommunes(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        reinitialiser_dossier(CHEMIN_OUTPUT_DATA)

    def test_charger_donnees_pat(self):
        # When
        df_donnees_pats = charger_donnees_pats(CHEMIN_INPUT_DATA / "pats.csv", CHEMIN_INPUT_DATA / "pats-simplifie.geojson")
        # Then
        df_donnees_pats.to_csv(CHEMIN_OUTPUT_DATA / "donnees_pats.csv", sep=";", encoding="utf-8", index=False)
        assert_csv_files_are_equals(
            CHEMIN_EXPECTED_DATA / "donnees_pats.csv",
            CHEMIN_OUTPUT_DATA / "donnees_pats.csv",
        )


if __name__ == "__main__":
    unittest.main()
