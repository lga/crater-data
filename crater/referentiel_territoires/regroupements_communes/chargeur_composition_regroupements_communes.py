from pathlib import Path

import numpy as np
import pandas
import pandas as pd
from pandas import DataFrame

from crater.referentiel_territoires.regroupements_communes.config.config_regroupements_communes import (
    CATEGORIES_REGROUPEMENTS_COMMUNES,
    REGROUPEMENTS_COMMUNES_PARTICULIERS,
    RegroupementCommunesParticulier,
)
from crater.referentiel_territoires.regroupements_communes.outils_pats import (
    charger_source_definitions_pats,
)
from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.calculs.outils_verification import (
    verifier_absence_na,
    verifier_nommage_ids_territoires,
    verifier_colonne2_unique_par_colonne1,
    verifier_absence_doublons,
)
from crater.commun.logger import log
from crater.config.config_globale import ANNEE_REFERENTIEL_TERRITOIRES_INSEE
from crater.config.config_sources import DOSSIER_PNR, FichiersRegroupementsCommunes


def charger_composition_regroupements_communes(
    fichiers_regroupements_communes: list[FichiersRegroupementsCommunes],
    fichier_definitions_pats: Path,
    communes: DataFrame,
    regroupements_communes_particuliers: list[RegroupementCommunesParticulier] = REGROUPEMENTS_COMMUNES_PARTICULIERS,
) -> DataFrame:
    if ANNEE_REFERENTIEL_TERRITOIRES_INSEE != 2024:
        raise ValueError(
            "ERREUR lors du chargement des regroupements de communes. L'année du référentiel de communes INSEE n'est pas reconnue. "
            "Il faut la modifier en adaptant si besoin les traitements à effectuer :"
            "\n 1. maj regroupements de communes importés depuis crater-data-source (voir fichiers_regroupements_communes)"
            "\n 2. maj regroupements de communes particuliers importés via REGROUPEMENTS_COMMUNES_PARTICULIERS (vérifier notamment que les id des "
            "territoires utilisés n'ont pas changé)"
            "\n 3. maj regroupements de communes importés depuis la source France PAT"
        )

    composition_regroupements_communes_sources = _charger_composition_regroupements_communes_sources(fichiers_regroupements_communes)
    composition_regroupements_communes_pat = _charger_composition_regroupements_communes_pats(fichier_definitions_pats)
    composition_regroupements_communes_particuliers = _charger_composition_regroupements_communes_particuliers(
        communes, regroupements_communes_particuliers
    )

    composition_regroupements_communes = pd.concat(
        [composition_regroupements_communes_sources, composition_regroupements_communes_pat, composition_regroupements_communes_particuliers]
    )

    return composition_regroupements_communes


def _charger_composition_regroupements_communes_sources(fichiers_regroupements_communes: list[FichiersRegroupementsCommunes]) -> DataFrame:
    df = pd.DataFrame(
        columns=[
            "id_commune",
            "nom_commune",
            "id_regroupement_communes",
            "nom_regroupement_communes",
            "categorie_regroupement_communes",
        ]
    )
    df2 = pd.DataFrame(columns=["id_regroupement_communes"])

    for rc in fichiers_regroupements_communes:
        categorie_regroupement_communes = CATEGORIES_REGROUPEMENTS_COMMUNES[rc["code"]]["categorie"]
        prefixe_id_regroupement_communes = CATEGORIES_REGROUPEMENTS_COMMUNES[rc["code"]]["prefixe_id"]
        for f in rc["fichiers_sources"]:
            log.info(
                "   => chargement de la composition des regroupements de communes %s depuis %s",
                categorie_regroupement_communes,
                f,
            )
            df_a_ajouter = pd.read_excel(f, sheet_name=0, usecols="A:D", skiprows=4, dtype="str", na_values=[""])
            df_a_ajouter.columns = [  # type: ignore[assignment]  # TODO: open issue https://github.com/pandas-dev/pandas-stubs/issues/73
                "id_commune",
                "nom_commune",
                "id_regroupement_communes",
                "nom_regroupement_communes",
            ]
            df_a_ajouter = _dupliquer_communes_appartenant_a_plusieurs_regroupements(f, df_a_ajouter)
            df_a_ajouter = df_a_ajouter.loc[~df_a_ajouter["id_regroupement_communes"].isnull()]
            df_a_ajouter["id_regroupement_communes"] = prefixe_id_regroupement_communes + "-" + df_a_ajouter["id_regroupement_communes"]
            df_a_ajouter["categorie_regroupement_communes"] = categorie_regroupement_communes

            if categorie_regroupement_communes == CATEGORIES_REGROUPEMENTS_COMMUNES["BV2022"]["categorie"]:
                df_a_ajouter = _ajouter_id_regroupement_aux_noms_des_regroupements_communes_non_uniques(df_a_ajouter)
                df_a_ajouter["nom_regroupement_communes"] = "Bassin de vie de " + df_a_ajouter["nom_regroupement_communes"]

            if categorie_regroupement_communes == CATEGORIES_REGROUPEMENTS_COMMUNES["PAYS_PETR"]["categorie"]:
                mask_pays_petr_a_renommer = df_a_ajouter["nom_regroupement_communes"].str.contains("COMMUNAUTE", regex=False)
                df_a_ajouter.loc[mask_pays_petr_a_renommer, "nom_regroupement_communes"] = (
                    "PAYS/PETR " + df_a_ajouter.loc[mask_pays_petr_a_renommer, "nom_regroupement_communes"]
                )

            verifier_absence_na(df_a_ajouter, "id_commune", "id_regroupement_communes")
            verifier_absence_na(df_a_ajouter, "id_commune", "nom_regroupement_communes")
            verifier_nommage_ids_territoires(df_a_ajouter, "id_regroupement_communes")
            verifier_colonne2_unique_par_colonne1(df_a_ajouter, "id_regroupement_communes", "nom_regroupement_communes")
            verifier_colonne2_unique_par_colonne1(
                df_a_ajouter,
                "nom_regroupement_communes",
                "id_regroupement_communes",
            )

            df = pd.concat([df, df_a_ajouter])
            df2 = pd.concat(
                [
                    df2,
                    df_a_ajouter.loc[:, ["id_regroupement_communes"]].drop_duplicates(),
                ]
            )

    verifier_absence_doublons(df2, "id_regroupement_communes")
    df = _traduire_code_insee_vers_id_crater(df)

    return df


def _ajouter_id_regroupement_aux_noms_des_regroupements_communes_non_uniques(df):
    df = df.copy()

    df["nom_regroupement_communes_avec_id_regroupement"] = df["nom_regroupement_communes"] + " (" + df["id_regroupement_communes"] + ")"
    regroupements_communes_avec_nom_non_unique = (
        df.loc[:, ["id_regroupement_communes", "nom_regroupement_communes"]]
        .drop_duplicates()
        .groupby("nom_regroupement_communes")
        .count()
        .rename(columns={"id_regroupement_communes": "nb"})
        .query("nb > 1")
        .index.to_list()
    )
    mask_regroupements_communes_avec_nom_non_unique = df.nom_regroupement_communes.isin(regroupements_communes_avec_nom_non_unique)
    df.loc[mask_regroupements_communes_avec_nom_non_unique, "nom_regroupement_communes"] = df.loc[
        mask_regroupements_communes_avec_nom_non_unique, "nom_regroupement_communes_avec_id_regroupement"
    ]
    df.drop(columns=["nom_regroupement_communes_avec_id_regroupement"], inplace=True)

    return df


def _dupliquer_communes_appartenant_a_plusieurs_regroupements(fichier_source, df):
    if str(fichier_source).find(DOSSIER_PNR) >= 0:
        if fichier_source.name == "com2024_2025-02-04.xlsx":
            df = _dupliquer_commune_Porte_de_Savoie_appartenant_a_2_PNR(df)
        else:
            raise ValueError(
                "ERREUR lors du chargement des PNRs. Le fichier source n'est pas connu :"
                "\nIl faut l'ajouter en renseignant si besoin les traitements à effectuer."
                "\nIl peut s'agir de dupliquer des communes à cheval sur plusieurs PNR."
                "\nUne erreur dans l'id généré devrait ressortir dans ce cas là du fait de la présence d'espaces (eg. 'FR8000031 et FR8000004')."
                "\nPour trouver les communes problématiques, rechercher 'et PNR' dans la colonne 'pnr_libgeo' du fichier des PNR."
            )
    return df


def _dupliquer_commune_Porte_de_Savoie_appartenant_a_2_PNR(df) -> DataFrame:
    # dans le fichier des PNR de l'observatoire des territoires,
    # la commune Porte-de-Savoie (73151) appartient à 2 PNR et n'apparait que sur une ligne (PNR = 'PNR du Massif des Bauges et PNR de Chartreuse')
    mask_commune = df.id_commune == "73151"
    df.loc[mask_commune, "id_regroupement_communes"] = df.loc[mask_commune].id_regroupement_communes.str.split(" et ")
    df.loc[mask_commune, "nom_regroupement_communes"] = df.loc[mask_commune].nom_regroupement_communes.str.split(" et ")
    df = df.explode(["id_regroupement_communes", "nom_regroupement_communes"])

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    return df


def _charger_composition_regroupements_communes_particuliers(
    communes: DataFrame, regroupements_communes_particuliers: list[RegroupementCommunesParticulier]
) -> DataFrame:
    log.info("   => ajout des regroupements de communes particuliers")
    composition_regroupements_communes_particuliers = pandas.DataFrame()
    for row in regroupements_communes_particuliers:
        communes_regroupement = communes.query(row["requete"]).loc[:, ["id_commune", "nom_commune"]]
        communes_regroupement["id_regroupement_communes"] = row["id"]
        communes_regroupement["nom_regroupement_communes"] = row["nom"]
        communes_regroupement["categorie_regroupement_communes"] = row["categorie"]

        composition_regroupements_communes_particuliers = pandas.concat([composition_regroupements_communes_particuliers, communes_regroupement])

    return composition_regroupements_communes_particuliers


def _charger_composition_regroupements_communes_pats(fichier_definitions_pats: Path) -> DataFrame:

    df_france_pat = charger_source_definitions_pats(fichier_definitions_pats)

    df_france_pat = df_france_pat.loc[:, ["id_pat", "nom_administratif", "nom_usuel", "communes_code_insee"]].rename(
        columns={"id_pat": "id_regroupement_communes"}
    )

    verifier_absence_doublons(df_france_pat, "id_regroupement_communes")

    df_france_pat["categorie_regroupement_communes"] = CATEGORIES_REGROUPEMENTS_COMMUNES["PAT"]["categorie"]
    df_france_pat["nom_regroupement_communes"] = df_france_pat["nom_administratif"].replace("?", pandas.NA).fillna(df_france_pat["nom_usuel"])

    PREFIXES_NOMS_PAT_VALIDES = ("PAT", "PAiT", "PAAT")
    df_france_pat["nom_regroupement_communes"] = np.where(
        df_france_pat["nom_administratif"].str.startswith(PREFIXES_NOMS_PAT_VALIDES),
        df_france_pat["nom_administratif"],
        np.where(
            df_france_pat["nom_usuel"].str.startswith(PREFIXES_NOMS_PAT_VALIDES),
            df_france_pat["nom_usuel"],
            np.nan,
        ),
    )
    df_france_pat = _corriger_noms_PATs_invalides_2024(df_france_pat)
    verifier_absence_na(df_france_pat, "id_regroupement_communes", "nom_regroupement_communes")

    df_france_pat["id_commune"] = df_france_pat["communes_code_insee"].str.split(",")
    df_france_pat = df_france_pat.explode("id_commune")
    df_france_pat["id_commune"] = traduire_code_insee_vers_id_commune_crater(df_france_pat["id_commune"])

    return df_france_pat.loc[:, ["id_commune", "id_regroupement_communes", "nom_regroupement_communes", "categorie_regroupement_communes"]]


def _corriger_noms_PATs_invalides_2024(df_france_pat: DataFrame) -> DataFrame:
    df_france_pat.loc[df_france_pat["id_regroupement_communes"] == "PAT-891", ["nom_regroupement_communes"]] = "PAT de Redon Agglomération"
    df_france_pat.loc[df_france_pat["id_regroupement_communes"] == "PAT-1333", ["nom_regroupement_communes"]] = "PAT de Norge et Tille"
    return df_france_pat
