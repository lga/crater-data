from typing import TypedDict

CATEGORIES_REGROUPEMENTS_COMMUNES = {
    "ND": {"categorie": "NOUVEAU_DEPARTEMENT", "prefixe_id": "ND"},
    "BV2022": {"categorie": "BASSIN_DE_VIE_2022", "prefixe_id": "BV2022"},
    "PN": {"categorie": "PARC_NATIONAL", "prefixe_id": "PN"},
    "PNR": {"categorie": "PARC_NATUREL_REGIONAL", "prefixe_id": "PNR"},
    "PAYS_PETR": {"categorie": "PAYS_PETR", "prefixe_id": "PAYS_PETR"},
    "SCOT": {"categorie": "SCHEMA_COHERENCE_TERRITORIAL", "prefixe_id": "SCOT"},
    "PAT": {"categorie": "PROJET_ALIMENTAIRE_TERRITORIAL", "prefixe_id": "PAT"},
    "AT": {"categorie": "AUTRE_TERRITOIRE", "prefixe_id": "AT"},
}


# Typed dict pour les éléments de REGROUPEMENTS_COMMUNES_PARTICULIERS
class RegroupementCommunesParticulier(TypedDict):
    nom: str
    id: str
    categorie: str
    requete: str


REGROUPEMENTS_COMMUNES_PARTICULIERS: list[RegroupementCommunesParticulier] = [
    {
        # composition = Département du Rhône (D-69) avec la Métropole de Lyon (E-200046977) en moins
        "nom": "Nouveau-Rhône",
        "id": CATEGORIES_REGROUPEMENTS_COMMUNES["ND"]["prefixe_id"] + "-69D",
        "categorie": CATEGORIES_REGROUPEMENTS_COMMUNES["ND"]["categorie"],
        "requete": """(id_departement == "D-69") & (id_epci != "E-200046977")""",
    },
    {
        # composition = départements Bas-Rhin (D-67) + Haut-Rhin (D-68)
        "nom": "Collectivité Européenne d'Alsace",
        "id": CATEGORIES_REGROUPEMENTS_COMMUNES["AT"]["prefixe_id"] + "-CEA",
        "categorie": CATEGORIES_REGROUPEMENTS_COMMUNES["AT"]["categorie"],
        "requete": """id_departement in ['D-67', 'D-68']""",
    },
    {
        # composition =
        # CA Pau Béarn Pyrénées (200067254) +
        # CC du Nord Est Béarn (200067296) +
        # CC des Luys en Béarn (200067239) +
        # CC de Lacq-Orthez (200039204) +
        # CC du Béarn des Gaves (200067288) +
        # CC du Haut Béarn (200067262) +
        # CC de la Vallée d'Ossau (246400337) +
        # CC Pays de Nay (246401756)
        "nom": "Pôle métropolitain du Pays de Béarn",
        "id": "PAYS_DE_BEARN",
        "categorie": CATEGORIES_REGROUPEMENTS_COMMUNES["AT"]["categorie"],
        "requete": "id_epci in ['E-200067254', 'E-200067296', 'E-200067239', 'E-200039204', \
        'E-200067288', 'E-200067262', 'E-246400337', 'E-246401756']",
    },
]
