from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_commune_crater,
    traduire_code_insee_vers_id_epci_crater,
    traduire_code_insee_vers_id_departement_crater,
    traduire_code_insee_vers_id_region_crater,
)
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.logger import log


def charger_communes(chemin_fichier: Path) -> DataFrame:
    log.info("   => chargement communes depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="COM",
            usecols="A:F",
            skiprows=5,
            dtype={
                "CODGEO": "str",
                "LIBGEO": "str",
                "DEP": "str",
                "REG": "str",
                "EPCI": "str",
            },
            na_values=[""],
        )
        .rename(
            columns={
                "CODGEO": "id_commune",
                "LIBGEO": "nom_commune",
                "EPCI": "id_epci",
                "DEP": "id_departement",
                "REG": "id_region",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_commune",
                "nom_commune",
                "id_epci",
                "id_departement",
                "id_region",
            ]
        )
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_commune")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    df.id_epci = traduire_code_insee_vers_id_epci_crater(df.id_epci)
    df.id_departement = traduire_code_insee_vers_id_departement_crater(df.id_departement)
    df.id_region = traduire_code_insee_vers_id_region_crater(df.id_region)
    return df
