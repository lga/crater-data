from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import (
    traduire_code_insee_vers_id_departement_crater,
    traduire_code_insee_vers_id_region_crater,
    traduire_code_insee_vers_id_commune_crater,
)
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.logger import log


def charger_departements(chemin_fichier: Path) -> DataFrame:
    log.info("   => chargement départements depuis %s", chemin_fichier)
    df = (
        pd.read_csv(
            chemin_fichier,
            sep=",",
            dtype={
                "dep": "str",
                "reg": "str",
                "libelle": "str",
                "cheflieu": "str",
                # Noms de colonnes en majuscule dans le fichier source à partir de 2021 => on prend les 2 formats pour retro compatibilité
                "DEP": "str",
                "REG": "str",
                "LIBELLE": "str",
                "CHEFLIEU": "str",
            },
            na_values=[""],
        )
        .rename(
            columns={
                "dep": "id_departement",
                "reg": "id_region",
                "libelle": "nom_departement",
                "cheflieu": "id_cheflieu",
                "DEP": "id_departement",
                "REG": "id_region",
                "LIBELLE": "nom_departement",
                "CHEFLIEU": "id_cheflieu",
            },
        )
        .reindex(columns=["id_departement", "nom_departement", "id_region", "id_cheflieu"])
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_departement")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_departement = traduire_code_insee_vers_id_departement_crater(df.id_departement)
    df.id_region = traduire_code_insee_vers_id_region_crater(df.id_region)
    df.id_cheflieu = traduire_code_insee_vers_id_commune_crater(df.id_cheflieu)
    return df
