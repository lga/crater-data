from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.calculs.outils_verification import verifier_absence_doublons
from crater.commun.logger import log


def charger_arrondissements(chemin_fichier: Path) -> DataFrame:
    log.info("   => chargement arrondissements depuis %s", chemin_fichier)

    df = (
        pd.read_excel(
            chemin_fichier,
            sheet_name="ARM",
            skiprows=5,
            # A partir de pandas 2, erreur sur usecols si une colonne n'existe pas dans le fichier
            # on n'indique pas de param usecols ici, pour permettre la retro compatibilité entre les fichiers sources
            # (à partir de 2021 la colonne COM est en position R et plus en C)
            dtype={"CODGEO": "str", "LIBGEO": "str", "COM": "str"},
            na_values=[""],
        )
        .rename(
            columns={
                "CODGEO": "id_arrondissement",
                "LIBGEO": "nom_arrondissement",
                "COM": "id_commune",
            },
            errors="raise",
        )
        .reindex(columns=["id_arrondissement", "nom_arrondissement", "id_commune"])
    )

    df = _traduire_code_insee_vers_id_crater(df)
    verifier_absence_doublons(df, "id_arrondissement")

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune = traduire_code_insee_vers_id_commune_crater(df.id_commune)
    df.id_arrondissement = traduire_code_insee_vers_id_commune_crater(df.id_arrondissement)
    return df
