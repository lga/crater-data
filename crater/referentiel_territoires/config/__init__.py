# Chemin vers config, peut être redéfinie par les tests pour utiliser une config simplifiée
from pathlib import Path

FICHIER_CONFIG_GENRES_NOMBRES_PREPOSITIONS_TERRITOIRES: Path = Path(__file__).parent / "genres_nombres_prepositions_territoires.csv"
