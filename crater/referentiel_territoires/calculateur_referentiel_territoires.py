from pathlib import Path

import numpy as np
import pandas as pd
from pandas import DataFrame, Series
from crater.referentiel_territoires import chargeur_territoires_parcel, config
from crater.referentiel_territoires.chargeur_arrondissements import charger_arrondissements
from crater.referentiel_territoires.chargeur_codes_postaux import charger_codes_postaux
from crater.referentiel_territoires.chargeur_communes import charger_communes
from crater.referentiel_territoires.chargeur_departements import charger_departements
from crater.referentiel_territoires.chargeur_epcis import charger_epcis
from crater.referentiel_territoires.chargeur_regions import charger_regions
from crater.referentiel_territoires.regroupements_communes.chargeur_composition_regroupements_communes import (
    charger_composition_regroupements_communes,
)
from crater.referentiel_territoires.regroupements_communes.chargeur_donnees_pat import charger_donnees_pats

from crater.commun.calculs.mouvements_communes.chargeur_mouvements_communes import charger_fusions_scissions_communes
from crater.commun.calculs.outils_dataframes import DictDataFrames, normaliser_chaine_caractere, merge_strict
from crater.commun.calculs.outils_territoires import (
    obtenir_arrondissements_depuis_territoires,
    trier_territoires,
    extraire_df_communes_appartenant_a_une_categorie_de_territoire,
    traduire_categorie_territoire_vers_id_categorie_territoire,
    vider_colonne_selon_mouvements_communes,
)
from crater.commun.calculs.outils_verification import verifier_absence_doublons, verifier_absence_na
from crater.commun.exports.export_fichier import reinitialiser_dossier, exporter_df_indicateurs_par_territoires
from crater.commun.logger import log
from crater.config.config_globale import (
    IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX,
    ANNEE_REFERENTIEL_TERRITOIRES_INSEE,
    NUMEROS_REGIONS_HORS_DROM,
    NUMEROS_DEPARTEMENTS_DROM,
    ID_FRANCE,
)
from crater.config.config_sources import ANNEE_REFERENTIEL_TERRITOIRES_PARCEL, FichiersRegroupementsCommunes


def calculer_referentiel_territoires(
    chemin_fichier_communes_et_arrondissements: Path,
    chemin_fichier_mouvements_communes: Path,
    chemin_fichier_epcis: Path,
    chemin_fichier_departements: Path,
    chemin_fichier_regions: Path,
    chemin_fichier_codes_postaux: Path,
    fichiers_regroupements_communes: list[FichiersRegroupementsCommunes],
    chemin_fichier_definitions_pats: Path,
    chemin_fichier_contours_pats: Path,
    chemin_fichier_correspondance_territoires_parcel_crater: Path,
    chemin_dossier_output: Path,
) -> Path:
    log.info("##### CALCUL DU REFERENTIEL TERRITOIRES #####")

    reinitialiser_dossier(chemin_dossier_output)

    sources_territoires = _charger_sources_et_config_territoires(
        chemin_fichier_communes_et_arrondissements,
        chemin_fichier_mouvements_communes,
        chemin_fichier_epcis,
        chemin_fichier_departements,
        chemin_fichier_regions,
        chemin_fichier_codes_postaux,
        fichiers_regroupements_communes,
        chemin_fichier_definitions_pats,
        chemin_fichier_correspondance_territoires_parcel_crater,
    )
    territoires = _generer_referentiel_territoires(sources_territoires, chemin_fichier_definitions_pats, chemin_fichier_contours_pats)

    return exporter_df_indicateurs_par_territoires(territoires, chemin_dossier_output, "referentiel_territoires")


def _generer_referentiel_territoires(sources: DictDataFrames, chemin_fichier_definitions_pats: Path, chemin_fichier_contours_pats: Path) -> DataFrame:
    territoires = _assembler_differents_territoires(sources)

    territoires = _ajouter_id_pays_aux_territoires(territoires)

    territoires.loc["D-75", "nom_territoire"] = "Paris, département"
    if ANNEE_REFERENTIEL_TERRITOIRES_INSEE == 2024:
        territoires.loc["E-200030385", "nom_territoire"] = "Communauté d'agglomération de Blois Agglopolys"
    else:
        raise ValueError(
            "ERREUR lors du renommage de l'EPCI de Blois."
            "\nLe fichier source n'est pas connu."
            "\nMerci de vérifier que le traitement est correct pour le référentiel utilisé."
            "\nIl faut notamment chercher qu'il n'y a pas usage de guillements ("
            ") dans les noms d'EPCI."
        )
    territoires.loc[territoires.categorie_territoire == "EPCI", "nom_territoire"] = _modifier_noms_epcis(
        territoires.loc[territoires.categorie_territoire == "EPCI", "nom_territoire"]
    )

    territoires = _ajouter_regroupements_communes_aux_communes(territoires, sources["composition_regroupements_communes"])

    territoires = _ajouter_arrondissements(territoires, sources["arrondissements"])

    territoires = _ajouter_codes_postaux_communes(territoires, sources["codes_postaux"])

    territoires = _ajouter_annee_dernier_mouvement_communes(territoires, sources["mouvements_communes"])

    territoires = _ajouter_id_departement_et_region_aux_epcis_et_regroupements_communes(territoires)

    territoires = _ajouter_id_nom_territoire(territoires)

    territoires = _ajouter_colonne_id_territoire_parcel(territoires, sources["territoires_parcel"])

    territoires = _ajouter_nombre_de_communes_par_territoires(territoires)

    territoires = _ajouter_est_cheflieu_departements(territoires, sources["departements"])

    territoires = _ajouter_genres_nombres_prepositions_noms(territoires, sources["genres_nombres_prepositions"])

    territoires = _ajouter_colonnes_donnees_pat(territoires, chemin_fichier_definitions_pats, chemin_fichier_contours_pats)

    territoires = _reordonner_colonnes(territoires)

    territoires = trier_territoires(territoires)

    return territoires


def _assembler_differents_territoires(sources: DictDataFrames) -> DataFrame:
    communes = _normaliser_df_territoires(sources["communes"], "id_commune", "nom_commune", "COMMUNE")
    epcis = _normaliser_df_territoires(sources["epcis"], "id_epci", "nom_epci", "EPCI")
    regroupements_communes = (
        sources["composition_regroupements_communes"]
        .loc[
            :,
            ["id_regroupement_communes", "nom_regroupement_communes", "categorie_regroupement_communes"],
        ]
        .drop_duplicates()
    )
    regroupements_communes = _normaliser_df_territoires(
        regroupements_communes,
        "id_regroupement_communes",
        "nom_regroupement_communes",
        "REGROUPEMENT_COMMUNES",
    )
    departements = _normaliser_df_territoires(sources["departements"], "id_departement", "nom_departement", "DEPARTEMENT")
    regions = _normaliser_df_territoires(sources["regions"], "id_region", "nom_region", "REGION")
    pays = _creer_base_de_donnees_pays()

    territoires = pd.concat(
        [
            pays.sort_values(by=["id_territoire"]),
            regions.sort_values(by=["id_territoire"]),
            departements.sort_values(by=["id_territoire"]),
            regroupements_communes.sort_values(by=["id_territoire"]),
            epcis.sort_values(by=["id_territoire"]),
            communes.sort_values(by=["id_territoire"]),
        ],
        ignore_index=True,
    ).set_index("id_territoire")

    return territoires


def _normaliser_df_territoires(df_territoires, colonne_id, colonne_nom, categorie):
    df = df_territoires.copy()
    df.rename(
        {colonne_id: "id_territoire", colonne_nom: "nom_territoire"},
        axis=1,
        inplace=True,
        errors="raise",
    )
    df.insert(loc=2, column="categorie_territoire", value=categorie)

    return df


def _reordonner_colonnes(territoires: DataFrame) -> DataFrame:
    territoires = territoires.reindex(
        columns=[
            "id_nom_territoire",
            "nom_territoire",
            "categorie_territoire",
            "id_pays",
            "id_region",
            "id_departement",
            "ids_regroupements_communes",
            "categorie_regroupement_communes",
            "id_epci",
            "categorie_epci",
            "ids_arrondissements_commune",
            "codes_postaux_commune",
            "annee_dernier_mouvement_commune",
            "type_mouvement_commune",
            "est_cheflieu_departement",
            "niveau_labellisation_pat",
            "id_url_france_pat",
            "id_territoire_parcel",
            "nb_communes",
            "genre_nombre",
            "preposition",
        ],
        fill_value="missing",
    )

    return territoires


def _ajouter_annee_dernier_mouvement_communes(territoires: DataFrame, mouvements_communes: DataFrame) -> DataFrame:
    synthese_mouvements_communes = (
        pd.concat(
            [
                mouvements_communes.rename(columns={"id_commune_final": "id_commune"}),
                mouvements_communes.rename(columns={"id_commune_initial": "id_commune"}),
            ]
        )
        .sort_values(by=["annee_modification"])
        .drop_duplicates(subset=["id_commune"], keep="last")
        .loc[:, ["id_commune", "annee_modification", "type"]]
        .rename(
            columns={
                "id_commune": "id_territoire",
                "annee_modification": "annee_dernier_mouvement_commune",
                "type": "type_mouvement_commune",
            }
        )
        .set_index("id_territoire")
    )

    territoires = territoires.join(synthese_mouvements_communes)

    return territoires


def _creer_base_de_donnees_pays() -> DataFrame:
    pays = pd.DataFrame(
        {
            "id_territoire": [ID_FRANCE],
            "nom_territoire": ["France"],
            "categorie_territoire": ["PAYS"],
        }
    )
    return pays


def _ajouter_id_pays_aux_territoires(territoires: DataFrame) -> DataFrame:
    territoires.loc[territoires.categorie_territoire != "PAYS", "id_pays"] = ID_FRANCE

    return territoires


def _modifier_noms_epcis(serie: Series) -> Series:
    return (
        serie.replace("^(CC)", "Communauté de communes", regex=True)
        .replace("^(CA)", "Communauté d'agglomération", regex=True)
        .replace("^(CU)", "Communauté urbaine", regex=True)
        .replace(" 4 ", " Quatre ", regex=True)  # cas d'une des trois "communauté des quatre rivières"
        .replace(" \\(.*?\\)", "", regex=True)  # pour supprimer les choses entre parentheses
        .str.strip()
    )


def _ajouter_id_nom_territoire(territoires: DataFrame) -> DataFrame:
    territoires = territoires.copy()

    departements = (
        territoires.loc[territoires.categorie_territoire == "DEPARTEMENT", ["nom_territoire"]]
        .reset_index()
        .rename(columns={"id_territoire": "id_departement", "nom_territoire": "nom_departement"})
    )
    territoires = pd.merge(territoires.reset_index(), departements, on="id_departement", how="left").set_index("id_territoire")

    territoires["id_nom_territoire"] = normaliser_chaine_caractere(territoires.nom_territoire)

    territoires_avec_nom_non_unique = (
        territoires.loc[:, ["id_nom_territoire"]]
        .groupby(territoires.id_nom_territoire)
        .count()
        .rename(columns={"id_nom_territoire": "nb"})
        .query("nb > 1")
    )
    mask_communes_et_epcis_avec_nom_non_unique = territoires.categorie_territoire.isin(["COMMUNE", "EPCI"]) & (
        territoires.id_nom_territoire.isin(territoires_avec_nom_non_unique.index)
    )
    territoires.loc[mask_communes_et_epcis_avec_nom_non_unique, "id_nom_territoire"] = (
        territoires.loc[mask_communes_et_epcis_avec_nom_non_unique, "id_nom_territoire"]
        + "_("
        + normaliser_chaine_caractere(territoires.loc[mask_communes_et_epcis_avec_nom_non_unique, "nom_departement"])
        + ")"
    )
    territoires.drop(columns=["nom_departement"], inplace=True)

    verifier_absence_doublons(territoires.reset_index(), "id_territoire")
    verifier_absence_doublons(territoires, "id_nom_territoire")

    return territoires


def _ajouter_colonne_id_territoire_parcel(territoires: DataFrame, territoires_parcel: DataFrame) -> DataFrame:
    id_territoires_parcel = territoires_parcel.loc[:, ["id_territoire", "id_territoire_parcel"]].set_index("id_territoire")

    territoires = territoires.join(id_territoires_parcel)
    territoires = vider_colonne_selon_mouvements_communes(territoires, ANNEE_REFERENTIEL_TERRITOIRES_PARCEL, "id_territoire_parcel")

    verifier_absence_doublons(territoires.reset_index(), "id_territoire")

    return territoires


def _ajouter_nombre_de_communes_par_territoires(territoires: DataFrame) -> DataFrame:
    territoires["nb_communes"] = 0

    territoires.loc[territoires["categorie_territoire"] == "COMMUNE", "nb_communes"] = 1

    for id_echelle in IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX:
        territoires.update(territoires.loc[territoires["categorie_territoire"] == "COMMUNE"].groupby(id_echelle).size().rename("nb_communes"))

    regroupements_communes = pd.DataFrame(territoires.ids_regroupements_communes.str.split("|"))
    regroupements_communes = regroupements_communes.explode("ids_regroupements_communes")
    territoires.update(regroupements_communes.groupby("ids_regroupements_communes").size().rename("nb_communes"))

    territoires["nb_communes"] = territoires["nb_communes"].astype("int")

    return territoires


def _ajouter_id_departement_et_region_aux_epcis_et_regroupements_communes(
    territoires: DataFrame,
) -> DataFrame:
    territoires = _ajouter_id_departement_et_region_aux_ensembles_de_communes(territoires, "EPCI")
    territoires = _ajouter_id_departement_et_region_aux_ensembles_de_communes(territoires, "REGROUPEMENT_COMMUNES")
    return territoires


def _ajouter_id_departement_et_region_aux_ensembles_de_communes(territoires: DataFrame, categorie_ensemble_de_communes: str) -> DataFrame:
    # NB on attribue le département (et donc la région) qui contient le plus de communes de l'ensemble de communes
    communes = extraire_df_communes_appartenant_a_une_categorie_de_territoire(territoires, categorie_ensemble_de_communes)
    id_ensemble_de_communes = traduire_categorie_territoire_vers_id_categorie_territoire(categorie_ensemble_de_communes)

    nb_communes_par_ensemble_de_communes_et_departement = (
        communes.groupby([id_ensemble_de_communes, "id_departement"])
        .size()
        .reset_index()
        .rename(columns={0: "nb_communes_par_ensemble_de_communes_et_departement"})
    )
    index_lignes_avec_max_nb_communes = (
        nb_communes_par_ensemble_de_communes_et_departement.reset_index()
        .groupby([id_ensemble_de_communes])["nb_communes_par_ensemble_de_communes_et_departement"]
        .idxmax()
    )

    departements_appartenance_ensemble_de_communes = nb_communes_par_ensemble_de_communes_et_departement.loc[index_lignes_avec_max_nb_communes]

    departements_regions = (
        territoires.query("categorie_territoire == 'DEPARTEMENT'")
        .loc[:, ["id_region"]]
        .reset_index()
        .rename(columns={"id_territoire": "id_departement"})
    )

    departements_appartenance_ensemble_de_communes = pd.merge(
        departements_appartenance_ensemble_de_communes,
        departements_regions,
        how="left",
        on="id_departement",
    ).set_index(id_ensemble_de_communes)

    territoires.update(departements_appartenance_ensemble_de_communes)

    return territoires


def _ajouter_regroupements_communes_aux_communes(
    territoires: DataFrame,
    composition_regroupements_communes: DataFrame,
) -> DataFrame:
    regroupements_communes = (
        (composition_regroupements_communes.loc[:, ["id_commune", "id_regroupement_communes"]])
        .sort_values(by=["id_commune", "id_regroupement_communes"])
        .groupby(["id_commune"], as_index=False)
        .agg({"id_regroupement_communes": "|".join})
        .rename(
            columns={
                "id_regroupement_communes": "ids_regroupements_communes",
                "id_commune": "id_territoire",
            }
        )
        .set_index("id_territoire")
    )

    territoires = territoires.join(regroupements_communes)

    return territoires


def _ajouter_arrondissements(
    territoires: DataFrame,
    arrondissements: DataFrame,
) -> DataFrame:
    arrondissements = (
        (arrondissements.loc[:, ["id_commune", "id_arrondissement"]])
        .drop_duplicates()
        .sort_values(by=["id_commune", "id_arrondissement"])
        .groupby(["id_commune"], as_index=False)
        .agg({"id_arrondissement": "|".join})
        .rename(
            columns={
                "id_arrondissement": "ids_arrondissements_commune",
                "id_commune": "id_territoire",
            }
        )
        .set_index("id_territoire")
    )

    territoires = territoires.join(arrondissements)

    return territoires


def _ajouter_codes_postaux_communes(territoires: DataFrame, codes_postaux: DataFrame) -> DataFrame:
    # les codes postaux sont donnés par arrondissement ou commune quand celle-ci n'a pas d'arrondissements
    # on doit donc remplacer l'id arrondissement par l'id de sa commune le cas échéant
    codes_postaux_final = codes_postaux.copy()
    arrondissements = obtenir_arrondissements_depuis_territoires(territoires)
    codes_postaux_final = ajouter_id_commune_de_arrondissement(codes_postaux_final, arrondissements)
    codes_postaux_final = (
        (codes_postaux_final.loc[:, ["id_commune", "code_postal"]])
        .drop_duplicates()
        .sort_values(by=["id_commune", "code_postal"])
        .groupby(["id_commune"], as_index=False)
        .agg({"code_postal": "|".join})
        .rename(
            columns={
                "code_postal": "codes_postaux_commune",
                "id_commune": "id_territoire",
            }
        )
        .set_index("id_territoire")
    )

    territoires = territoires.join(codes_postaux_final)

    return territoires


def ajouter_id_commune_de_arrondissement(df: DataFrame, arrondissements: DataFrame) -> DataFrame:
    df = merge_strict(
        df,
        arrondissements[["id_arrondissement", "id_commune"]],
        how="left",
        left_on="id_commune_ou_arrondissement",
        right_on="id_arrondissement",
        correspondance="lache",
    )
    df["id_commune"] = np.where(df["id_commune"].isnull(), df["id_commune_ou_arrondissement"], df["id_commune"])

    return df


def _ajouter_est_cheflieu_departements(territoires: DataFrame, departements: DataFrame) -> DataFrame:
    territoires_cheflieu = territoires.copy().reset_index()
    territoires_cheflieu["est_cheflieu_departement"] = territoires_cheflieu["id_territoire"].isin(departements["id_cheflieu"])
    return territoires_cheflieu.set_index("id_territoire")


def _ajouter_genres_nombres_prepositions_noms(territoires: DataFrame, genres_nombres_prepositions: DataFrame) -> DataFrame:
    territoires = territoires.join(genres_nombres_prepositions.set_index("id_territoire").loc[:, ["genre_nombre", "preposition"]])

    territoires["genre_nombre"] = np.where(
        ~territoires["genre_nombre"].isna(),
        territoires["genre_nombre"],
        np.where(
            territoires.categorie_territoire.isin(["COMMUNE", "EPCI"]),
            "FEMININ_SINGULIER",
            np.where(territoires.categorie_territoire.isin(["REGROUPEMENT_COMMUNES"]), "MASCULIN_SINGULIER", ""),
        ),
    )

    territoires["preposition"] = np.where(
        ~territoires["preposition"].isna(),
        territoires["preposition"],
        np.where(
            (
                territoires.categorie_territoire.isin(["COMMUNE"])
                | (territoires["nom_territoire"].str.endswith("Métropole") & ~territoires["nom_territoire"].str.contains("Communauté"))
            ),
            "à",
            "dans",
        ),
    )

    verifier_absence_na(territoires, "id_territoire", "genre_nombre")
    verifier_absence_na(territoires, "id_territoire", "preposition")

    return territoires


def _ajouter_colonnes_donnees_pat(territoires: DataFrame, chemin_fichier_definitions_pats: Path, chemin_fichier_contours_pats: Path) -> DataFrame:
    df_donnees_pats = charger_donnees_pats(chemin_fichier_definitions_pats, chemin_fichier_contours_pats)

    df_donnees_pats["niveau_labellisation_pat"] = np.where(
        df_donnees_pats["niveaux_de_labelisation"].str.contains("Niveau 1"),
        "NIVEAU_1",
        np.where(
            df_donnees_pats["niveaux_de_labelisation"].str.contains("Niveau 2"),
            "NIVEAU_2",
            "INCONNU",
        ),
    )

    REGEX_DERNIERE_PARTIE_URL = "/([^/]+)/?$"
    df_donnees_pats["id_url_france_pat"] = df_donnees_pats["url"].str.extract(REGEX_DERNIERE_PARTIE_URL)

    territoires = territoires.join(df_donnees_pats.set_index("id_pat").loc[:, ["niveau_labellisation_pat", "id_url_france_pat"]])

    return territoires


def _charger_sources_et_config_territoires(
    chemin_fichier_communes_et_arrondissements: Path,
    chemin_fichier_mouvements_communes: Path,
    chemin_fichier_epcis: Path,
    chemin_fichier_departements: Path,
    chemin_fichier_regions: Path,
    chemin_fichier_codes_postaux: Path,
    fichiers_regroupements_communes: list[FichiersRegroupementsCommunes],
    chemin_fichier_france_pat: Path,
    chemin_fichier_correspondance_territoires_parcel_crater: Path,
) -> DictDataFrames:
    communes = charger_communes(chemin_fichier_communes_et_arrondissements)
    arrondissements = charger_arrondissements(chemin_fichier_communes_et_arrondissements)
    mouvements_communes = charger_fusions_scissions_communes(chemin_fichier_mouvements_communes)
    epcis = charger_epcis(chemin_fichier_epcis)
    departements = charger_departements(chemin_fichier_departements)
    regions = charger_regions(chemin_fichier_regions)
    codes_postaux = charger_codes_postaux(chemin_fichier_codes_postaux)
    territoires_parcel = chargeur_territoires_parcel.charger(chemin_fichier_correspondance_territoires_parcel_crater)
    genres_nombres_prepositions = pd.read_csv(config.FICHIER_CONFIG_GENRES_NOMBRES_PREPOSITIONS_TERRITOIRES, sep=";")

    composition_regroupements_communes = charger_composition_regroupements_communes(
        fichiers_regroupements_communes, chemin_fichier_france_pat, communes
    )

    communes, epcis = _traiter_communes_sans_epci(communes, epcis)

    (
        communes,
        arrondissements,
        mouvements_communes,
        epcis,
        composition_regroupements_communes,
        departements,
        regions,
        codes_postaux,
    ) = _supprimer_DROMs(
        communes,
        arrondissements,
        mouvements_communes,
        epcis,
        composition_regroupements_communes,
        departements,
        regions,
        codes_postaux,
    )

    return {
        "communes": communes,
        "arrondissements": arrondissements,
        "mouvements_communes": mouvements_communes,
        "epcis": epcis,
        "composition_regroupements_communes": composition_regroupements_communes,
        "departements": departements,
        "regions": regions,
        "codes_postaux": codes_postaux,
        "territoires_parcel": territoires_parcel,
        "genres_nombres_prepositions": genres_nombres_prepositions,
    }


def _traiter_communes_sans_epci(communes: DataFrame, epcis: DataFrame) -> tuple[DataFrame, DataFrame]:
    communes.loc[communes.id_epci == "E-ZZZZZZZZZ", "id_epci"] = np.nan
    return communes, epcis.query('id_epci != "E-ZZZZZZZZZ"')


def _supprimer_DROMs(
    communes: DataFrame,
    arrondissements: DataFrame,
    mouvements_communes: DataFrame,
    epcis: DataFrame,
    composition_regroupements_communes: DataFrame,
    departements: DataFrame,
    regions: DataFrame,
    codes_postaux: DataFrame,
) -> tuple[
    DataFrame,
    DataFrame,
    DataFrame,
    DataFrame,
    DataFrame,
    DataFrame,
    DataFrame,
    DataFrame,
]:
    communes = communes.loc[~communes["id_region"].str[2:4].isin(["{:02}".format(num) for num in NUMEROS_REGIONS_HORS_DROM])]
    arrondissements = arrondissements.loc[~arrondissements["id_arrondissement"].str[2:4].isin(NUMEROS_DEPARTEMENTS_DROM)]
    mouvements_communes = mouvements_communes.loc[~mouvements_communes["id_commune_final"].str[2:4].isin(NUMEROS_DEPARTEMENTS_DROM)]
    mouvements_communes = mouvements_communes.loc[~mouvements_communes["id_commune_initial"].str[2:4].isin(NUMEROS_DEPARTEMENTS_DROM)]
    epcis = epcis.loc[epcis["id_epci"].isin(communes.id_epci.dropna())]
    composition_regroupements_communes = composition_regroupements_communes.loc[
        composition_regroupements_communes["id_commune"].isin(communes.id_commune)
    ]
    departements = departements.loc[~departements["id_region"].str[2:4].isin(["{:02}".format(num) for num in NUMEROS_REGIONS_HORS_DROM])]
    regions = regions.loc[~regions["id_region"].str[2:4].isin(["{:02}".format(num) for num in NUMEROS_REGIONS_HORS_DROM])]
    codes_postaux = codes_postaux.loc[~codes_postaux["id_commune_ou_arrondissement"].str[2:4].isin(NUMEROS_DEPARTEMENTS_DROM)]

    return (
        communes,
        arrondissements,
        mouvements_communes,
        epcis,
        composition_regroupements_communes,
        departements,
        regions,
        codes_postaux,
    )
