from pathlib import Path

import pandas as pd
from pandas import DataFrame

from crater.commun.calculs.outils_territoires import traduire_code_insee_vers_id_commune_crater
from crater.commun.logger import log


def charger_codes_postaux(chemin_fichier: Path) -> DataFrame:
    log.info("   => chargement codes postaux depuis %s", chemin_fichier)

    df = (
        pd.read_csv(
            chemin_fichier,
            sep=",",
            dtype={
                "code_commune_insee": "str",
                "nom_de_la_commune": "str",
                "code_postal": "str",
            },
            na_values=[""],
        )
        .rename(
            columns={
                "code_commune_insee": "id_commune_ou_arrondissement",
                "nom_de_la_commune": "nom_commune_ou_arrondissement",
            },
            errors="raise",
        )
        .reindex(
            columns=[
                "id_commune_ou_arrondissement",
                "nom_commune_ou_arrondissement",
                "code_postal",
            ]
        )
    )

    df = _traduire_code_insee_vers_id_crater(df)

    return df


def _traduire_code_insee_vers_id_crater(df: DataFrame) -> DataFrame:
    df.id_commune_ou_arrondissement = traduire_code_insee_vers_id_commune_crater(df.id_commune_ou_arrondissement)
    return df
