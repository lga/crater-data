# Chemin vers config, peut être redéfinie par les tests pour utiliser une config simplifiée
from pathlib import Path

FICHIER_CONFIG_CULTURES_ET_GROUPES_CULTURES: Path = Path(__file__).parent / "cultures_et_groupes_cultures.csv"
FICHIER_CONFIG_CORRESPONDANCES_RPG_VERS_CRATER: Path = Path(__file__).parent / "correspondance_nomenclature_rpg_vers_crater.csv"
