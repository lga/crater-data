#
# CONFIG GLOBALES partagées par plusieurs modules
#

ANNEE_REFERENTIEL_TERRITOIRES_INSEE = 2024
# NOTE : pour essayer de retrouver le référentiel des communes d'un jeu de données, il existe un outil : https://www.observatoire-des-territoires.gouv.fr/les-outils-interactifs/2021-outils-diacog

CRS_PROJET = "EPSG:2154"
CRS_POUR_CALCUL_SURFACES = {"proj": "cea"}  # Cylindrical equal-area

NOM_DOSSIER_CACHE = ".cache"

ID_FRANCE = "P-FR"
CATEGORIES_TERRITOIRES = [
    "COMMUNE",
    "EPCI",
    "REGROUPEMENT_COMMUNES",
    "DEPARTEMENT",
    "REGION",
    "PAYS",
]
IDS_CATEGORIES_TERRITOIRES_SUPRACOMMUNAUX = [
    "id_epci",
    "ids_regroupements_communes",
    "id_departement",
    "id_region",
    "id_pays",
]
NUMEROS_REGIONS_HORS_DROM = range(1, 10, 1)
NUMEROS_DEPARTEMENTS_DROM = ["97", "98"]
