#
# CONFIG DES EXPORTS (chemins et dossiers de sortie pour crater-data-resultats (utilisés également pour les tests)
#

from pathlib import Path

CHEMIN_RESULTAT_DATA = Path("../crater-data-resultats/data")
CHEMIN_RESULTAT_REFERENTIEL_TERRITOIRES = CHEMIN_RESULTAT_DATA / "referentiel_territoires"
CHEMIN_RESULTAT_DONNEES_SOURCES = CHEMIN_RESULTAT_DATA / "donnees_sources"
CHEMIN_RESULTAT_INDICATEURS = CHEMIN_RESULTAT_DATA / "indicateurs"
CHEMIN_RESULTAT_CONTOURS = CHEMIN_RESULTAT_DATA / "contours"
CHEMIN_RESULTAT_SITEMAPS = CHEMIN_RESULTAT_DATA / "sitemaps"
