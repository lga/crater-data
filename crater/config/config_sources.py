#
# CONFIG DES SOURCES
#
# Config des emplacements des données sources
# * sources crater de crater-data-sources
# * sources externes utilisées par les collecteurs


from pathlib import Path
from typing import TypedDict

CHEMIN_CRATER_DATA_SOURCES = Path("../crater-data-sources")


# Référentiel des territoires INSEE
CHEMIN_SOURCE_COMMUNES_ET_ARRONDISSEMENTS = Path(
    CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/communes/2024/table_appartenance/table-appartenance-geo-communes-2024.xlsx"
)
CHEMIN_SOURCE_FUSIONS_SCISSIONS_COMMUNES = Path(
    CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/communes/2024/tables_passage/table_passage_geo2003_geo2024.xlsx"
)
CHEMIN_SOURCE_MOUVEMENTS_COMMUNES = Path(
    CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/communes/2024/tables_passage/table_passage_annuelle_2024.xlsx"
)
CHEMIN_SOURCE_EPCIS = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/epcis/2024/EPCI_au_01-01-2024.xlsx")
CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES = Path(CHEMIN_CRATER_DATA_SOURCES / "regroupements_geographiques")
DOSSIER_PNR = "parcs_naturels_regionaux"


# Typed dict pour les éléments de fichiers_regroupements_communes
class FichiersRegroupementsCommunes(TypedDict):
    code: str
    fichiers_sources: list[Path]


FICHIERS_REGROUPEMENTS_COMMUNES_SOURCES: list[FichiersRegroupementsCommunes] = [
    {
        # NB: une ligne de l'onglet Composition_communale a été supprimée pour que l'entête soit en ligne 5.
        # L'onglet a été déplacé en première position
        "code": "BV2022",
        "fichiers_sources": [CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "2024" / "bassins_de_vie_2022" / "BV2022_au_01-01-2024.xlsx"],
    },
    {
        "code": "PN",
        "fichiers_sources": [CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "2024" / "parcs_nationaux" / "com2024.xlsx"],
    },
    {
        "code": "PNR",
        "fichiers_sources": [CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "2024" / DOSSIER_PNR / "com2024_2025-02-04.xlsx"],
    },
    {
        "code": "PAYS_PETR",
        "fichiers_sources": [CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "2024" / "pays_et_petr" / "com2024.xlsx"],
    },
    {
        "code": "SCOT",
        "fichiers_sources": [CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "2023" / "scot" / "commune_scot_2023_fedescot_reformate_2025-02-04.xlsx"],
    },
    {
        "code": "AT",
        "fichiers_sources": [
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "CML_carre_metropolitain_de_lyon.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "ISL_interscot_de_lyon.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "BIOVALLEE_vallee_de_la_drome.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "GE_gardiennes_de_l_eau_MEL.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "VALLEE_BARGUILLIERE_vallee_de_la_barguilliere.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "GAL_001_sud_seine_et_marne.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "PROJET_PNR_ASTARAC.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "PLATEAU_SACLAY.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "BASSIN_SEINE_NORMANDIE.xlsx",
            CHEMIN_SOURCE_REGROUPEMENTS_COMMUNES / "autres" / "PLAINE_FABREGUES_POUSSAN.xlsx",
        ],
    },
]
CHEMIN_SOURCE_DEFINITIONS_PAT = Path(CHEMIN_CRATER_DATA_SOURCES / "france_pat" / "2024" / "pats-20241219.csv")
CHEMIN_SOURCE_CONTOURS_PAT = Path(CHEMIN_CRATER_DATA_SOURCES / "france_pat" / "2024" / "pats-simplifie-20241219.geojson")


CHEMIN_SOURCE_DEPARTEMENTS = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/departements_regions/2024/departement_2024.csv")
CHEMIN_SOURCE_REGIONS = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/departements_regions/2024/region_2024.csv")
CHEMIN_SOURCE_CORRESPONDANCES_ANCIENNES_NOUVELLES_REGIONS = Path(
    CHEMIN_CRATER_DATA_SOURCES / "insee/geographie/departements_regions/correspondance_regions/correspondance_anciennes_nouvelles_regions.csv"
)
CHEMIN_SOURCE_CODES_POSTAUX = Path(CHEMIN_CRATER_DATA_SOURCES / "la_poste/2024/base-officielle-codes-postaux.csv")

# Géométries des territoires IGN
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_GEOMETRIES = 2024
CHEMIN_SOURCE_GEOMETRIES_COMMUNES = Path(CHEMIN_CRATER_DATA_SOURCES / "ign/admin_express/2024/ADE_3-2_SHP_LAMB93_FXX-ED2024-02-15/COMMUNE")
CHEMIN_SOURCE_GEOMETRIES_EPCIS = Path(CHEMIN_CRATER_DATA_SOURCES / "ign/admin_express/2024/ADE_3-2_SHP_LAMB93_FXX-ED2024-02-15/EPCI")
CHEMIN_SOURCE_GEOMETRIES_DEPARTEMENTS = Path(CHEMIN_CRATER_DATA_SOURCES / "ign/admin_express/2024/ADE_3-2_SHP_LAMB93_FXX-ED2024-02-15/DEPARTEMENT")
CHEMIN_SOURCE_GEOMETRIES_REGIONS = Path(CHEMIN_CRATER_DATA_SOURCES / "ign/admin_express/2024/ADE_3-2_SHP_LAMB93_FXX-ED2024-02-15/REGION")

# Populations INSEE
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE = 2019
CHEMIN_SOURCE_POPULATION_TOTALE = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/population/2017/Communes.csv")

ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_POPULATION_TOTALE_HISTORIQUE = 2017
CHEMIN_SOURCE_POPULATION_TOTALE_HISTORIQUE = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/population/1968_2015/base-cc-serie-historique-2015.xlsx")

ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_GRILLE_DENSITE_COMMUNALE = 2024
CHEMIN_SOURCE_GRILLE_DENSITE_COMMUNALE = Path(CHEMIN_CRATER_DATA_SOURCES / "insee/grille_communale_densite/2024/grille_densite_7_niveaux_2024.xlsx")

# Recensements agricoles AGRESTE
# Rq les données du RA 2010 sont basées sur les géometries de 2011
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2010 = 2011
CHEMIN_SOURCE_AGRESTE_RA_2010 = Path(CHEMIN_CRATER_DATA_SOURCES / "agreste/1970-2010")
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_AGRESTE_RA_2020 = 2020
CHEMIN_SOURCE_AGRESTE_RA_2020 = Path(
    CHEMIN_CRATER_DATA_SOURCES / "agreste/2020/cartostat/cartostat_nb-exploitations_otex-12-postes_SAU_PBS_extrait_le_2023_03_23.csv"
)

# Corin Land Cover CLC
CHEMIN_SOURCE_OCCUPATION_SOLS_CLC = Path(CHEMIN_CRATER_DATA_SOURCES / "clc/2018/clc_etat_com_n1.csv")
ANNEE_SOURCE_OCCUPATION_SOLS_CLC = 2018  # Millésime CLC utilisé
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_OCCUPATION_SOLS_CLC = 2016  # Ex : CLC 2018 est basé sur le ref territoire 2016...

# Artificialisation CEREMA
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_ARTIFICIALISATION = 2022
CHEMIN_SOURCE_ARTIFICIALISATION = Path(CHEMIN_CRATER_DATA_SOURCES / "cerema/2009-2021/obs_artif_conso_com_2009_2021.csv")

# Logements Observatoire des Territoires
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_NB_LOGEMENTS = 2021
CHEMIN_SOURCE_NB_LOGEMENTS = Path(CHEMIN_CRATER_DATA_SOURCES / "observatoire_des_territoires/logements/2021/nombre_de_logements.xlsx")
CHEMIN_SOURCE_NB_LOGEMENTS_VACANTS = Path(CHEMIN_CRATER_DATA_SOURCES / "observatoire_des_territoires/logements/2021/nombre_de_logements_vacants.xlsx")

# Arrêtés sécheresse
# TODO : Partie PROPLUVIA, a supprimer au profit de Vigieau
CHEMIN_SOURCE_PROPLUVIA = CHEMIN_CRATER_DATA_SOURCES / "propluvia" / "2023_05"
CHEMIN_SOURCE_GEOMETRIES_ZONES_ALERTE_SECHERESSE = CHEMIN_SOURCE_PROPLUVIA / "SHP_2022-08-06.zip"
CHEMINS_SOURCES_ARRETES_SECHERESSE = [
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2012.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2013.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2014.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2015.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2016.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2017.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2018.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2019.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2020.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2021.csv",
    CHEMIN_SOURCE_PROPLUVIA / "propluvia_export_2022.csv",
]
# Vigieau
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_VIGIEAU = 2024
CHEMIN_SOURCE_VIGIEAU_ANNEE_COURANTE = CHEMIN_CRATER_DATA_SOURCES / "vigieau" / f"{ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_VIGIEAU}"
CHEMIN_SOURCE_VIGIEAU_OUTPUT_COLLECTEUR = CHEMIN_CRATER_DATA_SOURCES / "vigieau" / "output_collecteur"


# HVN Solagro
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_HVN = 2018
CHEMIN_SOURCE_HVN = Path(CHEMIN_CRATER_DATA_SOURCES / "solagro/hvn/2000_2010_2017/FR_OSM2018_Score_HVN_RPG2017.xlsx")

# Agence Bio
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_BIO = 2024
ANNEE_SOURCE_BIO = 2023  # Millésime utilisé
CHEMIN_SOURCE_BIO = Path(CHEMIN_CRATER_DATA_SOURCES / "agence_bio/2008-2023/donnees-communales-surfaces-cheptels-2008-2023-agencebio.xlsx")

# Pesticides Office France Biodiversité
CHEMIN_DOSSIER_SOURCE_BNVD = Path(CHEMIN_CRATER_DATA_SOURCES / "office_francais_biodiversite/bnvd/2021")
CHEMINS_SOURCES_BNVD = [
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2015",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2016",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2017",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2018",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2019",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_VENTE_2020",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2015",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2016",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2017",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2018",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2019",
    CHEMIN_DOSSIER_SOURCE_BNVD / "BNVD_TRACABILITE_20211213_ACHAT_2020",
]
CHEMIN_SOURCE_DU_2019 = Path(CHEMIN_CRATER_DATA_SOURCES / "ministere_agriculture_alimentation/produits_phytosanitaires/arrete_2019_annexe.xlsx")
CHEMIN_SOURCE_DU_2017 = Path(CHEMIN_CRATER_DATA_SOURCES / "ministere_agriculture_alimentation/produits_phytosanitaires/arrete_2017_annexe5.csv")
CHEMIN_SOURCE_USAGES_PRODUITS = Path(CHEMIN_CRATER_DATA_SOURCES / "anses/e-phy/decisionamm-intrant-format-csv-20220126/produits_usages_v3_utf8.csv")

# Commerces Open Street Map et BPE
CHEMIN_SOURCE_COMMERCES_OSM = CHEMIN_CRATER_DATA_SOURCES / "open_street_map" / "commerces" / "2021" / "osm-shop-fr-20211214.csv"
CHEMIN_SOURCE_COMMERCES_BPE = CHEMIN_CRATER_DATA_SOURCES / "insee" / "bpe" / "2020" / "bpe20_ensemble_xy_csv.zip"
NOM_FICHIER_SOURCE_COMMERCES_BPE = "bpe20_ensemble_xy.csv"

# Carroyage du territoire INSEE
CHEMIN_SOURCE_CARROYAGE_INSEE = CHEMIN_CRATER_DATA_SOURCES / "insee/carroyage/2015_carreaux_200m/Filosofi2015_carreaux_200m_metropole.gpkg.zip"

# HUBEAU Prélèvements en eau
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_PRELEVEMENTS_EAU_IRRIGATION = 2017
CHEMIN_SOURCE_HUBEAU = CHEMIN_CRATER_DATA_SOURCES / "hubeau"
CHEMINS_SOURCES_PRELEVEMENTS_EAU_IRRIGATION = [
    CHEMIN_SOURCE_HUBEAU / "2012" / "prelevements_eau_par_communes_20230512.csv",
    CHEMIN_SOURCE_HUBEAU / "2013" / "prelevements_eau_par_communes_20230512.csv",
    CHEMIN_SOURCE_HUBEAU / "2014" / "prelevements_eau_par_communes_20230512.csv",
    CHEMIN_SOURCE_HUBEAU / "2015" / "prelevements_eau_par_communes_20230512.csv",
    CHEMIN_SOURCE_HUBEAU / "2016" / "prelevements_eau_par_communes_20230512.csv",
    CHEMIN_SOURCE_HUBEAU / "2017" / "prelevements_eau_par_communes_20230512.csv",
    CHEMIN_SOURCE_HUBEAU / "2018" / "prelevements_eau_par_communes_20230512.csv",
    CHEMIN_SOURCE_HUBEAU / "2019" / "prelevements_eau_par_communes_20230512.csv",
    CHEMIN_SOURCE_HUBEAU / "2020" / "prelevements_eau_par_communes_20230512.csv",
]

CHEMIN_SOURCE_SURFACES_RIZ_PAR_DEPARTEMENTS = CHEMIN_CRATER_DATA_SOURCES / "franceagrimer" / "2021" / "surfaces_riz_par_departements.csv"

# Flux d'azote (Corentin Pinsard)
ANNEE_REFERENTIEL_TERRITOIRES_SOURCE_FLUX_AZOTE = 2022
CHEMIN_SOURCE_FLUX_AZOTE = CHEMIN_CRATER_DATA_SOURCES / "corentin_pinsard" / "2024" / "flux_azote_2024_02_14.xlsx"

# Serres chauffées (CTIFL)
CHEMIN_SOURCE_CTIFL_SERRES = CHEMIN_CRATER_DATA_SOURCES / "ctifl" / "2016" / "synthese_etude_serres_ctifl_2016.xlsx"

# Taux de pauvreté (INSEE - FILOSOFI)
ANNEE_REFERENTIEL_TERRITOIRES_TAUX_PAUVRETE = 2020
CHEMIN_SOURCE_TAUX_PAUVRETE_COMMUNES = CHEMIN_CRATER_DATA_SOURCES / "insee" / "filosofi" / "2019" / "indic-struct-distrib-revenu-2019-COMMUNES_csv"
CHEMIN_SOURCE_TAUX_PAUVRETE_SUPRA_COMMUNES = CHEMIN_CRATER_DATA_SOURCES / "insee" / "filosofi" / "2019" / "indic-struct-distrib-revenu-2019-SUPRA_csv"
NOM_FICHIERS_TAUX_PAUVRETE = "FILO2019_DISP_Pauvres"

# Précarité alimentaire (OBSOALIM)
ANNEE_REFERENTIEL_TERRITOIRES_RISQUE_PRECARITE_ALIMENTAIRE = 2023
CHEMIN_SOURCE_RISQUE_PRECARITE_ALIMENTAIRE = CHEMIN_CRATER_DATA_SOURCES / "obsoalim" / "2024" / "resultats_2025-01-20.csv"

# Besoins en surfaces (PARCEL)
CHEMIN_SOURCE_PARCEL = CHEMIN_CRATER_DATA_SOURCES / "parcel"
ANNEE_REFERENTIEL_TERRITOIRES_PARCEL = 2019
DOSSIER_PARCEL = Path("parcel")

CHEMIN_DOSSIER_PARCEL_ANNEE_COURANTE = CHEMIN_CRATER_DATA_SOURCES / DOSSIER_PARCEL / f"{ANNEE_REFERENTIEL_TERRITOIRES_PARCEL}"
CHEMIN_FICHIER_CORRESPONDANCE_TERRITOIRES_PARCEL_CRATER = (
    CHEMIN_DOSSIER_PARCEL_ANNEE_COURANTE / "correspondance_territoires_parcel_crater_20240718.csv"
)
CHEMIN_FICHIER_BESOINS_PARCEL_ASSIETTE_ACTUELLE = (
    CHEMIN_DOSSIER_PARCEL_ANNEE_COURANTE / "reponse_api_parcel_besoins_sau_par_produits_assiette_actuelle_20220324.csv"
)
CHEMIN_FICHIER_BESOINS_PARCEL_ASSIETTE_50P = (
    CHEMIN_DOSSIER_PARCEL_ANNEE_COURANTE / "reponse_api_parcel_besoins_sau_par_produits_assiette_moins_50p_20220324.csv"
)

# IGN RPG
URL_SOURCE_RPG = "ftp://RPG_ext:quoojaicaiqu6ahD@ftp3.ign.fr/"
NOMS_FICHIERS_RPG = {
    2023: {
        # METROPOLE
        "R-11": "RPG_2-2__SHP_LAMB93_R11_2023-01-01.7z",
        "R-24": "RPG_2-2__SHP_LAMB93_R24_2023-01-01.7z",
        "R-27": "RPG_2-2__SHP_LAMB93_R27_2023-01-01.7z",
        "R-28": "RPG_2-2__SHP_LAMB93_R28_2023-01-01.7z",
        "R-32": "RPG_2-2__SHP_LAMB93_R32_2023-01-01.7z",
        "R-44": "RPG_2-2__SHP_LAMB93_R44_2023-01-01.7z",
        "R-52": "RPG_2-2__SHP_LAMB93_R52_2023-01-01.7z",
        "R-53": "RPG_2-2__SHP_LAMB93_R53_2023-01-01.7z",
        "R-75": "RPG_2-2__SHP_LAMB93_R75_2023-01-01.7z",
        "R-76": "RPG_2-2__SHP_LAMB93_R76_2023-01-01.7z",
        "R-84": "RPG_2-2__SHP_LAMB93_R84_2023-01-01.7z",
        "R-93": "RPG_2-2__SHP_LAMB93_R93_2023-01-01.7z",
        "R-94": "RPG_2-2__SHP_LAMB93_R94_2023-01-01.7z",
    },
    2017: {
        # METROPOLE
        "R-11": "RPG_2-0__SHP_LAMB93_R11-2017_2017-01-01.7z.001",
        "R-24": "RPG_2-0__SHP_LAMB93_R24-2017_2017-01-01.7z.001",
        "R-27": "RPG_2-0__SHP_LAMB93_R27-2017_2017-01-01.7z.001",
        "R-28": "RPG_2-0__SHP_LAMB93_R28-2017_2017-01-01.7z.001",
        "R-32": "RPG_2-0__SHP_LAMB93_R32-2017_2017-01-01.7z.001",
        "R-44": "RPG_2-0__SHP_LAMB93_R44-2017_2017-01-01.7z.001",
        "R-52": "RPG_2-0__SHP_LAMB93_R52-2017_2017-01-01.7z.001",
        "R-53": "RPG_2-0__SHP_LAMB93_R53-2017_2017-01-01.7z.001",
        "R-75": "RPG_2-0__SHP_LAMB93_R75-2017_2017-01-01.7z.001",
        "R-76": "RPG_2-0__SHP_LAMB93_R76-2017_2017-01-01.7z.001",
        "R-84": "RPG_2-0__SHP_LAMB93_R84-2017_2017-01-01.7z.001",
        "R-93": "RPG_2-0__SHP_LAMB93_R93-2017_2017-01-01.7z.001",
        "R-94": "RPG_2-0__SHP_LAMB93_R94-2017_2017-01-01.7z.001",
    },
}
CHEMIN_SOURCE_RPG = CHEMIN_CRATER_DATA_SOURCES / "ign" / "rpg"

# ClimateChange Explorer (CC Explorer)
CHEMIN_FICHIER_CC_EXPLORER_ANALOGUES_CLIMATIQUES_2050 = CHEMIN_CRATER_DATA_SOURCES / "ccexplorer" / "2024"
NOM_FICHIER_CC_EXPLORER_ANALOGUES_CLIMATIQUES_2050 = "analogues-climatiques-ccexplorer-2000-2050.csv"
