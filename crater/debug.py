# Exemple de main permettant de lancer une étape seule, sans charger l'environnement dagster
# Utile par ex pour lancer en mode debug dans un IDE

import os

from crater.commun import environnement
from crater.commun.chargeurs.chargeur_territoires import charger_referentiel_territoires
from crater.config.config_resultats import CHEMIN_RESULTAT_REFERENTIEL_TERRITOIRES, CHEMIN_RESULTAT_INDICATEURS

from crater.config.config_sources import (
    CHEMIN_SOURCE_TAUX_PAUVRETE_COMMUNES,
    CHEMIN_SOURCE_TAUX_PAUVRETE_SUPRA_COMMUNES,
    CHEMIN_SOURCE_RISQUE_PRECARITE_ALIMENTAIRE,
)
from crater.indicateurs.consommation.calculateur_indicateurs_consommation import calculer_indicateurs_consommation

environnement.ENVIRONNEMENT_EXECUTION = "PROD"
if __name__ == "__main__":
    os.chdir("..")
    territoires = charger_referentiel_territoires(
        CHEMIN_RESULTAT_REFERENTIEL_TERRITOIRES / "referentiel_territoires.csv",
    )

    calculer_indicateurs_consommation(
        territoires,
        CHEMIN_RESULTAT_INDICATEURS / "population" / "population.csv",
        CHEMIN_SOURCE_TAUX_PAUVRETE_COMMUNES,
        CHEMIN_SOURCE_TAUX_PAUVRETE_SUPRA_COMMUNES,
        CHEMIN_SOURCE_RISQUE_PRECARITE_ALIMENTAIRE,
        CHEMIN_RESULTAT_INDICATEURS / "consommation",
    )
