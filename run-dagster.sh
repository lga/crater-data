#!/bin/bash

DOSSIER_SCRIPT="$(dirname "$(readlink -f "$0")")"
export DAGSTER_HOME=$DOSSIER_SCRIPT/.dagster

# Pour supprimer les warning liés à l'utilisation des tags dagster (ExperimentalWarning)
export PYTHONWARNINGS="ignore:Parameter \`tags\` of function \`asset\` is experimental"

MODULE_DEFINITIONS_DAGSTER=crater.dagster

echo "Info : DAGSTER_HOME positionné sur $DAGSTER_HOME"

if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$2" = "-h" ] || [ "$2" = "--help" ] || [ "$3" = "-h" ] || [ "$3" = "--help" ] || [ "$4" = "-h" ] || [ "$4" = "--help" ]
then
    OPTION_MODULE_DEFINITIONS_DAGSTER=""
else
    OPTION_MODULE_DEFINITIONS_DAGSTER="-m crater.dagster"
fi

echo "Exécution de la commande : 'dagster $1 $2 $3 $4 $5 $6 $7 $8 $9 $OPTION_MODULE_DEFINITIONS_DAGSTER' "
echo "--------------------------------------"
dagster $1 $2 $3 $4 $5 $6 $7 $8 $9 $OPTION_MODULE_DEFINITIONS_DAGSTER
echo "--------------------------------------"

if [ -z "$1" ]
then
    echo "Exemple de commandes :"
    echo "  './run-dagster.sh job list' : lister tous les jobs (=suite d'étapes) disponibles"
    echo "  './run-dagster.sh job execute --job job_incomplet_rapide' : lancer le job 'job_incomplet_rapide'"
    echo "  './run-dagster.sh job execute --job job_complet' : lancer le job 'job_complet'"
    echo "  './run-dagster.sh asset list' : lister toutes les étapes disponibles"
    echo "  './run-dagster.sh asset materialize --select energie' : exécuter l'étape energie"
fi




