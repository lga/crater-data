from dagster import materialize

from crater.dagster import donnees_territoires

if __name__ == "__main__":
    materialize([donnees_territoires])
