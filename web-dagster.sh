#!/bin/bash

DOSSIER_SCRIPT="$(dirname "$(readlink -f "$0")")"
export DAGSTER_HOME=$DOSSIER_SCRIPT/.dagster

# Pour supprimer les warning liés à l'utilisation des tags dagster (ExperimentalWarning)
export PYTHONWARNINGS="ignore:Parameter \`tags\` of function \`asset\` is experimental"

dagster dev -m crater.dagster
