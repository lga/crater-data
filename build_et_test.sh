echo "## Etape Build et test"
echo "    => Lancement de Ruff (linter) en mode vérification"
python -m ruff check crater  || { echo 'Erreur lors de la vérification Ruff : Corrigez puis relancez la validation' ; exit 1; }
echo "    => Lancement de Mypy (vérification des types)"
mypy crater || { echo 'Erreur lors de la vérification mypy : Corrigez puis relancez la validation' ; exit 1; }
echo "    => Lancement des tests unitaires..."
# Fait dossier par dossier pour éviter de lancer un discover sur le dossier dagster qui pose des pb de dépendances
python -m unittest discover -s crater.commun -p "*__test*.py"  || { echo 'Erreur lors des tests unitaires crater.commun  : Corrigez puis relancez la validation' ; exit 1; }
echo "        - tests crater.commun OK"
python -m unittest discover -s crater.collecteurs -p "*__test*.py"  || { echo 'Erreur lors des tests unitaires crater.collecteurs  : Corrigez puis relancez la validation' ; exit 1; }
echo "        - tests crater.collecteurs OK"
python -m unittest discover -s crater.referentiel_territoires -p "*__test*.py"  || { echo 'Erreur lors des tests unitaires crater.referentiel_territoires  : Corrigez puis relancez la validation' ; exit 1; }
echo "        - tests crater.referentiel_territoires OK"
python -m unittest discover -s crater.contours -p "*__test*.py"  || { echo 'Erreur lors des tests unitaires crater.contours  : Corrigez puis relancez la validation' ; exit 1; }
echo "        - tests crater.contours OK"
python -m unittest discover -s crater.donnees_sources -p "*__test*.py"  || { echo 'Erreur lors des tests unitaires crater.donnees_sources  : Corrigez puis relancez la validation' ; exit 1; }
echo "        - tests crater.donnees_sources OK"
python -m unittest discover -s crater.indicateurs -p "*__test*.py"  || { echo 'Erreur lors des tests unitaires crater.indicateurs  : Corrigez puis relancez la validation' ; exit 1; }
echo "        - tests crater.indicateurs OK"