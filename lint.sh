echo "## Lint des sources avec fix auto"
echo "     => Lancement de Black (autoformat)"
python -m black . --line-length=150
echo "     => Lancement de Ruff en mode fix"
python -m ruff check . --fix

